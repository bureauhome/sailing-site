﻿CREATE TABLE [dbo].[Entities.Fleet] (
    [FleetID]     INT            IDENTITY (1, 1) NOT NULL,
    [FleetName]   NVARCHAR (50)  NOT NULL,
    [FoundedDate] NCHAR (10)     NULL,
    [Description] NVARCHAR (255) NULL,
    [DateFounded] DATETIME       NULL,
    [DateDeleted] DATETIME       NULL,
    [CaptainID]   INT            NULL,
    [TestData] BIT NULL, 
    CONSTRAINT [PK_Entities.Fleet] PRIMARY KEY CLUSTERED ([FleetID] ASC),
    CONSTRAINT [FK_Entities.Fleet_Entities.Crew] FOREIGN KEY ([CaptainID]) REFERENCES [dbo].[Entities.Crew] ([CrewID])
);

