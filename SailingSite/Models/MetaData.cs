﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MetaData.cs" company="">
//   
// </copyright>
// <summary>
//   The meta data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Models
{
    /// <summary>
    /// The meta data.
    /// </summary>
    public class MetaData
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }
    }
}