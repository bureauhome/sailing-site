﻿CREATE TABLE [dbo].[Logs.Comments.Sail] (
    [CommentID] INT NOT NULL,
    [SailID]    INT NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Sail] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Comments.Sail] PRIMARY KEY CLUSTERED ([CommentID] ASC, [SailID] ASC),
    CONSTRAINT [FK_Logs.Comments.Sail_Logs.Comments] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]),
    CONSTRAINT [FK_Logs.Comments.Sail_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID]) ON DELETE CASCADE
);

