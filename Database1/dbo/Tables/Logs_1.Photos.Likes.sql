﻿CREATE TABLE [dbo].[Logs.Photos.Likes] (
    [PhotoID] INT        NOT NULL,
    [CrewID]  INT        NOT NULL,
    [Date]    ROWVERSION NOT NULL,
    CONSTRAINT [PK_Logs.Photos.Likes] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Photos.Likes] PRIMARY KEY CLUSTERED ([PhotoID] ASC, [CrewID] ASC),
    CONSTRAINT [FK_Logs.Photos.Likes_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Logs.Photos.Likes_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID])
);

