﻿CREATE TABLE [dbo].[Entities.Config] (
    [ConfigID]    INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (255) NULL,
    [Uncommon] BIT NULL DEFAULT 0, 
    [Descriptor] NVARCHAR(50) NULL, 
    CONSTRAINT [pk_ConfigID] PRIMARY KEY CLUSTERED ([ConfigID] ASC)
);

