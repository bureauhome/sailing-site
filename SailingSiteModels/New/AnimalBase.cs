﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AnimalBase.cs" company="">
//   
// </copyright>
// <summary>
//   The concrete model of an animal.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    using SailingSiteModels.Common;

    /// <summary>
    /// The concrete model of an animal.
    /// </summary>
    [Serializable]
    public class AnimalBase : IDatabaseModel, ITaggableModel, IEntityModel
    {
        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
         {
             get
             {
                 return "Animal";
             }
         }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimalBase"/> class.
        /// </summary>
        public AnimalBase()
         {
         }

        /// <summary>
        /// The to animal.
        /// </summary>
        /// <returns>
        /// The <see cref="Animal"/>.
        /// </returns>
        public Animal ToAnimal()
         {
             return new Animal()
                        {
                            Aliases = this.Aliases, 
                            Name = this.Name, 
                            Category = this.Category, 
                            DateDeleted = this.DateDeleted, 
                            CreatedDate = this.CreatedDate, 
                            UpdatedDate = this.UpdatedDate, 
                            CreatedBy = this.CreatedBy, 
                        };
         }

        /// <summary>
        /// The process aliases.
        /// </summary>
        /// <param name="aliases">
        /// The aliases.
        /// </param>
        internal void processAliases(string aliases)
        {
            var retVal = new List<string>();
            if (!string.IsNullOrEmpty(aliases))
            {
                if (aliases.Contains(","))
                {
                    retVal = aliases.Split(',').ToList();
                }
                else retVal.Add(aliases);

                if (retVal.Any())
                {
                    var over = from i in retVal where i.Length > 20 select i;
                    foreach (var i in over)
                    {
                        retVal.Remove(i);
                        retVal.Add(i.Substring(0, 20));
                    }
                }
            }

            Aliases = retVal;
        }

             /// <summary>
    /// The primary key.
    /// </summary>
        public int ID {get; set;}

     /// <summary>
    /// The animal name.
    /// </summary>
        public string Name {get; set;}

        /// <summary>
        /// The description.
        /// </summary>
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the aliases.
        /// </summary>
        public List<string> Aliases { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the updated date.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        public Crew CreatedBy { get; set; }


        /// <summary>
        /// The get aliases.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetAliases()
        {
            string result = null;
            if (Aliases != null & Aliases.Any())
            {
                result = string.Join(",", Aliases);
            }

            return result;
        }

        /// <summary>
        /// The main photo.
        /// </summary>
        public PhotoBase Photo { get; set; }

        /// <summary>
    /// The animal category.
    /// </summary>
        public string Category {get; set;}

           /// <summary>
    /// A list of user-defined tags.
    /// </summary>
        public IEnumerable<Tags> Tags {get; set;}

           /// <summary>
           /// Gets or sets the date deleted.
           /// </summary>

        public DateTime? DateDeleted { get; set; }

        /// <summary>
        /// Gets the primary key.
        /// </summary>
        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }


        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get { return "New"; }
        }
    }
}
