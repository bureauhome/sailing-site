﻿CREATE TABLE [dbo].[Entities.Photos] (
    [PhotoID]     INT            IDENTITY (1, 1) NOT NULL,
    [PhotoPath]   NVARCHAR (MAX) NOT NULL,
    [ThumbPath]   NVARCHAR (MAX) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (255) NULL,
    [DeleteDate]  DATETIME       NULL,
    CONSTRAINT [pk_PhotoID] PRIMARY KEY CLUSTERED,
    CONSTRAINT [pk_PhotoID] PRIMARY KEY CLUSTERED ([PhotoID] ASC)
);

