﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Services
{
    using System.Data.Entity.Migrations.Builders;
    using System.Device.Location;

    using SailingSite.Common;
    using SailingSite.Models;

    using SailingSiteModels.GPX;
    using SailingSiteModels.New;

    /// <summary>
    /// The comment service.
    /// </summary>
    public class CommentService : ServiceBase, ICommentService
    {
        public IEnumerable<SailingSiteModels.New.Tags> GetTags(int entityID)
        {
            return CommentRepository.GetTags(entityID);
        }

        public SailReport GetSailReport(int trackId)
        {
            return this.CommentRepository.GetSailReport(trackId);
        }

        public void AddTags(int entityId, string tagString)
        {
            var tags = TagService.GenerateTagsFromString(tagString);
            List<int> tagIds = new List<int>();
            foreach (var tag in tags)
            {

                var id = TagService.SaveTag(tag, null);
                if (id != null && (int)id > 0)
                {
                    tagIds.Add((int)id);
                }
            }
            if (tagIds.Count > 0)
            {
                this.CommentRepository.AddTags(tagIds, entityId);
            }
        }



        public int DeleteTrack(string trackKey)
        {
           return this.CommentRepository.DeleteTrack(trackKey);
        }

        public void DeleteTag(int entityId, int tagId)
        {
            CommentRepository.DeleteTag(tagId, entityId);
        }

        public int Create(SailingSiteModels.New.Comment entity)
        {
            entity.ID= CommentRepository.Create(entity);
            CommentRepository.ProcessLandmarkBearings(entity);
            return entity.ID;
        }

        public void Delete(int id, int userid)
        {
            CommentRepository.Delete(id, userid);
        }

        public SailingSiteModels.New.Comment Get(int id)
        {
            var result= CommentRepository.Get(id);
            if (result != null && result.LogId != null)
            {
                result.Vessel = SailRepository.GetVesselForSail((int)result.LogId);
            }

            return result;
        }

        public IEnumerable<SailingSiteModels.New.CommentBase> GetAll(object search = null)
        {
            return CommentRepository.GetAll(search);
        }

        public void Update(SailingSiteModels.New.Comment entity)
        {
            if (!entity.IsTrackPointOnly() && entity.TrackKey != null)
            {
                entity.TrackKey = null;
                entity.IsCoordinateOnly = false;
            }
            //TODO: Test
            this.CommentRepository.Update(entity);
            if (entity.IsLog)
            {
                SailRepository.UpdateComment(entity);
                //TODO: Test
            }
        }

        private bool IsLogIllegalDate(int sailId, DateTime entryDate)
        {
            bool result = false;
            var date = SailRepository.GetSailDate(sailId);
            if (date == null)
            {
                throw new EntityNotFoundException("the sail was not found in the database");
            }
            var calculatedDate = (DateTime)date;
            if (calculatedDate.AddHours(-12) < entryDate)
            {
                result = true;
            }
            return result;

        }
        // TODO: Investigate merge with get overlapping track
        private void HandleOverlappingComments(GpxTrackPoint entry, Comment log, CommentBase generatedComment)
        {
            TimeSpan defaultSpan = new TimeSpan(0, 0, 4, 0);
            if (log != null && log.Created.Subtract(entry.Date) < defaultSpan)
            {
                
                if (log.Longitude == null && log.Latitude == null)
                {
                    log.MergeTrackPoint(entry);
                    log.IsCoordinateOnly = false;
                    this.CommentRepository.Update(log);
                    return;
                }

                if (log.Latitude != null && log.Longitude != null
                                                        && (Math.Abs((double)generatedComment.Latitude - (double)log.Latitude) > .003
                                                            || Math.Abs((double)generatedComment.Longitude - (double)log.Longitude) > .003))
                {
                    throw new Exception(
                        "there is already an entry for this date that is more than 600 feet away from this entry.  Current technology does not allow a vessel to be in two places at the same time.");
                }

                if (log != null && log.Longitude != null && log.Latitude != null)
                {
                    var oldCoordinate = new GeoCoordinate(
                        (double)log.Latitude,
                        (double)log.Longitude);
                    var distance = oldCoordinate.GetDistanceTo(entry.Coordinate);
                    generatedComment.MilesLogged = (decimal)distance;
                }
            }
        }

        public ParsedLogResult ParseLogs(SailingSiteModels.GPX.GpxTrack track, int sailId, int userId)
        {
            var result = new ParsedLogResult();
            
            if (track == null || track.Track == null || !track.Track.Any())
            {
                result.BaseError = "There were no logs for this result";
            }

            else 
            {
                var logIds = this.CommentRepository.GetSailLogIDS(sailId);
                var logs = this.CommentRepository.Get(logIds);
                string key = Guid.NewGuid().ToString();
                foreach (var entry in track.Track)
                {
                    try
                    {
                        if (this.IsLogIllegalDate(sailId, entry.Date))
                        {
                            throw new Exception("Log date cannot precede the sail date.");
                        }

                        if (entry == null || entry.Coordinate == null || entry.Coordinate.Longitude == null
                            || entry.Coordinate.Latitude == null)
                        {
                            throw new Exception("Entry has no coordinates.");
                        }

                        CommentBase comment = new CommentBase(entry, key);
                        comment.CreatedBy = new CrewFoundation() { ID = userId };
                        comment.LogId = sailId;

                        if (logs != null && logs.Any())
                        {
                            var log =
                                logs.OrderBy(i => i.Created.Subtract(entry.Date))
                                    .ThenBy(i => i.Longitude)
                                    .ThenBy(i => i.Latitude)
                                    .FirstOrDefault();
                            this.HandleOverlappingComments(entry, log, comment);
                        }
                        // If we're doing nothing else with this, it is a coordinate-only entry
                        comment.IsCoordinateOnly = true;
                        // TODO: What if someone edits it so that it is no longer coord-only??

                        this.CommentRepository.Create(comment.ToComment());

                    }

                    catch (Exception e)
                    {
                        var error = new ErroredGpxTrackPoint(entry, e.Message);
                        result.TrackPointErrors.Add(error);
                       
                    }

                }    
            }

            return result;
        }

    }
}