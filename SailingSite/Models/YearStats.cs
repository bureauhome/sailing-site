﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models
{
    public class YearStats
    {
        public int Year { get; set; }
        public int SailCount { get; set; }
        public decimal MilesSailed { get; set; }
        public decimal AvgWind { get; set; }
        public decimal AvgMiles { get; set; }
    }
}