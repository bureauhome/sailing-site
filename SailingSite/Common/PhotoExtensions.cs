﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    using System.Configuration;

    using SailingSiteModels.New;

    public static class PhotoExtensions
    {
        public static string ReturnURLString(this string entity)
        {
            var result = string.Empty;
            var folder = "\\" + StaticTokens.Defaults.ImageFolder();
            if (entity != null && entity.Contains(folder))
            {
                var indexKey = entity.IndexOf(folder);
                var url = ConfigurationManager.AppSettings["Url"];
                if (indexKey != -1)
                {
                    var toReplace = entity.Substring(0, indexKey);
                    result = entity.Replace(toReplace, url);
                }
            }
            return result;
        }
    }
}