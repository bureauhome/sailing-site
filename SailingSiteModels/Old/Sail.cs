﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Old
{
    public class Sail
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public List<Crew> SailCrew { get; set; }
        public decimal WindHigh { get; set; }
        public decimal WindLow { get; set; }
        public string WindDirection { get; set; }
        public Location Location { get; set; }
        public Vessel Vessel { get; set; }
        public List<Configuration> Configurations { get; set; }
        public List<Maneuvers> Maneuvers { get; set; }
        public decimal Barometer { get; set; }
        public decimal Miles { get; set; }
        public decimal Hours { get; set; }
        public string Comment { get; set; }
        public string Direction { get; set; }
        public decimal Temperature { get; set; }

        public bool ValidateModel()
        {
            return (Date > new DateTime(2001,1,1) && SailCrew.Count > 1 && WindHigh >= 0 && WindLow >= 0 && Location != null && Vessel != null && Barometer >= 0 && Miles >= 0);
        }

    }
}
