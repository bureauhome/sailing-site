﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using New = SailingSiteModels.New;
using Old = SailingSiteModels.Old;
using OldDataConnection;

namespace Conversion.Factories
{
    class ManeuverFactory
    {
        public static New.Maneuver CreateManeuver(Old.Maneuvers entity)
        {
            return CreateManeuver(entity.Name);
        }

        public static New.Maneuver CreateManeuver(string entity)
        {
            Old.Maneuvers old = new Old.Maneuvers() { Name = entity };
            New.Maneuver retVal = new New.Maneuver(old);
            int id = OldSQL.SQLStuff.AddNewManeuver(retVal);
            switch (id)
            {
                case -4:
                    Console.WriteLine("Duplicate detected.");
                    break;
                case -1:
                    Console.WriteLine("Error occured.");
                    throw new Exception("SQL ERROR OCCURED.  EXITING.");
                    break;
                default:
                    Console.WriteLine("New SQL entity created with ID: {0}", id);
                    break;
            }
            retVal.SetID(id);
            return retVal;

        }
    }
}
