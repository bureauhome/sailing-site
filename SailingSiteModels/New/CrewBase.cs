﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SailingSiteModels.New
{
    using Newtonsoft.Json;

    public class CrewBase : CrewFoundation
    {
        public CrewBase()
        { }
        public CrewBase(CrewFoundation entity)
        {
            UserName = entity.UserName;
            FirstName = entity.FirstName;
            LastName = entity.LastName;
            ID = entity.ID;
        }
        public int SailCount { get; set; }
        public decimal MileCount { get; set; }
        public decimal HourCount { get; set; }
        [EmailAddress(ErrorMessage = "Email address must be formatted as an email address (example: blank@blank.com)")]
        public string EmailAddress { get; set; }
        public Crew ToCrew()
        {
            return new Crew
            {
                UserName = this.UserName,
                FirstName = this.FirstName,
                LastName = this.LastName,
                ID = this.ID,
                SailCount = this.SailCount,
                MileCount = this.MileCount,
                HourCount = this.MileCount,
                EmailAddress = this.EmailAddress
            };
        }


    }
}
