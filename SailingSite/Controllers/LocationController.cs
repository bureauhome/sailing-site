﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.Net;

    using SailingSite.Common;
    using SailingSite.Services;

    using SailingSiteModels.New;

    public class LocationController : BaseController
    {
        // GET: Location
        public ActionResult Index()
        {
            var data = LocationService.GetLocationsAndLandmarks();
            return this.View(data);
        }

        public ActionResult Edit(int id)
        {
            return RedirectToAction("New", new { id = id });
        }

        public ActionResult New(int? id = null)
        {
            if (CurrentUser == null || CurrentUser.SID < Crew.SIDEnum.PowerUser)
            {
                SailingSite.ViewModels.RedirectModel model = new SailingSite.ViewModels.RedirectModel()
                {
                    ActionName =
                        "NewLandMark",
                    ControllerName
                        =
                        "Location",
                    Description = "You do not have permission to perform this action."
                };
                return this.View("NoPermissionPage", model);
            }
            SailingSiteModels.New.Location entity = new Location();
            if (id != null)
            {
               entity=LocationService.Get((int)id);
            }
            // TODO Create mini view
            return View("_NewLocationMini", entity);

        }


        public ActionResult NewLandMark(int? id = null)
        {
            if (CurrentUser == null || CurrentUser.SID<Crew.SIDEnum.PowerUser)
            {
                SailingSite.ViewModels.RedirectModel model = new SailingSite.ViewModels.RedirectModel()
                                                                 {
                                                                     ActionName =
                                                                         "NewLandMark",
                                                                     ControllerName
                                                                         =
                                                                         "Location",
                                                                     Description = "You do not have permission to perform this action."
                                                                 };
                return this.View("NoPermissionPage", model);
            }
            SailingSiteModels.New.LandMarkBase entity = new LandMarkBase();
            if (id != null)
            {
                entity = LocationService.GetLandMarkBase((int)id);
            }
            return View("_NewLandmarkMini", entity);
        }

        public ActionResult Get(int id)
        {
            var data = this.LocationService.Get(id);
            if (data != null)
            {
                return this.View("_LocationGet", data);
            }
            else return this.View("NullObject");
        }

        public ActionResult GetLandMark(int id)
        {
           var data = this.LocationService.GetLandMarkBase(id);
            if (data != null)
            {
                return this.View("_LandmarkGet",data);
            }
            else return this.View("NullObject");
        }

        public ActionResult GetLandMarkMini(int id)
        {
            var data = this.LocationService.GetLandMarkBase(id);
            return this.PartialView("GeoLocation/LocationMini", data);
        }

        public ActionResult GetLocationMini(int id)
        {
            var data = this.LocationService.Get(id);
            return this.PartialView("GeoLocation/LocationMini", data);
        }

        public ActionResult SearchLandmarkByLetter(string letter)
        {
            var request = new GenericQuery();
            if (string.IsNullOrEmpty(letter) || !char.IsLetter(letter[0]))
            {
                this.LocationService.SearchLandmarks(request);
            }
            var data = this.LocationService.SearchLandmarks(request);
            return this.View("_LandMarkSearch", data);
        }

        public ActionResult SearchLocationByLetter(string letter)
        {
            var request = new GenericQuery();
            if (string.IsNullOrEmpty(letter) || !char.IsLetter(letter[0]))
            {
                this.LocationService.SearchLocations(request);
            }
            request.FirstInitial = letter;
            var data = this.LocationService.SearchLocations(request);
            return this.View("_LocationSearch", data);
        }

        public ActionResult Delete(int id)
        {
            if (CurrentUser != null && CurrentUser.SID>=Crew.SIDEnum.PowerUser)
            {
                LocationService.Delete(id, CurrentUser.ID);
                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Authentication error.  You must log in to perform this action.");
            }
        }

        public ActionResult DeleteLandMark(int id)
        {
            if (CurrentUser != null && CurrentUser.SID >= Crew.SIDEnum.PowerUser)
            {
                LocationService.DeleteLandMark(id, CurrentUser.ID);
                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Authentication error.  You must log in to perform this action.");
            }
        }

        public ActionResult CheckUnique(SailingSiteModels.New.Location entity)
        {
            try
            {
                var system = LocationService.isUnique(entity);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, e.Message);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        [HttpPost]
         public ActionResult CreateLandMark(SailingSiteModels.New.LandMarkBase entity)
        {
            if (CurrentUser == null || CurrentUser.SID<Crew.SIDEnum.PowerUser)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Authentication error.  You must log in to perform this action.");
            }

            LocationService.CreateLandMark(entity);
            
            base.FlagCacheUpdate(StaticTokens.CachingTokens.LocationEnum);
            
            return this.Redirect("Index");
        }

        [HttpPost]
        public ActionResult Create(SailingSiteModels.New.Location entity, HttpPostedFileBase file)
        {
            if (CurrentUser == null || CurrentUser.SID < Crew.SIDEnum.PowerUser)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Authentication error.  You must log in to perform this action.");
            }
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (file != null)
            {
                var photo = new SailingSiteModels.New.Photo() { Name = entity.Name, FileName = file.FileName, EntityName = entity.Name, Subdirectory = @Resources.SessionVariables.ImageFolderCrew };
                photo.ID = this.PhotoService.SavePhoto(file, photo, this.Context, Server.MapPath("~/imagedir"));
                entity.ProfilePhoto = photo;
            }

            this.LocationService.Create(entity);
            base.FlagCacheUpdate(StaticTokens.CachingTokens.LocationEnum);
            return this.Redirect("Index");
        }
    }
}