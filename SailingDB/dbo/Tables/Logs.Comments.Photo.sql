﻿CREATE TABLE [dbo].[Logs.Comments.Photo] (
    [PhotoID]   INT NOT NULL,
    [CommentID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Photo] PRIMARY KEY CLUSTERED ([PhotoID] ASC, [CommentID] ASC),
    CONSTRAINT [FK_Logs.Comments.Photo_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]),
    CONSTRAINT [FK_Logs.Comments.Photo_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE
);

