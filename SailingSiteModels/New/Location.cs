﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Location.cs" company="">
//   
// </copyright>
// <summary>
//   The location.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;

    using SailingSiteModels.Common;

    /// <summary>
    /// The location.
    /// </summary>
    [Serializable]
    public class Location : LocationBase, IDatabaseModel, ITaggableModel, IEntityModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Location"/> class.
        /// </summary>
        public Location()
        {
            Tags = new List<Tags>();
            Comments = new List<Comment>();


        }

        public bool IsTest { get; set; }

        /// <summary>
        /// Gets or sets the tags.
        /// </summary>
        public IEnumerable<Tags> Tags { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public IEnumerable<CommentBase> Comments { get; set; }

        /// <summary>
        /// Gets or sets the profile photo.
        /// </summary>
        public PhotoBase ProfilePhoto { get; set; }

        /// <summary>
        /// Gets or sets the photos.
        /// </summary>
        public IEnumerable<PhotoBase> Photos { get; set; }

        /// <summary>
        /// Gets the primary key.
        /// </summary>
        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }

        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }

    }
}
