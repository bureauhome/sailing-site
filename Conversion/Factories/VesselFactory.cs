﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using New = SailingSiteModels.New;
using Old = SailingSiteModels.Old;
using OldDataConnection;

namespace Conversion.Factories
{
    public class VesselFactory
    {
        public New.Vessel CreateVessel(Old.Vessel entity)
        {
            var vessel = new New.Vessel(entity);
            int id = OldSQL.SQLStuff.AddNewVessel(vessel);
            switch (id)
            {
                case -4:
                    Console.WriteLine("Duplicate detected.");
                    break;
                case -1:
                    Console.WriteLine("Error occured. Exiting.");
                    throw new Exception("SQL ERROR OCCURED.  EXITING.");
                    break;
                default:
                    Console.WriteLine("New SQL entity created with ID: {0}", id);
                    break;
            }
            vessel.SetID(id);
            Console.WriteLine("Converted into new vessel Name: {0}, ID:{1}", vessel.Name, vessel.ID);
            return vessel;
        }
    }
}
