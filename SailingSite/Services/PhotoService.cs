﻿namespace SailingSite.Services
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Helpers;

    using SailingSite.Common;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    /// <summary>
    /// The photo service.
    /// </summary>
    public class PhotoService : ServiceBase, IPhotoService
    {
        private IPhotoRepository repository;
        public IPhotoRepository Repository {
         
            get {
                if (repository == null)
                {
                    repository = new Repositories.PhotoRepository();
                }
                return repository;
            }
            set {
                repository = value;
            }
         }


        public const int thumbSize = 200;

        private string GetThumbnailName(string filename, string thumbdirectory)
        {
            if (filename == null)
            {
                return null;
            }
            string extension = Path.GetExtension(filename);
            string fileName = Path.GetFileNameWithoutExtension(filename);
            if (thumbdirectory == null || extension == null || fileName == null)
            {
                return null;
            }

            return string.Format(
                "{0}\\{1}{2}{3}",
                thumbdirectory,
                fileName,
                StaticTokens.Defaults.ThumbnailSuffix,
                extension);

        }

        private string GetUniquePhotoName(string fileName, string directory, string thumbdirectory)
        {
            string result = null;
            if (fileName == null || directory == null)
            {
                return result;
            }
            int count = 0;
            do
            {
                var fileNoExtension = Path.GetFileNameWithoutExtension(fileName);
                result = string.Format("{0}\\{4}\\{5}\\{1}_{2}{3}", directory, fileNoExtension.Replace(" ", ""), count.ToString(), Path.GetExtension(fileName), DateTime.Now.Year,DateTime.Now.Month);
                count++;
            } while (System.IO.File.Exists(result) || System.IO.File.Exists(this.GetThumbnailName(result, thumbdirectory)));
            
            return result;
        }

        private string GetCarouselSlidePath(string path)
        {
            var fileName = System.IO.Path.GetFileName(path);
            var directory = Path.GetDirectoryName(path);
            var result = Path.Combine(directory, "Carousel", fileName);
            return result;
        }

        public async Task PurgePhoto(int id)
        {
            await this.Repository.Purge(id);
        }

        public async Task<PhotoBase> GetPhotoBase(int id)
        {
            return await this.Repository.GetPhotoBase(id);
             
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="rawFile">
        /// The raw file.
        /// </param>
        /// <param name="photo">
        /// The photo.
        /// </param>
        private void Save(HttpPostedFileBase rawFile, Photo photo, AuditContext context, string serverroute)
        {
            if (rawFile == null)
            {
                this.LoggingService.GenerateLog("User attempted to save file, but file did not exist", Log.LogTypes.UserError, context);
                throw new Exception("Photo cannot be null");
            }

            this.LoggingService.GenerateLog("Saving photo", Log.LogTypes.AuditCreate, context, rawFile.FileName);
            if (rawFile != null && rawFile.ContentLength > 0)
            {
                
                var rawpath = StaticTokens.Defaults.ImagePath(serverroute);
                var rawthumbPath = Path.Combine(rawpath, DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), StaticTokens.Defaults.ThumbnailSubdirectory);
                
                try
                {
                    if (!Directory.Exists(rawpath))
                    {
                        Directory.CreateDirectory(rawpath);
                    }
                    
                    if (!Directory.Exists(rawthumbPath))
                    {
                        Directory.CreateDirectory(rawthumbPath);
                    }

                    int count = 0;
                    
                    WebImage img = new WebImage(rawFile.InputStream);
                    
                    string path = this.GetUniquePhotoName(rawFile.FileName, rawpath, rawthumbPath);

                    photo.CarouselPath = this.GetCarouselSlidePath(path);
                    this.ResizeCarousel(img, photo.CarouselPath);

                    this.LoggingService.GenerateLog("Unique filename created.  Saving physical file.", Log.LogTypes.AuditCreate, context, path);
                    img = this.ResizeMain(img);
                    photo.Width = img.Width;
                    photo.Height = img.Height;
                    
                    img.Save(path);
                    photo.Path = path;

                    img = this.ResizeThumb(img);

                    this.LoggingService.GenerateLog("Saving thumbnail.", Log.LogTypes.AuditCreate, context, this.GetThumbnailName(path, rawthumbPath));
                    img.Save(this.GetThumbnailName(path, rawthumbPath));
                    photo.ThumbPath = this.GetThumbnailName(path, rawthumbPath);
                }
                catch (Exception ex)
                {
                    this.LoggingService.GenerateLog(ex, context);
                    return;
                }
            }

            return;
        }


        public string SaveLog(HttpPostedFileBase rawFile, int userId)
        {
            if (rawFile != null && rawFile.ContentLength > 0)
            {
                var rawpath = @"c:\logs\";
                rawpath = Path.Combine(rawpath, userId.ToString());
                string path = null;
                try
                {
                    if (!Directory.Exists(rawpath))
                    {
                        Directory.CreateDirectory(rawpath);
                    }
                    do
                    {
                        string fileName = string.Format("{0}{1}{2}{3}{4}",DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,DateTime.Now.Millisecond,".gpx");
                        path = Path.Combine(rawpath, fileName);
                    } while (System.IO.File.Exists(path));

                    rawFile.SaveAs(path);
                    return path;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return null;
        }
    

        public WebImage ResizeThumb(WebImage image)
        {
            image.Resize(8000,200, true, false);
            //if (image.Width > image.Height)
            //{
            //    image.Resize(thumbSize * 3, thumbSize, true, false);
            //    var remainder = (image.Width - thumbSize) / 2;
            //    image.Crop(0, remainder, 0, remainder);
            //    image.Resize(thumbSize, thumbSize, false, false);
            //}
            //else
            //{
            //    image.Resize(thumbSize, thumbSize * 3, true, false);
            //    var remainder = (image.Height - thumbSize) / 2;
            //    image.Crop(remainder, 0, remainder, 0);
            //    image.Resize(thumbSize, thumbSize, false, false);
            //}
            return image;
                    
        }

        public WebImage ResizeMain(WebImage image)
        {
                image.Resize(8000,900,true, false);           

            return image;

        }

        private void ResizeCarousel(WebImage img, string destPath)
        {

            var path = "c:\\carouselTemp\\" + DateTime.Now.Ticks +"."+img.ImageFormat;
            var directory = System.IO.Path.GetDirectoryName(path);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            var extension = Path.GetExtension(destPath);
            var image = img.Clone();
            image.Resize(400, 400, true, false);
            int pointX = (400-image.Width)/2;
            int pointY = (400 - image.Height)/2;
            
            image.Save(path);
            //Image frame;

            var frame = Image.FromFile(path);
            using (frame)
            {
                using (var bitmap = new Bitmap(400, 400))
                {
                    using (var canvas = System.Drawing.Graphics.FromImage(bitmap))
                    {
                       
                        canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        canvas.DrawImage(frame, new Rectangle(pointX,pointY,400,400),new Rectangle(0,0,400,400), GraphicsUnit.Pixel );
                        canvas.Save();
                    }
                    ImageFormat format=ImageFormat.Jpeg;
                    if (extension.ToLower() == ".png")
                    {
                        format = ImageFormat.Png;
                    }
                    if (extension.ToLower() == ".gif")
                    {
                        format=ImageFormat.Gif;
                    }
                    if (extension.ToLower() == ".bmpg")
                    {
                        format = ImageFormat.Bmp;
                    }
                    var destinationDir = Path.GetDirectoryName(destPath);
                    if (!Directory.Exists(destinationDir))
                    {
                        Directory.CreateDirectory(destinationDir);
                    }
                    bitmap.Save(destPath, format);
                }
            }
            
        }

        public int SavePhoto(HttpPostedFileBase rawFile, SailingSiteModels.New.Photo photo, AuditContext context, string serverRoute)
        {
            this.Save(rawFile, photo, context,serverRoute);
            return Repository.Create(photo);
            
        }

        public void DeletePhoto(SailingSiteModels.New.Photo entity, AuditContext context)
        {
            this.LoggingService.GenerateLog("User deleted photo.", Log.LogTypes.AuditDelete, context);
            this.Repository.Delete(entity.ID, context.ActiveCrew.ID);
        }

        private void rollBackPhoto(SailingSiteModels.New.Photo entity)
        {
            try
            {
                if (File.Exists(entity.Path))
                {
                    File.Delete(entity.Path);
                }
            }
            catch { }
            try
            {
                if (File.Exists(entity.ThumbPath))
                {
                    File.Delete(entity.ThumbPath);
                }
            }
            catch { }
        }
    }
}