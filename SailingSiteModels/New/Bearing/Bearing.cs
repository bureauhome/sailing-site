﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Bearing.cs" company="">
//   
// </copyright>
// <summary>
//   A general bearing class, to be used in bearings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New.Bearing
{
    using System.ComponentModel.DataAnnotations;

    using SailingSiteModels.Common;

    /// <summary>
    /// A general bearing class, to be used in bearings.
    /// </summary>
    public class Bearing : IEntityModel
    {
                public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }

        /// <summary>
        /// Gets or sets the compass reading.
        /// </summary>
        [Range(0, 360)]
        public int CompassReading { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return "Sailing";
            }
        }
    }
}
