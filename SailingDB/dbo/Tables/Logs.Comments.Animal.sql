﻿CREATE TABLE [dbo].[Logs.Comments.Animal] (
    [AnimalID]  INT NOT NULL,
    [CommentID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Animal] PRIMARY KEY CLUSTERED ([AnimalID] ASC, [CommentID] ASC),
    CONSTRAINT [FK_Logs.Comments.Animal_Entities.Animals.Names] FOREIGN KEY ([AnimalID]) REFERENCES [dbo].[Entities.Animals.Names] ([AnimalID]),
    CONSTRAINT [FK_Logs.Comments.Animal_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]) ON DELETE CASCADE
);

