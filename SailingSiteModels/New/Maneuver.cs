﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SailingSiteModels.Common;

namespace SailingSiteModels.New
{
    public class Maneuver :IDatabaseModel, IEntityModel
    {
        public string ControllerName {
            get
            {
                return null;
            }
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAssociated { get; set; }
        public void SetID(int id)
        {
            ID = id;
        }
        public Maneuver()
        { }
        public Maneuver(Old.Maneuvers entity)
        {
            Name = entity.Name;
        }

        public object[] PrimaryKey
        {
            get { return new object[] { this.ID }; }
        }

        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
    }
}
