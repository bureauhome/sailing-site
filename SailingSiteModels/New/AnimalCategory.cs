﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    [Serializable]
    public class AnimalCategory :SailingSiteModels.Common.IDatabaseModel
    {
        public int ID { get; set; }
        public string ParentName { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public PhotoBase Photo { get; set; }
        public IEnumerable<Animal> Animals { get; set; }
        public IEnumerable<AnimalCategory> Subcategories { get; set; }

        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }
    }
}
