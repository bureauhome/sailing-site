﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGeographicalModel.cs" company="">
//   
// </copyright>
// <summary>
//   The GeographicalModel interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The GeographicalModel interface.
    /// </summary>
    public interface ILocationContainerModel : IEntityModel
    {
        /// <summary>
        /// Gets or sets the locations.
        /// </summary>
        IEnumerable<LocationBase> Locations { get; set; }
    }
}
