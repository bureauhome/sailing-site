﻿USE [Sailing]
GO

Delete FROM [dbo].[Entities.Config]
GO
SET IDENTITY_INSERT [dbo].[Entities.Config] ON

INSERT INTO [dbo].[Entities.Config]
           ([ConfigID],
		   [Name]
           ,[Description],[Uncommon], [Descriptor])
     VALUES
           (0,
		   'MainSail'
           ,'Unreefed Mainsail',0,'Mainsails'),
		   (1,'Jib'
           ,'A standard jib or headsail',0,'Headsails & Staysails'),
		   (2,'Spinnaker'
           ,'A spinnaker or suicide sail',0,'Headsails & Staysails'),
		   (3,'Trysail'
           ,'',0,'Headsails & Staysails'),
		   (4,'Staysail'
           ,'',0,'Headsails & Staysails'),
		   (5, 'Stunsail'
           ,'',0,'Headsails & Staysails'),
		   (6, 'Studding Sail'
           ,'',0,'Block'),
		   (7, 'Course'
           ,'A standard blocksail usually set closest to deck',0,'Square Sails'),
		   (8, 'Royal'
           ,'Upper sail',0,'Square Sails'),
		   (9, 'Skysail'
           ,'Uppermost blocksail',0,'Square Sails'),
		   (10, 'Gollywobbler or Fisherman'
           ,'A large quadrilateral staysail generally set on a schooner',0,'Headsails & Staysails')
GO
SET IDENTITY_INSERT [dbo].[Entities.Config] OFF
GO