﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Views.Tags
{
    using SailingSite.Controllers;

    public class TagController : BaseController
    {
            //
        // GET: /Tag/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllTagResults(int id)
        { 
            var model=TagService.GetHydratedTag(id);
            if (model != null)
            {
                return View(model);
            }
            else return this.View("NullObject");
        }
	}
}