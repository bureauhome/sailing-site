﻿CREATE TABLE [dbo].[Logs.Photos.Vessel] (
    [PhotoID]  INT NOT NULL,
    [VesselID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Photos.Vessel] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Photos.Vessel] PRIMARY KEY CLUSTERED ([PhotoID] ASC, [VesselID] ASC),
    CONSTRAINT [FK_Logs.Photos.Vessel_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Photos.Vessel_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]) ON DELETE CASCADE
);

