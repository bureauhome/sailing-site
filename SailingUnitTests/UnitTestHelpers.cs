﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingUnitTests
{
    using SailingSiteModels.New;

    public static class UnitTestHelpers
    {
        public static Random rdm=new Random();
        public static Crew GenerateCrew()
        {
            string guid = Guid.NewGuid().ToString();
            Crew result = new Crew()
                       {
                           UserName = GetRandomString(rdm.Next(5,8)),
                           EmailAddress = GetRandomString(rdm.Next(8,20)) + "@hotmail.com",
                           Password=GetRandomString(rdm.Next(7,9))+rdm.Next(100,999),
                           FirstName = GetRandomString(rdm.Next(5,20)),
                           LastName = GetRandomString(rdm.Next(8, 30)),
                           IsTestData = true
                       };
            return result;
        }



        public static string GetRandomString(int times)
        {
            const string chars = "1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
            StringBuilder sb=new StringBuilder();
            for (int i = 0; i < times; i++)
            {
                sb.Append(chars[rdm.Next(0, chars.Length - 1)]);
            }
            return sb.ToString();
        }
    }

    

}
