﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Services
{
    using System.ComponentModel;
    using System.Threading.Tasks;
    using System.Web.ModelBinding;

    using Antlr.Runtime;

    using DotNet.Highcharts.Options;

    using SailingSite.Common;
    using SailingSite.Controllers;
    using SailingSite.Models;
    using SailingSite.Repositories;

    using SailingSiteModels.New.Debug;

    public class CrewService :ServiceBase, ICrewService
    {


        public IEnumerable<New.Tags> GetCrewTag(int id)
        {
            return this.CrewRepository.GetTags(id);
        }


        public CrewService()
        {
            if (this.CrewRepository == null)
            { 
                this.CrewRepository=new Repositories.CrewRepository();
            }
        }
        
        public IEnumerable<SailingSiteModels.New.Crew> Get(GeneralSearchRequest query)
        {
            IEnumerable<SailingSiteModels.New.Crew> result = null;
            result=this.CrewRepository.Get(query);
            return result;

        }

        public SearchReturnModel<IEnumerable<SailingSiteModels.New.CrewBase>> GetSearchModel(GeneralSearchRequest query)
        {
            var result = new SearchReturnModel<IEnumerable<SailingSiteModels.New.CrewBase>>();
            result.Result = this.CrewRepository.Get(query);

            return result;
        }

        public SailingSiteModels.New.Crew GetSingle(int id)
        {
            SailingSiteModels.New.Crew result = this.CrewRepository.GetDetailedCrew(id);
            var commentsMade = this.CrewRepository.GetCommentsMade(new int[] { id });
            if (commentsMade != null && commentsMade.Any())
            {
                result.CommentsMade = CommentRepository.GetCommentBases(commentsMade.ToArray());
            }
            //var commentsAbout = this.CrewRepository.GetCommentIDsForSail(new int[] { id });
            //if (commentsAbout != null)
            //{
            //    result.Comments = CommentRepository.GetCommentBases(commentsAbout.ToArray());
            //}
            result = HydrateCrewStats(result);
            return result;
            
        }

        public void AddComment(int entityId, New.Comment entity)
        {
            int commentId = 0;
            if (entity.ID < 1)
            {
                entity.ID = this.CommentRepository.Create(entity);
            }
            CrewRepository.AddComment(commentId, entity.ID);
        }

        public void AddPhoto(int entityId, int photoID)
        {
            CrewRepository.AddPhoto(photoID,entityId);
        }

        public void DeletePhoto(int entityId, int photoID)
        {
            CrewRepository.DeletePhoto(photoID,entityId);
        }

        public IEnumerable<SailingSiteModels.New.CrewFoundation> GetSimple(GeneralSearchRequest query)
        {
            return this.CrewRepository.CrewSearch(query);
        }

        public IEnumerable<SailingSiteModels.New.CrewFoundation> GetAll_Foundation()
        {
            IEnumerable<SailingSiteModels.New.CrewFoundation> result = null;
            var dbEntities = this.CrewRepository.GetAllCrew();
            if (dbEntities != null && dbEntities.Any())
            {
                result = dbEntities.Select(Factories.CrewFactory.CreateCrewFoundation);
            }
            return result;
           
        }

        public void DeleteCrewTags(int tagId, int crewId)
        {
            this.CrewRepository.DeleteTag(tagId,crewId);
        }

        public New.CrewSecurity VerifyToken(string token)
        {
            return this.CrewRepository.VerifyToken(token);
        }

        public New.Crew HydrateCrewStats(New.Crew entity)
        {
            if (entity != null)
            {
                if (entity.Sails != null && entity.Sails.Any())
                {
                    entity.MileCount = entity.Sails.Sum(i => i.MilesSailed);
                    entity.HourCount = entity.Sails.Sum(i => i.HoursSailed);
                    entity.SailCount = entity.Sails.Count();
                }
            }
            return entity;
        }

        public New.CrewSecurity Login(string username, string password, AuditContext context)
        {
            LoggingService.GenerateLog(string.Format("User {0} requested login.  Verifying user existence is locked.",username),Log.LogTypes.AuditLogin, context);
            if (this.CrewRepository.UserExists(username))
            {

                LoggingService.GenerateLog(
                    string.Format("User {0} exists.  Verifying lockout status.", username),
                    Log.LogTypes.AuditLogin,
                    context);

                if (this.CrewRepository.IsUserLocked(username))
                {
                    LoggingService.GenerateLog(
                        string.Format("User {0} locked out.  Exiting.", username),
                        Log.LogTypes.AuditLogin,
                        context);

                    throw new UserLockedException();
                }
                LoggingService.GenerateLog(
                    string.Format("Verifying password match for user {0}.", username),
                    Log.LogTypes.AuditLogin,
                    context);
                if (this.CrewRepository.MatchPW(username, password))
                {
                    LoggingService.GenerateLog(
                    string.Format("User {0} password verified.  Loading user.", username),
                    Log.LogTypes.AuditLogin,
                    context);
                    var result = this.CrewRepository.GetSecurity(username, password);
                    if (result == null)
                    {
                        this.LoggingService.GenerateLog(
                    string.Format("User {0} could not be found. Exiting.", username),
                    Log.LogTypes.AuditLoginError,
                    context);
                        return null;
                    }
                    LoggingService.GenerateLog(
                    string.Format("User {0} loaded.  Setting Session tokens.", username),
                    Log.LogTypes.AuditLogin,
                    context);
                    
                    result.Token = context.CorrelationId;
                    this.CrewRepository.SetUserSession(result.ID,context.CorrelationId);

                    LoggingService.GenerateLog(
                    string.Format("Loading combined vessel list for.", username),
                    Log.LogTypes.AuditLogin,
                    context);
                    var combinedVesselList = new List<New.VesselFoundation>();
                    if (result.VesselsCaptained != null && result.VesselsCrewed != null)
                    {
                        combinedVesselList = result.VesselsCaptained.Concat(result.VesselsCrewed).ToList();
                    }
                    else
                    {
                        if (result.VesselsCaptained != null)
                        {
                            combinedVesselList = result.VesselsCaptained.ToList();
                        }
                        else if (result.VesselsCrewed != null)
                        {
                            combinedVesselList = result.VesselsCrewed.ToList();
                        }
                    }
                    var fleetIds = this.CrewRepository.GetFleetIDsForCrew(result.ID);
                    if (fleetIds != null && fleetIds.Any())
                    {
                        foreach (var id in fleetIds)
                        {
                            var vessels = this.VesselRepository.GetVesselBases(id);
                            if (vessels != null)
                            {
                                combinedVesselList = combinedVesselList.Concat(vessels).ToList();
                            }
                        }

                    }
                    //TODO: Refactor to separate method!
                    result.VesselTotal = combinedVesselList.Distinct().ToList();
                    LoggingService.GenerateLog(
                    string.Format("User {0} loaded success.  Nullifying bad tokens.", username),
                    Log.LogTypes.AuditLogin,
                    context);
                    this.CrewRepository.NullUserBadLoginCount(username);
                    return result;
                }
                else
                {
                    this.LoggingService.GenerateLog(
                    string.Format("Failed to match password for {0}. Incrementing bad login count and exiting.", username),
                    Log.LogTypes.AuditLoginError,
                    context);
                    this.CrewRepository.IncrementUserBadLoginCount(username);
                }
            }
            else
            {
                LoggingService.GenerateLog(
                        string.Format("User {0} does not exist. Exiting", username),
                        Log.LogTypes.AuditLogin,
                        context);
            }
            return null;
        }


        public IEnumerable<New.SailBase> GetSailsForCrew(IEnumerable<int> ids)
        {
            IEnumerable<New.SailBase> result = null;
            var idList = CrewRepository.GetSailIDSForCrew(ids);
            var temp = SailRepository.GetSailBases(idList.ToArray());
            if (temp != null)
            {
                result = temp.ToArray();
            }
            return result;
        }

        public New.Crew GetDetailedCrew(int id)
        {
            var result = this.CrewRepository.GetDetailedCrew(id);
            if (result != null)
            {
                result.MileCount = this.CrewRepository.GetCrewMileCount(id);
                result.HourCount = this.CrewRepository.GetCrewHourCount(id);
                result.SailCount = this.CrewRepository.GetCrewSailCount(id);
                result.SailStats.WildestRide = this.SailRepository.GetWindiestSail(id);
                result.SailStats.LongestSail = this.SailRepository.GetLongestSail(id);
            }
            return result;
        }

        public void AddCrewTags(int crewId, string tagStr)
        {
            var tags = TagService.GenerateTagsFromString(tagStr);
            List<int> tagIds = new List<int>();
            foreach (var tag in tags)
            {

                var id = TagService.SaveTag(tag, null);
                if (id != null && (int)id > 0)
                {
                    tagIds.Add((int)id);
                }
            }
            if (tagIds.Count > 0)
            {
                this.CrewRepository.AddTags(tagIds, crewId);
            }
        }

        public bool CheckUsername(string username)
        {
            return this.CrewRepository.CheckUserExists(username, null);
        }

        public int Create(SailingSiteModels.New.Crew entity, AuditContext context)
        {
            this.LoggingService.GenerateLog("Crew created", Log.LogTypes.AuditCreate, context, entity);
            return this.CrewRepository.Create(entity);
        }

        public bool Update(SailingSiteModels.New.Crew entity, AuditContext context)
        {
            this.LoggingService.GenerateLog("Crew updated", Log.LogTypes.AuditUpdate, context, entity);
            return this.CrewRepository.Update(entity);
        }

        public bool Delete(int id, int crewId, AuditContext context)
        {
            var entity=this.CrewRepository.Get(new GeneralSearchRequest() { ID = id });
            this.LoggingService.GenerateLog("Crew deleted", Log.LogTypes.AuditUpdate, context, entity);
            return this.CrewRepository.Delete(id, crewId);
        }


        public IEnumerable<New.CommentBase> GetComments(int[] id)
        {
            var ids=CrewRepository.GetCommentIDsForSail(id);
            return CommentRepository.GetCommentBases(ids);
        }

        public async Task ProcessQuestion(New.SecurityQuestion entity)
        {
            if (await this.CrewRepository.IsQuestionDuplicate(entity.CrewID, entity.Question))
            {
                throw new ArgumentException("That question already exists.  Please enter a different question or edit the existing one.");

            }
            if (entity.ID == 0)
            {
                await this.CrewRepository.InsertQuestion(entity);
            }
            else
            {
                await this.CrewRepository.UpdateQuestion(entity);
            }
        }

        public async Task<string> MatchAnswersAndSetToken(
            int id,
            IEnumerable<New.SecurityQuestion> entity,
            AuditContext context)
        {
            if (this.CrewRepository.IsUserLocked(id))
            {
                this.LoggingService.GenerateLog("User is currently locked out of system.", Log.LogTypes.AuditLoginError, context);
                throw new AccessViolationException("Too many login attempts.");
            }
            this.LoggingService.GenerateLog("Questions accepted, generating token.", Log.LogTypes.AuditLogin, context);
            string result = null;
            if (await this.MatchAnswer(id, entity, context))
            {
                result = await this.CrewRepository.CreateToken(id);
                this.LoggingService.GenerateLog("Token generated.", Log.LogTypes.AuditLogin, context);
            }
            else
            {
                this.CrewRepository.IncrementBadLoginCount(id);
                this.LoggingService.GenerateLog("Question failed.  Exiting.", Log.LogTypes.AuditLoginError, context);
            }
            return result;
        }

        private async System.Threading.Tasks.Task<bool> MatchAnswer(int id, IEnumerable<New.SecurityQuestion> entity, AuditContext context)
        {
            bool result = false;
            if (entity != null && entity.Any() && entity.Count()>=3)
            {
                
                var count = 0;
                foreach (var thing in entity)
                {
                    if (await this.CrewRepository.MatchAnswer(thing.ID, thing.Answer))
                    {
                        this.LoggingService.GenerateLog(
                            string.Format("Question {0} correct", count),
                            Log.LogTypes.AuditLogin,
                            context);
                        count++;
                    }
                    else
                    {
                        this.LoggingService.GenerateLog(
                            string.Format("Question {0} incorrect", count),
                            Log.LogTypes.AuditLogin,
                            context);
                    }
                }

                this.LoggingService.GenerateLog(string.Format("{0} answers correct.", count), Log.LogTypes.AuditLogin, context);
                result = count >= 3;
            }
            else
            {
                this.LoggingService.GenerateLog(
                    "User does not have three or more questions.",
                    Log.LogTypes.AuditLogin,
                    context);
                    throw new InvalidEnumArgumentException("The account does not have an adequate number of questions.");
                }
            return result;
        }

        public async System.Threading.Tasks.Task DeleteQuestion(int id, AuditContext context)
        {
            this.LoggingService.GenerateLog(
                string.Format("Security Question {0} requested for deletion.", id),
                Log.LogTypes.AuditLogin,
                context);
            await this.CrewRepository.DeleteQuestion(id);
        }

        public async System.Threading.Tasks.Task<IEnumerable<New.SecurityQuestion>> GetQuestions(
            string userName,
            AuditContext context)
        {
            this.LoggingService.GenerateLog(
                string.Format("ID requested for username: {0}.", userName),
                Log.LogTypes.AuditLogin,
                context);

            var id = await this.CrewRepository.GetUserId(userName);
            if (id != null)
            {

                return await this.GetQuestions((int)id, context);
            }
            else
            {
                throw new ArgumentException("Username not found.");
            }
        }

        public async System.Threading.Tasks.Task<IEnumerable<New.SecurityQuestion>> GetQuestions(int crewId, AuditContext context)
        {
            this.LoggingService.GenerateLog(
                string.Format("{0} requested security questions.", crewId),
                Log.LogTypes.AuditLogin,
                context);
            var result = await this.CrewRepository.GetQuestions(crewId);
            if (result != null && result.Count() > 4)
            {
                var list = new List<New.SecurityQuestion>();
                var countList = new List<int>();
                for (int i = 0; i < 4; i++)
                {
                    var rand=new Random();
                    int j = 0;
                    do
                    {
                        j = rand.Next(result.Count() + 1);

                    }
                    while (j != 0 || countList.Contains(j));
                    countList.Add(j);
                    list.Add(result.ElementAt(j));
                    result = list;

                }

            }
            return result;
        }


        public async Task ResetPassword(int id, string token, string password, AuditContext context)
        {

            this.LoggingService.GenerateLog(string.Format("{0} posted a new password.  Verifying token.", id), Log.LogTypes.AuditLogin, context);
            if (!await this.CrewRepository.VerifyToken(id, token))
            {
                this.LoggingService.GenerateLog("Token mismatch. Exiting", Log.LogTypes.AuditLoginError, context);
                throw new MismatchedTokenException("Incorrect token.  Verify login attempt in last 2 hours.");
            }
            this.LoggingService.GenerateLog("Token matched. Updating password.", Log.LogTypes.AuditLoginError, context);
            var crew = new New.Crew() { ID = id };
            crew.Salt = Guid.NewGuid().ToString();
            crew.ChangePassword(password);
            await this.CrewRepository.UpdatePassword(id, crew.Salt, crew.Password);

        }
    }
}