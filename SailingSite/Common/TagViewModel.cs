﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite
{
    public class TagViewModel
    {
        public IEnumerable<SailingSiteModels.New.Tags> Tags { get; set; }
        public string Controller { get; set; }
        public int ID { get; set; }
    }
}