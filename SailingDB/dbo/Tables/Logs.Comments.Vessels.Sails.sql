﻿CREATE TABLE [dbo].[Logs.Comments.Vessel.Sails](
	[LogID] [int] NOT NULL,
	[SheetID] [int] NOT NULL,
	[Percentage] [float] NOT NULL,
 CONSTRAINT [PK_Logs.Comments.Vessel.Sheets] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC,
	[SheetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Logs.Comments.Vessel.Sails]  WITH CHECK ADD  CONSTRAINT [FK_Logs.Comments.Vessel.Sheets_Entities.Comment] FOREIGN KEY([LogID])
REFERENCES [dbo].[Entities.Comment] ([CommentID])
GO

ALTER TABLE [dbo].[Logs.Comments.Vessel.Sails] CHECK CONSTRAINT [FK_Logs.Comments.Vessel.Sheets_Entities.Comment]
GO

ALTER TABLE [dbo].[Logs.Comments.Vessel.Sails]  WITH CHECK ADD  CONSTRAINT [FK_Logs.Comments.Vessel.Sheets_Entities.Vessels.Sails] FOREIGN KEY([SheetID])
REFERENCES [dbo].[Entities.Vessels.Sails] ([ID])
GO

ALTER TABLE [dbo].[Logs.Comments.Vessel.Sails] CHECK CONSTRAINT [FK_Logs.Comments.Vessel.Sheets_Entities.Vessels.Sails]
GO