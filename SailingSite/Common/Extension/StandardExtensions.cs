﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common.Extension
{
    public static class StandardExtensions
    {
        public static string ToFeedString(this DateTime time)
        {
            return time.ToString("MMMM dd yyyy hh:mm tt");
        }
    }
}