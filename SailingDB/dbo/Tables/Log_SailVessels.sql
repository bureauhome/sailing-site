﻿CREATE TABLE [dbo].[Log_SailVessels] (
    [VesselID] INT NOT NULL,
    [SailID]   INT NOT NULL,
    CONSTRAINT [PK_Log_SailVessels] PRIMARY KEY CLUSTERED ([VesselID] ASC, [SailID] ASC),
    CONSTRAINT [FK_Log_SailVessels_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]),
    CONSTRAINT [FK_Log_SailVessels_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID])
);

