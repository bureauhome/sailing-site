﻿CREATE TABLE [dbo].[Stats.General] (
    [EntryDate]                 DATETIME   CONSTRAINT [DF_Stats.General_EntryDate] DEFAULT (getdate()) NOT NULL,
    [HighWindMean]              FLOAT (53) NOT NULL,
    [TiltMean]                  FLOAT (53) NOT NULL,
    [SpeedMean]                 FLOAT (53) NOT NULL,
    [HourMean]                  FLOAT (53) NOT NULL,
    [HourStandardDeviation]     FLOAT (53) NOT NULL,
    [HighWindStandardDeviation] FLOAT (53) NOT NULL,
    [TiltStandardDeviation]     FLOAT (53) NOT NULL,
    [SpeedStandardDeviation]    FLOAT (53) NOT NULL,
    [DistanceMean]              FLOAT (53) NOT NULL,
    [DistanceStandardDeviation] FLOAT (53) NOT NULL,
    [MaxDistance]               FLOAT (53) NOT NULL,
    [MaxTilt]                   INT        NOT NULL,
    [MaxWind]                   FLOAT (53) NOT NULL,
    [MaxDistanceID]             INT        NOT NULL,
    [MaxTiltID]                 INT        NOT NULL,
    [MaxWindID]                 INT        NOT NULL,
    CONSTRAINT [PK_Stats.General] PRIMARY KEY CLUSTERED ([EntryDate] ASC),
    CONSTRAINT [FK_Stats.General_Logs.SailsMAXDISTANCE] FOREIGN KEY ([MaxDistanceID]) REFERENCES [dbo].[Logs.Sails] ([SailID]),
    CONSTRAINT [FK_Stats.General_Stats.GeneralMAXTILT] FOREIGN KEY ([MaxTiltID]) REFERENCES [dbo].[Logs.Sails] ([SailID]),
    CONSTRAINT [FK_Stats.General_Stats.GeneralMAXWIND] FOREIGN KEY ([MaxWindID]) REFERENCES [dbo].[Logs.Sails] ([SailID])
);

