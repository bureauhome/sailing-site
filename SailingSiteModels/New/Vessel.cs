﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Vessel.cs" company="">
//   
// </copyright>
// <summary>
//   The concrete model of a sailing vessel.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    using SailingSiteModels.Common;

    /// <summary>
    /// The concrete model of a sailing vessel.
    /// </summary>
    [Serializable]
    public class Vessel : VesselBase, IDatabaseModel, ITaggableModel, IPhotographedEntity, ICommentableEntity, IEntityModel, ISailable
    {
        /// <summary>
        /// Gets or sets the init tags.
        /// </summary>
        public string initTags { get; set; }

        /// <summary>
        /// The is editor or captain.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsEditorOrCaptain(int userId)
        {
            bool result = false;
            if (this.ID > 0)
            {
                if ((this.Captain != null && this.Captain.ID == userId))
                {
                    result = true;
                }
          
                   if (this.Editors != null && this.Editors.Any(i=>i!=null))
                {
                    if (this.Editors.Any(i => i.ID == userId))
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Gets or sets the animals spotted.
        /// </summary>
        public IEnumerable<Animal> AnimalsSpotted { get; set; }

        public IEnumerable<PhotoBase> Photos { get; set; } 

        /// <summary>
        /// Gets or sets the photo.
        /// </summary>
        public PhotoBase Photo {get; set;}

        /// <summary>
        /// Gets or sets the sail count.
        /// </summary>
        public int SailCount { get; set; }

        /// <summary>
        /// Gets or sets the hours count.
        /// </summary>
        public decimal HoursCount { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer.
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the boolean inidicating whether or not this is test data.
        /// </summary>
        public bool IsTest { get; set; }


        /// <summary>
        /// Gets or sets the editors.
        /// </summary>
        public IEnumerable<CrewFoundation> Editors { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vessel"/> class.
        /// </summary>
        public Vessel()
        {
            Tags = new List<Tags>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vessel"/> class.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public Vessel(Old.Vessel entity)
        {
            Name = entity.Name;
        }

        /// <summary>
        /// Gets or sets the crew members.
        /// </summary>
        public List<Crew> CrewMembers { get; set; }

        /// <summary>
        /// The set id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public void SetID(int id)
        {
            ID = id;
        }

        /// <summary>
        /// The primary key.
        /// </summary>

        /// <summary>
        /// Hull ID numbers.
        /// </summary>
        [StringLength(50)]
        public string HullNumbers { get; set; }

        /// <summary>
        /// The primary hull colors.
        /// </summary>
        [StringLength(10)]
        public string HullColors { get; set; }

        /// <summary>
        /// The length of the vessel.
        /// </summary>
        [StringLength(50)]
        public string SailColors { get; set; }

        /// <summary>
        /// ID numbers on the sail.
        /// </summary>
        [StringLength(50)]
        public string SailNumbers { get; set; }

        /// <summary>
        /// The length of the vessel.
        /// </summary>
        [StringLength(10)]
        public string LetteringColors { get; set; }

        /// <summary>
        /// Gets or sets the distinguishing features.
        /// </summary>
        public string DistinguishingFeatures { get; set; }

        /// <summary>
        /// The hull types.
        /// </summary>
        public enum HullTypes {
            /// <summary>
            /// The single.
            /// </summary>
            Single, 

            /// <summary>
            /// The double.
            /// </summary>
            Double, 

            /// <summary>
            /// The triple.
            /// </summary>
            Triple}

        /// <summary>
        /// The rig types.
        /// </summary>
        public enum RigTypes {
            /// <summary>
            /// The sloop.
            /// </summary>
            Sloop, 

            /// <summary>
            /// The schooner.
            /// </summary>
            Schooner, 

            /// <summary>
            /// The yawl.
            /// </summary>
            Yawl, 

            /// <summary>
            /// The ketch.
            /// </summary>
            Ketch, 

            /// <summary>
            /// The ship.
            /// </summary>
            Ship, 

            /// <summary>
            /// The sprit.
            /// </summary>
            Sprit, 

            /// <summary>
            /// The lug.
            /// </summary>
            Lug, 

            /// <summary>
            /// The cat.
            /// </summary>
            Cat, 

            /// <summary>
            /// The lateen.
            /// </summary>
            Lateen, 

            /// <summary>
            /// The junk.
            /// </summary>
            Junk, 

            /// <summary>
            /// The cutter.
            /// </summary>
            Cutter}

        /// <summary>
        /// The keel types.
        /// </summary>
        public enum KeelTypes {
            /// <summary>
            /// The fin.
            /// </summary>
            Fin, 

            /// <summary>
            /// The swinging.
            /// </summary>
            Swinging, 

            /// <summary>
            /// The daggerboard.
            /// </summary>
            Daggerboard, 

            /// <summary>
            /// The full.
            /// </summary>
            Full, 

            /// <summary>
            /// The bilge.
            /// </summary>
            Bilge, 

            /// <summary>
            /// The skeg.
            /// </summary>
            Skeg, 

            /// <summary>
            /// The none.
            /// </summary>
            None}

        /// <summary>
        /// Gets or sets the keel.
        /// </summary>
        public KeelTypes? Keel { get; set; }


        /// <summary>
        /// Gets or sets the sails.
        /// </summary>
        public IEnumerable<SailBase> Sails { get; set; }

        /// <summary>
        /// Gets or sets the masts.
        /// </summary>
        public int Masts { get; set; }

        /// <summary>
        /// A list of photos.
        /// </summary>
        public IEnumerable<CrewBase> Crew { get; set; }

        /// <summary>
        /// A list of user-defined tags.
        /// </summary>
        public IEnumerable<Tags> Tags { get; set; }

        /// <summary>
        /// A list of comments.
        /// </summary>
        public IEnumerable<CommentBase> Comments { get; set; }

        /// <summary>
        /// Gets the primary key.
        /// </summary>
        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }

    }
}
