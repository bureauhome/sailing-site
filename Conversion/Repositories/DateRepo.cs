﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OldDataConnection;

namespace Conversion.Repositories
{
    class DateRepository
    {
        public static List<DateTime> SailDates = new List<DateTime>();

        public DateRepository()
        {
            Console.WriteLine("Loading Sail Dates.");
            if (SailDates.Count < 1)
            {
                SailDates = OldSQL.SQLStuff.LoadDates();
                foreach (var date in SailDates)
                {
                    Console.WriteLine(date.ToShortDateString());
                }
            }
        }
    }
}
