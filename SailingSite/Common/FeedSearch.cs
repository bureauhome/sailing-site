﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    public class FeedSearch
    {
        public FeedSearch()
        {
            this.NumberToGrab = 100;
        }

        public IEnumerable<int> VesselIDs { get; set; }

        public IEnumerable<int> CrewIDs { get; set; }

        public IEnumerable<int> CommentCrewIDs { get; set; }

        public IEnumerable<int> FilterSearch { get; set; } 

        public DateTime? StartDate { get; set; }

        public int? DaysBack { get; set; }

        public int NumberToGrab { get; set; }

        public DateTime? AnniversaryDate { get; set; }

        public int? Priority { get; set; }

    }
}