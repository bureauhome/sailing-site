﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    using System.Text;

    public static class Helpers
    {
        public static Random Rand = new Random();

        public static string GetWebProp(this decimal dec)
        {
            var obj = dec.ToString() as object;
            return obj.GetWebProp();
        }
        public static string GetWebProp(this string dec)
        {
            var obj = dec as object;
            return obj.GetWebProp();
        }

        public static string StripStringforComparison(this string entity)
        {
            StringBuilder sb=new StringBuilder();
            foreach (var c in entity)
            {
                if (char.IsLetterOrDigit(c))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string GetWebProp(this int dec)
        {
            var obj = dec.ToString() as object;
            return obj.GetWebProp();
        }

        public static string GetWebProp(this object obj)
        {
            if (obj == null)
            {
                return "Unavailable";
            }
            return obj.ToString();
        }

        public static string Chop(this String str, int limit)
        {
            if (str!=null)
            {
                if (str.Length > limit)
                {
                    str = str.Substring(0, limit - 1) + "...";
                }
            }
            return str;
        }
       
    }
}