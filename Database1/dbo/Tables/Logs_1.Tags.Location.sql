﻿CREATE TABLE [dbo].[Logs.Tags.Location] (
    [LocationID] INT NOT NULL,
    [TagID]      INT NOT NULL,
    CONSTRAINT [PK_Logs.Tags.Location] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Tags.Location] PRIMARY KEY CLUSTERED ([LocationID] ASC, [TagID] ASC),
    CONSTRAINT [FK_Logs.Tags.Location_Entities.Locations] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Entities.Locations] ([LocationID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Tags.Location_Entities.Tags] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Entities.Tags] ([TagID])
);

