﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseStructure.cs" company="">
//   
// </copyright>
// <summary>
//   The base structure.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Common
{
    using System.Collections.Generic;

    using SailingSite.Services;

    /// <summary>
    /// The base structure.
    /// </summary>
    public abstract class BaseStructure
    {
        /// <summary>
        /// The tag service.
        /// </summary>
        private ITagService tagService=null;

        internal ITagService TagService
        {
            get
            {
                if (tagService == null)
                {
                    tagService = new TagService();
                }
                return tagService;
            }
            set
            {
                tagService = value;
            }

        }

        /// <summary>
        /// Processes tags.
        /// </summary>
        /// <param name="tagStr">
        /// The tag str.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        internal List<int> ProcessTags(string tagStr)
        {
            var tags = this.TagService.GenerateTagsFromString(tagStr);
            List<int> tagIds = new List<int>();
            foreach (var tag in tags)
            {
                var id = this.TagService.SaveTag(tag, null);
                if (id != null && (int)id > 0)
                {
                    tagIds.Add((int)id);
                }
            }

            return tagIds;
        }
    }
}