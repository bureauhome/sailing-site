﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SailingService.cs" company="">
//   
// </copyright>
// <summary>
//   The sailing service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data.Linq.Mapping;
    using System.Linq;
    using System.Runtime.InteropServices.ComTypes;
    using System.Threading.Tasks;

    using SailingSite.Common;
    using SailingSite.Factories;
    using SailingSite.Models;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    /// <summary>
    /// The sailing service.
    /// </summary>
    public class SailingService : ServiceBase, ISailService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SailingService"/> class.
        /// </summary>
        public SailingService()
        {
        }

        public async Task<IEnumerable<VesselEntry>> GetVesselEntriesForFleet(int fleetId)
        {
          return await this.VesselRepository.GetVesselEntriesForFleet(fleetId);
        }

        private bool isVesselInDefaultFleet(int vesselId)
        {
            return this.VesselRepository.IsVesselInFleet(vesselId, StaticTokens.Defaults.DefaultFleetId);
        }

        /// <summary>
        /// The get weather base.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<WeatherBase> GetWeatherBase()
    {
        return this.SailRepository.GetWeather();
    }

        public IEnumerable<SailingSiteModels.New.SheetSail> GetSheetSailLocker(int id)
        {
            return this.SailRepository.GetSheetSailLocker(id);
        }

        public void AddVesselToFleet(VesselEntry entry, int fleetId, AuditContext context)
        {
            if (entry != null && entry.Vessel != null)
            {
                this.LoggingService.GenerateLog(
                    string.Format("Vessel {0} added to fleet {1}", entry.Vessel.ID, entry),
                    Log.LogTypes.Audit,
                    context);
                if (this.isVesselInDefaultFleet(entry.Vessel.ID))
                {
                    this.VesselRepository.RemoveVesselFromFleet(entry.Vessel.ID,StaticTokens.Defaults.DefaultFleetId);
                }
                FleetRepository.AddVesselToFleet(entry, fleetId);
            }
            else
            {
                this.LoggingService.GenerateLog(
                    string.Format("Attempted to add vessel to fleet {0},  but request was malformed.", fleetId),
                    Log.LogTypes.UserError,
                    context,entry);
                throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// The get crew sid.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <returns>
        /// The <see cref="Crew.SIDEnum"/>.
        /// </returns>
        public Crew.SIDEnum GetCrewSID(int userId)
        {
            return this.CrewRepository.GetUserSID(userId);
        }

        /// <summary>
        /// The is user fleet crew or admin.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <param name="adminOnly">
        /// Are we only allowing admins access to this function.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUserFleetCrewOrSecurity(int userId, int fleetId, bool adminOnly)
        {
            bool result = false;
            var isFleetCrew = this.FleetRepository.IsUserFleetCrew(userId, fleetId);
            var sid = this.GetCrewSID(userId);
            if (isFleetCrew || sid >= (adminOnly ? Crew.SIDEnum.Admin : Crew.SIDEnum.PowerUser))
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// The is user fleet crew or security.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUserFleetCrewOrSecurity(int userId, int fleetId)
        {
            return this.IsUserFleetCrewOrSecurity(userId, fleetId, false);
        }

        /// <summary>
        /// The create sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int CreateSail(Sail entity)
        {
            var result = this.SailRepository.CreateSail(entity);
            return result;
        }

        /// <summary>
        /// The delete vessel.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="crewId">
        /// The crew id.
        /// </param>
        public void DeleteVessel(int id, int crewId, AuditContext context)
        {
            var entity = this.VesselRepository.Get(id);
            this.LoggingService.GenerateLog("Vessel deletion requested", Log.LogTypes.Audit, context, entity);
            this.VesselRepository.Delete(id, crewId);
        }

        /// <summary>
        /// The add sheet sail to existing vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public int AddSheetSailToExistingVessel(SheetSail entity, AuditContext context)
        {
            this.LoggingService.GenerateLog("Sheetsail created", Log.LogTypes.AuditCreate, context, entity);
            if (entity.Vessel == null || entity.Vessel.ID == 0)
            {
                throw new ArgumentNullException("Vessel cannot be null");
            }
            return this.VesselRepository.AddSheetSailToExistingVessel(entity, entity.Vessel.ID);
        }

        /// <summary>
        /// The create vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int CreateVessel(Vessel entity, AuditContext context)
        {
            if (entity.Captain.ID == -1)
            {
                entity.Captain = null;
            }
            this.LoggingService.GenerateLog("Vessel created.", Log.LogTypes.AuditCreate, context, entity);
            return this.VesselRepository.Create(entity);
     
        }

        /// <summary>
        /// The is userin crew.
        /// </summary>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUserinCrew(int vesselId, int userId)
        {
            return this.VesselRepository.isUserVesselCrewOrSecurity(userId, vesselId);
        }

        /// <summary>
        /// The is userin crew or security.
        /// </summary>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="adminOnly">
        /// The admin only.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool isUserVesselCrewOrSecurity(int vesselId, int userId, bool adminOnly)
        {
            bool result = false;
            var isInCrew = this.IsUserinCrew(vesselId, userId);
            var SID = this.CrewRepository.GetUserSID(userId);
            if (isInCrew || SID >= (adminOnly ? Crew.SIDEnum.Admin : Crew.SIDEnum.PowerUser))
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// The is userin crew or security.
        /// </summary>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool isUserVesselCrewOrSecurity(int vesselId, int userId)
        {
            return this.IsUserFleetCrewOrSecurity(userId, vesselId, false);
        }

        /// <summary>
        /// The get sheet sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="SheetSail"/>.
        /// </returns>
        public SheetSail GetSheetSail(int id)
        {
            return this.VesselRepository.GetSheetSail(id);
        }

        /// <summary>
        /// The jiffy vessel.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="Vessel"/>.
        /// </returns>
        public Vessel JiffyVessel(AuditContext context)
        {
            this.LoggingService.GenerateLog("Jiffy vessel requested.", Log.LogTypes.AuditCreate, context);
            var result= this.VesselRepository.JiffySaveVessel();
            result.Captain = null;
            result.Editors = null;
            this.LoggingService.GenerateLog("Jiffy vessel created.", Log.LogTypes.AuditCreate, context, result);
            return result;
        }

        /// <summary>
        /// The remove vessel from fleet.
        /// </summary>
        /// <param name="vesselID">
        /// The vessel id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public void RemoveVesselFromFleet(int vesselID, int fleetId, AuditContext context)
        {
            //TODO: Test
            if (context != null && context.ActiveCrew != null
                && this.IsUserFleetCrewOrSecurity(context.ActiveCrew.ID, fleetId))
            {
                this.LoggingService.GenerateLog(
                    string.Format("User requested vessel ID {0} removed from Fleet with ID {0}", vesselID, fleetId),
                    Log.LogTypes.AuditDelete,
                    context);
                var fleetIds = this.VesselRepository.GetFleetIDSForVessel(vesselID).ToArray();
                if (fleetIds != null && fleetIds.Any())
                {
                    VesselRepository.RemoveVesselFromFleet(vesselID, fleetId);
                }
                else
                {
                    this.LoggingService.GenerateLog(
                    "Vessel not found in fleet",
                    Log.LogTypes.SystemError,
                    context);
                    throw new EntityNotFoundException("The vessel was not found in the referenced fleet.");
                }
                if (fleetIds.Count() == 1)
                {
                    this.FleetRepository.AddVesselToFleet(
                        new VesselEntry() { DateAdded = DateTime.Now, Vessel = new VesselBase() { ID = vesselID } },
                        StaticTokens.Defaults.DefaultFleetId);
                }
            }
            else
            {
                this.LoggingService.GenerateLog(
                    "User does not have permission to perform this action",
                    Log.LogTypes.SystemError,
                    context);
                throw new AccessViolationException("You are not allowed to perform that function.  Please see your fleet administrator.");
            }

        }

        /// <summary>
        /// The get maneuvers.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Maneuver> GetManeuvers()
        {
            return this.SailRepository.GetManeuvers();
        }

        /// <summary>
        /// The get locations.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<LocationBase> GetLocations()
        {
            return this.SailRepository.GetLocations();
        }

        /// <summary>
        /// The add vessel photo.
        /// </summary>
        /// <param name="photoID">
        /// The photo id.
        /// </param>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        public void AddVesselPhoto(int photoID, int vesselId)
        {
            VesselRepository.AddPhoto(photoID, vesselId);
        }

        /// <summary>
        /// The get vessel id for sail.
        /// </summary>
        /// <param name="sailId">
        /// The sail id.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? GetVesselIDForSail(int sailId)
        {
            return VesselRepository.GetVesselIDForSail(sailId);
        }

        /// <summary>
        /// The delete vessel photo.
        /// </summary>
        /// <param name="photoID">
        /// The photo id.
        /// </param>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        public void DeleteVesselPhoto(int photoID, int vesselId)
        {
            VesselRepository.DeletePhoto(photoID, vesselId);
        }

        /// <summary>
        /// The get crew names.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<CrewFoundation> GetCrewNames()
        {
            return this.SailRepository.GetCrewNames();
        }

        /// <summary>
        /// The get vessel names.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<VesselFoundation> GetVesselNames()
        {
            return this.SailRepository.GetVesselNames();
        }

        /// <summary>
        /// The hydrate vessel stats.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="VesselBase"/>.
        /// </returns>
        public VesselBase HydrateVesselStats(VesselBase entity)
        {
            entity.CrewTotal = this.VesselRepository.GetCrewCount(entity.ID);
            entity.NumberOfMasts = this.VesselRepository.SailTotal(entity.ID);
            return entity;

        }

        /// <summary>
        /// The jiffy sail.
        /// </summary>
        /// <param name="creatorID">
        /// The creator ID.
        /// </param>
        /// <param name="test">
        /// The test.
        /// </param>
        /// <returns>
        /// The <see cref="Sail"/>.
        /// </returns>
        public Sail JiffySail(AuditContext context, int? creatorID=null,bool test=false)
        {
            if (creatorID == null)
            {
                creatorID = CrewRepository.GetSystemUserID();
            }
            this.PurgeOldSails();

            this.LoggingService.GenerateLog("User requested new jiffy sail.", Log.LogTypes.AuditCreate, context);

            Logs_Sails sail = new Logs_Sails
                                  {
                                     
                                      Barometer = 0, 
                                      Date = DateTime.Now, 
                                      DateCreated = DateTime.Now,
                                      DateDeleted = DateTime.Now, 
                                      WinDirection = 0, 
                                      MaxWind = 0, 
                                      MinWind = 0, 
                                      Hours = 0, 
                                      TempF = 0, 
                                      Miles=0,
                                      TestData = true,
                                      Logs_Sails_Vessels_Sails = new List<Logs_Sails_Vessels_Sails>(),
                                      CreatedByID=creatorID==null?6:creatorID
                                  };
            sail.TestData = test;
            SailRepository.JiffySave(sail);
            sail.Entities_Vessel = null;
            var result = SailFactory.CreateSailbase(sail).ToSail();
            this.LoggingService.GenerateLog("New jiffy sail created.", Log.LogTypes.AuditCreate, context, result);
            var data= SailFactory.CreateSailbase(sail).ToSail();
            data.Vessel = null;
            return data;
        }

        public void PurgeOldSails()
        {
            var ids = this.SailRepository.GetExpiredSailIds();
            if (ids != null && ids.Any())
            {
                    this.SailRepository.PurgeSail(ids);
                
            }
        }

        public Decimal? DeriveCompassRose(string input)
        {
            decimal? output = null;
            input = input.ToLower().Replace(" ", "").Replace("by", "");
            output = SailRepository.GetCompassRose(input);
            return output;
        }

        public string DeriveCompassRose(int input)
        {
            string output = string.Empty;
            output = SailRepository.GetCompassRose(input);
            return output.ToUpper();
        }

        /// <summary>
        /// The get vessel bases.
        /// </summary>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<VesselBase> GetVesselBases(int? fleetId = null)
        {
            var result = this.VesselRepository.GetVesselBases(fleetId);
            if (result != null && result.Any())
            { 
                foreach (var vessel in result)
                {
                    HydrateVesselStats(vessel);
                }
            }

            return result;
        }

        /// <summary>
        /// The get configurations.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Configuration> GetConfigurations()
        {
            return SailRepository.GetConfigurations();
        }

        /// <summary>
        /// The hydrate fleet stats.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="FleetBase"/>.
        /// </returns>
        public FleetBase HydrateFleetStats(FleetBase entity)
        {
            entity.CrewCount = this.VesselRepository.GetCrewTotalForFleet(entity.ID);
            entity.VesselCount = this.VesselRepository.SailTotalForFleet(entity.ID);
            entity.MileCount = this.VesselRepository.GetMileTotalForFleet(entity.ID);
            return entity;
        }
        
        /// <summary>
        /// The get sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Sail"/>.
        /// </returns>
        public Sail GetSail(int id)
        {
            var result= this.SailRepository.GetSail(id);
            var logIds = this.CommentRepository.GetSailLogIDS(id);
            var trackIds = this.CommentRepository.GetTrackIDs(id);
            if (logIds != null && logIds.Any())
            {
                result.Logbook = this.CommentRepository.GetCommentBases(logIds);
            }

            if (trackIds != null && trackIds.Any())
            {
                result.Track = this.CommentRepository.GetCommentBases(trackIds);
            }
            return result;
        }

        /// <summary>
        /// The get vessel.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Vessel"/>.
        /// </returns>
        public Vessel GetVessel(int id)
        {
            var result = this.VesselRepository.Get(id);

            result.Tags = this.VesselRepository.GetTagsForVessel(id);
            if (result.Sails != null)
            {
                result.MileTotal = result.Sails.Sum(i => i.MilesSailed);
                result.SailCount = result.Sails.Count();
                result.HoursCount = result.Sails.Sum(i => i.HoursSailed);
            }
            //result.AnimalsSpotted = this.VesselRepository.GetAnimalsForVessel(id);
            //TODO: LOAD THIS INTO VESSELBASE
            // TODO: LOAD SAME DATA FOR VesselBase
            return result;
        }

        /// <summary>
        /// The delete vessel tag.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        public void DeleteVesselTag(int id, int vesselId)
        {
            this.VesselRepository.DeleteTag(id, vesselId);
        }

        /// <summary>
        /// The get vessel tag.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Tags> GetVesselTag(int id)
        {
            return this.VesselRepository.GetTagsForVessel(id);
        }

        /// <summary>
        /// The get fleet tags.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Tags> GetFleetTags(int id)
        {
            return FleetRepository.GetTags(id);
        }

        /// <summary>
        /// The delete fleet tag.
        /// </summary>
        /// <param name="tagId">
        /// The tag id.
        /// </param>
        /// <param name="fleetID">
        /// The fleet id.
        /// </param>
        public void DeleteFleetTag(int tagId, int fleetID)
        {
            FleetRepository.DeleteTag(tagId, fleetID);
        }

        /// <summary>
        /// The add vessel tags.
        /// </summary>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <param name="tagStr">
        /// The tag str.
        /// </param>
        public void AddVesselTags(int vesselId, string tagStr)
        {
            var tagIds = this.ProcessTags(tagStr);
            if (tagIds.Count > 0)
            {
                this.VesselRepository.AddTags(tagIds, vesselId);
            }
        }

        /// <summary>
        /// The add fleet tags.
        /// </summary>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <param name="tagStr">
        /// The tag str.
        /// </param>
        public void AddFleetTags(int fleetId, string tagStr)
        {
            var tagIds = this.ProcessTags(tagStr);
            if (tagIds.Count > 0)
            {
                FleetRepository.AddTags(tagIds, fleetId);
            }
        }

        /// <summary>
        /// The add sail tags.
        /// </summary>
        /// <param name="sailID">
        /// The sail id.
        /// </param>
        /// <param name="tagStr">
        /// The tag str.
        /// </param>
        public void AddSailTags(int sailID, string tagStr)
        {
            var tagIds = this.ProcessTags(tagStr);
            if (tagIds.Count > 0)
            {
                SailRepository.AddTags(tagIds, sailID);
            }
        }

        /// <summary>
        /// The update sail.
        /// </summary>
        /// <param name="newEntity">
        /// The new entity.
        /// </param>
        public void UpdateSail(Sail newEntity, AuditContext context)
        {
            this.LoggingService.GenerateLog("User updated sail.", Log.LogTypes.AuditUpdate, context, newEntity);
            newEntity.PreProcess();
            SailRepository.UpdateSail(newEntity);
        }

        /// <summary>
        /// The create fleet.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public int CreateFleet(Fleet entity, AuditContext context)
        {
            this.LoggingService.GenerateLog("Fleet added", Log.LogTypes.AuditCreate, context, entity);
            if (!CheckFleetName(entity.Name))
            {
                return FleetRepository.CreateFleet(entity);
            }
            else throw new Exception("A fleet with the same name already exists.");
        }

        /// <summary>
        /// The check fleet name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckFleetName(string name)
        {
            return FleetRepository.DoesFleetNameExist(name);
        }

        public IEnumerable<SailBase> GetSailsForVessel(IEnumerable<int> ids)
        {
            var sids = VesselRepository.GetSailIDSforVessel(ids);
            return SailRepository.GetSailBases(sids);
        }

        /// <summary>
        /// The delete fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public void DeleteFleet(int id, AuditContext context)
        {
            var entity=this.FleetRepository.GetFleet(id);
            this.LoggingService.GenerateLog("Fleet updated", Log.LogTypes.AuditDelete, context, entity);
            
            this.FleetRepository.DeleteFleet(id);
        }

        /// <summary>
        /// The update fleet.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void UpdateFleet(Fleet entity, AuditContext context)
        {
            this.LoggingService.GenerateLog("Fleet updated", Log.LogTypes.AuditUpdate, context, entity);
            this.FleetRepository.UpdateFleet(entity);
        }

        /// <summary>
        /// The get vessel foundation.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="VesselFoundation"/>.
        /// </returns>
        public VesselFoundation GetVesselFoundation(int id)
        {
            return VesselRepository.GetVesselFoundation(id);
        }

        /// <summary>
        /// The get all fleets.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<FleetBase> GetAllFleets()
        {
            return FleetRepository.GetAllFleetBases();
        }

        /// <summary>
        /// The get fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Fleet"/>.
        /// </returns>
        public Fleet GetFleet(int id)
        {
            var fleet = FleetRepository.GetFleet(id);
            fleet.MileTotal = FleetRepository.SelectMileSum(id);
            fleet.SailTotal = FleetRepository.SelectFleetSailCount(id);
            fleet.CrewTotal = FleetRepository.SelectCrewSum(id);
            if (fleet.VesselEntries != null && fleet.VesselEntries.Any())
            {
                foreach (var vessel in fleet.VesselEntries)
                {
                    if (vessel.Vessel != null)
                    {
                        vessel.SailTotal = this.VesselRepository.SailTotal(vessel.Vessel.ID);
                        vessel.MilesSailed = this.VesselRepository.VesselMileCount(vessel.Vessel.ID);
                    }
                }
            }
            return fleet;
        }

        /// <summary>
        /// The get hydrated fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Fleet"/>.
        /// </returns>
        public Fleet GetHydratedFleet(int id)
        {
            return FleetRepository.HydrateFleet(id);
        }

        /// <summary>
        /// The update vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public void UpdateVessel(Vessel entity)
        {
            if (entity.Captain.ID == -1)
            {
                entity.Captain = null;
            }
            this.VesselRepository.Update(entity);

        }

        /// <summary>
        /// The add sail photo.
        /// </summary>
        /// <param name="photoID">
        /// The photo id.
        /// </param>
        /// <param name="sailId">
        /// The sail id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool AddSailPhoto(int photoID, int sailId)
        {
            try
            {
                SailRepository.AddPhoto(photoID, sailId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddSailComment(Comment entity, int sailID, AuditContext context)
        {
            // Checking to see if any tracks occured in a short time span.  If so merging.
            this.LoggingService.GenerateLog("User submitted a sail comment.", Log.LogTypes.AuditLog, context, entity);
            if (entity.Latitude != null && entity.Longitude != null)
            {
                var matchingTrackEntry = this.CommentRepository.GetOverlappingTrack(entity, 2);
                if (matchingTrackEntry != null)
                {
                    entity.Longitude = matchingTrackEntry.Longitude;
                    entity.Latitude = matchingTrackEntry.Latitude;
                    entity.ID = matchingTrackEntry.ID;
                    entity.Updated = DateTime.Now;
                    entity.IsCoordinateOnly = false;
                    entity.ModifiedBy = entity.CreatedBy;
                    this.CommentRepository.Update(entity);
                }
            }
            
            entity.ID = entity.ID > 0 ? entity.ID : CommentRepository.Create(entity);
            
            this.CommentRepository.ProcessLandmarkBearings(entity);
            this.SailRepository.AddComment(entity.ID,sailID);
            this.LoggingService.GenerateLog(string.Format("Comment {0} added to Sail {1}.", entity.ID, sailID), Log.LogTypes.AuditLog, context);
            
        }

        public IEnumerable<CommentBase> GetCommentBasesForSail(IEnumerable<int> ids)
        {
            var cids = this.SailRepository.GetCommentIDsForSail(ids);
            return this.CommentRepository.GetCommentBases(cids);
        }

        /// <summary>
        /// The delete sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public void DeleteSail(int id, AuditContext context)
        {
            var entity=this.SailRepository.GetSailBases(new List<int>() { id });
            this.LoggingService.GenerateLog("Sail deleted.", Log.LogTypes.AuditDelete, context, entity);
            this.SailRepository.DeleteSail(id);
        }

        /// <summary>
        /// The add sheet sail to existing vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int AddSheetSailToExistingVessel(SheetSail entity, int parentId, AuditContext context)
        {
            this.LoggingService.GenerateLog(string.Format("Sheetsail created for vessel {0}", parentId), Log.LogTypes.AuditCreate, context, entity);
            return this.VesselRepository.AddSheetSailToExistingVessel(entity, parentId);
        }

        /// <summary>
        /// The update sheet sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public void UpdateSheetSail(SheetSail entity, AuditContext context)
        {
            this.LoggingService.GenerateLog("Sheetsail updated", Log.LogTypes.AuditUpdate, context, entity);
            this.VesselRepository.UpdateSheetSail(entity);
        }

        /// <summary>
        /// The delete sheet sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public void DeleteSheetSail(int id, AuditContext context)
        {
            var entity = this.VesselRepository.GetSheetSail(id);
            this.LoggingService.GenerateLog("Sheetsail deleted", Log.LogTypes.AuditDelete, context, entity);
            this.VesselRepository.DeleteSheetSail(id);
        }


        public Models.SearchReturnModel<IEnumerable<VesselBase>> SearchVessel(GeneralSearchRequest search)
            {
            var result=new SearchReturnModel<IEnumerable<VesselBase>>();
            result.StartsWithIndex = this.VesselRepository.GetFirstInitialIndex();
            result.Result = this.VesselRepository.Search(search);
            return result;
            }



        public SearchReturnModel<IEnumerable<FleetBase>> SearchFleet(GenericQuery search)
        {
            var result = new SearchReturnModel<IEnumerable<FleetBase>>();
            result.StartsWithIndex = this.FleetRepository.GetFirstInitials();
            result.Result = this.FleetRepository.Search(search);
            return result;
        }
    }
}