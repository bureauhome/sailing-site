﻿CREATE TABLE [dbo].[Logs_Photos_Entities] (
    [LocationID] INT NOT NULL,
    [PhotoID]    INT NOT NULL,
    CONSTRAINT [PK_Logs_Photos_Entities] PRIMARY KEY CLUSTERED ([LocationID] ASC, [PhotoID] ASC),
    CONSTRAINT [FK_Logs_Photos_Entities_Entities.Locations] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Entities.Locations] ([LocationID]),
    CONSTRAINT [FK_Logs_Photos_Entities_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID])
);

