﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Services
{
    using System.Threading.Tasks;

    using SailingSite.Common;

    using SailingSiteModels.New;

    public interface IPhotoService
    {
        int SavePhoto(HttpPostedFileBase rawPhoto, SailingSiteModels.New.Photo photo, AuditContext context, string serverRoute);

        string SaveLog(HttpPostedFileBase rawFile, int userId);

        Task PurgePhoto(int id);

        Task<PhotoBase> GetPhotoBase(int id);
    }
}