﻿CREATE TABLE [dbo].[Logs.Comments.Locations] (
    [CommentID]  INT NOT NULL,
    [LocationID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Locations] PRIMARY KEY CLUSTERED ([CommentID] ASC, [LocationID] ASC),
    CONSTRAINT [FK_Logs.Comments.Locations_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]),
    CONSTRAINT [FK_Logs.Comments.Locations_Entities.Locations] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Entities.Locations] ([LocationID])
);

