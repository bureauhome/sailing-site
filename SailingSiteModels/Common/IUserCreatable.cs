﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Common
{
    public interface IUserCreatable
    {
        int CreatedbyID { get; }
    }
}
