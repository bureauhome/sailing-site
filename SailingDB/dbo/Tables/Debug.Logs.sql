﻿CREATE TABLE [dbo].[Debug.Logs](
	[ID] INT IDENTITY (1, 1),
	[Type] [nvarchar](50) NOT NULL,
	[Message] [nvarchar](256) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CorrelationID] [nvarchar](128) NULL,
	[ExceptionID] [nvarchar](50) NULL,
	[ExceptionMessage] [nvarchar](256) NULL,
	[ActiveUser] [nvarchar](50) NULL,
	[ViewName] [nvarchar](50) NULL, 
     [AttachedEntity] NVARCHAR(4000) NULL, 
    [Verb] NVARCHAR(50) NULL, 
    [URL] NVARCHAR(255) NULL, 
    CONSTRAINT [PK_DebugLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]