﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Photo.cs" company="">
//   
// </copyright>
// <summary>
//   The photo.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;

    using SailingSiteModels.Common;

    /// <summary>
    /// The photo.
    /// </summary>
    [Serializable]
    public class Photo : PhotoBase, IDatabaseModel, ICommentableEntity, ITaggableModel, IZooModel, IManeuverableModel,ICrewable,IMeteorologicalModel,IVesselable,ILocationContainerModel
    {

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public IEnumerable<CommentBase> Comments { get; set; }

        public IEnumerable<Configuration> Configurations { get; set; } 

        /// <summary>
        /// Gets or sets the tags.
        /// </summary>
        public IEnumerable<Tags> Tags { get; set; }

        /// <summary>
        /// Gets the primary key.
        /// </summary>
        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }

public IEnumerable<Animal> Animals
{
    get; set; }

public IEnumerable<CrewFoundation> Crew { get;
    set;
}

        public IEnumerable<WeatherBase> Weather { get; set; }

        public IEnumerable<VesselFoundation> Vessels { get; set; }

        public IEnumerable<LocationBase> Locations { get; set; }

        public IEnumerable<Maneuver> Maneuvers { get; set; } 
    }
}
