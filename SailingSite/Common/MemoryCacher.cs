﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;

namespace SailingSite.Common
{
    /// <summary>
    /// The memory cache.
    /// </summary>
    public class MemoryCacher
    {
        /// <summary>
        /// The get value.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetValue(string key)
        {
            if (key != null)
            {
                MemoryCache memoryCache = MemoryCache.Default;
                return memoryCache.Get(key);
            }

            return null;
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absExpiration">
        /// The abs expiration.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Add(string key, object value, DateTimeOffset absExpiration)
        {
            if (value != null)
            {
                MemoryCache memoryCache = MemoryCache.Default;
                return memoryCache.Add(key, value, absExpiration);
            }

            return false;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absExpiration">
        /// The abs expiration.
        /// </param>
        public void Update(string key, object value, DateTimeOffset absExpiration)
        {
            var cacheVal = this.GetValue(key);
            if (cacheVal != null)
            {
                this.Delete(key);
            }
            this.Add(key, value, absExpiration);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        public void Delete(string key)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            if (memoryCache.Contains(key))
            {
                memoryCache.Remove(key);
            }
        }

        /// <summary>
        /// The clear.
        /// </summary>
        public void Clear()
        {
            MemoryCache memoryCache = MemoryCache.Default;
            memoryCache.Dispose();
        }


    }
}