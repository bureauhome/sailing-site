﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SailingSiteModels.Common;

namespace SailingSiteModels.New
{
    public class Configuration : IDatabaseModel, IEntityModel

    {
        public string ControllerName
        {
            get
            {
                return "Configuration";
            }
        }

        public Configuration()
        {
        }

        public Configuration(Old.Configuration entity)
        {
            Name = entity.Name;
        }

        public void SetID(int id)
        {
            ID = id;
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public object[] PrimaryKey
        {
            get
            {
                return new object[] { this.ID };
            }
        }

        public string DefaultDelete
        {
            get
            {
                return "Delete";
            }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
    }
}
