﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;
namespace SailingSite.Factories
{
    using System.Data.Linq.Mapping;

    using SailingSite.Common.Extension;
    using SailingSite.Models;

    using SailingSiteModels.Common;

    public class SailFactory
    {

        public static New.LocationBase CreateLocationBase(Entities_Locations entity)
        {
            return new New.LocationBase()
                       {
                           Name=entity.Name,
                           Latitude = entity.Latitude,
                           Longitude = entity.Longitude
                       };
        }

        public static New.Location CreateLocation(Entities_Locations entity)
        {
            var tags = entity.Entities_Tags != null && entity.Entities_Tags.Any()
                           ? entity.Entities_Tags.Select(CommonFactory.CreateTag).ToArray()
                           : null;
            var comments = entity.Entities_Comment != null && entity.Entities_Comment.Any()
                               ? entity.Entities_Comment.Select(CommonFactory.CreateCommentBase).ToArray()
                               : null;
            var result = CreateLocationBase(entity).ToLocation();
            result.MajorLocation = entity.MajorLocation;
            result.MinorLocation = entity.MinorLocation;
            result.ProfilePhoto = entity.Entities_Photos == null
                                      ? null
                                      : PhotoFactory.CreatePhotoBase(entity.Entities_Photos);
            result.Photos = entity.Entities_Photos1 != null && entity.Entities_Photos1.Any()
                                ? entity.Entities_Photos1.Select(PhotoFactory.CreatePhotoBase)
                                : null;
            result.Tags = entity.Entities_Tags != null && entity.Entities_Tags.Any()
                              ? entity.Entities_Tags.Select(CommonFactory.CreateTag)
                              : null;
            result.Comments = entity.Entities_Comment != null && entity.Entities_Comment.Any()
                                  ? entity.Entities_Comment.Select(CommonFactory.CreateCommentBase)
                                  : null;
            if (entity.LocationType != null)
            {
                result.Location = New.LocationBase.LocationTypes.Where(i => i.ID == (int)entity.LocationType).FirstOrDefault();
            }
            if (tags != null)
            {
                result.Tags = tags;
            }

            if (comments != null)
            {
                result.Comments = comments;
            }

            return result;

        }

        //public static Entities_Locations CreateLocation(SailingSiteModels.New.Location entity)
        //{
        //    var gps = System.Data.Entity.Spatial.DbGeography.FromText(entity.GPSCoordinates);
        //    Entities_Locations result = new Entities_Locations()
        //                                    {
        //                                        Name = entity.Name, GPSCoords = gps,
        //                                        MinorLocation = entity.MinorLocation,
        //                                        MajorLocation=entity.MajorLocation,
        //                                        LocationType = entity.Location.ID,
        //                                        TestData = entity.IsTest,
        //                                        Entities_Photos = entity.ProfilePhoto==null?null:PhotoFactory.CreatePhoto(entity.ProfilePhoto.ToPhoto())                                                
        //                                    };
        //    return result;
        //}

        public static SailFeedViewModel CreateFeedModel(Logs_Sails entity)
        {
            SailFeedViewModel data= CreateSailbase(entity).ToFeed();
            return data;
        }

        public static New.SailBase CreateSailbase(Logs_Sails entity)
        {
            List<New.CrewFoundation> crew = new List<New.CrewFoundation>();
            if (entity.Entities_Crew3 != null && entity.Entities_Crew3.Any())
            {
                crew = entity.Entities_Crew3.Select(CrewFactory.CreateCrewFoundation).ToList();
            }
            New.VesselBase vessel = new New.Vessel() { Name = "Unknown", ID = 0 };
            if (entity.Entities_Vessel != null)
            {
                vessel = FleetFactory.CreateVesselBase(entity.Entities_Vessel);
            }
            New.LocationBase location = new New.Location() { Name = "Unknown", ID = 0 };
            if (entity.Entities_Locations != null)
            {
                location = Factories.CommonFactory.CreateLocationBase(entity.Entities_Locations);
            }
           
            var result= new New.SailBase()
            {
                Crew = crew,
                Date = entity.Date,
                ID = entity.SailID,
                HoursSailed = entity.Hours,
                MilesSailed = entity.Miles,
                Barometer=entity.Barometer,
                GeneralObservations = entity.GeneralNotes,
                TempF=entity.TempF,
                MaxWind=entity.MaxWind,
                SeaState = entity.SeaState,
                MinWind=entity.MinWind,
                Created=entity.DateCreated==null?DateTime.MinValue:(DateTime)entity.DateCreated,
                CreatedBy=entity.Entities_Crew2==null?null:Factories.CrewFactory.CreateCrewFoundation(entity.Entities_Crew2),
                WindDirection = entity.WinDirection,
                Centerboard=entity.CenterBoardPct==null?null : entity.CenterBoardPct,
                Tilt=entity.MaxTilt,
                Location = location,
                RainSeverity = entity.RainSeverity,
                Vessel = vessel
            };
            return result;
        }

        public static New.Configuration CreateConfiguration(Entities_Config entity)
        {
            if (entity != null)
            { 
                return new New.Configuration()
                {
                  ID=entity.ConfigID,
                  Description=entity.Description,
                  Name=entity.Name
                };
            }
            return null;
        }

        public static New.Maneuver CreateManeuver(Entities_Maneuvers entity)
        {
            New.Maneuver result= null;
            if (entity != null)
            {
                result = new New.Maneuver()
                {
                    Description = entity.Description,
                    ID = entity.ManeuverID,
                    Name = entity.Name
                };
            }
            return result;
        }

        public static New.Sail CreateSail(Logs_Sails entity)
        {
            New.Sail result = CreateSailbase(entity).ToSail();
            result.SeaState = entity.SeaState;
            result.Tags = entity.Entities_Tags == null ? null : entity.Entities_Tags.Select(CommonFactory.CreateTag).ToArray();
            result.Comments = entity.Entities_Comment == null ? null : entity.Entities_Comment.Select(CommonFactory.CreateCommentBase).ToArray();
            result.SheetSailLogs = entity.Logs_Sails_Vessels_Sails.Select(CommonFactory.CreateSheetSailLog).ToList();
            result.Animals = entity.Entities_Animals_Names == null ? null : entity.Entities_Animals_Names.Select(AnimalFactory.CreateAnimal).ToList();
            result.Maneuvers = entity.Entities_Maneuvers.Select(CreateManeuver).ToList();
            result.Photos = entity.Entities_Photos == null
                                ? null
                                : entity.Entities_Photos.Select(PhotoFactory.CreatePhotoBase).ToList();
            result.IsTest = entity.TestData==null?false:(bool)entity.TestData;
            result.Weather = entity.Entities_Weather == null
                                           ? null
                                           : entity.Entities_Weather.Select(CommonFactory.CreateWeatherBase).ToList();
            return result;
        }

        public static Logs_Sails CreateSail(New.Sail entity)
        {
            Logs_Sails result = new Logs_Sails();
            result.DateCreated = entity.Created;
            result.CreatedByID = entity.CreatedBy.ID;
            result.SailID = entity.Vessel.ID;
            result.WinDirection = entity.WindDirection;
            result.AvgWind = entity.AvgWind;
            result.Barometer = entity.Barometer;
            result.CenterBoardPct = entity.Centerboard;
            result.Date = entity.Date;
            result.SeaState = entity.SeaState;
            result.RainSeverity = entity.RainSeverity;
            result.Hours = entity.HoursSailed;
            result.HoursMotored = entity.HoursMotored;
            result.LocationID = entity.Location.ID;
            result.MaxTilt = entity.Tilt == null ? 0 : (int)entity.Tilt;
            result.MaxWind = entity.MaxWind;
            result.Miles = entity.MilesSailed;
            result.MilesMotored = entity.MilesMotored;
            result.GeneralNotes = entity.GeneralObservations;
            result.MinWind = entity.MinWind;
            result.TempF = entity.TempF;
            result.TestData = entity.IsTest;
            result.WinDirection = entity.WindDirection;
            return result;
        }
    }
}