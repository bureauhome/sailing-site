﻿CREATE TABLE [dbo].[Debug.SecutityQuestions](
	[QuestionID] [int] IDENTITY(1,1) NOT NULL,
	[Question] [nvarchar](max) NOT NULL,
	[Answer] [nvarchar](max) NOT NULL,
	[CrewID] [int] NOT NULL,
 CONSTRAINT [PK_Debug.SecutityQuestions] PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Debug.SecutityQuestions]  WITH CHECK ADD  CONSTRAINT [FK_Debug.SecutityQuestions_Entities.Crew] FOREIGN KEY([CrewID])
REFERENCES [dbo].[Entities.Crew] ([CrewID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Debug.SecutityQuestions] CHECK CONSTRAINT [FK_Debug.SecutityQuestions_Entities.Crew]
GO
