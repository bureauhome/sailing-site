﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using System.Security.Cryptography.X509Certificates;

    public interface ICommentableRepository
    {
        void AddComment(int commentID, int entityID);

        IEnumerable<int> GetCommentIDsForSail(IEnumerable<int> entityID);
    }


}