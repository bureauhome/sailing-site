﻿CREATE TABLE [dbo].[Logs.Animals.Alias] (
    [AnimalID] INT        NOT NULL,
    [Alias]    NCHAR (10) NOT NULL,
    CONSTRAINT [PK_Logs.Animals.Alias] PRIMARY KEY CLUSTERED ([AnimalID] ASC, [Alias] ASC),
    CONSTRAINT [FK_Logs.Animals.Alias_Entities.Animals.Names] FOREIGN KEY ([AnimalID]) REFERENCES [dbo].[Entities.Animals.Names] ([AnimalID])
);

