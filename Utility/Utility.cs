﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Globalization;


namespace SailingSiteModels.Utility
{
    
    public static class Passwords
    {
        public static Random Rand = new Random();

        private static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = MD5.Create();  //or use SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        private static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static string RenderPassword(string password, string salt)
        {
            // Convert the salt string and the password string to a single
            // array of bytes. Note that the password string is Unicode and
            // therefore may or may not have a zero in every other byte.
            var shash = salt.GetHashCode();
            var phash = password.GetHashCode();

            return string.Concat(phash, shash);

        }

        public static bool CheckPassword(string attempt, string salt, string dbPassword)
        {
            var passed = RenderPassword(attempt, salt);
            return (passed == dbPassword);
        }

        public static string GenerateSalt()
        {
            return Guid.NewGuid().ToString();
        }

        public static char GetUniqueCharacter()
        {
            var count = Rand.Next(0, 6);
            var retVal = ' ';
            switch (count)
            {
                case 0:
                    retVal = '_';
                    break;
                case 1:
                    retVal = '-';
                    break;
                case 2:
                    retVal = '~';
                    break;
                case 3:
                    retVal = '^';
                    break;
                case 4:
                    retVal = '@';
                    break;
                case 5:
                    retVal = '<';
                    break;
                default:
                    retVal = '_';
                    break;
            }
            return retVal;
        }

        public static string GeneratePassword()
        {
            string output = GetRandomString(Rand.Next(6, 8));
            output += Rand.Next(1000, 9000);
            return output;
        }



        private static string GetRandomString(int count)
        {
            string output="";
            for (int i = 0; i <= count; i++)
            {
                output = output + GetRandomChar();
            }
            return output;

        }
        private static string GetRandomChar()
        {
            int upper=Rand.Next(0,2);
            int num = Rand.Next(0, 26);
            char letter = (char)('a' + num);
            if (upper == 0) return letter.ToString();
            else return letter.ToString().ToUpper();


        }
    }
}
