﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Controllers.Base
{
    using System.Web.Security;

    public interface IAuthenticationFacade
    {
        void SetAuthCookie(string username);
    }

    /// <summary>
    /// The authentication context.
    /// </summary>
    public class AuthenticationFacade : IAuthenticationFacade
    {
        public void SetAuthCookie(string username)
        {
            FormsAuthentication.SetAuthCookie(username, true);
        }
    }
}