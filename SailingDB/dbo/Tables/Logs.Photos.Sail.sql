﻿CREATE TABLE [dbo].[Logs.Photos.Sail] (
    [SailID]  INT NOT NULL,
    [PhotoID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Photos.Sail] PRIMARY KEY CLUSTERED ([SailID] ASC, [PhotoID] ASC),
    CONSTRAINT [FK_Logs.Photos.Sail_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Photos.Sail_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID]) ON DELETE CASCADE
);

