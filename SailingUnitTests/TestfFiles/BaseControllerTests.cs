﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SailingUnitTests.TestfFiles
{
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Moq;

    using SailingSite.Common;
    using SailingSite.Controllers;

    using SailingSiteModels.New;

    [TestClass]
    public class BaseControllerTests
    {
        private CrewController controller;
        [TestMethod]
        public void TestSessions()
        {
            this.controller=new CrewController();
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
 
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
            context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());
            this.controller.ControllerContext = context.Object;
 
            this.controller.GetSessionVariables();
            this.VerifyCache(StaticTokens.CachingTokens.VesselEnum,typeof(IEnumerable<VesselFoundation>));
            this.VerifyCache(StaticTokens.CachingTokens.AnimalEnum, typeof(IEnumerable<Animal>));
            this.VerifyCache(StaticTokens.CachingTokens.CrewEnum, typeof(IEnumerable<CrewFoundation>));
            this.VerifyCache(StaticTokens.CachingTokens.TagEnum, typeof(IEnumerable<string>));
            this.VerifyCache(StaticTokens.CachingTokens.FleetEnum, typeof(IEnumerable<FleetBase>));
            this.VerifyCache(StaticTokens.CachingTokens.LandMarkEnum, typeof(IEnumerable<LandMarkBase>));
            this.VerifyCache(StaticTokens.CachingTokens.LocationEnum, typeof(IEnumerable<LocationBase>));
            this.VerifyCache(StaticTokens.CachingTokens.WeatherEnum, typeof(IEnumerable<WeatherBase>));
            this.VerifyCache(StaticTokens.CachingTokens.ManeuverEnum, typeof(IEnumerable<Maneuver>));
        }

        private void VerifyCache(string key, Type type)
        {
            var data = this.controller.Cache.GetValue(key);
            Assert.IsNotNull(data);
            Assert.IsInstanceOfType(data, type);
        }
    }
}
