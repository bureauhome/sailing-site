﻿CREATE TABLE [dbo].[Logs.Photos.Crew] (
    [PhotoID] INT NOT NULL,
    [CrewID]  INT NOT NULL,
    CONSTRAINT [PK_Logs.Photos.Crew] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Photos.Crew] PRIMARY KEY CLUSTERED ([PhotoID] ASC, [CrewID] ASC),
    CONSTRAINT [FK_Logs.Photos.Crew_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Photos.Crew_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE
);

