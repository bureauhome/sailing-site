﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using New = SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.SqlServer;
    using System.Data.Entity.Validation;

    using New;

    using SailingSite.Common;
    using SailingSite.Factories;
    using SailingSite.Models;

    using SailingSiteModels.Common;

    using WebGrease.Css.Extensions;

    public class SailRepository : Common.BaseStructure, ISailRepository
    
    {

        private Services.ITagService tagService = null;
        private Services.ITagService TagService {
            get {
                if (tagService == null)
                {
                    tagService = new Services.TagService();
                }
                return tagService;
            }
            set {
                tagService = value;
            }
        }

        public string GetCompassRose(int input)
        {
            string result = string.Empty;
            using (var context = new SailingEntities())
            {
                var choices = context.Static_CompassBearings.Where(i => i.Alias.Length < 4).OrderBy(i=>Math.Abs(i.Bearing-input));
                var entity=choices.FirstOrDefault();
                if (entity != null)
                {
                    result = entity.Alias;
                }
            }
            return result;
        }

        public IEnumerable<int> GetExpiredSailIds()
        {
            IEnumerable<int> result = null;
            using (var context = new SailingEntities())
            {
                var timeToExpire = DateTime.Now.AddHours(-6);
                var dbResults = context.Logs_Sails.Where(i => i.TestData == true && i.DateCreated < timeToExpire);
                if (dbResults != null && dbResults.Any())
                {
                    result = dbResults.Select(i => i.SailID).ToArray();
                }
            }
            return result;
        }

        public decimal? GetCompassRose(string input)
        {
            decimal? result = null;
            using (var context = new SailingEntities())
            {
                var entity = context.Static_CompassBearings.FirstOrDefault(i => i.Alias == input);
                if (entity != null)
                {
                    result = (decimal)entity.Bearing;
                }
            }
            return result;
        }

        public void DeleteTag(int id, int eid)
        {
            using (var context = new SailingEntities())
            {
                var trial = (from i in context.Entities_Tags where i.TagID == id select i).FirstOrDefault();
                if (trial != null)
                {
                    var sail = (from i in trial.Logs_Sails where i.SailID == eid select i).FirstOrDefault();
                    if (sail != null)
                    {
                        trial.Logs_Sails.Remove(sail);
                        context.SaveChanges();
                    }


                }
            }
        }

        public IEnumerable<SailFeedViewModel> GetFeedItems(FeedSearch search)
        {
            IEnumerable<SailFeedViewModel> result = new List<SailFeedViewModel>();
            if (search == null)
            {
                return null;
            }
            using (var context = new SailingEntities())
            {
                var query = context.Logs_Sails.Include(j => j.Entities_Crew1)
                       .Include(j => j.Entities_Crew)
                       .Include(j => j.Entities_Vessel)
                       .Include(j => j.Entities_Vessel.Entities_Vessels_Sails)
                       .Include(j => j.Entities_Vessel.Entities_Crew)
                       .Include(j=>j.Entities_Vessel.Entities_Vessels_Sails)
                       .Include(j => j.Entities_Vessel.Entities_Vessels_Sails.Select(i=>i.Entities_Config))
                       .Include(j => j.Entities_Vessel.Entities_Vessels_Sails.Select(i => i.Entities_Vessel))
                       .Include(j => j.Entities_Vessel.Entities_Vessels_Sails.Select(i => i.Entities_Vessel).Select(k=>k.Entities_Crew))
                       .Include(j => j.Entities_Crew2)
                       .Include(j => j.Entities_Crew3)
                       .Include(j => j.Entities_Locations).Where(i => i.DateDeleted == null &&i.TestData==null);
                if (search.DaysBack > 0 && search.StartDate != null)
                {
                    var minDate = ((DateTime)search.StartDate).AddDays(-1 * (Math.Abs((int)search.DaysBack)));
                    query = query.Where(i => i.DateCreated >= minDate);
                }
                if (search.VesselIDs != null && search.VesselIDs.Any())
                {
                    query=query.Where(i => search.VesselIDs.Contains(i.VesselID));
                }
                if (search.AnniversaryDate != null)
                {
                    query = query.Where(i => (SqlFunctions.DateDiff("DAY", i.DateCreated, search.AnniversaryDate) % 365) == 0);
                }
                if (search.CrewIDs != null && search.CrewIDs.Any())
                {
                    query = query.Where(i => i.Entities_Crew3.Any(j => search.CrewIDs.Contains(j.CrewID)));
                }
                query = query.OrderByDescending(i=>i.DateCreated).Take(search.NumberToGrab);
                var queryResult = query.ToArray();
                if (queryResult.Any())
                {
                    result = queryResult.Select(Factories.SailFactory.CreateFeedModel);
                }
            }
            if (result != null && result.Any() && search.Priority != null)
            {
                result.ForEach(i=>i.Order=(int)search.Priority);
            }

            return result;
        }

        public int JiffySave(Logs_Sails entity)
        {
            using (var context = new SailingEntities())
            {
                entity.Entities_Vessel =
                    context.Entities_Vessel.Include(i => i.Entities_Vessels_Sails)
                        .Include(i => i.Entities_Crew)
                        .FirstOrDefault();
                entity.Entities_Crew2 = context.Entities_Crew.FirstOrDefault(i => i.CrewID == entity.CreatedByID);
                entity.Entities_Locations = context.Entities_Locations.FirstOrDefault();
                context.Logs_Sails.Add(entity);
                    context.SaveChanges();

                return entity.SailID;
            }
        }

        

        public void AddTags(List<int> tagIds, int id)
        {
            using (var context = new SailingEntities())
            {
                List<Entities_Tags> tags = new List<Entities_Tags>();
                foreach (var tagId in tagIds)
                {
                    var tag = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                    if (tag != null && tag.Name != null)
                    {
                        tags.Add(tag);
                    }
                }

                var sail = (from i in context.Logs_Sails where i.SailID == id select i).FirstOrDefault();
                if (sail != null && sail.SailID >0)
                {
                    foreach (var tag in tags)
                    {
                        var match = sail.Entities_Tags.Where(i => i.TagID == tag.TagID).FirstOrDefault();
                        if (match == null || match.TagID <= 0)
                        {
                            sail.Entities_Tags.Add(tag);
                        }
                    }
                }
                context.SaveChanges();

            }

        }

        public IEnumerable<New.Tags> GetTags(int id)
        {
            IEnumerable<New.Tags> result = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Logs_Sails.Include(j => j.Entities_Tags) where i.SailID == id select i.Entities_Tags.Where(k => k.DateDeleted == null)).FirstOrDefault();
                result = query.ToArray().Select(Factories.CommonFactory.CreateTag);
            }
            return result;
        }

        public IEnumerable<New.Maneuver> GetManeuvers() {
            IEnumerable<New.Maneuver> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Maneuvers.Distinct().ToArray();
                if (query != null)
                {
                    result = query.Select(Factories.CommonFactory.CreateManeuver);
                }
            }
            return result;
        }

        public IEnumerable<New.Configuration> GetConfigurations()
        {
            IEnumerable<New.Configuration> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Config.Where(i => !string.IsNullOrEmpty(i.Name)).Distinct().ToArray();
                if (query != null)
                {
                    result = query.Select(Factories.CommonFactory.CreateConfiguration);
                }
            }

            return result;
        }

        public IEnumerable<New.WeatherBase> GetWeather()
        {
            IEnumerable < New.WeatherBase > result= null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Weather.Distinct().ToArray();
                if (query != null)
                {
                    result = query.Select(Factories.CommonFactory.CreateWeatherBase).ToArray();
                }
            }
            return result;
    }

        public IEnumerable<New.FleetBase> GetAllFleets()
        {
            IEnumerable<New.FleetBase> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Fleet.ToArray();
                if (query != null)
                {
                    result = query.Select(Factories.FleetFactory.CreateFleetBase);
                }
               
            }
            return result;
        }

        public IEnumerable<New.LocationBase> GetLocations() {
            IEnumerable<New.LocationBase> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Locations.Include(i=>i.Entities_Photos).ToArray();
                if (query != null)
                {
                    result = query.Select(CommonFactory.CreateLocationBase);
                }
            }
            return result;
        }
        public IEnumerable<SailingSiteModels.New.VesselFoundation> GetVesselNames()
        {
            IEnumerable<SailingSiteModels.New.VesselFoundation> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Vessel.Include(i=>i.Entities_Crew).Distinct();
                result = query.ToArray().Select(Factories.FleetFactory.CreateVesselFoundation);
            }
            return result;
        }

        public IEnumerable<New.CrewFoundation> GetCrewNames()
        {
            IEnumerable<New.CrewFoundation> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Crew.Where(i=>i.DateDeleted==null&&i.UserName!="sysadmin").OrderBy(i=>i.LastName).Distinct().ToArray();
                if (query != null)
                {
                    result = query.Select(Factories.CrewFactory.CreateCrewFoundation).ToArray();
                }
            }
            return result;
        }

        public New.SailBase GetWindiestSail(int crewID)
        {
            New.SailBase result = null;
            using (var context = new SailingEntities())
            {
                var query =
                    context.Entities_Crew.Where(c => c.CrewID == crewID)
                        .Select(
                            t =>
                            t.Logs_Sails.Where(i => i.DateDeleted == null)
                                .OrderByDescending(k => k.MaxWind)
                                .ThenBy(l => l.MaxTilt)
                                .Select(o => o.SailID))
                        .FirstOrDefault();
                var qresult = query.FirstOrDefault();
                if (qresult!=null)
                result= GetSailBases(new int[] {qresult}).FirstOrDefault();
            }
            return result;
        }

        public New.SailBase GetLongestSail(int crewID)
        {
            New.SailBase result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Crew.Where(c => c.CrewID == crewID).Select(t => t.Logs_Sails.Where(i=>i.DateDeleted==null).OrderByDescending(k => k.Miles).Select(o => o.SailID)).FirstOrDefault();
                var qresult = query.Take(1).FirstOrDefault();
                if (qresult != null)
                    result = GetSailBases(new int[] { qresult }).FirstOrDefault();
            }
            return result;
        }

        public IEnumerable<SheetSail> GetSheetSailLocker(int id)
        {
            IEnumerable<SheetSail> result = null;
            using (var context = new SailingEntities())
            {
                var dbresult = context.Entities_Vessel.Where(i => i.VesselID == id).Include(i=>i.Entities_Vessels_Sails).Include(i=>i.Entities_Vessels_Sails.Select(j=>j.Entities_Config))
                    .Select(i => i.Entities_Vessels_Sails).FirstOrDefault();
                if (dbresult != null && dbresult.Any())
                {
                    result = dbresult.Select(CommonFactory.CreateSheetSail).ToArray();
                }
               
            }
             return result;
        }


        public int CreateSail(New.Sail data, bool jiffy = false)
        {
            int result = 0;
            // TODO add photos  
            if (data == null)
            {
                return 0;
            }

            var entity = Factories.SailFactory.CreateSail(data);


            using (var context = new SailingEntities())
            {
                if (jiffy)
                {
                    entity.DateDeleted = null;
                    if (entity.Entities_Crew == null)
                    {
                        entity.Entities_Crew = context.Entities_Crew.FirstOrDefault(
                            i => i.CrewID == entity.CreatedByID);
                    }
                    entity.Entities_Vessel =
                        context.Entities_Vessel.Where(i => i.VesselID == data.Vessel.ID).FirstOrDefault();
                    entity.Entities_Locations =
                        context.Entities_Locations.Where(i => i.LocationID == data.Location.ID).FirstOrDefault();
                }


                if (data.Tags != null && data.Tags.Any())
                {
                    foreach (var tag in data.Tags.Distinct())
                    {
                        Entities_Tags entityTag = null;
                        TagService.SaveTag(tag, entityTag);
                        if (entityTag.TagID > 0)
                        {
                            entity.Entities_Tags.Add(entityTag);
                        }
                    }
                }

                if (data.Weather != null && data.Weather.Any())
                {
                    var weatherInts = data.Weather.Select(i => i.ID);
                    entity.Entities_Weather =
                        context.Entities_Weather.Where(i => weatherInts.Contains(i.WeatherID)).ToArray();

                }

                entity.VesselID = data.Vessel.ID;

                entity.LocationID = data.Location.ID;

                if (data.Animals != null && data.Animals.Any())
                {
                    var animalIdList = data.Animals.Select(i => i.ID);
                    entity.Entities_Animals_Names =
                        context.Entities_Animals_Names.Where(i => animalIdList.Contains(i.AnimalID)).ToArray();
                }
                if (data.Crew != null && data.Crew.Any())
                {
                    var crewIdList = data.Crew.Select(i => i.ID);
                    entity.Entities_Crew3 = context.Entities_Crew.Where(i => crewIdList.Contains(i.CrewID)).ToArray();
                }
                if (data.Maneuvers != null && data.Maneuvers.Any())
                {
                    var maneuvers = data.Maneuvers.Select(i => i.ID);
                    entity.Entities_Maneuvers =
                        context.Entities_Maneuvers.Where(i => maneuvers.Contains(i.ManeuverID)).ToArray();
                }

                context.Logs_Sails.Add(entity);
                context.SaveChanges();
                if (entity != null)
                {
                    this.ParseSheetSailLog(entity.SailID, data.SheetSailLogs);
                    result = entity.SailID;
                }
            }


            return result;
        }

        /// <summary>
        /// The parse sheet sail configurations.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="entries">
        /// The entries.
        /// </param>
        private void ParseSheetSailLog(int id, IEnumerable<New.SheetSailLog> entries)
        {
            if (entries != null && entries.Any())
            {
                using (var context = new SailingEntities())
                {
                    var sail = context.Logs_Sails.FirstOrDefault(i => i.SailID == id);
                    if (sail != null)
                    {
                        foreach (var entry in entries)
                        {
                            if (entry != null && entry.SheetSail != null)
                            {

                                var log = CommonFactory.CreateSheetSailLog_Sail(entry);
                                sail.Logs_Sails_Vessels_Sails.Add(log);
                                context.SaveChanges();
                            }
                        }
                    }
                    //TODO: Make enum of items rather than foreach.
                }
            }
        }

        public void DeleteSail(int id)
        {
            using (var context = new SailingEntities())
            {
                var sail = context.Logs_Sails.FirstOrDefault(i => i.SailID == id);
                sail.DateDeleted = DateTime.Now;
                context.SaveChanges();
            }
        }

        public void PurgeSail(IEnumerable<int> ids)
        {
            using (var context = new SailingEntities())
            {
                if (ids != null)
                {
                    var results = context.Logs_Sails.Where(i => ids.Contains(i.SailID));
                    if (results.Any())
                    {
                        foreach (var result in results)
                        {
                            result.Logs_Sails_Vessels_Sails.Clear();
                            result.Entities_Animals_Names.Clear();
                            result.Entities_Crew3.Clear();
                            result.Entities_Comment.Clear();
                            result.Entities_Maneuvers.Clear();
                            result.Logs_Sails_Favorites_Monthly.Clear();
                            result.Entities_Vessel1.Clear();
                            context.SaveChanges();
                        }
                        context.Logs_Sails.RemoveRange(results);
                        context.SaveChanges();
                    }
                }
            }
        }

        public void Sterilize()
        {
            using (var context = new SailingEntities())
            {
                var landMarks = context.Entities_LandMark.Where(i => i.Test == true);
                if (landMarks != null && landMarks.Any())
                {
                    foreach (var landMark in landMarks)
                    {

                        var bearings=landMark.Bearings_LandMark.Select(i => i.Entities_Bearing);
                        landMark.Bearings_LandMark.Clear();
                        if (bearings != null && bearings.Any())
                        {
                            context.Entities_Bearing.RemoveRange(bearings);
                        }
                    }

                    context.Entities_LandMark.RemoveRange(landMarks);
                    context.SaveChanges();
                }

                var data = context.Logs_Sails.Where(i => i.TestData == true).ToArray();
                if (data.Any())
                {
                    foreach (var datum in data)
                    {
                       
                        var commentsAll = context.Entities_Comment.Where(i => i.Logs_Sails.SailID==datum.SailID).ToArray();
                        if (commentsAll != null && commentsAll.Any())
                        {
                            var commentSailSheets =
                                commentsAll.Select(i => i.Logs_Comments_Vessel_Sails).ToArray().FirstOrDefault();
                            if (commentSailSheets != null && commentSailSheets.Any())
                            {
                                context.Logs_Comments_Vessel_Sails.RemoveRange(commentSailSheets);
                            }
                            context.Entities_Comment.RemoveRange(commentsAll);
                                context.SaveChanges();
                        }

                    }
                    context.Logs_Sails.RemoveRange(data);
                }
                context.SaveChanges();
                var locations = context.Entities_Locations.Where(i => i.TestData == true).ToArray();
                if (locations.Any())
                {
                    context.Entities_Locations.RemoveRange(locations);
                }
                context.SaveChanges();
                var comments = context.Entities_Comment.Where(i => i.TestData == true).ToArray();
                if (comments.Any())
                {
                    context.Entities_Comment.RemoveRange(comments);
                }
                context.SaveChanges();
                var crew = context.Entities_Crew.Where(i => i.TestData == true);
                if (crew.Any())
                {
                    context.Entities_Crew.RemoveRange(crew);
                }
                
                var fleets = context.Entities_Fleet.Where(i => i.TestData == true).ToArray();
                if (fleets != null && fleets.Any())
                {
                    context.Entities_Fleet.RemoveRange(fleets);
                }
                context.SaveChanges();

            }
            this.SterilizeFleet();
        }

        private void SterilizeFleet()
        {
            using (var context = new SailingEntities())
            {
                var vessels = context.Entities_Vessel.Where(i => i.TestData == true).ToArray();
                if (vessels != null && vessels.Any())
                {
                    var sails = vessels.Select(i => i.Entities_Vessels_Sails).ToArray().FirstOrDefault();
                    if (sails != null && sails.Any())
                    {
                        context.Entities_Vessels_Sails.RemoveRange(sails);
                        context.SaveChanges();
                    }

                    context.Entities_Vessel.RemoveRange(vessels);
                    context.SaveChanges();
                }
            }
        }

        public void UpdateComment(New.Comment entity)
        {
            using (var context = new SailingEntities())
            {
                var sail = context.Logs_Sails.FirstOrDefault(i => i.Entities_Comment.Any(j => j.CommentID == entity.ID));
                if (sail != null)
                {
                    AddComment(entity.ID, sail.SailID);
                }
            }
        }

        public void UpdateSail(New.Sail newEntity)
        {
            using (var context = new SailingEntities())
            {
                var oldEntity = context.Logs_Sails.FirstOrDefault(i => i.SailID == newEntity.ID);
                oldEntity.DateDeleted = null;
                oldEntity.Entities_Vessel =
                    context.Entities_Vessel.FirstOrDefault(i => i.VesselID == newEntity.Vessel.ID);
                oldEntity.WinDirection = newEntity.WindDirection;
                oldEntity.MinWind = newEntity.MinWind;
                oldEntity.MaxWind = newEntity.MaxWind;
                oldEntity.AvgWind = newEntity.AvgWind;
                oldEntity.TempF = newEntity.TempF;
                oldEntity.Hours = newEntity.HoursSailed;
                oldEntity.HoursMotored = newEntity.HoursMotored;
                oldEntity.Barometer = newEntity.Barometer;
                oldEntity.Miles = newEntity.MilesSailed;
                oldEntity.RainSeverity = newEntity.RainSeverity;
                oldEntity.SeaState = newEntity.SeaState;
                oldEntity.MilesMotored = newEntity.MilesMotored;
                oldEntity.WinDirection = newEntity.WindDirection;
                oldEntity.TestData = newEntity.IsTest;
                oldEntity.Entities_Locations =
                    context.Entities_Locations.FirstOrDefault(i => i.LocationID == newEntity.Location.ID);
                oldEntity.Date = newEntity.Date;
                oldEntity.GeneralNotes = newEntity.GeneralObservations;
                oldEntity.CenterBoardPct = newEntity.Centerboard;
                oldEntity.MaxTilt = newEntity.Tilt == null ? (int?)null : (int)newEntity.Tilt;
                oldEntity.DateDeleted = null;
                oldEntity.Logs_Sails_Vessels_Sails.Clear();
                context.SaveChanges();
                if (newEntity != null && newEntity.SheetSailLogs!=null&& newEntity.SheetSailLogs.Any())
                {
                    this.ParseSheetSailLog(newEntity.ID,newEntity.SheetSailLogs);
                }

                if (newEntity.Animals != null && newEntity.Animals.Any())
                {
                    var p = newEntity.Animals.Select(j => j.ID);
                    var animeaux =
                        context.Entities_Animals_Names.Where(
                            i => p.Contains(i.AnimalID)).ToList();
                    oldEntity.Entities_Animals_Names.Clear();
                    oldEntity.Entities_Animals_Names = animeaux;
                }

                else
                {
                    oldEntity.Entities_Animals_Names.Clear();
                }

                if (newEntity.Crew != null && newEntity.Crew.Any())
                {
                    var p = newEntity.Crew.Select(j => j.ID);
                    oldEntity.Entities_Crew3.Clear();
                    var crew =
                        context.Entities_Crew.Where(
                            i => p.Contains(i.CrewID));
                    oldEntity.Entities_Crew3 = crew.ToList();
                }
                else
                {
                    oldEntity.Entities_Crew3.Clear();
                }

                if (newEntity.Maneuvers != null && newEntity.Maneuvers.Any())
                {
                    var p = newEntity.Maneuvers.Select(j => j.ID);
                    oldEntity.Entities_Maneuvers.Clear();
                    var mans =
                        context.Entities_Maneuvers.Where(
                            i => p.Contains(i.ManeuverID)).ToArray();
                    oldEntity.Entities_Maneuvers = mans.ToList();
                }
                else
                {
                    oldEntity.Entities_Maneuvers.Clear();
                }

                if (newEntity.Vessels != null && newEntity.Vessels.Any())
                {
                    var p = newEntity.Vessels.Select(j => j.ID);
                    oldEntity.Entities_Vessel1.Clear();
                    var vessels = context.Entities_Vessel.Where(i=>p.Contains(i.VesselID));
                    oldEntity.Entities_Vessel1 = vessels.ToList();
                }
                else
                {
                    oldEntity.Entities_Vessel1.Clear();
                }

                if (newEntity.Weather != null && newEntity.Weather.Any())
                {
                    var p = newEntity.Weather.Select(j => j.ID);
                    oldEntity.Entities_Weather.Clear();
                    var weather =
                        context.Entities_Weather.Where(i => p.Contains(i.WeatherID));
                    oldEntity.Entities_Weather = weather.ToList();
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<New.SailBase> GetSailBases(IEnumerable<int> ids)
        {
           ids = ids.Distinct();
           IEnumerable<New.SailBase> result = null;
           using (var context=new SailingEntities())
           {
               var query = from i in context.Logs_Sails where i.DateDeleted == null && ids.Contains(i.SailID) select i;
               query =
                   query.Include(j => j.Entities_Crew1)
                       .Include(j => j.Entities_Crew)
                       .Include(j => j.Entities_Vessel)
                       .Include(j => j.Entities_Vessel.Entities_Vessels_Sails)
                       .Include(j => j.Entities_Vessel.Entities_Crew)
                       .Include(j=>j.Entities_Vessel.Entities_Vessels_Sails)
                       .Include(j => j.Entities_Vessel.Entities_Vessels_Sails.Select(i=>i.Entities_Config))
                       .Include(j => j.Entities_Vessel.Entities_Vessels_Sails.Select(i => i.Entities_Vessel))
                       .Include(j => j.Entities_Vessel.Entities_Vessels_Sails.Select(i => i.Entities_Vessel).Select(k=>k.Entities_Crew))
                       .Include(j => j.Entities_Crew2)
                       .Include(j => j.Entities_Crew3)
                       .Include(j => j.Entities_Locations);
               var dbEntity = query.ToArray();
               if (dbEntity.Any(i => i.Entities_Crew == null))
               {
                   foreach (var entity in dbEntity.Where(i => i.Entities_Crew == null))
                   {
                       entity.Entities_Crew = context.Entities_Crew.FirstOrDefault(i => i.CrewID == entity.CreatedByID);
                   }
               }
               if (dbEntity != null && dbEntity.Any())
               {
                   result = dbEntity.Select(Factories.SailFactory.CreateSailbase);
               }

           }
           return result;
       }

       public New.Sail GetSail(int id)
       {
           New.Sail result = null;
           using (var context = new SailingEntities())
           {
               var query =
                   context.Logs_Sails.Where(i => i.SailID == id)
                   .Include(i => i.Logs_Sails_Vessels_Sails)
                       .Include(i => i.Logs_Sails_Vessels_Sails.Select(j => j.Entities_Vessels_Sails))
                       .Include(i => i.Logs_Sails_Vessels_Sails.Select(j => j.Entities_Vessels_Sails.Entities_Config))
                       .Include(i => i.Logs_Sails_Vessels_Sails.Select(j => j.Entities_Vessels_Sails.Entities_Vessel))
                       .Include(i => i.Logs_Sails_Vessels_Sails.Select(j => j.Entities_Vessels_Sails.Entities_Vessel.Entities_Crew))
                       .Include(i => i.Entities_Crew)
                       .Include(i => i.Entities_Animals_Names)
                       .Include(
                           i =>
                           i.Entities_Comment)
                       .Include(i => i.Entities_Locations)
                       .Include(i => i.Entities_Tags)
                       .Include(i => i.Entities_Vessel)
                       .Include(i=>i.Entities_Vessel.Entities_Vessels_Sails)
                       .Include(i=>i.Entities_Vessel.Entities_Vessels_Sails.Select(j=>j.Entities_Config))
                       .Include(i=>i.Entities_Comment)
                       .Include(i=>i.Entities_Comment1.Select(j=>j.Entities_Crew))
                       .Include(i => i.Entities_Weather)
                       .Include(i => i.Entities_Maneuvers)
                       .Include(i => i.Entities_Comment1)
                       .Include(i=>i.Entities_Crew2)
                       .Include(i => i.Entities_Photos);
               var dbentity = query.FirstOrDefault();
               if (dbentity != null)
               {
                   dbentity.Entities_Crew3 = dbentity.Entities_Crew3.Where(i => i.DateDeleted == null).ToArray();
                   dbentity.Entities_Comment =
                       dbentity.Entities_Comment.Where(
                           i => i.DateDeleted == null && i.LogId == null && i.BearingOnly != true)
                           .OrderByDescending(j => j.DateCreated)
                           .ToArray();
                   dbentity.Entities_Photos = dbentity.Entities_Photos.Where(i => i.DeleteDate == null).ToArray();
                   result = Factories.SailFactory.CreateSail(dbentity);
               }


           }
           return result;
       }

        public DateTime? GetSailDate(int entityId)
        {
            DateTime? result = null;
            using (var context = new SailingEntities())
            {
                var entry = context.Logs_Sails.FirstOrDefault();
                if (entry != null)
                {
                    result = entry.Date;
                }
            }
            return result;
        }


        public void AddPhoto(int photoID, int entityId)
       {
           using (var context = new SailingEntities())
           {
               var sail = context.Logs_Sails.FirstOrDefault(i => i.SailID == entityId);
               var photo = context.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoID);
               var foundPhoto = sail.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoID);
               if (foundPhoto == null)
               {
                   sail.Entities_Photos.Add(photo);
                   context.SaveChanges();
               }

           }

       }

        // TODO: ADD COMMENT.  Have comment validate stats against sail, replace if higher!

       public void DeletePhoto(int photoID, int entityId)
       {
           using (var context = new SailingEntities())
           {
               var photo = context.Entities_Photos.FirstOrDefault(i=>i.PhotoID==photoID);
               var sail = context.Logs_Sails.FirstOrDefault(i=>i.SailID==entityId);
               if (photo != null && sail != null)
               {
                   if (sail.Entities_Photos.Any(i => i.PhotoID == photoID))
                   {
                       sail.Entities_Photos.Remove(photo);
                       context.SaveChanges();
                   }
               }
           }
       }

       public void AddComment(int commentID, int entityID)
       {
           using (var context = new SailingEntities())
           {
               var comment = context.Entities_Comment.FirstOrDefault(i => i.CommentID == commentID);
               var sail = context.Logs_Sails.FirstOrDefault(i => i.SailID == entityID);
               if (comment != null && sail != null)
               {
                  
                       if (comment.WindSpeed != null && comment.WindSpeed >sail.MaxWind)
                       {
                           sail.MaxWind = (decimal)comment.WindSpeed;
                       }
                       if (comment.WindSpeed != null && comment.WindSpeed < sail.MinWind)
                       {
                           sail.MinWind = (decimal)comment.WindSpeed;
                       }

                       if (comment.HeelPct != null && comment.HeelPct > sail.MaxTilt)
                       {
                           sail.MaxTilt = (int)comment.HeelPct;
                       }

                       if (comment.Entities_Animals_Names!=null&&comment.Entities_Animals_Names.Any())
                       {
                           var lista = comment.Entities_Animals_Names.Select(a => a.AnimalID);
                           var listb = sail.Entities_Animals_Names.Select(a => a.AnimalID);
                           var difference = lista.Except(listb).Distinct().ToArray();
                           if (difference.Any())
                           {
                               var animals =
                                   context.Entities_Animals_Names.Where(a => difference.Contains(a.AnimalID)).ToArray();
                               foreach (var animal in animals)
                               {
                                   sail.Entities_Animals_Names.Add(animal);
                               }
                           }
                       }

                       if (sail.Entities_Comment == null
                           || !sail.Entities_Comment.Any(i => i.CommentID == comment.CommentID))
                       {
                           sail.Entities_Comment.Add(comment);
                       }

                       context.SaveChanges();
                   
                   
               }
           }
       }

       public IEnumerable<int> GetCommentIDsForSail(IEnumerable<int> entityID)
       {
           IEnumerable<int> result = null;
           using (var context = new SailingEntities())
           {
               var comment =
                   context.Logs_Sails.Where(i => entityID.Contains(i.SailID))
                       .Select(i => i.Entities_Comment.Select(j => j.CommentID)).FirstOrDefault();
               var comment2 = context.Logs_Sails.Where(i => entityID.Contains(i.SailID))
                       .Select(i => i.Entities_Comment1.Select(j => j.CommentID)).FirstOrDefault();
               if (comment != null&&comment2!=null)
               {
                   comment.Concat(comment2);
               }
               else if (comment2!=null&&comment==null)
               {
                   comment = comment2;
               }
               if (comment != null)
               {
                   result = comment.ToArray();
               }

           }
           return result;
       }

       public IEnumerable<int> GetTrackandLogIDsForSail(IEnumerable<int> entityID)
       {
           IEnumerable<int> result = null;
           using (var context = new SailingEntities())
           {
               var comment =
                   context.Entities_Comment.Where(i => i.LogId != null && entityID.Contains((int)i.LogId))
                       .Select(i => i.CommentID);
               if (comment != null)
               {
                   result = comment.ToArray();
               }


           }
           return result;
       }

        /// <summary>
        /// The get vessel for sail.
        /// </summary>
        /// <param name="sailId">
        /// The sail id.
        /// </param>
        /// <returns>
        /// The <see cref="VesselBase"/>.
        /// </returns>
        public New.VesselBase GetVesselForSail(int sailId)
       {
           New.VesselBase result = null;
           using (var context = new SailingEntities())
           {
               var query = context.Logs_Sails.FirstOrDefault(i => i.SailID == sailId);
               if (query != null && query.Entities_Vessel != null)
               {
                   result = Factories.FleetFactory.CreateVesselBase(query.Entities_Vessel);
               }
           }

           return result;
       }


        public int CreateLandMark(LandMarkBase entity)
        {
            int result = 0;
            if (entity!=null)
            using (var context = new SailingEntities())
            {
                var dbLandMark = Factories.CommonFactory.CreateLandMark(entity);
                dbLandMark.Latitude = dbLandMark.Latitude * 1.0;
                dbLandMark.Longitude = dbLandMark.Longitude * 1.0;
                context.Entities_LandMark.Add(dbLandMark);
                context.SaveChanges();
                result = dbLandMark.Id;
            }

            return result;
        }
    }
}