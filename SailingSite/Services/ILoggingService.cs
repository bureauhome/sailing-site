﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILoggingService.cs" company="">
//   
// </copyright>
// <summary>
//   The LoggingService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using System;
    using System.Threading.Tasks;

    using SailingSite.Common;

    using SailingSiteModels.New.Debug;

    /// <summary>
    /// The LoggingService interface.
    /// </summary>
    public interface ILoggingService
    {
        /// <summary>
        /// The generate log.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void GenerateLog(Exception exception, AuditContext context);

        /// <summary>
        /// The generate log.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void GenerateLog(string message, Log.LogTypes type, AuditContext context);

        /// <summary>
        /// The generate log.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void GenerateLog(
            string message,
            SailingSiteModels.New.Debug.Log.LogTypes type,
            Common.AuditContext context,
            object entity);

        /// <summary>
        /// The generate log.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        int GenerateLog(Log log);
    }
}
