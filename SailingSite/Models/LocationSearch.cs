﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models
{
    using SailingSiteModels.New;

    public class LocationSearch
    {
        public string City { get; set; }
        public string Region { get; set; }
        public LocationType LocationType { get; set; }
    }
}