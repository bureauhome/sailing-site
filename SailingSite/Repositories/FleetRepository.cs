﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using System.Data.Entity.Validation;

    using New;

    using SailingSite.Factories;

    public class FleetRepository :iFleetRepository
    {
        private IVesselRepository vesselrepository;
        public IVesselRepository VesselRepository {
            get {
                if (vesselrepository == null)
                {
                    vesselrepository = new VesselRepository();
                }
                return vesselrepository;
            }
        }
        public New.FleetBase SelectFleetBase(int id) {
            var entity=SelectSingle(id);
            var result=Factories.FleetFactory.CreateFleetBase(entity);
            result.CrewCount = entity.Logs_Fleet_Crew.Count();
            result.VesselCount = entity.Logs_Fleet_Vessel.Count();
            result.MileCount = VesselRepository.GetMileTotalForFleet(id);
            return result;
        }

        /// <summary>
        /// The select fleet sail count.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int SelectFleetSailCount(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                result =
                    context.Entities_Vessel.Where(i => i.Logs_Fleet_Vessel.Any(j => j.FleetID == id))
                        .Select(j => j.Logs_Sails)
                        .Count();
            }
            return result;
        }

        /// <summary>
        /// The select mile sum.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal SelectMileSum(int id)
        {
            decimal result = 0;
            using (var context = new SailingEntities())
            {
                var sails = context.Logs_Sails.Where(i => i.Entities_Vessel.Logs_Fleet_Vessel.Any(j => j.FleetID == id));
                if (sails != null && sails.Any())
                {
                    result = sails.Sum(i => i.Miles);
                }
            }

            return result;
        }

        /// <summary>
        /// The select crew sum.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int SelectCrewSum(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                result =
                    context.Entities_Crew.Where(
                        i => i.Logs_Sails3.Any(j => j.Entities_Vessel.Logs_Fleet_Vessel.Any(k => k.FleetID == id)))
                        .Count();
            }

            return result;
        }

        public IEnumerable<New.Tags> GetTags(int id)
        {
            IEnumerable<New.Tags> result = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Fleet.Include(j => j.Entities_Tags) where i.FleetID == id select i.Entities_Tags.Where(k=>k.DateDeleted==null)).FirstOrDefault();
                result = query.ToArray().Select(Factories.CommonFactory.CreateTag);
            }
            return result;
        }

        public bool DoesFleetNameExist(string name)
        {
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Fleet.Where(i => i.FleetName.ToLower() == name.ToLower()).FirstOrDefault();
                if (query != null&&query.FleetID>0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// The does user have fleet security.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUserFleetCrew(int userId, int fleetId)
        {
            bool result = false;

            using (var context = new SailingEntities())
            {
                result = context.Logs_Fleet_Crew.Any(i => i.FleetID == fleetId && i.CrewID == userId);
            }

            return result;
        }

        /// <summary>
        /// The select fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Fleet"/>.
        /// </returns>
        public New.Fleet SelectFleet(int id)
        {
            var entity=SelectSingle(id);
            var result=Factories.FleetFactory.CreateFleet(entity);
            if (result.Crew != null && result.Crew.Any())
            {
                foreach (var crew in result.Crew)
                {
                    crew.MileCount = GetCrewMileCountForFleet(crew.ID, id);
                    crew.SailCount = GetCrewSailCountForFleet(crew.ID, id);
                    crew.HourCount = GetCrewHourCountForFleet(crew.ID, id);

                }
                result.CrewCount = entity.Logs_Fleet_Crew.Count();
            }
            if (result.VesselEntries != null && result.VesselEntries.Any())
            {
                result.VesselCount = entity.Logs_Fleet_Vessel.Count();
                result.MileCount = VesselRepository.GetMileTotalForFleet(id);
            }

            return result;

        }


        public decimal GetVesselMileCountForFleet(int vesselId)
        {
            decimal result = 0;
            using (var context = new SailingEntities())
            {
                result = context.Logs_Sails.Where(i => i.Entities_Vessel.Logs_Fleet_Vessel.Any(j=>j.VesselID==vesselId)).Distinct().Sum(i => i.Miles);
            }
            return result;
        }

        public int GetVesselSailCountForFleet(int vesselId)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                result = context.Logs_Sails.Where(i => i.Entities_Vessel.Logs_Fleet_Vessel.Any(j => j.VesselID == vesselId)).Distinct().Count();
            }
            return result;
        }



        private decimal GetCrewMileCountForFleet(int crewId, int fleetId)
        {
            decimal result=0;
            using (var context=new SailingEntities())
            {
                result =
                    context.Logs_Sails.Where(
                        i =>
                        i.Entities_Vessel.Logs_Fleet_Vessel.Any(j => j.FleetID == fleetId)
                        && i.Entities_Crew3.Any(j => j.CrewID == crewId&&j.DateDeleted==null)).Distinct().Sum(i => i.Miles);
            }
            return result;
        }

        private int GetCrewSailCountForFleet(int crewId, int fleetId)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                result = context.Logs_Sails.Where(i => i.Entities_Vessel.Logs_Fleet_Vessel.Any(j => j.FleetID == fleetId) && i.Entities_Crew3.Any(j => j.CrewID == crewId && j.DateDeleted == null)).Distinct().Count();
            }
            return result;
        }

        private decimal GetCrewHourCountForFleet(int crewId, int fleetId)
        {
            decimal result = 0;
            using (var context = new SailingEntities())
            {
                result =
                    context.Logs_Sails.Where(
                        i =>
                        i.Entities_Vessel.Logs_Fleet_Vessel.Any(j => j.FleetID == fleetId)
                        && i.Entities_Crew3.Any(j => j.CrewID == crewId && j.DateDeleted == null))
                        .Distinct()
                        .Sum(i => i.Hours);
            }
            return result;
        }

        private ICrewRepository crewRepository;
        private ICrewRepository CrewRepository
        {
            get {
                if (crewRepository == null)
                {
                    crewRepository = new CrewRepository();
                }
                return crewRepository;
            }
            set {
                crewRepository = value;
            }
        }

        public New.Fleet GetFleet(int id)
        {
            return SelectFleet(id);
        }

        public New.Fleet HydrateFleet(int id)
        {
            var entity = this.SelectFleet(id);
            if (entity.VesselEntries != null && entity.VesselEntries.Any())
            {
                var nums = entity.VesselEntries.Select(i => i.Vessel).Select(j=>j.ID);
                entity.Crew = crewRepository.GetCrewFoundationForVessel(nums).Select(i=>i.ToCrewBase()).ToList();
            }
            return entity;
        }

        public IEnumerable<New.FleetBase> GetAllFleetBases() {
            var query = SelectMultiple();
            var result = query.Select(Factories.FleetFactory.CreateFleetBase);
            foreach (var entry in result)
            {
                hydrateBaseFleetStats(entry);
                
            }
            return result;
        }

        private void hydrateBaseFleetStats(New.FleetBase entity)
        {
            entity.CrewCount = GetFleetCrewCount(entity.ID);
            entity.VesselCount = GetFleetVesselCount(entity.ID);
            entity.MileCount = GetFleetMileCount(entity.ID);
        }

        public int? GetFleetCrewCount(int id)
        {
            int? result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Logs_Sails.Where(i => i.Entities_Vessel.Logs_Fleet_Vessel.Any(j => j.FleetID == id)).Select(i => i.Entities_Crew3.Where(k=>k.DateDeleted!=null).Select(j=>j.CrewID)).ToArray();
                var query2 = query.Distinct();
   
                result = query2.Count();
                return result;
            }
        }

        public int? GetFleetVesselCount(int id)
        {
            int? result = null;
            using (var context = new SailingEntities())
            {
                result = context.Entities_Vessel.Where(i => i.Logs_Fleet_Vessel.Any(j => j.FleetID == id)).Distinct().Count();
                return result;
            }
        }

        public decimal? GetFleetMileCount(int id)
        {
            decimal? result = null;
            using (var context = new SailingEntities())
            {
                result = context.Entities_Vessel.Where(i => i.Logs_Fleet_Vessel.Any(k => k.FleetID == id)).Select(i => i.Logs_Sails.Sum(j => j.Miles)).FirstOrDefault();
                return result;
            }
        }

        public int CreateFleet(New.Fleet entity) {
            var dbentity = Factories.FleetFactory.CreateFleet(entity);
            var crewIDs = entity.AssignedCrew==null? null:entity.AssignedCrew.Select(i => i.ID).ToArray();
            using (var context = new SailingEntities())
            {
                dbentity.Entities_Crew = entity.Captain==null? null: context.Entities_Crew.FirstOrDefault(i => i.CrewID == entity.Captain.ID);
                if (entity.AssignedCrew != null&&entity.AssignedCrew.Any())
                {
                    List<Logs_Fleet_Crew> crew = new List<Logs_Fleet_Crew>();
                    foreach (var id in crewIDs)
                    {
                        dbentity.Logs_Fleet_Crew.Add(
                            new Logs_Fleet_Crew()
                                {
                                    Entities_Crew =
                                        context.Entities_Crew.FirstOrDefault(i => i.CrewID == id),
                                });
                    }
                    dbentity.Logs_Fleet_Crew = dbentity.Logs_Fleet_Crew.Where(i=>i.Entities_Crew.DateDeleted==null).Distinct().ToArray();

                }
                
                context.Entities_Fleet.Add(dbentity);
                context.SaveChanges();
                return dbentity.FleetID;
            }

        }

        /// <summary>
        /// The add vessel to fleet.
        /// </summary>
        /// <param name="entry">
        /// The entry.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        public void AddVesselToFleet(New.VesselEntry entry, int fleetId)
        {
            using (var context = new SailingEntities())
            {
                var fleet = context.Entities_Fleet.FirstOrDefault(i => i.FleetID == fleetId);
                if (fleet != null && entry != null && entry.Vessel != null && entry.Vessel.ID > 0)
                {
                    var entity = FleetFactory.CreateVesselEntry(entry);
                    entity.Entities_Vessel =
                        context.Entities_Vessel.FirstOrDefault(i => i.VesselID == entry.Vessel.ID);
                    entity.Entities_Fleet = context.Entities_Fleet.FirstOrDefault(i => i.FleetID == fleetId);
                    var match =
                        context.Logs_Fleet_Vessel.FirstOrDefault(
                            i => i.FleetID == fleetId && i.Entities_Vessel.VesselID == entry.Vessel.ID);
                    if (match != null)
                    {
                        match.DateAdded = entity.DateAdded;
                        match.Nickname = entity.Nickname;
                        match.Ordinal = entity.Ordinal;

                    }
                    else
                    {
                        fleet.Logs_Fleet_Vessel.Add(entity);
                    }
                    context.SaveChanges();
                }

            }
        }

        public void DeleteFleet(int id)
        {
            using (var context = new SailingEntities())
            {
                var entity = context.Entities_Fleet.Where(i => i.FleetID == id).FirstOrDefault();
                entity.DateDeleted = DateTime.Today;
                context.SaveChanges();
            }
        }

        public void DeleteTag(int id, int fleetID)
        {
            using (var context = new SailingEntities())
            {
                var fleet = context.Entities_Fleet.Where(i => i.FleetID == fleetID).FirstOrDefault();
                var tag = fleet.Entities_Tags.Where(i => i.TagID == id).FirstOrDefault();
                if (tag != null)
                {
                    fleet.Entities_Tags.Remove(tag);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<FleetBase> Search(GenericQuery request)
        {
            IEnumerable<FleetBase> result = null;
            using (var context = new SailingEntities())
            {
                var query =
                    context.Entities_Fleet.Where(i => i.FleetName != null);

                if (!string.IsNullOrEmpty(request.FirstInitial))
                {
                    query = query.Where(i => i.FleetName.StartsWith(request.FirstInitial));
                }

                if (!string.IsNullOrEmpty(request.Name))
                {
                    query = query.Where(i => i.FleetName == request.Name);
                }

                if (query != null)
                {
                    result = query.Select(FleetFactory.CreateFleetBase).ToArray();
                }
            }

            return result;
        }

        public List<string> GetFirstInitials()
        {
            List<string> result = new List<string>();
            using (var context = new SailingEntities())
            {
                result = context.Entities_Fleet.Where(i => !string.IsNullOrEmpty(i.FleetName)).Select(i => i.FleetName.Substring(0, 1).ToUpper()).Distinct().ToList();
            }
            return result;
        }

        public void AddTags(List<int> tagIds, int fleetID)
        {
            using (var context = new SailingEntities())
            {
                List<Entities_Tags> tags = new List<Entities_Tags>();
                foreach (var tagId in tagIds)
                {
                    var tag = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                    if (tag != null && tag.Name != null)
                    {
                        tags.Add(tag);
                    }
                }

                var fleet = (from i in context.Entities_Fleet where i.FleetID == fleetID select i).FirstOrDefault();
                if (fleet != null && fleet.FleetID != null)
                {
                    foreach (var tag in tags)
                    {
                        var match = fleet.Entities_Tags.Where(i => i.TagID == tag.TagID).FirstOrDefault();
                        if (match == null || match.TagID <= 0)
                        {
                            fleet.Entities_Tags.Add(tag);
                        }
                    }
                }
                context.SaveChanges();

            }

        }
        public void UpdateFleet(New.Fleet entity) {

            using (var context = new SailingEntities())
            {
                var dbEntity = context.Entities_Fleet.Where(i => i.FleetID == entity.ID).FirstOrDefault();
                dbEntity.FleetName = entity.Name;
                if (entity.Captain != null)
                {
                    dbEntity.CaptainID = entity.Captain.ID;
                }
                dbEntity.Description = entity.Description;
                context.SaveChanges();

            }

        }

        private IEnumerable<Entities_Fleet> SelectMultiple(GenericSelector s=null) {
            IEnumerable<Entities_Fleet> result = null;
            using (var context = new SailingEntities())
            {
                result =
                    context.Entities_Fleet.Where(i=>i.DateDeleted==null&&i.TestData!=true)
                        .Include(i => i.Logs_Fleet_Vessel)
                        .Include(i => i.Logs_Fleet_Crew).ToArray();
                if (s == null || s.SelectDeleted == false)
                {
                    result = result.Where(i => i.DateDeleted == null);
                }
            }

            return result;
        }

        private Entities_Fleet SelectSingle(int id) {
            Entities_Fleet result = null;
            using (var context = new SailingEntities())
            {
                result=context.Entities_Fleet.Include(i=>i.Entities_Crew).Include(i=>i.Entities_Tags).Include(i=>i.Logs_Fleet_Vessel).Include(i=>i.Logs_Fleet_Vessel.Select(j=>j.Entities_Vessel)).Include(i=>i.Logs_Fleet_Vessel.Select(j=>j.Entities_Vessel).Select(k=>k.Entities_Vessels_Sails)).Include(i=>i.Logs_Fleet_Crew).Include(i=>i.Logs_Fleet_Crew.Select(j=>j.Entities_Crew)).Where(i => i.FleetID == id).FirstOrDefault();
                result.Logs_Fleet_Vessel = result.Logs_Fleet_Vessel.Where(i => i.Entities_Vessel.DateDeleted == null).ToArray();
                result.Logs_Fleet_Crew =
                    result.Logs_Fleet_Crew.Where(i => i.Entities_Crew.DateDeleted == null).ToArray();

            }
            return result;
        }
    }
}