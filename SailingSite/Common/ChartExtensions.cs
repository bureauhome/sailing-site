﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    using System.Collections;
    using System.Drawing;

    using DotNet.Highcharts;
    using DotNet.Highcharts.Enums;
    using DotNet.Highcharts.Helpers;
    using DotNet.Highcharts.Options;

    using SailingSite.Models;

    using SailingSiteModels.Common;
    using SailingSiteModels.New;
    using SailingSiteModels.Old;

    using Vessel = SailingSiteModels.New.Vessel;

    public static class ChartExtensions
    {
        public static ChartCollection GenerateChartCollection(this ISailable entity)
        {
            ChartCollection result = null;
            if (entity != null && entity.Sails != null && entity.Sails.Any())
            {
                result=new ChartCollection();
                var list = new List<ChartEntry>();
                list.Add(new ChartEntry(GetMileChart(entity.Sails),"Miles per Sail"));
                list.Add(new ChartEntry(YearOverYearMileCount(entity.Sails), "Yr VS Yr - Miles Sailed"));
                list.Add(new ChartEntry(YearOverYearSailCount(entity.Sails), "Yr VS Yr - # of Sails"));
                list.Add(new ChartEntry(YearOverYearAvgWind(entity.Sails), "Yr VS Yr - Avg Wind"));
                //list.Add(new ChartEntry(GetMaxWindChart(entity.Sails),"Highest Wind Readings"));
                if (entity is Vessel)
                {
                    list.Add(new ChartEntry(CrewChart(entity.Sails),"Crew Sails"));
                }
                result.Collection = list;
            }
            return result;
        }

        private static Highcharts CrewChart(IEnumerable<SailBase> entity)
        {
            var chart1 = new Highcharts("CrewSailCount").InitChart(new Chart() { ZoomType = ZoomTypes.X, Type = ChartTypes.Column });

            var firstSail = entity.Min(i => i.Date);
            var lastSail = entity.Max(i => i.Date);
            var crew = entity.SelectMany(i=>i.Crew).Distinct(new CrewBase.Comparer()).ToArray();
            var crewNames = crew.OrderBy(i=>i.FullName()).Select(i => string.Format("{0} cid#{1}", i.FullName(), i.ID)).ToArray();
            var crewCount = crew.OrderBy(i => i.FullName()).Select(i => new object[] { entity.Count(j=>j.Crew.Any(k=>k.ID==i.ID)) }).ToArray();
            chart1.SetTitle(new Title() { Text = String.Format("Muster Book: {0}-{1}", firstSail.ToString("MM/dd/yyyy"), lastSail.ToString("MM/dd/yyyy")) });
            
            chart1.SetXAxis(new XAxis() { Categories = crewNames, Labels = new XAxisLabels() { UseHTML = true, Formatter = "function() {return renderCrewIds(this);}" } });
            chart1.SetYAxis(new YAxis[] { new YAxis() { Title = new YAxisTitle() { Text = "# of Sails" } } })
                //.SetTooltip(
                //    new Tooltip()
                //        {
                //            Enabled = true,
                //            Formatter = @"function(){return+'<b>'+ this.seriesName+'</b>'+this.x+':'+this.y;}"
                //        })
                .SetPlotOptions(
                    new PlotOptions()
                    {
                        Column =
                            new PlotOptionsColumn()
                            {
                                DataLabels =
                                    new PlotOptionsColumnDataLabels()
                                    {
                                        Enabled = true
                                    },
                                Tooltip = new PlotOptionsColumnTooltip() { HeaderFormat = "<span style=\"font-size: 14px\">{series.name}</span><br/>", PointFormat = "<b>{point.y}</b>" },
                                EnableMouseTracking = true
                            }
                    })
                .SetSeries(new[] { new Series() { Name = "# of Sails", Color = Color.Blue, Data = new Data(crewCount) }});
            return chart1;
        }

        private static Highcharts YearOverYearAvgWind(IEnumerable<SailBase> entity)
        {
            var firstSail = entity.Min(i => i.Date);
            var lastSail = entity.Max(i => i.Date);
            var chart1 = new Highcharts("YearOverYearAvgWind").InitChart(new Chart() { ZoomType = ZoomTypes.X, Type = ChartTypes.Column });
            if (entity != null && entity.Any())
            {
                var years = entity.Select(i => i.Created.Date.Year).Distinct();
                var categories = years.Select(i => i.ToString()).ToArray();
                List<object> vals = new List<object>();
                foreach (var year in years)
                {
                    vals.Add(entity.Where(i => i.Created.Date.Year == year).Average(j=>j.MaxWind));
                }

                chart1.SetTitle(new Title() { Text = String.Format("Year over year avg wind: {0}-{1}", firstSail.ToString("MM/dd/yyyy"), lastSail.ToString("MM/dd/yyyy")) });

                chart1.SetXAxis(new XAxis() { Categories = categories, Labels = new XAxisLabels() { } });
                chart1.SetYAxis(new YAxis[] { new YAxis() { Title = new YAxisTitle() { Text = "# of Sails" } } })
                    //.SetTooltip(
                    //    new Tooltip()
                    //        {
                    //            Enabled = true,
                    //            Formatter = @"function(){return+'<b>'+ this.seriesName+'</b>'+this.x+':'+this.y;}"
                    //        })
                    .SetPlotOptions(
                        new PlotOptions()
                        {
                            Column =
                                new PlotOptionsColumn()
                                {
                                    DataLabels =
                                        new PlotOptionsColumnDataLabels()
                                        {
                                            Enabled = true
                                        },
                                    Tooltip = new PlotOptionsColumnTooltip() { HeaderFormat = "<span style=\"font-size: 14px\">{series.name}</span><br/>", PointFormat = "<b>{point.y}</b>" },
                                    EnableMouseTracking = true
                                }
                        })
                    .SetSeries(new[] { new Series() { Name = "Max Wind", Color = Color.Green, Data = new Data(vals.ToArray()) } });

            }
            return chart1;
        }

        private static Highcharts YearOverYearSailCount(IEnumerable<SailBase> entity)
        {
            var firstSail = entity.Min(i => i.Date);
            var lastSail = entity.Max(i => i.Date);
            var chart1 = new Highcharts("YearOverYear").InitChart(new Chart() { ZoomType = ZoomTypes.X, Type = ChartTypes.Column });
            if (entity != null && entity.Any())
            {
                var years = entity.Select(i => i.Created.Date.Year).Distinct();
                var categories = years.Select(i => i.ToString()).ToArray();
                List<object> vals=new List<object>();
                foreach (var year in years)
                {
                    vals.Add(entity.Where(i => i.Created.Date.Year==year).Count());
                }

                chart1.SetTitle(new Title() { Text = String.Format("Year over year sail count: {0}-{1}", firstSail.ToString("MM/dd/yyyy"), lastSail.ToString("MM/dd/yyyy")) });

                chart1.SetXAxis(new XAxis() { Categories = categories, Labels = new XAxisLabels() { } });
                chart1.SetYAxis(new YAxis[] { new YAxis() { Title = new YAxisTitle() { Text = "# of Sails" } } })
                    //.SetTooltip(
                    //    new Tooltip()
                    //        {
                    //            Enabled = true,
                    //            Formatter = @"function(){return+'<b>'+ this.seriesName+'</b>'+this.x+':'+this.y;}"
                    //        })
                    .SetPlotOptions(
                        new PlotOptions()
                        {
                            Column =
                                new PlotOptionsColumn()
                                {
                                    DataLabels =
                                        new PlotOptionsColumnDataLabels()
                                        {
                                            Enabled = true
                                        },
                                    Tooltip = new PlotOptionsColumnTooltip() { HeaderFormat = "<span style=\"font-size: 14px\">{series.name}</span><br/>", PointFormat = "<b>{point.y}</b>" },
                                    EnableMouseTracking = true
                                }
                        })
                    .SetSeries(new[] { new Series() { Name = "# of Sails", Color = Color.Green, Data = new Data(vals.ToArray()) } });
                
            }
            return chart1;
        }

        private static Highcharts YearOverYearMileCount(IEnumerable<SailBase> entity)
        {
            var firstSail = entity.Min(i => i.Date);
            var lastSail = entity.Max(i => i.Date);
            var chart1 = new Highcharts("MilePerYear").InitChart(new Chart() { ZoomType = ZoomTypes.X, Type = ChartTypes.Column });
            if (entity != null && entity.Any())
            {
                var years = entity.Select(i => i.Created.Date.Year).Distinct();
                var categories = years.Select(i => i.ToString()).ToArray();
                List<object> vals = new List<object>();
                foreach (var year in years)
                {
                    vals.Add(entity.Where(i => i.Created.Date.Year == year).Sum(i => i.MilesSailed));
                }

                chart1.SetTitle(new Title() { Text = String.Format("Year over year sail count: {0}-{1}", firstSail.ToString("MM/dd/yyyy"), lastSail.ToString("MM/dd/yyyy")) });

                chart1.SetXAxis(new XAxis() { Categories = categories, Labels = new XAxisLabels() { } });
                chart1.SetYAxis(new YAxis[] { new YAxis() { Title = new YAxisTitle() { Text = "# of Sails" } } })
                    //.SetTooltip(
                    //    new Tooltip()
                    //        {
                    //            Enabled = true,
                    //            Formatter = @"function(){return+'<b>'+ this.seriesName+'</b>'+this.x+':'+this.y;}"
                    //        })
                    .SetPlotOptions(
                        new PlotOptions()
                        {
                            Column =
                                new PlotOptionsColumn()
                                {
                                    DataLabels =
                                        new PlotOptionsColumnDataLabels()
                                        {
                                            Enabled = true
                                        },
                                    Tooltip = new PlotOptionsColumnTooltip() { HeaderFormat = "<span style=\"font-size: 14px\">{series.name}</span><br/>", PointFormat = "<b>{point.y}</b>" },
                                    EnableMouseTracking = true
                                }
                        })
                    .SetSeries(new[] { new Series() { Name = "# of Sails", Color = Color.Green, Data = new Data(vals.ToArray()) } });

            }
            return chart1;
        }

        // private static Highcharts GetMaxWindChart(IEnumerable<SailBase> entity)
        //{
        //    var chart1 = new Highcharts("MilesPerSail").InitChart(new Chart(){ Type = ChartTypes.Pie});
            
        //    var firstSail = entity.Min(i => i.Date);
        //    var lastSail = entity.Max(i => i.Date);
        //     string[] categories = { "0-5", "5-10", "10-15", "15-25", "25-35", "35+!" };
        //     var toFiveData = entity.Count(i => i.MaxWind <= 5);
        //     var toTenData = entity.Count(i => i.MaxWind > 5 && i.MaxWind <= 10);
        //     var toFifteenData = entity.Count(i => i.MaxWind > 10 && i.MaxWind <= 15);
        //     var toTwentyFiveData = entity.Count(i => i.MaxWind > 15 && i.MaxWind <= 25);
        //     var toThirtyFiveeData = entity.Count(i => i.MaxWind > 20 && i.MaxWind <= 35);
        //     var thirtyFivePlusData = entity.Count(i => i.MaxWind > 35);
        //     object[] yAxis =
        //         {
        //             toFiveData, toTenData, toFifteenData, toTwentyFiveData, toThirtyFiveeData,
        //             thirtyFivePlusData
        //         };
        //    chart1.SetTitle(new Title(){Text=String.Format("Max Wind Readings: {0}-{1}",firstSail.ToString("MM/dd/yyyy"),lastSail.ToString("MM/dd/yyyy"))});
        //    chart1.SetXAxis(new XAxis() { Categories = categories, Labels = new XAxisLabels() { }});
        //    chart1.SetYAxis(new YAxis[] { new YAxis() {  Title = new YAxisTitle() { Text = "Highest Wind Reading" }}})
        //        //.SetTooltip(
        //        //    new Tooltip()
        //        //        {
        //        //            Enabled = true,
        //        //            Formatter = @"function(){return+'<b>'+ this.seriesName+'</b>'+this.x+':'+this.y;}"
        //        //        })
        //        .SetPlotOptions(
        //            new PlotOptions()
        //                {
        //                    Pie = 
        //                        new PlotOptionsPie()
        //                            {
        //                                AllowPointSelect = true,
        //                                Cursor = Cursors.Pointer,
        //                                DataLabels =
        //                                    new PlotOptionsPieDataLabels()
        //                                        {
        //                                            Format = "{category}:{y}({point.percentage}%)",
        //                                            Enabled = true
        //                                        },
        //                                Tooltip = new PlotOptionsPieTooltip() { HeaderFormat = "<span style=\"font-size: 14px\">{series.name}</span><br/>", PointFormat = "<b>{point.y}</b>" },
        //                                EnableMouseTracking = true
        //                            }
        //                })
        //                .SetSeries(new Series() {Name="Highest Wind Readings", Data=new Data(new DotNet.Highcharts.Options.Point[] {new DotNet.Highcharts.Options.Point(){Name="0-5", Y = toFiveData}})}};
        //    return chart1;
        //}

        private static Highcharts GetMileChart(IEnumerable<SailBase> entity)
        {
            var chart1 = new Highcharts("MilesPerSail").InitChart(new Chart(){ZoomType = ZoomTypes.X, Type = ChartTypes.Line});
            
            var firstSail = entity.Min(i => i.Date);
            var lastSail = entity.Max(i => i.Date);
            var xDataMonths = entity.OrderBy(i => i.Date).Select(i => string.Format("<b>{0}</b> #{1}", i.Date.ToString("yy-MMM-dd ddd"), i.ID));
            //var xDataMonths = entity.OrderBy(i => i.Date).Select(i => string.Format("{0} <a href=\"{1}\">View</a>", i.Date.ToString("yy-MMM-dd ddd"), i.ID));
            var mileData = entity.OrderBy(i => i.Date).Select(i => new object[] { i.MilesSailed }).ToArray();
            var maxWindData = entity.OrderBy(i => i.Date).Select(i => new object[] { i.MaxWind }).ToArray();
            var minWindData = entity.OrderBy(i => i.Date).Select(i => new object[] { i.MinWind }).ToArray();
            chart1.SetTitle(new Title(){Text=String.Format("Sailing History: {0}-{1}",firstSail.ToString("MM/dd/yyyy"),lastSail.ToString("MM/dd/yyyy"))});
            chart1.SetXAxis(new XAxis() { Categories = xDataMonths.ToArray(), Labels = new XAxisLabels() { UseHTML =true, Formatter = "function() {return renderChartIds(this);}" } });
            chart1.SetYAxis(new YAxis[] { new YAxis() { Title = new YAxisTitle() { Text = "Miles" } }, new YAxis() { Title = new YAxisTitle() { Text = "High Wind" } },new YAxis() { Title = new YAxisTitle() { Text = "Low Wind" } }  })
                //.SetTooltip(
                //    new Tooltip()
                //        {
                //            Enabled = true,
                //            Formatter = @"function(){return+'<b>'+ this.seriesName+'</b>'+this.x+':'+this.y;}"
                //        })
                .SetPlotOptions(
                    new PlotOptions()
                        {
                            Line =
                                new PlotOptionsLine
                                    {
                                        DataLabels =
                                            new PlotOptionsLineDataLabels
                                                {
                                                    Enabled = true
                                                },
                                        Tooltip = new PlotOptionsLineTooltip() { HeaderFormat = "<span style=\"font-size: 14px\">{series.name}</span><br/>", PointFormat = "<b>{point.y}</b>" },
                                        EnableMouseTracking = true
                                    }
                        })
                .SetSeries(new[] { new Series() { Name = "Miles", Color = Color.Green, Data = new Data(mileData) }, new Series() { Name = "High Wind", Color = Color.CornflowerBlue, Data = new Data(maxWindData) }, new Series() { Name = "Low Wind", Color = Color.Gold, Data = new Data(minWindData) } });
            return chart1;
        }
    }

   
}