﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Resources;
using New=SailingSiteModels.New;
namespace SailingSite.Controllers
{
    using System.Net;
    using System.Threading.Tasks;

    using New;

    using SailingSite.App_GlobalResources;
    using SailingSite.Common;
    using SailingSite.Models;
    using SailingSite.Services;
    using SailingSite.ViewModels;

    public class FleetController : BaseController
    {
        public const string Name = "Fleet";
        
        //
        // GET: /Fleet/
        public ActionResult Index()
        {
            var data = Sailservice.GetAllFleets();
            return View(data);
        }

        public ActionResult Get(int id)
        {
            bool editor = false;
            var data = Sailservice.GetFleet(id);

            if (this.CurrentUser != null)
            {
                editor = Sailservice.IsUserFleetCrewOrSecurity(this.CurrentUser.ID, data.ID);
            }

            this.ViewData["editable"] = editor;
            if (data != null)
            {
                return View(data);
            }
            else return this.View("NullObject");
        }

        [HttpPost]
        public async Task<ActionResult> AddVesselToFleet(VesselEntry entry)
        {
            if (this.CurrentUser == null)
            {
                var model = new RedirectModel() { Description = "You must be logged in to add a vessel to a fleet." };
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "You must be logged in to perform that action.");
            }

            if (entry == null || entry.Vessel == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "It appear there was something wrong with the request or no vessel was created.");
            }

            if (!this.Sailservice.IsUserFleetCrewOrSecurity(this.CurrentUser.ID, entry.FleetID)
                || !this.Sailservice.isUserVesselCrewOrSecurity(entry.Vessel.ID, this.CurrentUser.ID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "You must be an admin, or a member of both the vessel and fleet crew to perform this function.");
            }

            this.Sailservice.AddVesselToFleet(entry, entry.FleetID, this.Context);
            var data = await this.Sailservice.GetVesselEntriesForFleet(entry.FleetID);
            return this.PartialView("_VesselEntryList", data);
        }

        public ActionResult AddVesselEntry(int fleetId)
        {
            var model=new VesselEntry();
            model.FleetID = fleetId;
            return this.PartialView("_VesselEntry", model);
        }

        [HttpPut]
        public ActionResult RemoveVesselFromFleet(int vesselID, int fleetID)
        {
            if (this.CurrentUser == null)
            {
                var model = new RedirectModel() { Description = "You must be logged in to add a vessel to a fleet." };
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "You must be logged in to perform that action.");
            }

            if (!this.Sailservice.IsUserFleetCrewOrSecurity(this.CurrentUser.ID, fleetID)
                || !this.Sailservice.isUserVesselCrewOrSecurity(vesselID, this.CurrentUser.ID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "You must be an admin, or a member of both the vessel and fleet crew to perform this function.");
            }

            try
            {
                Sailservice.RemoveVesselFromFleet(vesselID, fleetID, this.Context);
            }
            catch (Exception e)
            {
                HttpStatusCode result= HttpStatusCode.BadRequest;
                if (e is EntityNotFoundException)
                {
                    result=HttpStatusCode.Gone;
                    
                }

                if (e is AccessViolationException)
                {
                    result = HttpStatusCode.Forbidden;
                }

                return new HttpStatusCodeResult(result, e.Message);
            }

            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        [HttpPost]
        public ActionResult Create(New.Fleet entity, string tagList)
        {
            if (this.CurrentUser == null || (this.CurrentUser != null && this.Sailservice.GetCrewSID(this.CurrentUser.ID)<Crew.SIDEnum.PowerUser))
            {
                var model = new RedirectModel() { Description = "You must be a power user or admin to complete this task." };

                return this.View("NoPermissionPage", model);
            }

            if (entity != null)
            {

                if (entity.ID > 0)
                {
                    if (this.Sailservice.IsUserFleetCrewOrSecurity(this.CurrentUser.ID, entity.ID, true))
                    {
                        this.Sailservice.UpdateFleet(entity, this.Context);
                    }

                    else
                    {
                        var model = new RedirectModel() { Description = "You must be a member of the fleet crew or an admin to perform that task." };

                        return this.View("NoPermissionPage", model);
                    }
                }
                else {
                    entity.ID=Sailservice.CreateFleet(entity, this.Context);
                }
                if (!string.IsNullOrEmpty(tagList))
                {
                    Sailservice.AddFleetTags(entity.ID, tagList);
                }
            }

            this.FlagCacheUpdate(StaticTokens.CachingTokens.FleetEnum);
            return RedirectToAction("index");
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int fleetId)
        {
            if (this.CurrentUser == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, SharedStrings.NotAFleetAdmin);
            }

            if (this.Sailservice.IsUserFleetCrewOrSecurity(this.CurrentUser.ID,fleetId,true))
            {
                this.Sailservice.DeleteFleet(fleetId, this.Context);
                this.FlagCacheUpdate(StaticTokens.CachingTokens.FleetEnum);
                this.GetSessionVariables();
                return this.View("FleetList", this.Session[StaticTokens.CachingTokens.FleetEnum]);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, SharedStrings.NotAFleetAdmin);
            }
        }

        public ActionResult VerifyName(string name)
        {
            if (Sailservice.CheckFleetName(name))
            {
                throw new HttpException("Error");
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult AddTags(int id, string tags)
        {
            Sailservice.AddFleetTags(id, tags);
            var result = Sailservice.GetFleetTags(id);
            ViewData["controller"] = "fleet";
            ViewData["entityID"] = id;
            return PartialView("TagList", result);
        }

        public ActionResult Edit(int? id)
        {

            return RedirectToAction("New", new { id = id });

        }

        public ActionResult SearchByLetter(string letter)
        {
            var request = new GeneralSearchRequest();
            if (string.IsNullOrEmpty(letter) || !char.IsLetter(letter[0]))
            {
                this.Search(request);
            }

            request.FirstInitial = letter;
            return this.Search(request);
        }

        public ActionResult Search(GenericQuery query)
        {
            this.Session[StaticTokens.Security.IsAdminToken] = this.CurrentUser.SID >= Crew.SIDEnum.Admin;
            SearchReturnModel<IEnumerable<SailingSiteModels.New.FleetBase>> result = null;
            result = this.Sailservice.SearchFleet(query);
            return View("_FleetSearch", result);
        }

        public ActionResult New(int? id=null)
        {
         
            if (this.CurrentUser != null && this.CurrentUser.SID < Crew.SIDEnum.PowerUser)
            {
                var redirectModel = new RedirectModel() { ActionName = "New", ControllerName = "Fleet" };
                return this.View("_AdminRedirect", redirectModel);
            }
           var entity=new New.Fleet();
            if (id!=null&&id>0) {
                entity = Sailservice.GetFleet((int)id);
            }
            this.GetSessionVariables();

            return this.View(entity);
        }
       // TODO: SHIP MANAGEMENT (move to default)

        public class ActionNames { 
            public const string Index="";
            public const string Edit="Edit";
            public const string Get="Get";

        }
	}
}