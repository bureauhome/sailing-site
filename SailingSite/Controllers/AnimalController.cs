﻿

namespace SailingSite.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.IO;
    using System.Net;
    using System.Web.Mvc;

    using SailingSite.Common;
    using SailingSite.ViewModels;

    using SailingSiteModels.New;

    public class AnimalController : BaseController
    {
        public AnimalController()
        {
            
        }

        public ActionResult Test()
        {
            return View();
        }
        //
        // GET: /Animal/
        public ActionResult Index()
        {
            var model=this.AnimalService.GetTopLevelCategories();
            return View(model);
        }

        public ActionResult GetAllCategories()
        {
            var model=this.AnimalService.GetAllCategories();
            return View(model);
        }

        public ActionResult DeleteCategory(string name)
        {
            string parent = null;
            string error=null;
            try
            {
                parent = this.AnimalService.DeleteCategory(name);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            base.FlagCacheUpdate(StaticTokens.CachingTokens.AnimalEnum);
            var model=this.AnimalService.GetCategory(parent);
            return PartialView("CategoryList", model);
        }

        public ActionResult DeleteAnimal(int id)
        {
            if (CurrentUser != null && CurrentUser.SID >= Crew.SIDEnum.PowerUser)
            {
                LocationService.DeleteLandMark(id, CurrentUser.ID);
                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }
            string error = null;
            string parent = null;
            try
            {
                parent = this.AnimalService.DeleteAnimal(id);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            base.FlagCacheUpdate(StaticTokens.CachingTokens.AnimalEnum);
            return RedirectToAction("GetCategory", new { name = parent});
        }

        public ActionResult SearchByLetter(string letter)
        {
            var request = new GeneralSearchRequest();
            if (string.IsNullOrEmpty(letter) || !char.IsLetter(letter[0]))
            {
                this.AnimalService.Search(request);
            }
            request.FirstInitial = letter;
            var data = this.AnimalService.Search(request);
            return this.View("_ZooSearch", data);

        }

        public ActionResult GetCategory(string name)
        {
            if (name == null)
            {
               var first=this.AnimalService.GetAllCategories();
               if (first != null&&first.Any())
               {
                   name = first.First().Name;
               }
            }
            var categories = this.AnimalService.GetSiblingCategories(name);
            if (categories != null)
            {
                TempData["SisterCategories"] = categories;
            }
            var model = this.AnimalService.GetCategory(name);
            return View(model);
        }

        public ActionResult GetAnimal(int id=1)
        {
           
            if (Session["Tags"] == null)
            {
                var strings=this.TagService.GetAllTagsByString();
                Session["Tags"] = strings;
            }
            Session["Crew"] = true;
            var Animal = this.AnimalService.GetDetailedAnimal(id);
            //TODO: Merge animal and GetAnimal
            return View(Animal);
        
        }

        public ActionResult DeleteTag(int id, int entityId)
        {
            this.AnimalService.DeleteAnimalTag(id, entityId);
            var result = this.AnimalService.GetAnimalTags(entityId);
            return PartialView("TagList", result);
        }

        public void CreateCategory(string name, string category)
        {
            if (string.IsNullOrEmpty(category)||category.ToLower()=="null")
            {
                category = null;
            }
            var entity = new SailingSiteModels.New.AnimalCategory();
            entity.Name = name;
            entity.ParentName = category;

            this.AnimalService.CreateCategory(entity);
            RedirectToAction("GetCategory", new { name = category });
        }

        public ActionResult Create(Animal entity, string tagList, HttpPostedFileBase file)
        {
            if (this.CurrentUser == null || (this.CurrentUser != null && this.CurrentUser.SID < Crew.SIDEnum.PowerUser))
            {
                var model = new RedirectModel() { Description = "You must be an admin to complete this task." };

                return this.View("NoPermissionPage", model);
            }
            Photo photo=null;
            if (file != null)
                {
                    photo = new SailingSiteModels.New.Photo() { Name = entity.Name, FileName = file.FileName, EntityName = entity.Name, Subdirectory = @Resources.SessionVariables.ImageFolderCrew };
                    photo.ID = this.PhotoService.SavePhoto(file, photo, this.Context, Server.MapPath("~/imagedir"));
                    entity.Photo = photo;
                }

            if (entity.ID > 0)
            {
                AnimalService.UpdateAnimal(entity, this.Context);
            }
            else
            {
                AnimalService.CreateAnimal(entity, this.Context);
            } 

            base.FlagCacheUpdate(StaticTokens.CachingTokens.AnimalEnum);
            return RedirectToAction("GetCategory", new { name = entity.Category });
        }

        public ActionResult UpdateCategory(string name)
        {
            var Names = this.AnimalService.GetAllCategories();
            var category = this.AnimalService.GetCategory(name);
            TempData["AnimalCategories"] = Names.ToList();
            return View(category);
        }

        public ActionResult Patch(SailingSiteModels.New.Animal entity, string aliases, HttpPostedFileBase file)
        {
            string error=null;
            var animal = new SailingSiteModels.New.Animal(entity, aliases);
            if (file != null)
            {
                var photo = new SailingSiteModels.New.Photo() { Name = animal.Name, FileName = file.FileName, EntityName = animal.Name, Subdirectory = Resources.SessionVariables.ImageFolderAnimal };
                photo.ID = this.PhotoService.SavePhoto(file, photo, this.Context, Server.MapPath("~/imagedir"));
                animal.Photo = photo;
            }
            try
            {
                this.AnimalService.UpdateAnimal(animal, this.Context);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            return RedirectToAction("Update", new { id=entity.ID.ToString(), error=error });
        }

        public ActionResult AddComment(int entityId, SailingSiteModels.New.Comment comment)
        {
            if (comment.ID > 0)
            {
                this.CommentService.Update(comment);
            }
            else
            {
                comment.ID = this.CommentService.Create(comment);
            }
            this.AnimalService.AddAnimalComment(comment, entityId);
            var data = AnimalService.GetAnimalComments(entityId);
            ViewData["controller"] = "animal";
            ViewData["entityID"] = entityId;
            return PartialView(data);
        }

        public ActionResult AddTags(int id, string tags)
        {
            this.AnimalService.AddTags(id,tags);
            var result = this.AnimalService.GetAnimalTags(id).ToArray();
            ViewData["controller"] = "animal";
            ViewData["entityID"] = id;
            return PartialView("TagList", result);
        }

        public ActionResult PatchCategory(SailingSiteModels.New.AnimalCategory entity)
        {

            string parent = null;
            try
            {
                parent = this.AnimalService.UpdateCategory(entity, this.Context);
            }
            catch (Exception e)
            {
                var error = e.Message;
                return RedirectToAction("UpdateCategory", new { name = entity.Name});
            }
            if (parent == null)
            {
                return RedirectToAction("GetAllCategories");
            }
            else return RedirectToAction("GetCategory", new { name = parent });
        }


        public ActionResult New(string category=null, string name=null, string keywords=null, string description=null, string error=null)
        {
            if (this.CurrentUser != null || (this.CurrentUser != null && this.CurrentUser.SID < Crew.SIDEnum.Admin))
            {
                var model = new RedirectModel() { Description = "You must be an admin to complete this task." };

                return this.View("NoPermissionPage", model);
            }

            this.GetSessionVariables();
            return View();

        }

        public ActionResult GetAnimalsForCategory(string category)
        {
            var result = this.AnimalService.GetCategory(category);
            return PartialView("AnimalList");
        }

        public ActionResult NewCategory(string category = null, string name = null, string error = null)
        {
            var Names = this.AnimalService.GetAllCategories();
            Session["AnimalCategories"] = Names.ToList();
            if (!string.IsNullOrEmpty(error))
            {
                TempData["Error"] = error;
            }

            if (!string.IsNullOrEmpty(category))
            {
                TempData["Category"] = category;
            }
            if (!string.IsNullOrEmpty(name))
            {
                TempData["Name"] = name;
            }
            return View();

        }
	}
}