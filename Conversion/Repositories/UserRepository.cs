﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Old=SailingSiteModels.Old;
using New=SailingSiteModels.New;
using OldDataConnection;

namespace Conversion.Repositories
{
    class CrewRepository: IRepository<New.Crew>
    {
        private static Dictionary<Old.Crew, New.Crew> Crew;
        private Factories.CrewFactory CrewFactory;
        // Loads a master dictionary of crews
        public CrewRepository()
        {
            Console.WriteLine("Processing old crew");
            CrewFactory = new Factories.CrewFactory();
            var oldCrew=OldSQL.SQLStuff.LoadSailCrew();
            foreach (var crew in oldCrew.Distinct())
            {
                Console.WriteLine("Processing old crew: {0}", crew);
                var old = new Old.Crew()
                {
                    Name = crew
                };

                var newCrew = Factories.CrewFactory.CreateCrew(old);
               
                var count = Crew.Where(i => i.Key.Name == old.Name && i.Value.ID == newCrew.ID).Count();
                if (count < 1)
                {
                    Crew.Add(old, newCrew);
                    Console.WriteLine("Tying: {0} to {1}", old, newCrew.UserName);
                }
                else Console.WriteLine("{0} : {1} already detected in database.", old, newCrew.UserName);

            }
        }

        public static IEnumerable<New.Crew> Get(IEnumerable<string> ids)
    {
        return Crew.Where(i => ids.Contains(i.Key.Name)).Select(i=>i.Value);
    
    }



        public New.Crew get(object id)
        {
            if (id is int)
            {
                int guid;
                int.TryParse(id.ToString(), out guid);
                var retVal = Crew.Values.Where(i => i.ID == guid);
                return retVal.FirstOrDefault();
            }
            else return null;
        }

        public IEnumerable<New.Crew> GetAll(object id)
        {
            return Crew.Values;
        }

        public New.Crew Get(Old.Crew key)
        {
            var retPair = Crew.Where(i => i.Key.Name == key.Name).FirstOrDefault();
            return retPair.Value;
        }

        public int Insert(New.Crew entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(New.Crew entity)
        {
            throw new NotImplementedException();
        }

        public void Update(New.Crew entity)
        {
            throw new NotImplementedException();
        }
    }
}
