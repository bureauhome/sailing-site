﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Services
{
    /// <summary>
    /// The meta data service.
    /// </summary>
    public class MetaDataService : ServiceBase, IMetadataService
    {
        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public string Get(string key)
        {
            return this.MetadataRepository.Get(key);
        }

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Process(Models.MetaData entity)
        {
            if (entity == null || entity.Key == null)
            {
                return;
            }

            if (this.MetadataRepository.Get(entity.Key) == null)
            {
                this.MetadataRepository.Insert(entity);
            }
            else
            {
                this.MetadataRepository.Update(entity);
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        public void Delete(string key)
        {
            if (key != null)
            {
                this.MetadataRepository.Delete(key);
            }
        }
    }
}