﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Exceptions
{
    public class PasswordDoesNotMeetRequirementsException : Exception
    {
        public PasswordDoesNotMeetRequirementsException() :
            base("Password does not meet minimal requirements.  Password must be 7-50 characters in length and have an upper, lower, and number.")
        { }

    }
}
