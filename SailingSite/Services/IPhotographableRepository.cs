﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSite.Services
{
    interface IPhotographableService
    {
        void AddPhoto(int photoID, int entityId);
        void DeletePhoto(int photoID, int entityId);
    }
}
