﻿namespace SailingSiteModels.Common
{
    using System;
    using System.Dynamic;

    using SailingSiteModels.New;

    public interface INewsFeedable : IEntityModel, IUserCreatable
    {
        DateTime Created { get; set; }
        PhotoBase RandomlyChosenPhoto { get; set; }
        string BlurbPartial { get;  }
        string Blurb { get; }
        int Order { get; set; }
    }
}