﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.Net;
    using System.Web.Security;
    using Microsoft.Owin.Security.Cookies;

    using Resources;

    using SailingSite.App_GlobalResources;
    using SailingSite.Common;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    public class BadLoginException : Exception
    {
        public BadLoginException()
    {

    }

    public BadLoginException(string message)
        : base(message)
    {
    }

    public BadLoginException(string message, Exception inner)
        : base(message, inner)
    {
    }
    }

    public class AutoLogOutException : Exception
    {
         public AutoLogOutException()
    {

    }

    public AutoLogOutException(string message)
        : base(message)
    {
    }

    public AutoLogOutException(string message, Exception inner)
        : base(message, inner)
    {
    }
    }

    public class LoginController : BaseController
    {
        //
        // GET: /Cookie/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadSessionUser()
        {
            var tokenUser = Session[StaticTokens.CachingTokens.UserSessionResources] as CrewSecurity;
            var tokenTime = Session[StaticTokens.Security.UserToken] as DateTime?;
            if (tokenUser == null
                || tokenTime == null
                    || tokenTime
                    > DateTime.Now.AddMinutes(-5))
            {
                var cookie = Request.Cookies[SharedStrings.UserCookie];
                DateTime expiry = DateTime.MinValue;
                if (cookie != null)
                {
                    DateTime.TryParse(cookie[SharedStrings.CookieExpiresToken], out expiry);
                }

                if (cookie != null && cookie[SharedStrings.TokenCookie] != null && expiry > DateTime.Now)
                {
                    var token = Request.Cookies[SharedStrings.UserCookie][SharedStrings.TokenCookie];
                    var user = this.CrewService.VerifyToken(token);
                    cookie[SharedStrings.CookieExpiresToken] = DateTime.Now.AddMinutes(120).ToString();
                    this.SetSessionUser(user);
                }
                else if (User.Identity.IsAuthenticated && (cookie == null || cookie.Expires < DateTime.Now))
                {
                    this.SetSessionUser(null);

                }
            }
            else
            {
                this.SetSessionUser(null);
            }
            return new EmptyResult();
        }
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            //TODO move to srvice
            CrewSecurity entity = null;
            try
            {
                entity = this.CrewService.Login(username, password, this.Context);
            }
            catch (EntityNotFoundException ex)
            {
                this.LoggingService.GenerateLog(
                    "Bad login attempt",
                    Log.LogTypes.AuditLoginError,
                    this.Context,
                    username);
                return new HttpStatusCodeResult(HttpStatusCode.Conflict, "Invalid password.");
            }
            catch (UserLockedException ex)
            {
                this.LoggingService.GenerateLog(
                    "Your account is locked",
                    Log.LogTypes.AuditLockOut,
                    this.Context,
                    username);
                return new HttpStatusCodeResult(HttpStatusCode.Conflict, "This account has been locked.");
            }
            if (entity != null && entity.Token != null)
            {
                this.LoggingService.GenerateLog("Login attempt", Log.LogTypes.AuditLogin, this.Context, username);
                this.AuthenticationFacade.SetAuthCookie(username);
                //FormsAuthentication.SetAuthCookie(username, true);
                HttpCookie cookie = new HttpCookie(SharedStrings.UserCookie);
                cookie[SharedStrings.TokenCookie] = entity.Token;
                DateTime expiration = DateTime.Now.AddMinutes(120);
                cookie[SharedStrings.CookieExpiresToken] = expiration.ToString();
                // Here we set the active user in the auditcontext.
                this.Context.ActiveCrew = entity;
                this.Response.Cookies.Add(cookie);
                this.SetSessionUser(entity);
                this.LoadUserSpecificSessionVariables();
            }
            else
            {
                //TODO: Fix
                this.LoggingService.GenerateLog(
                    "Bad login attempt",
                    Log.LogTypes.AuditLoginError,
                    this.Context,
                    username);
                return new HttpStatusCodeResult(HttpStatusCode.Conflict,"Invalid password.");
            }

            return this.Json(entity);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            this.LoggingService.GenerateLog("User logged out", Log.LogTypes.AuditLogout, this.Context, this.CurrentUser);
            HttpCookie cookie = new HttpCookie(SharedStrings.UserCookie);
            cookie.Expires = DateTime.Now.AddDays(-1);
            this.SetSessionUser(null);
            this.Context.ActiveCrew = null;
            return new EmptyResult();
        }

        private void SetSessionUser(SailingSiteModels.New.CrewSecurity entity)
        {
            Session[StaticTokens.CachingTokens.UserSessionResources] = entity;
            this.Context.ActiveCrew = entity;
            if (entity!=null && entity.SID == Crew.SIDEnum.Admin)
            {
                this.Session[StaticTokens.Security.IsAdminToken] = true;
            }
            else
            {
                this.Session[StaticTokens.Security.IsAdminToken] = false;
            }
            if (entity != null)
            {
                Session[StaticTokens.Security.UserToken] = DateTime.Now;
            }
        }

    }
}