﻿CREATE TABLE [dbo].[Entities.Maneuvers] (
    [ManeuverID]  INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (255) NULL,
    CONSTRAINT [pk_ManeuverID] PRIMARY KEY CLUSTERED,
    CONSTRAINT [pk_ManeuverID] PRIMARY KEY CLUSTERED ([ManeuverID] ASC)
);

