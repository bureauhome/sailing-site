﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models
{
    public class VesselSearch : GenericQuery
    {
        public string Name { get; set; }
        public int? fleet { get; set; }
    }
}