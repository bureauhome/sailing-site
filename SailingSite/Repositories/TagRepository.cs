﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using New = SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using Microsoft.Ajax.Utilities;

    using SailingSite.Common;

    public class TagRequest
    {
        public string Name { get; set; }
        public int? ID { get; set; }
    }
    public class TagRepository : ITagRepository
    {
        

        public IEnumerable<string> GetAllTagsByString()
        {
            string[] results = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Tags select i.Name).ToArray();
                if (query != null && query.Any())
                {
                    results = query;
                }
                return results;
            }

        }

        public int? SaveTag(New.Tags entity, Entities_Tags tagResult=null)
        {
            var id = uniqueOrId(entity.Name);
            if (entity != null && entity.Name != null&&id==0)
            {
                using (var context = new SailingEntities())
                {
                    tagResult = (from i in context.Entities_Tags where i.Name == entity.Name select i).FirstOrDefault();
                    if (tagResult != null && !string.IsNullOrEmpty(tagResult.Name))
                    {
                        return tagResult.TagID;
                    }
                    tagResult = Factories.CommonFactory.CreateTag(entity);
                    tagResult.Entities_Crew = (from i in context.Entities_Crew where i.CrewID == tagResult.CrewID select i).FirstOrDefault();

                    try
                    {
                        context.Entities_Tags.Add(tagResult);
                        context.SaveChanges();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        bool i = true;
                    }

                    int? result = null;
                    if (tagResult.TagID > 0)
                    {
                        result = tagResult.TagID;
                    }
                    return result;
                }
            }
            return id;
        }

        /// <summary>
        /// The is unique.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private int uniqueOrId(string name)
        {
            using (var context = new SailingEntities())
            {
                var newString = name.StripStringforComparison();
                var test =
                    context.Entities_Tags.FirstOrDefault(i => i.Name.Replace(" ", string.Empty).Replace("-", String.Empty).Replace("(", String.Empty).Replace(")", string.Empty).Replace(".", String.Empty).Replace(",", String.Empty).Replace("&", string.Empty).Replace("*", String.Empty).Replace("$", String.Empty).Replace("@", String.Empty).Replace("~", string.Empty).Equals(newString, StringComparison.CurrentCultureIgnoreCase));
                var result = test != null && test.TagID > 0 ? test.TagID : 0;
                return result;
            }

        }



        public IEnumerable<New.Tags> GetAllTags()
        {
            New.Tags[] results = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Tags select i).ToArray();
                if (query != null && query.Any())
                {
                    results = query.Select(Factories.CommonFactory.CreateTag).ToArray();
                }
            }
            return results;
        }

        public New.TagHydrated GetHydratedTag(int id)
        {
            New.TagHydrated result = null;
            using (var context = new SailingEntities())
            { 
                var query=from i in context.Entities_Tags.Include(j=>j.Entities_Crew).Include(j=>j.Entities_Crew1.Where(l=>l.DateDeleted==null)).Include(j=>j.Entities_Comment.Select(k=>k.Entities_Crew1)).Include(j=>j.Entities_Photos.Where(k=>k.DeleteDate==null)).Include(j=>j.Entities_Animals_Names.Where(k=>k.DateToDelete==null)).Include(j=>j.Entities_Comment).Include(j=>j.Entities_Crew1.Where(k=>k.DateDeleted==null)).Include(j=>j.Entities_Fleet.Where(k=>k.DateDeleted==null)).Include(j=>j.Entities_Locations).Include(j=>j.Entities_Vessel).Include(j=>j.Entities_Photos.Select(k=>k.Entities_Photos_Details)
                              ) where i.TagID==id select i;
                var dbentity=query.FirstOrDefault();
                if (dbentity != null)
                {
                    result = Factories.CommonFactory.CreateHydratedTag(dbentity);
                }

            }
            return result;

        }


        public void DeleteTag(int id)
        {
            using (var context = new SailingEntities())
            {
                var entity = context.Entities_Tags.Where(i => i.TagID == id).FirstOrDefault();
                if (entity != null)
                {
                    entity.Entities_Animals_Names.Clear();
                    entity.Entities_Comment.Clear();
                    entity.Entities_Fleet.Clear();
                    entity.Entities_Locations.Clear();
                    entity.Entities_Photos.Clear();
                    entity.Entities_Vessel.Clear();
                    entity.Logs_Sails.Clear();
                    context.Entities_Tags.Remove(entity);
                }


            }
        }

        public New.Tags GetTag(TagRequest request)
        {
            New.Tags result = null;
            using (var context = new SailingEntities())
            {
                var query = from i in context.Entities_Tags select i;
                if (request.ID != null && request.ID != 0)
                {
                    query = query.Where(i => i.TagID == (int)request.ID);
                }
                if (request.Name != null)
                {
                    query = query.Where(i => i.Name == request.Name);
                }
                var returned = query.FirstOrDefault();
                if (returned != null)
                {
                    result = Factories.CommonFactory.CreateTag(returned);
                }
                return result;
            }
        }
    }
}