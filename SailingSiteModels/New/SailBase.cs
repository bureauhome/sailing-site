﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SailBase.cs" company="">
//   
// </copyright>
// <summary>
//   The sail base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Device.Location;
    using System.Dynamic;

    using SailingSiteModels.Common;

    /// <summary>
    /// The sail base.
    /// </summary>
    public class SailBase : IEntityModel, ISingleLocationEntity, IMetadata
    {



        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return "Sail";
            }
        }

        /// <summary>
        /// Gets or sets the public key.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The averaged wind direction.
        /// </summary>
        public decimal WindDirection { get; set; }

        /// <summary>
        /// The date of the sail.  Can include both a time and a date.
        /// </summary>
        [Required(ErrorMessage="Please add a date."), DataType(DataType.DateTime, ErrorMessage = "Please input a valid Date.")]
        public DateTime Date { get; set; }

        /// <summary>
        /// The location of the sail.
        /// </summary>
        [Required(ErrorMessage="Please add a location.")]
        public LocationBase Location { get; set; }

        /// <summary>
        /// The sailing vessel.
        /// </summary>
        [Required(ErrorMessage="Please select a vessel.")]
        public VesselBase Vessel { get; set; }

        /// <summary>
        /// The total number of miles sailed.
        /// </summary>
        [Range(0, 1800, ErrorMessage = "Please input a valid number for miles sailed.")]
        public decimal MilesSailed { get; set; }

        /// <summary>
        /// The total number of hours sailed.
        /// </summary>
        [Range(0, 24, ErrorMessage = "Hours sailed must be between 0 and 24.")]
        public decimal HoursSailed { get; set; }

        /// <summary>
        /// A list of the crew.
        /// </summary>
        public IEnumerable<CrewFoundation> Crew { get; set; }

        /// <summary>
        /// The average barometer reading
        /// </summary>
        [Range(0, 33, ErrorMessage = "Please enter a valid barometric pressure reading (in inches hygrometer.)")]
        public decimal Barometer { get; set; }

        /// <summary>
        /// The highest tilt.
        /// </summary>
        [Range(0, 360, ErrorMessage = "Centerboard % Must Be Between 0 and 360.")]
        public decimal? Tilt { get; set; }

        /// <summary>
        /// The percent to which a centerboard, daggerboard, or swing keel is dropped.
        /// </summary>
        [Range(0, 100, ErrorMessage = "Centerboard % Must Be Between 0 and 100.")]
        public decimal? Centerboard { get; set; }

        /// <summary>
        /// Average temperature in Farenheit.
        /// </summary>
        [Range(-25, 180, ErrorMessage = "Please input a valid temperature.")]
        public decimal TempF { get; set; }

        /// <summary>
        /// Minimum wind recorded.
        /// </summary>
        [Range(0, 180, ErrorMessage = "Please input a valid minimum wind.")]
        public decimal MinWind { get; set; }

        /// <summary>
        /// The maximum wind recorded.
        /// </summary>
        [Range(0, 180, ErrorMessage = "Please input a valid maximum wind.")]
        public decimal MaxWind { get; set; }

        public string GeneralObservations { get; set; }

            /// <summary>
        /// The maximum wind recorded.
        /// </summary>
        [Range(0, 9, ErrorMessage = "Please input a valid number for sea state (1-9).")]
        public int? SeaState { get; set; }
        // TODO: TOOL TIPS FOR: sea state, tilt, centerboard %, make centerboard dependent on presence of swinging keel or CB

        /// <summary>
        /// The to sail.
        /// </summary>
        /// <returns>
        /// The <see cref="Sail"/>.
        /// </returns>
        public Sail ToSail()
        {
            Sail result = new Sail
            {
                ID = this.ID, 
                Barometer = this.Barometer, 
                Centerboard = this.Centerboard, 
                Date = this.Date, 
                HoursSailed = this.HoursSailed, 
                Location = this.Location, 
                MilesSailed = this.MilesSailed, 
                Crew = this.Crew, 
                Vessel = this.Vessel,
                SeaState=this.SeaState,
                Tilt = this.Tilt,
                TempF = this.TempF,
                GeneralObservations = this.GeneralObservations,
                MaxWind=this.MaxWind,
                WindDirection = this.WindDirection,
                MinWind=this.MinWind,
                Created=this.Created,
                RainSeverity = this.RainSeverity,
                CreatedBy = this.CreatedBy,
            };
            return result;
        }

        private int? rainSeverity;

        public int? RainSeverity
        {
            get
            {
                return this.rainSeverity;
            }
            set
            {
                if (value > 10)
                {
                    this.rainSeverity = 10;
                }
                else if (value < 0)
                {
                    this.rainSeverity = null;
                }
                else
                {
                    this.rainSeverity = value;
                }
            }
        }

        public DateTime Created { get; set; }

        public CrewFoundation CreatedBy { get; set; }

        public int CreatedbyID {
            get
            {
                if (this.CreatedBy == null)
                {
                    return 0;
                }
                return this.CreatedBy.ID;
            }
            set
            {
                this.CreatedBy = new CrewFoundation() { ID = value };
            }
        }

        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
    }
}
