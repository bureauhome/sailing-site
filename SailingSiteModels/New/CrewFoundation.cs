﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SailingSiteModels.New
{
    public class CrewFoundation : Common.IEntityModel
    {
        public CrewFoundation()
        { }
        public int ID { get; set; }
        [Required(ErrorMessage = "First Name is required"), StringLengthAttribute(50, ErrorMessage = "First name must be 50 or fewer characters")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required"), StringLength(50, ErrorMessage = "Last name must be 50 or fewer characters")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Username is required"), StringLength(25, ErrorMessage = "Username must be 25 or fewer characters")]
        public string UserName { get; set; }

        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public bool IsAssociated { get; set; }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
        public int ModifiedByID { get; set; }
        public string ControllerName
        {
            get
            {
                return "Crew";
            }
        }
        
        public string FullName(bool lastfirst = false, string lfDelimeter=",")
        {
            if (!lastfirst) return FirstName + " " + LastName;
            else
            {
                return LastName + lfDelimeter + FirstName;
            }
        }

        public CrewBase ToCrewBase()
        {
            return new CrewBase
            {
                ID=this.ID,
                FirstName = this.FirstName,
                LastName = this.LastName,
                UserName = this.UserName
            };
        }


        public CrewSecurity ToCrewSecurity()
        {
            return new CrewSecurity { FirstName = this.FirstName, ID=this.ID, LastName = this.LastName, UserName = this.UserName };
        }

        public bool IsTestData { get; set; }

        public class Comparer : IEqualityComparer<CrewFoundation>
        {

            public bool Equals(CrewFoundation x, CrewFoundation y)
            {
                return x.ID == y.ID;
            }

            public int GetHashCode(CrewFoundation obj)
            {
                return obj.ID.GetHashCode();
            }
        }
    }
}
