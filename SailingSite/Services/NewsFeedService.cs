﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Services
{
    using SailingSite.Common;
    using SailingSite.Models.FeedModels;

    using SailingSiteModels.Common;
    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    using Sail = SailingSiteModels.Old.Sail;

    public class NewsFeedService : ServiceBase, INewsFeedService
    {
        private FeedSearch anniversarySearch(CrewSecurity crew)
        {
            if (crew != null)
            {
                return new FeedSearch() { AnniversaryDate = DateTime.Today, Priority = 0, CrewIDs = new int[] {crew.ID}};
            }
            return null;
        }

        private FeedSearch crewSearch(CrewSecurity crew)
        {
            if (crew != null)
            {
                return new FeedSearch() { CrewIDs = new int[] { crew.ID }, Priority = 2, NumberToGrab = 10, DaysBack = 200};
            }
            return null;
        }

        private FeedSearch vesselCaptainSearch(CrewSecurity crew)
        {
            if (crew != null&&crew.VesselsCaptained!=null&&crew.VesselsCaptained.Any())
            {
                var vesselIds = crew.VesselsCaptained.Select(i => i.ID);
                return new FeedSearch() { VesselIDs = vesselIds, Priority = 1, NumberToGrab = 10, DaysBack = 200};
            }
            return null;
        }

        private FeedSearch vesselCrewHistorySearch(CrewSecurity crew)
        {
            if (crew != null&&crew.VesselsCaptained!=null&&crew.VesselsCaptained.Any())
            {
                var vesselIds = crew.VesselsCaptained.Select(i => i.ID);
                return new FeedSearch() { VesselIDs = vesselIds, Priority=3, NumberToGrab = 10, DaysBack = 200};
            }
            return null;
        }

        private FeedSearch personalCommentsSearch(CrewSecurity crew)
        {
            if (crew != null && crew.VesselsCaptained!=null)
            {
                var vesselIds = crew.VesselsCaptained.Select(i => i.ID);
                return new FeedSearch() { VesselIDs = vesselIds, Priority=1, NumberToGrab = 20, DaysBack = 200};
            }
            return null;
        }

        private IEnumerable<INewsFeedable> GetCommentFeed(CrewSecurity crew, IEnumerable<int> sailIds)
        {
            IEnumerable<INewsFeedable> result = new List<INewsFeedable>();;
            var anniversaryQuery = this.anniversarySearch(crew);
            var vesselCrewQuery = this.vesselCrewHistorySearch(crew);
            var commentsAboutMe = personalCommentsSearch(crew);
            if (sailIds != null && sailIds.Any())
            {
                anniversaryQuery.FilterSearch = vesselCrewQuery.FilterSearch = commentsAboutMe.FilterSearch = sailIds;
            }
            var feedList = new List<INewsFeedable>();
            var anniv = this.CommentRepository.GetFeedItems(anniversaryQuery);
            if (anniv != null && anniv.Any())
            {
                feedList.AddRange(anniv);
            }
            var commentsAboutMeFeed = this.CommentRepository.GetFeedItems(commentsAboutMe);
            if (commentsAboutMeFeed != null && commentsAboutMeFeed.Any())
            {
                feedList.AddRange(commentsAboutMeFeed);
            }
            var vesselFeed = this.CommentRepository.GetFeedItems(vesselCrewQuery);
            if (vesselFeed != null && vesselFeed.Any())
            {
                feedList.AddRange(vesselFeed);
            }
            if (feedList != null && feedList.Any())
            {
                result = feedList.Distinct();
            }
            return result;
        }

        private IEnumerable<INewsFeedable> GetSailFeed(CrewSecurity crew, AuditContext context)
        {

            IEnumerable<INewsFeedable> result = new List<INewsFeedable>();
            this.LoggingService.GenerateLog("Generating anniversary query.", Log.LogTypes.Audit, context);
            var anniversaryQuery = this.anniversarySearch(crew);
            this.LoggingService.GenerateLog("Generating crew query.", Log.LogTypes.Audit, context);
            var crewQuery = this.crewSearch(crew);
            this.LoggingService.GenerateLog("Generating captain query.", Log.LogTypes.Audit, context);
            var vesselCaptainQuery = this.vesselCaptainSearch(crew);
            this.LoggingService.GenerateLog("Generating crew history query.", Log.LogTypes.Audit, context);
            var vesselCrewQuery = this.vesselCrewHistorySearch(crew);

            var feedList = new List<INewsFeedable>();
            
            if (anniversaryQuery!=null)
            { 
                this.LoggingService.GenerateLog("Generating anniversary feed.", Log.LogTypes.Audit, context);
                var feed = this.SailRepository.GetFeedItems(anniversaryQuery);
                if (feed != null && feed.Any())
                {
                    this.LoggingService.GenerateLog(string.Format("anniversary feed populated {0} items.",feed.Count()), Log.LogTypes.Audit, context);
                    feedList.AddRange(feed);
                }
            }

            if (crewQuery != null)
            {
                this.LoggingService.GenerateLog("Generating crew feed.", Log.LogTypes.Audit, context);
                var feed = this.SailRepository.GetFeedItems(crewQuery);
                if (feed != null && feed.Any())
                {
                    this.LoggingService.GenerateLog(string.Format("crew feed populated {0} items.", feed.Count()), Log.LogTypes.Audit, context);
                    feedList.AddRange(feed);
                }
            }

            if (vesselCaptainQuery != null)
            {
                this.LoggingService.GenerateLog("Generating captain feed.", Log.LogTypes.Audit, context);
                var feed = this.SailRepository.GetFeedItems(vesselCaptainQuery);
                if (feed != null && feed.Any())
                {
                    this.LoggingService.GenerateLog(string.Format("vessel captain feed populated {0} items.", feed.Count()), Log.LogTypes.Audit, context);
                    feedList.AddRange(feed);
                }
            }
            if (vesselCrewQuery != null)
            {
                this.LoggingService.GenerateLog("Generating vessel crew feed.", Log.LogTypes.Audit, context);
                var feed = this.SailRepository.GetFeedItems(vesselCrewQuery);
                if (feed != null && feed.Any())
                {
                    this.LoggingService.GenerateLog(string.Format("vessel vessel crew feed populated {0} items.", feed.Count()), Log.LogTypes.Audit, context);
                    feedList.AddRange(feed);
                }
            }
            if (feedList != null && feedList.Any())
            {
                result = feedList.Distinct();
            }
            return result;
        }

        
    public NewsFeed GetNewsFeed(CrewSecurity crew, AuditContext context)
    {
        NewsFeed result = new NewsFeed();
        if (crew == null)
        {
            return result;
        }
        var sailFeed = this.GetSailFeed(crew, context);
        IEnumerable<int> sailIds = null;
        if (sailFeed != null && sailFeed.Any())
        {
            sailIds = sailFeed.Select(j => j.ID);
            this.LoggingService.GenerateLog(string.Format("found {0} feed IDS. Loading from database.", sailIds.Count()), Log.LogTypes.Audit, context);

            var commentFeed = this.GetCommentFeed(crew, sailIds);
            this.LoggingService.GenerateLog(string.Format("{0} items loaded.", commentFeed.Count()), Log.LogTypes.Audit, context);

            result.Feed = sailFeed.Concat(commentFeed);
        }
        
        
        return result;
    }
}
    }