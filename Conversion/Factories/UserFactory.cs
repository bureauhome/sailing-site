﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using New=SailingSiteModels.New;
using Old=SailingSiteModels.Old;
using OldDataConnection;

namespace Conversion.Factories
{
    public class CrewFactory
    {

        public static New.Crew CreateCrew(Old.Crew oldCrew)
         {
             var crew = new New.Crew(oldCrew);
             int id = OldSQL.SQLStuff.AddNewCrew(crew);
             crew.SetID(id);
             switch (id)
             {
                 case -4:
                     Console.WriteLine("Duplicate detected.");
                     break;
                 case -1:
                     Console.WriteLine("Error occured.");
                     throw new Exception("SQL ERROR OCCURED.  EXITING.");
                     break;
                 default:
                     Console.WriteLine("Converted into new crew member First: {0}, Last:{1}, ID:{2}, Username:{3}, InitialPass: {4}", crew.FirstName, crew.LastName
                 , crew.ID, crew.UserName, crew.GetFirstPass());
                     break;
             }
             
             return crew;
         }

       
    }
}
