﻿CREATE TABLE [dbo].[Logs.Sails] (
    [SailID]         INT            IDENTITY (1, 1) NOT NULL,
    [Date]           DATETIME       NOT NULL,
    [VesselID]       INT            NOT NULL,
    [LocationID]     INT            NOT NULL,
    [TempF]          DECIMAL (10, 2) NOT NULL,
    [Barometer]      DECIMAL (10, 3) NOT NULL,
    [MinWind]        DECIMAL (8, 2) NOT NULL,
    [MaxWind]        DECIMAL (8, 2) NOT NULL,
    [WinDirection]   DECIMAL(8, 2)     NOT NULL,
    [SailDirection]  NCHAR (10)     NULL,
    [Hours]          DECIMAL (8, 2) NOT NULL,
    [MilesMotored]   DECIMAL (8, 2) NULL,
    [Miles]          DECIMAL (8, 2) NOT NULL,
    [MaxTilt]        INT            NULL,
    [AvgWind]        DECIMAL (8, 2) NULL,
    [HoursMotored]   DECIMAL (8, 2) NULL,
    [CenterBoardPct] DECIMAL (8, 2) NULL,
    [DateDeleted]    DATETIME       NULL,
    [DeletedByID]    INT            NULL,
    [UpdatedByID]    INT            NULL,
    [UpdatedDate]    DATETIME       NULL,
    [CreatedByID] INT NULL, 
    [TestData] BIT NULL DEFAULT 0, 
    [GeneralNotes] NVARCHAR(MAX) NULL, 
    [SeaState] INT NULL, 
    [DateCreated] DATETIME NULL, 
	[RainSeverity] INT NULL , 
    CONSTRAINT [FK_Logs.CreatedByID] FOREIGN KEY([CreatedByID])REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [PK_Logs.Sails] PRIMARY KEY CLUSTERED ([SailID] ASC),
    CONSTRAINT [FK_Logs.DeletedByID] FOREIGN KEY ([DeletedByID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Logs.Sails_Entities.Locations] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Entities.Locations] ([LocationID]),
    CONSTRAINT [FK_Logs.Sails_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]),
    CONSTRAINT [FK_Logs.UpdatedID] FOREIGN KEY ([UpdatedByID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
);


GO
CREATE INDEX [IX_Logs.MaxWind] ON [dbo].[Logs.Sails] ([MaxWind])
GO
CREATE INDEX [IX_Logs.Date] ON [dbo].[Logs.Sails] ([Date])
GO
CREATE INDEX [IX_Logs.MinWind] ON [dbo].[Logs.Sails] ([MinWind])
GO
CREATE INDEX [IX_Logs.LocationID] ON [dbo].[Logs.Sails] ([LocationID])
GO
CREATE INDEX [IX_Logs.VesselID] ON [dbo].[Logs.Sails] ([VesselID])
GO
CREATE INDEX [IX_Logs.Miles] ON [dbo].[Logs.Sails] ([Miles])
GO
CREATE INDEX [IX_Logs.Hours] ON [dbo].[Logs.Sails] ([Hours])
GO
CREATE INDEX [IX_Logs.CreatedByID] ON [dbo].[Logs.Sails] ([CreatedByID])