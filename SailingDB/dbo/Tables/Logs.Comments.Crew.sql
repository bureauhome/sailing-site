﻿CREATE TABLE [dbo].[Logs.Comments.Crew] (
    [CommentID] INT NOT NULL,
    [CrewID]    INT NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Crew] PRIMARY KEY CLUSTERED ([CommentID] ASC, [CrewID] ASC),
    CONSTRAINT [FK_Logs.Comments.Crew_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Logs.Comments.Crew_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]) 
);

