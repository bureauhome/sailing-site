﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SailingUnitTests
{
    using System.ComponentModel.Design;
    using System.Threading.Tasks;

    using SailingSite;
    using SailingSite.Common;
    using SailingSite.Services;

    using SailingSiteModels.New.Debug;

    [TestClass]
    public class LoggingTests : TestBase
    {
        [TestMethod]
        public async Task TestStandardLogCreation()
        {
            var log = this.TestLog();
            var id = this.LoggingService.GenerateLog(log);
            var dbLog = this.LoggingRepository.GetLog(id);
            this.CompareLogs(log, dbLog);
        }

        private Log TestLog()
        {
            var log = StaticTokens.TestValues.TestContext.ToLog();
            log.ExceptionID = UnitTestHelpers.GetRandomString(12);
            log.ExceptionMessage = UnitTestHelpers.GetRandomString(50);
            log.Message = UnitTestHelpers.GetRandomString(40);
            log.Type=Log.LogTypes.Maintenance;
            return log;
        }

        private void CompareLogs(Log original, Debug_Logs compare)
        {
            if (original != null)
            {
                Assert.IsNotNull(compare);
                Assert.AreNotEqual(compare.Created,DateTime.MinValue);
                Assert.AreEqual(original.ActiveUser,compare.ActiveUser);
                Assert.AreEqual(original.ExceptionID, compare.ExceptionID);
                Assert.AreEqual(original.ExceptionMessage,compare.ExceptionMessage);
                Assert.AreEqual(original.Message,compare.Message);
                Assert.AreEqual(original.Type.ToString(), compare.Type);
                Assert.AreEqual(original.CorrelationID,compare.CorrelationID);
            }
        }
    }
}
