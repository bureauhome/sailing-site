﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models
{
    public class SailReport
    {
        public IEnumerable<SailReading<WindReading>> Wind { get; set; }
        public SailReading<decimal> Heel { get; set; }
        public DateTime ReportTime { get; set; }
        public SailReading<string> Comment { get; set; }

        public bool HasContents()
        {
            return ((Wind != null || Wind.Any()) || Heel != null 
                    || Comment != null);
        }
    }

    public class SailReading<T>
    {
        public T Reading { get; set; }
        public DateTime TimeofReading { get; set; }

        public SailReading(T entity, DateTime time)
        {
            this.Reading = entity;
            this.TimeofReading = time;
        }
    }

    public class WindReading
    {
        public decimal WindSpeed { get; set; }
        public decimal? WindDirection { get; set; }
    }
}