﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Old = SailingSiteModels.Old;
using New = SailingSiteModels.New;
using OldDataConnection;

namespace Conversion.Repositories 
{
    class ManeuverRepository : IRepository<New.Maneuver>
    {
        private static Dictionary<Old.Maneuvers, New.Maneuver> Maneuvers;
        public ManeuverRepository()
        {

            var list = OldSQL.SQLStuff.LoadSailingManeuvers();
            foreach (var str in list)
            {
                Console.WriteLine("Processing maneuver: {0}", str);
                New.Maneuver maneuver = Factories.ManeuverFactory.CreateManeuver(str);
                int id = OldSQL.SQLStuff.AddNewManeuver(maneuver);

                Old.Maneuvers oldManeuver = new Old.Maneuvers() { Name = str };

                var count = Maneuvers.Where(i => i.Key.Name == str && i.Value.ID == maneuver.ID).Count();
                if (count < 1)
                {
                    Maneuvers.Add(oldManeuver, maneuver);
                    Console.WriteLine("Tying: {0} to ID:{1}", str, oldManeuver.ID);
                }
                else Console.WriteLine("{0} : ID:{1} already detected in list.", str, oldManeuver.ID);

            }
        }
        public static IEnumerable<New.Maneuver> Get(IEnumerable<string> ids)
        {
            return Maneuvers.Where(i => ids.Contains(i.Key.Name)).Select(i => i.Value);
        }

        public New.Maneuver get(object id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<New.Maneuver> GetAll(object id)
        {
            throw new NotImplementedException();
        }

        public int Insert(New.Maneuver entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(New.Maneuver entity)
        {
            throw new NotImplementedException();
        }

        public void Update(New.Maneuver entity)
        {
            throw new NotImplementedException();
        }
    }
}
