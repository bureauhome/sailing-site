﻿CREATE TABLE [dbo].[Logs.Tags.Animal] (
    [AnimalID] INT NOT NULL,
    [TagID]    INT NOT NULL,
    CONSTRAINT [PK_Logs.Tags.Animal] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Tags.Animal] PRIMARY KEY CLUSTERED ([AnimalID] ASC, [TagID] ASC),
    CONSTRAINT [FK_Logs.Tags.Animal_Entities.Animals.Names] FOREIGN KEY ([AnimalID]) REFERENCES [dbo].[Entities.Animals.Names] ([AnimalID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Tags.Animal_Entities.Tags] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Entities.Tags] ([TagID])
);

