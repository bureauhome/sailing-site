﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    public class VesselEntitySelector
    {
        public enum OrderByType { Date, MileCount, HourCount, Maxtilt, Wind };
        public OrderByType OrderType { get; set; }
        public bool Ascending { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
}