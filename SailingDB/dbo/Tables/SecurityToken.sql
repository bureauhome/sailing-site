﻿CREATE TABLE [dbo].[Debug.SecurityTokens](
	[Token] VARCHAR(50) NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CrewID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
 [GUID] NCHAR(10) NULL, 
    CONSTRAINT [PK_Debug.SecurityTokens] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Debug.SecurityTokens]  WITH CHECK ADD  CONSTRAINT [FK_Debug.SecurityTokens_Entities.Crew] FOREIGN KEY([CrewID])
REFERENCES [dbo].[Entities.Crew] ([CrewID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Debug.SecurityTokens] CHECK CONSTRAINT [FK_Debug.SecurityTokens_Entities.Crew]
GO