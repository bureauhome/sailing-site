﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels
{
    class CardinalDirectionOutOfBounds : Exception
    {
        public CardinalDirectionOutOfBounds() :
            base("Cardinal direction out of bounds.")
        { }
        public CardinalDirectionOutOfBounds(int input) :
            base(String.Format("Cardinal {0} direction out of bounds.",input))
        { }
    }
}
