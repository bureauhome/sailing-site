﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Old = SailingSiteModels.Old;
using New = SailingSiteModels.New;
using OldDataConnection;

namespace Conversion.Repositories
{
    class VesselRepository
    {
        private static Dictionary<Old.Vessel, New.Vessel> Vessels;
        private Factories.VesselFactory VesselFactory;
        public VesselRepository()
        {
            Console.WriteLine("Processing old vessels.");
            VesselFactory = new Factories.VesselFactory();
            var oldVessels=OldSQL.SQLStuff.LoadVessels();
            foreach (var vessel in oldVessels.Distinct())
            {
                Console.WriteLine("Processing old vessel: {0}", vessel);


                var newVessel = VesselFactory.CreateVessel(vessel);
               
                var count = Vessels.Where(i => i.Key.Name == vessel.Name && i.Value.ID == newVessel.ID).Count();
                if (count < 1)
                {
                    Vessels.Add(vessel, newVessel);
                    Console.WriteLine("Tying: {0} to ID:{1}", vessel.Name, newVessel.ID);
                }
                else Console.WriteLine("{0} : ID:{1} already detected in list.", vessel.Name, newVessel.ID);

            }
        }



        public SailingSiteModels.New.Vessel Get(Old.Vessel entity)
        {
            return Vessels.Where(i => i.Key.Name == entity.Name).Select(i => i.Value).FirstOrDefault();
        }

        public IEnumerable<SailingSiteModels.New.Crew> GetAll(object id)
        {
            throw new NotImplementedException();
        }

        public int Insert(SailingSiteModels.New.Crew entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SailingSiteModels.New.Crew entity)
        {
            throw new NotImplementedException();
        }

        public void Update(SailingSiteModels.New.Crew entity)
        {
            throw new NotImplementedException();
        }
    }
}
