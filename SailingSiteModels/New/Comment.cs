﻿namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using SailingSiteModels.Common;
    using SailingSiteModels.New.Bearing;

    /// <summary>
    /// The Comment entity
    /// </summary>
    [Serializable]
    public class Comment : CommentBase, ILocationContainerModel, Common.IDatabaseModel, ICrewable, IVesselable, ICommentableModel, IPhotographableModel,ITaggableModel, IZooModel, IManeuverableModel, IMeteorologicalModel
    {
         public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }

        public bool isEditMode { get; set; }

        public VesselBase Vessel { get; set; }

        public IEnumerable<Tags> Tags { get; set; }

        public IEnumerable<VesselFoundation> Vessels { get; set; }

        public IEnumerable<PhotoBase> Photos { get; set; } 

        public IEnumerable<CrewFoundation> Crew { get; set; }

        public IEnumerable<LocationBase> Locations { get; set; }

        public IEnumerable<LandMarkBearing> LandMarkBearings { get; set; }

        public IEnumerable<SheetSailLog> SheetSailLog { get; set; }

        public SailBase LoggedSail { get; set; }

        public bool IsTest { get; set; }

        public bool ContainsSubLists()
        {
            List<IEnumerable<object>> coreLists = new List<IEnumerable<object>>()
                                                          {
                                                              Animals,
                                                              this.SheetSailLog,
                                                              Crew,
                                                              Maneuvers,
                                                              Weather,
                                                              Vessels,
                                                              Photos,
                                                              Locations
                                                          };
            return this.ContainedEntity(coreLists);
        }

        public bool IsTrackPointOnly()
        {
            bool result = false;
            if (this.Longitude != null && this.Latitude != null)
            {
                if (this.ContainsSubLists()||this.Message != null||this.ContainsSecondaryInfo()||this.ContainsWeatherInfo())
                {
                    return false;
                }
                return true;
            }
            return result;
        }

        private bool ContainedEntity(IEnumerable<IEnumerable<object>> entities)
        {
            foreach (var entity in entities)
            {
                if (entity != null && entity.Any())
                {
                    return true;
                }
            }
            return false;
        }

        public void Update(object[] input, Crew crew)
        {
            Updated=DateTime.Now;
            ModifiedBy=crew;
            Message="";
            foreach (var line in input)
            {
                if (line is string)
                {
                    Message=Message+line;
                }
            }
            
        }

        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }

        public IEnumerable<Animal> Animals { get; set; }

        public IEnumerable<Maneuver> Maneuvers { get; set; }

        public IEnumerable<WeatherBase> Weather { get; set; }
    }
}
