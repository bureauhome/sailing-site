﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Old = SailingSiteModels.Old;
using New = SailingSiteModels.New;
using OldDataConnection;

namespace Conversion.Repositories
{
    class ConfigurationRepository : IRepository<SailingSiteModels.New.Configuration>
    {
        private static Dictionary<Old.Configuration, New.Configuration> Configurations;
        private Factories.ConfigurationFactory ConfigurationFactory;
        public ConfigurationRepository()
        {
            ConfigurationFactory = new Factories.ConfigurationFactory();
            var oldConfigs = OldSQL.SQLStuff.LoadSailConfigs();
            foreach (var config in oldConfigs.Distinct())
            {
                Console.WriteLine("Processing old vessel: {0}", config);
                var oldVal = new Old.Configuration() { Name = config };

                var newConfig = Factories.ConfigurationFactory.CreateConfiguration(config);

                var count = Configurations.Where(i => i.Key.Name == config && i.Value.ID == newConfig.ID).Count();
                if (count < 1)
                {
                    Configurations.Add(oldVal, newConfig);
                    Console.WriteLine("Tying: {0} to ID:{1}", config, newConfig.ID);
                }
                else Console.WriteLine("{0} : ID:{1} already detected in list.", config, newConfig.ID);

            }
        }
        public static IEnumerable<New.Configuration> Get(IEnumerable<string> ids)
        {
            return Configurations.Where(i => ids.Contains(i.Key.Name)).Select(i => i.Value);
        }

        public New.Configuration get(object id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<New.Configuration> GetAll(object id)
        {
            throw new NotImplementedException();
        }

        public int Insert(New.Configuration entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(New.Configuration entity)
        {
            throw new NotImplementedException();
        }

        public void Update(New.Configuration entity)
        {
            throw new NotImplementedException();
        }
    }
}
