﻿CREATE TABLE [dbo].[Logs.Tags.Vessel] (
    [TagID]    INT NOT NULL,
    [VesselID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Tags.Vessel] PRIMARY KEY CLUSTERED ([TagID] ASC, [VesselID] ASC),
    CONSTRAINT [FK_Logs.Tags.Vessel_Entities.Tags] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Entities.Tags] ([TagID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Tags.Vessel_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]) ON DELETE CASCADE
);

