﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    public class PhotoDetails
    {
        public CrewFoundation Creator;
        public DateTime CreatedDate;
        public CrewFoundation ModifiedBy;
        public DateTime? ModifiedDate;
    }
}
