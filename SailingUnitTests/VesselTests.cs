﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SailingUnitTests
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using Moq;

    using SailingSite.Common;
    using SailingSite.Controllers;
    using SailingSite.Repositories;
    using SailingSite.Services;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    [TestClass]
    public class VesselTests : TestBase
    {
        /// <summary>
        /// The alpha and omega.
        /// </summary>
        [TestInitialize]
        [TestCleanup]
        public void AlphaAndOmega()
        {
            this.SailRepository.Sterilize();
            this.LoggingRepository.PurgeTestData();
        }
        [TestMethod]
        public void LoadUserVessels_Vessels()
        {
            var sailController = new SailController();
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
            var crew = new CrewSecurity()
            {
                ID = 2,
                SID = Crew.SIDEnum.None,
                VesselsCaptained = null,
                VesselsCrewed = new List<VesselBase> { new VesselBase() { ID = 1 } }
            };
            object result = null;
            session.Setup(m => m[StaticTokens.Security.UserToken]).Returns(DateTime.Now);
            session.Setup(m => m[StaticTokens.CachingTokens.UserSessionResources]).Returns(crew);
            session.SetupSet(m => m["Vessel"]=It.IsAny<object>()).Callback((string name, object val) =>
        {
           result = val;
        });
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
            context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());

            sailController.ControllerContext = context.Object;


            sailController.LoadUserSpecificSessionVariables();
            Assert.AreEqual(sailController.CurrentUser.ID, crew.ID);
            Assert.IsNotNull(result);
            var vessels=result as IEnumerable<VesselBase>;
            Assert.IsTrue(vessels.Count() == 1);
            Assert.IsTrue(vessels.FirstOrDefault().ID == 1);
        }

        [TestMethod]
        public void LoadUserVessels_NoVessels()
        {
            var sailController=new SailController();
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
            var crew=new CrewSecurity()
            {
                ID = 2,
                SID = Crew.SIDEnum.None,
                VesselsCaptained = null,
                VesselsCrewed = null
            };

            session.Setup(m => m[StaticTokens.CachingTokens.UserSessionResources]).Returns(crew);
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
            context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());
            object result = null;
            session.SetupSet(m => m["Vessel"] = It.IsAny<object>()).Callback((string name, object val) =>
            {
                result = val;
            });

            sailController.ControllerContext = context.Object;


            sailController.LoadUserSpecificSessionVariables();
            Assert.AreEqual(sailController.CurrentUser.ID, crew.ID);
            Assert.IsTrue(
                result == null
                || !(result as IEnumerable<Vessel>).Any());
        }

        [TestMethod]
        public void DeleteVessel_InCrewOrAdmin()
        {
            var vesselController = new VesselController();
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
            var sailService = new Mock<ISailService>();

            var crew = new CrewSecurity()
            {
                ID = 2,
                SID = Crew.SIDEnum.None,
                VesselsCaptained = null,
                VesselsCrewed = null
            };

            var vessels = new List<VesselBase> { new VesselBase() { ID = 2 } };

            sailService.Setup(i => i.isUserVesselCrewOrSecurity(It.IsAny<int>(), It.IsAny<int>(), true)).Returns(true);
            vesselController.Sailservice = sailService.Object;
            session.Setup(m => m[StaticTokens.Security.UserToken]).Returns(DateTime.Now);
            session.Setup(m => m[StaticTokens.CachingTokens.UserSessionResources]).Returns(crew);
            session.Setup(m => m[StaticTokens.CachingTokens.VesselEnum]).Returns(new List<VesselBase>());
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
            context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());
            sailService.Setup(m => m.GetVesselBases(It.IsAny<int>()))
                .Returns(new List<VesselBase>() { new VesselBase() { ID = 360 } });
            vesselController.ControllerContext = context.Object;
            vesselController.Sailservice = sailService.Object;
            object result = null;
            result = vesselController.Delete(2);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = (ViewResult)result;
            Assert.IsInstanceOfType(viewResult.Model, typeof(IEnumerable<VesselBase>));

        }
        
        [TestMethod]
        public void DeleteVessel_NotInCrewOrAdmin()
        {
            var vesselController = new VesselController();
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
            var sailService = new Mock<ISailService>();

            var crew = new CrewSecurity()
            {
                ID = 2,
                SID = Crew.SIDEnum.None,
                VesselsCaptained = null,
                VesselsCrewed = null
            };

            var vessels = new List<VesselBase> { new VesselBase() { ID = 2 } };
           
            sailService.Setup(i => i.isUserVesselCrewOrSecurity(It.IsAny<int>(), It.IsAny<int>())).Returns(false);
            vesselController.Sailservice = sailService.Object;
            session.Setup(m => m[StaticTokens.Security.UserToken]).Returns(DateTime.Now);
            session.Setup(m => m[StaticTokens.CachingTokens.UserSessionResources]).Returns(crew);
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
            context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());
            vesselController.ControllerContext = context.Object;
            vesselController.Sailservice = sailService.Object;
            object result = null;
            result = vesselController.Delete(2);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var code = (HttpStatusCodeResult)result;
            Assert.IsTrue(code.StatusCode==403);

        }

        [TestMethod]
        public void LoadUserVessels_Admin()
        {
            var sailController = new SailController();
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
            var sailService = new Mock<ISailService>();

            var crew = new CrewSecurity()
            {
                ID = 2,
                SID = Crew.SIDEnum.Admin,
                VesselsCaptained = null,
                VesselsCrewed = null
            };

            var vessels = new List<VesselBase> { new VesselBase() { ID = 2 } };

            sailService.Setup(i => i.GetVesselNames()).Returns(vessels);
            sailController.Sailservice = sailService.Object;
            session.Setup(m => m[StaticTokens.Security.UserToken]).Returns(DateTime.Now);
            session.Setup(m => m[StaticTokens.CachingTokens.UserSessionResources]).Returns(crew);
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
            context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());
            object result = null;
            session.SetupSet(m => m["Vessel"] = It.IsAny<object>()).Callback((string name, object val) =>
            {
                result = val;
            });

            sailController.ControllerContext = context.Object;


            sailController.LoadUserSpecificSessionVariables();
            Assert.AreEqual(sailController.CurrentUser.ID, crew.ID);
            Assert.IsNotNull(result);
            var resultingVessels = result as IEnumerable<VesselBase>;
            Assert.AreEqual(resultingVessels.Count(), 1);


        }

        [TestMethod]
        public void VerifyOnlyDistinctAnimalsSelected()
        {
            SailRepository.Sterilize();
            var vessel = this.GenerateVessel();
            vessel.ID = SailService.CreateVessel(vessel, this.Context);
            var animals = new List<Animal>() { new Animal() { ID = 1 }, new Animal() { ID = 2 } };
            var animals2 = new List<Animal>() { new Animal() { ID = 1 }, new Animal() { ID = 4 } };
            var sail1 = this.generateSail(vessel);
            var sail2 = this.generateSail(vessel);
            sail2.Animals = animals2;
            sail1.Animals = animals;
            sail1.ID = SailService.CreateSail(sail1);
            sail2.ID = SailService.CreateSail(sail2);
            var dbVessel = this.SailService.GetVessel(vessel.ID);
            Assert.AreEqual(dbVessel.AnimalsSpotted.Count(), 3);
        }

        [TestMethod]
        public void VerifyOnlyDistinctCrewSelected()
        {
            SailRepository.Sterilize();
            var vessel = this.GenerateVessel();
            vessel.ID = SailService.CreateVessel(vessel, this.Context);
            var sail1 = this.generateSail(vessel);
            var sail2 = this.generateSail(vessel);
            sail2.Crew = sail1.Crew.Concat(new List<Crew>() { new Crew() { ID = 9 } });
            sail1.ID = SailService.CreateSail(sail1);
            sail2.ID = SailService.CreateSail(sail2);
            var dbVessel = this.SailService.GetVessel(vessel.ID);
            Assert.AreEqual(dbVessel.Crew.Count(), 3);
        }

        [TestMethod]
        public void TagTest()
        {
            SailRepository.Sterilize();
            var vessel = this.GenerateVessel();
            vessel.ID = SailService.CreateVessel(vessel, this.Context);
            List<string> tagsList=new List<string>();
            IEnumerable<Tags> tags = TagService.GetAllTags();
            for (int i = 0; i < random.Next(3, 12); i++)
            {
                string element = string.Empty;
                do
                {
                    element = tags.ElementAt(random.Next(0, tags.Count() - 1)).Name;
                }
                while (tagsList.Any(j => j == element));
                tagsList.Add(element);

            }
            SailService.AddVesselTags(vessel.ID,string.Join(",",tagsList));
            var dbVessel= this.SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNotNull(dbVessel);
            Assert.AreEqual(dbVessel.Tags.Count(), tagsList.Count());
            foreach (var tag in dbVessel.Tags)
            {
                Assert.IsTrue(tagsList.Contains(tag.Name));
            }

            var tagToRemove = dbVessel.Tags.ElementAt(random.Next(0, dbVessel.Tags.Count() - 1));
            tagsList.Remove(tagToRemove.Name);
            SailService.DeleteVesselTag(tagToRemove.ID,dbVessel.ID);
            dbVessel = SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNotNull(dbVessel);
            Assert.AreEqual(dbVessel.Tags.Count(), tagsList.Count());
            foreach (var tag in dbVessel.Tags)
            {
                Assert.IsTrue(tagsList.Contains(tag.Name));
            }


            for (int i = 0; i < random.Next(1, 3); i++)
            {
                string element = string.Empty;
                do
                {
                    element = tags.ElementAt(random.Next(0, tags.Count() - 1)).Name;
                }
                while (tagsList.Any(j => j == element));
                tagsList.Add(element);

            }
            SailService.AddVesselTags(vessel.ID, string.Join(",", tagsList));
            dbVessel=SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNotNull(dbVessel);
            Assert.AreEqual(dbVessel.Tags.Count(), tagsList.Count());
            foreach (var tag in dbVessel.Tags)
            {
                Assert.IsTrue(tagsList.Contains(tag.Name));
            }

            SailRepository.Sterilize();
        }

        [TestMethod]
        public void TestJiffyVessel_Logging()
        {
            var loggingService = new Mock<ILoggingService>();
            var vesselRepository = new Mock<IVesselRepository>();
            var sailingService=new SailingService();
            AuditContext context = StaticTokens.TestValues.TestContext;
            vesselRepository.Setup(i => i.JiffySaveVessel()).Returns(new Vessel() { ID = 9, Name = "Test" });
            loggingService.Setup(i => i.GenerateLog(It.IsAny<string>(), Log.LogTypes.AuditCreate, context));
            loggingService.Setup(i => i.GenerateLog(It.IsAny<string>(), Log.LogTypes.AuditCreate, context, It.IsAny<object>()));
            sailingService.LoggingService = loggingService.Object;
            sailingService.VesselRepository = vesselRepository.Object;
            sailingService.JiffyVessel(context);
            loggingService.Verify(i => i.GenerateLog(It.IsAny<string>(), Log.LogTypes.AuditCreate, context), Times.Once);

            loggingService.Verify(i => i.GenerateLog(It.IsAny<string>(), Log.LogTypes.AuditCreate, context, It.IsAny<object>()), Times.Once);
        }

        [TestMethod]
        public void CommentTest()
        {
            SailRepository.Sterilize();
            var vessel = this.GenerateVessel();
            vessel.ID = SailService.CreateVessel(vessel, this.Context);
            var sail = this.generateSail(vessel);
            sail.ID=SailService.CreateSail(sail);
            var comment = this.GenerateSailLog(sail);
            var vessels = new List<Vessel>() { new Vessel() { ID = vessel.ID } };
            comment.Vessels = vessels;
            SailService.AddSailComment(comment, sail.ID, this.Context);
            var dbVessel = SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNotNull(dbVessel.Comments);
            Assert.IsTrue(dbVessel.Comments.Count()==1);
            var dbComment = dbVessel.Comments.FirstOrDefault();
            Assert.AreEqual(dbComment.Message,comment.Message);
            SailRepository.Sterilize();
        }

        [TestMethod]
        public void CreateVesselWithSailLocker_Success()
        {
            SailRepository.Sterilize();
            var vessel = GenerateVessel();
            var firstSheetSail = this.GenerateSheetSail();
            var secondSheetSail = this.GenerateSheetSail();
            vessel.SailLocker = new List<SheetSail>()
                                    {
                                        firstSheetSail,
                                        secondSheetSail
                                    };
            vessel.ID=this.SailService.CreateVessel(vessel, this.Context);
            var dbVessel = this.SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNotNull(dbVessel.SailLocker);
            Assert.IsTrue(dbVessel.SailLocker.Count() == 2);
            this.CompareSheetSail(firstSheetSail, dbVessel.SailLocker.ElementAt(0));
            this.CompareSheetSail(secondSheetSail, dbVessel.SailLocker.ElementAt(1));
            SailRepository.Sterilize();
        }

        [TestMethod]
        public void CreateVesselWithAnonCaptain_Success()
        {
            SailRepository.Sterilize();
            var vessel = GenerateVessel();
            vessel.Captain = new CrewFoundation() { ID = -1 };
            vessel.ID = this.SailService.CreateVessel(vessel, this.Context);
            var dbVessel = this.SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNull(dbVessel.Captain);
            SailRepository.Sterilize();
        }

        [TestMethod]
        public void AddSheetSailToExistingVessel_Success()
        {
            SailRepository.Sterilize();
            var vessel = GenerateVessel();
            var firstSheetSail = this.GenerateSheetSail();
            vessel.ID = this.SailService.CreateVessel(vessel, this.Context);
            firstSheetSail.ID = this.SailService.AddSheetSailToExistingVessel(firstSheetSail, vessel.ID, this.Context);
            var dbVessel = this.SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNotNull(dbVessel.SailLocker);
            Assert.IsTrue(dbVessel.SailLocker.Count() == 1);
            this.CompareSheetSail(firstSheetSail, dbVessel.SailLocker.ElementAt(0));
            SailRepository.Sterilize();
        }

        [TestMethod]
        public void UpdateSheetSail_Success()
        {
            SailRepository.Sterilize();
            var vessel = GenerateVessel();
            var firstSheetSail = this.GenerateSheetSail();
            vessel.SailLocker = new List<SheetSail>()
                                    {
                                        firstSheetSail
                                    };
            vessel.ID = this.SailService.CreateVessel(vessel, this.Context);
          
            var dbVessel = this.SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNotNull(dbVessel.SailLocker);
            Assert.IsTrue(dbVessel.SailLocker.Count() == 1);
            firstSheetSail.Location = UnitTestHelpers.GetRandomString(11);
            firstSheetSail.SailType.ID = UnitTestHelpers.rdm.Next(1, 6);
            firstSheetSail.ID = dbVessel.SailLocker.First().ID;

            this.SailService.UpdateSheetSail(firstSheetSail, this.Context);
            dbVessel = this.SailService.GetVessel(vessel.ID);

            this.CompareSheetSail(firstSheetSail, dbVessel.SailLocker.ElementAt(0));
            SailRepository.Sterilize();
        }

        [TestMethod]
        public void DeleteSheetSail_Success()
        {
            SailRepository.Sterilize();
            var vessel = GenerateVessel();
            var firstSheetSail = this.GenerateSheetSail();
            vessel.SailLocker = new List<SheetSail>()
                                    {
                                        firstSheetSail
                                    };
            vessel.ID = this.SailService.CreateVessel(vessel, this.Context);
            var dbVessel = this.SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsNotNull(dbVessel.SailLocker);
            Assert.IsTrue(dbVessel.SailLocker.Count() == 1);
            firstSheetSail.ID = dbVessel.SailLocker.First().ID;
            this.SailService.DeleteSheetSail(firstSheetSail.ID, this.Context);
            dbVessel = this.SailService.GetVessel(vessel.ID);
            Assert.IsNotNull(dbVessel);
            Assert.IsTrue(dbVessel.SailLocker == null || !dbVessel.SailLocker.Any());
        }

        [TestMethod]
        public void TestJiffyVessel_IntegrationSuccess()
        {;
            AuditContext context = StaticTokens.TestValues.TestContext;
            var vessel = this.SailService.JiffyVessel(context);
            Assert.IsNotNull(vessel);
            Assert.IsTrue(vessel.IsTest);
            Assert.AreNotEqual(vessel.ID, 0);
        }

        [TestMethod]
        public void CRUDTest()
        {
            SailRepository.Sterilize();
            var fleet = this.GenerateFleet();
            fleet.ID=SailService.CreateFleet(fleet, this.Context);
            var dbFleet = SailService.GetFleet(fleet.ID);
            Assert.IsTrue(CompareFleets(fleet,dbFleet));

            var vessel = GenerateVessel();
            vessel.ID=SailService.CreateVessel(vessel, this.Context);
            vessel.Fleets = new List<FleetBase> { fleet };
            SailService.AddVesselToFleet(GenerateVesselEntry(vessel), fleet.ID, this.Context);
            var sail = this.generateSail(vessel);

            var vessels = SailService.GetVesselBases(fleet.ID);
            Assert.IsTrue(vessels.Any(i=>i.ID==vessel.ID));
            
            sail.Animals = new List<Animal>() { new Animal() { ID = 2 }, new Animal() { ID = 1 } };
            
            sail.ID=SailService.CreateSail(sail);
            vessel.AnimalsSpotted = sail.Animals;
            vessel.Crew = sail.Crew.Select(i=>i.ToCrewBase());
            vessel.Sails = new List<SailBase>() { sail };

            var dbVessel = SailService.GetVessel(vessel.ID);

            Assert.IsTrue(compareVessel(vessel, dbVessel));

            var fleet2 = this.GenerateFleet();
            fleet2.ID = SailService.CreateFleet(fleet2, this.AdminContext);
            SailService.RemoveVesselFromFleet(vessel.ID, fleet.ID, this.AdminContext);

            vessels = SailService.GetVesselBases(fleet.ID);
            Assert.IsFalse(vessels.Any(i => i.ID == vessel.ID));
            
            SailService.AddVesselToFleet(GenerateVesselEntry(vessel),fleet2.ID, this.Context);
            
            vessels = SailService.GetVesselBases(fleet2.ID);
            Assert.IsTrue(vessels.Any(i => i.ID == vessel.ID));
            
            vessel.Fleets = new List<FleetBase> { fleet2 };
            ModVessel(vessel);

            SailService.UpdateVessel(vessel);
            var sail2 = this.generateSail(vessel);
            sail2.Animals = new List<Animal>() { new Animal() { ID = 4 }, new Animal() { ID = 1 } };
            sail2.ID=SailService.CreateSail(sail2);
            var animeaux2 = sail.Animals.ToList();
            animeaux2.Add(new Animal() { ID = 4 });
            vessel.AnimalsSpotted = animeaux2;
            dbVessel = SailService.GetVessel(vessel.ID);
            dbVessel.Crew = sail.Crew.Concat(sail2.Crew).GroupBy(c => c.ID).Select(g => g.First().ToCrewBase());
            vessel.Sails = new List<SailBase>() { sail, sail2 };
            Assert.IsTrue(compareVessel(vessel,dbVessel));
            VesselRepository.Delete(vessel.ID,7);

            vessels = SailService.GetVesselBases(fleet2.ID);
            Assert.IsFalse(vessels.Any(i => i.ID == vessel.ID));

            SailRepository.Sterilize();
        }
    }
}
