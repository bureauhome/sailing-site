﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    public interface ITaggableRepository
    {
        IEnumerable<New.Tags> GetTags(int id);
        void AddTags(List<int> tagIds, int id);
        void DeleteTag(int tagId, int id);
    }
}