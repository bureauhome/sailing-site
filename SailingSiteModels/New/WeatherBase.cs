﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    using SailingSiteModels.Common;

    public class WeatherBase :IEntityModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string IconPath { get; set; }
        public string Name { get; set; }
        public bool IsAssociated { get; set; }

        string IEntityModel.ControllerName
        {
            get
            {
                return "Weather";
            }
        }

        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
    }
}
