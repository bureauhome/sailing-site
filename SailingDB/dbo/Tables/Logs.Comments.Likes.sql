﻿CREATE TABLE [dbo].[Logs.Comments.Likes] (
    [CrewID]    INT        NOT NULL,
    [CommentID] INT        NOT NULL,
    [Date]      ROWVERSION NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Likes] PRIMARY KEY CLUSTERED ([CrewID] ASC, [CommentID] ASC),
    CONSTRAINT [FK_Logs.Comments.Likes_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]),
    CONSTRAINT [FK_Logs.Comments.Likes_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID])
);

