﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite.Factories
{
    using System.Data.Entity.Core.Metadata.Edm;
    using System.Device.Location;
    using System.Web.Configuration;
    using System.Web.Services.Description;

    using Microsoft.Ajax.Utilities;

    using SailingSite.Models;

    using SailingSiteModels.Common;
    using SailingSiteModels.New.Bearing;

    public static class CommonFactory
    {

        public static LandMarkBearing CreateLandMarkBearing(Bearings_LandMark entity)
        {
            if (entity != null && entity.Entities_Bearing != null && entity.Entities_LandMark != null)
            {
                return new LandMarkBearing()
                           {
                               LandMark = CreateLandMarkBase(entity.Entities_LandMark),
                               Bearing = CreateBearing(entity.Entities_Bearing)
                           };
            }
            return null;

        }

        public static Entities_MetaData CreateMetaData(MetaData entity)
        {
            Entities_MetaData result= new Entities_MetaData();
            if (entity != null)
            {
                result.MetadataKey = entity.Key;
                result.Value = entity.Value;
            }
            return result;
        }

        public static Bearing CreateBearing(Entities_Bearing entity)
        {
            if (entity != null)
            {
                return new Bearing() { CompassReading = (int)entity.CompassReading, };
            }
            return null;
        }

        public static New.WeatherBase CreateWeather(Entities_Weather entity)
        {
            if (entity != null)
            {
                return new New.WeatherBase()
                {
                    ID=entity.WeatherID,
                    Name=entity.Description,
                    IconPath=entity.IconPath
                };
            }
            return null;
        }

        public static Entities_Weather CreateWeather(New.WeatherBase entity)
        {
            if (entity != null)
            {
                return new Entities_Weather() { WeatherID = entity.ID, Description = entity.Name, IconPath = entity.IconPath };
            }
            return null;
        }

        public static New.Configuration CreateConfiguration(Entities_Config entity)
        {
            if (entity != null)
            {
                return new New.Configuration()
                           {
                               ID = entity.ConfigID,
                               Name = entity.Name,
                               Description = entity.Description
                           };
            }
            return null;
        }

        public static Entities_Config CreateConfiguration(New.Configuration entity)
        {
            if (entity != null)
            {
                return new Entities_Config()
                           {
                               ConfigID = entity.ID,
                               Description = entity.Description,
                               Name = entity.Name
                           };
            }
            return null;
        }

        public static Debug_Logs CreateLog(New.Debug.Log entity)
        {
            if (entity != null)
            {
                return new Debug_Logs()
                           {
                               ActiveUser = entity.ActiveUser,
                               CorrelationID = entity.CorrelationID,
                               Created = DateTime.Now,
                               ExceptionID = entity.ExceptionID,
                               ExceptionMessage = entity.ExceptionMessage,
                               Message = entity.Message,
                               Type = entity.Type.ToString(),
                               ViewName = entity.ViewName,
                               AttachedEntity = entity.AttachedEntity,
                               Verb = entity.Verb,
                               URL = entity.EntryPoint
                           };
            }

            return null;
        }

        public static Entities_LandMark CreateLandMark(New.LandMarkBase entity)
        {
            if (entity != null)
            {
                var result= new Entities_LandMark()
                           {
                               Longitude = entity.Longitude,
                               Latitude = entity.Latitude*1.0,
                               Description = entity.Description,
                               Name = entity.Name,
                               Id = entity.ID
                           };
                if (entity.Altitude!=null)
                {
                    result.Altitude = entity.Altitude;
                }
                return result;
            }
            return null;
        }

        public static New.LandMarkBase CreateLandMarkBase(Entities_LandMark entity)
        {
            if (entity != null)
            {
                return new New.LandMarkBase
                           {
                               Latitude = entity.Latitude,
                               Longitude = entity.Longitude,
                               Altitude = entity.Altitude,
                               Description = entity.Description,
                               ID = entity.Id,
                               Name = entity.Name
                           };
            }
            return null;
        }

        public static New.Maneuver CreateManeuver(Entities_Maneuvers entity)
        {
            if (entity != null)
            {
                return new New.Maneuver()
                {
                    ID=entity.ManeuverID,
                    Name=entity.Name,
                    Description=entity.Description==null?null:entity.Description
                };
            }
            return null;
        }

        public static Entities_Maneuvers CreateManeuver(New.Maneuver entity)
        {
            if (entity != null)
            {
                return new Entities_Maneuvers()
                {
                    ManeuverID = entity.ID,
                    Name = entity.Name,
                    Description = entity.Description == null ? null : entity.Description
                };
            }
            return null;
        }

        public static New.WeatherBase CreateWeatherBase(Entities_Weather entity)
        {
            New.WeatherBase result = null;
            if (entity != null)
            {
                result = new New.WeatherBase()
                {
                    Name = entity.Description,
                    Description = entity.Description,
                    IconPath = entity.IconPath,
                    ID = entity.WeatherID
                };
            }
            return result;

        }

        public static Debug_SecutityQuestions CreateQuestion(New.SecurityQuestion entity)
        {
            Debug_SecutityQuestions result = null;
            if (entity != null)
            {
                result = new Debug_SecutityQuestions()
                             {
                                 Answer = entity.Answer,
                                 CrewID = entity.CrewID,
                                 Question = entity.Question,
                                 QuestionID = entity.ID
                             };
            }
            return result;
        }

        public static New.SecurityQuestion CreateQuestion(Debug_SecutityQuestions entity)
        {
            New.SecurityQuestion result = null;
            if (entity != null)
            {
                result = new New.SecurityQuestion()
                             {
                                 CrewID = entity.CrewID,
                                 Question = entity.Question,
                                 Answer = entity.Answer,
                                 ID = entity.QuestionID
                             };
            }
            return result;
        }

        public static New.LocationBase CreateLocationBase(Entities_Locations entity)
        {
            return new New.Location()
            {
                ID = entity.LocationID,
                Name = entity.Name,
                Longitude = entity.Longitude,
                Latitude = entity.Latitude,
                MajorLocation = entity.MajorLocation==null?null:entity.MajorLocation,
                MinorLocation = entity.MinorLocation==null?null:entity.MinorLocation,
                Location = New.LocationBase.LocationTypes.FirstOrDefault(i => i.ID==entity.LocationType)
            };
        }

        public static New.Location CreateLocation(Entities_Locations entity)
        {
            var result = CreateLocationBase(entity).ToLocation();

            try
            {
                result.Photos = entity.Entities_Photos1 == null || !entity.Entities_Photos1.Any()
                                                    ? null
                                                    : entity.Entities_Photos1.Select(PhotoFactory.CreatePhotoBase);
            }
            catch
            {
            }
            try
            {
                result.Comments = entity.Entities_Comment == null
                                                        || !entity.Entities_Comment.Any()
                                                            ? null
                                                            : entity.Entities_Comment.Select(
                                                                CommonFactory.CreateCommentBase);
            }
            catch
            {
            }
            try
            {
                 result.Tags = entity.Entities_Tags == null || !entity.Entities_Tags.Any()
                                                 ? null
                                                 : entity.Entities_Tags.Select(CommonFactory.CreateTag);
            }
            catch
            {
            }
            return result;
        }

        public static Entities_Locations CreateLocation(New.Location entity)
        {
            return new Entities_Locations()
                       {
                           Name = entity.Name==null?null:entity.Name,
                           Longitude = entity.Longitude,
                           Latitude = entity.Latitude,
                           MajorLocation = entity.MajorLocation==null?null:entity.MajorLocation,
                           MinorLocation = entity.MinorLocation==null?null:entity.MinorLocation,
                           TestData = entity.IsTest
                       };
        }

        public static Entities_Locations CreateLocation(New.LocationBase entity)
        {
           return CreateLocation(entity.ToLocation());
        }

        public static New.Tags CreateTag(Entities_Tags entity)
        {
            return new New.Tags()
            {
                ID = entity.TagID,
                Created = entity.Created,
                Name = entity.Name,
                Description = entity.Description
            };
        }

        public static Entities_Tags CreateTag(New.Tags entity)
        {
            return new Entities_Tags()
            {
                TagID = entity.ID,
                Created = DateTime.Now,
                CrewID=entity.Creator.ID,
                Name = entity.Name,
                Description = entity.Description
            };
        }


        public static New.TagHydrated CreateHydratedTag(Entities_Tags entity)
        { 
            if (entity!=null){
                return new New.TagHydrated
                {
                    Name=entity.Name,
                    ID=entity.TagID,
                    Animals=entity.Entities_Animals_Names!=null&&entity.Entities_Animals_Names.Any()?entity.Entities_Animals_Names.Select(Factories.AnimalFactory.CreateAnimal).ToList():null,
                    Comments=entity.Entities_Comment!=null&&entity.Entities_Comment.Any()?entity.Entities_Comment.Select(Factories.CommonFactory.CreateCommentBase).ToList():null,
                    Created=entity.Created,
                    Creator=entity.Entities_Crew==null?null:Factories.CrewFactory.CreateCrew(entity.Entities_Crew),
                    Crews=entity.Entities_Crew1!=null&&entity.Entities_Crew1.Any()?entity.Entities_Crew1.Select(Factories.CrewFactory.CreateCrewBase).ToList():null,
                    Description=entity.Description,
                    Locations =
                        entity.Entities_Locations != null && entity.Entities_Locations.Any()
                            ? entity.Entities_Locations.Select(Factories.CommonFactory.CreateLocationBase).ToList()
                            : null,
                    Photos =
                        entity.Entities_Photos != null && entity.Entities_Photos.Any()
                            ? entity.Entities_Photos.Select(PhotoFactory.CreatePhotoBase).ToList()
                            : null,
                    Sails =
                        entity.Logs_Sails != null && entity.Logs_Sails.Any()
                            ? entity.Logs_Sails.Select(SailFactory.CreateSailbase).ToList()
                            : null,
                    Vessels=entity.Entities_Vessel!=null&&entity.Entities_Vessel.Any()?entity.Entities_Vessel.Select(FleetFactory.CreateVesselBase).ToList():null,
                    Fleets=entity.Entities_Fleet!=null&&entity.Entities_Fleet.Any()?entity.Entities_Fleet.Select(FleetFactory.CreateFleetBase).ToList():null
                };
            }
            return null;
        }

        public static CommentNewsFeedViewModel CreateNewsFeed(Entities_Comment entity)
        {
            CommentNewsFeedViewModel result = new CommentNewsFeedViewModel();
            result.ID = entity.CommentID;
            result.Message = entity.Comment;
            result.Title = entity.Title;
            result.Subject = entity.Subject;
            result.Created = entity.DateCreated;
            result.CreatedBy = CrewFactory.CreateCrewFoundation(entity.Entities_Crew);
            result.Barometer = entity.Barometer;
            result.MilesLogged = entity.MilesRun;
            result.TempF = entity.TemperatureF;
            result.GPSCoords = entity.GPSCoords == null ? null : entity.GPSCoords.ToString();
            result.HelmsPerson = entity.Entities_Crew3 == null
                                     ? null
                                     : Factories.CrewFactory.CreateCrewFoundation(entity.Entities_Crew3);
            result.WindSpeed = entity.WindSpeed;
            result.WindDirection = entity.WindDirection;
            result.HeelPct = entity.HeelPct;
            result.CompassBearing = entity.CompassBearing;
            result.SeaState = entity.SeaState;
            result.RainSeverity = entity.RainPerc;
            result.Sounding = entity.Soundings;
            result.CenterboardPct = entity.CenterboardPct;
            result.CloudCover = entity.CloudCoverPerc;
            result.IsLog = entity.ShipLog == true;
            result.LogId = entity.LogId;
            result.Longitude = entity.Longitude;
            result.Latitude = entity.Latitude;
            result.Elevation = entity.Elevation;
            result.Leeway = entity.Leeway;
            result.SpeedMph = entity.MPHFromLastPoint == null ? (double?)null : (double)entity.MPHFromLastPoint;
            result.TrackKey = entity.TrackKey;
            return result;
                                                
        }


        public static New.CommentBase CreateCommentBase(Entities_Comment entity)
        {
            var result=new New.CommentBase();

            result.ID = entity.CommentID;
            result.Message = entity.Comment;
            result.Title = entity.Title;
            result.Subject = entity.Subject;
            result.Created = entity.DateCreated;
            result.CreatedBy = CrewFactory.CreateCrewFoundation(entity.Entities_Crew);
            result.Barometer = entity.Barometer;
            result.MilesLogged = entity.MilesRun;
            result.TempF = entity.TemperatureF;
            result.GPSCoords = entity.GPSCoords == null ? null : entity.GPSCoords.ToString();
            result.HelmsPerson = entity.Entities_Crew3 == null
                                     ? null
                                     : Factories.CrewFactory.CreateCrewFoundation(entity.Entities_Crew3);
            result.WindSpeed = entity.WindSpeed;
            result.WindDirection = entity.WindDirection;
            result.HeelPct = entity.HeelPct;
            result.CompassBearing = entity.CompassBearing;
            result.SeaState = entity.SeaState;
            result.RainSeverity = entity.RainPerc;
            result.Sounding = entity.Soundings;
            result.CenterboardPct = entity.CenterboardPct;
            result.CloudCover = entity.CloudCoverPerc;
            result.IsLog = entity.ShipLog == true;
            result.LogId = entity.LogId;
            result.Longitude = entity.Longitude;
            result.Latitude = entity.Latitude;
            result.Elevation = entity.Elevation;
            result.Leeway = entity.Leeway;
            result.SpeedMph = entity.MPHFromLastPoint == null ? (double?)null : (double)entity.MPHFromLastPoint;
            result.TrackKey = entity.TrackKey;
            return result;
        }

        public static Entities_Comment CreateComment(New.Comment entity)
        {
           
            return new Entities_Comment
            {
                DateCreated= entity.Created,
                CommentID=entity.ID,
                Comment=entity.Message,
                DateModified=entity.Updated!=null?(DateTime)entity.Updated:DateTime.Now,
                CrewID=entity.CreatedBy.ID,
                Subject = entity.Subject,
                Title=entity.Title,
                Barometer = entity.Barometer,
                ModifiedCrewID = entity.CreatedBy.ID,
                CloudCoverPerc = entity.CloudCover,
                CenterboardPct = (int?)entity.CenterboardPct,
                RainPerc = entity.RainSeverity,
                MilesRun = entity.MilesLogged,
                TemperatureF = entity.TempF,
                WindDirection = entity.WindDirection,
                SeaState=entity.SeaState,
                WindSpeed = entity.WindSpeed,
                GPSCoords = string.IsNullOrEmpty(entity.GPSCoords) ? null : System.Data.Entity.Spatial.DbGeography.FromText(entity.GPSCoords),
                HeelPct = entity.HeelPct,
                ShipLog = entity.IsLog,
                LogId = entity.LogId,
                Soundings = entity.Sounding,
                Elevation = entity.Elevation,
                Longitude = entity.Longitude,
                BearingOnly = entity.IsCoordinateOnly,
                Latitude = entity.Latitude,
                CompassBearing = entity.CompassBearing,
                TestData = entity.IsTest,
                Leeway = entity.Leeway,
                MPHFromLastPoint = entity.SpeedMph == null ? (decimal?)null : (decimal)entity.SpeedMph,
                TrackKey = entity.TrackKey
            };
        }

        public static Logs_Sails_Vessels_Sails CreateSheetSailLog_Sail(New.SheetSailLog entity)
        {
            if (entity != null)
            {
                if (entity.SheetSail == null)
                {
                    throw new ArgumentNullException("New.SheetSail");
                }
                return new Logs_Sails_Vessels_Sails() { SheetID = entity.SheetSail.ID, Percentage = entity.Percentage };
            }
            return null;
        }

        public static Logs_Comments_Vessel_Sails CreateSheetSailLog_Comment(New.SheetSailLog entity)
        {
            if (entity != null)
            {
                if (entity.SheetSail == null)
                {
                    throw new ArgumentNullException("New.SheetSail");
                }
                return new Logs_Comments_Vessel_Sails() { SheetID = entity.SheetSail.ID, Percentage = entity.Percentage };
            }
            return null;
        }

        public static New.SheetSailLog CreateSheetSailLog(Logs_Comments_Vessel_Sails entity)
        {
            if (entity != null && entity.Entities_Vessels_Sails != null)
            {
                return new New.SheetSailLog()
                           {
                               Percentage = entity.Percentage,
                               SheetSail = CreateSheetSail(entity.Entities_Vessels_Sails)
                           };
            }
            return null;
        }

        public static New.SheetSailLog CreateSheetSailLog(Logs_Sails_Vessels_Sails entity)
        {
            if (entity != null && entity.Entities_Vessels_Sails != null)
            {
                return new New.SheetSailLog()
                {
                    Percentage = entity.Percentage,
                    SheetSail = CreateSheetSail(entity.Entities_Vessels_Sails)
                };
            }
            return null;
        }

        /// <summary>
        /// The create sheet sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Entities_Vessels_Sails"/>.
        /// </returns>
        public static Entities_Vessels_Sails CreateSheetSail(New.SheetSail entity)
        {
            // Always check for null.
            if (entity != null)
            {
                // if the entity type is null, something went wrong.  We throw an error.
                if (entity.SailType == null)
                {
                    throw new ArgumentNullException("New.SheetSail.SailType");
                }
                return new Entities_Vessels_Sails()
                           {
                               ID = entity.ID,
                               Location = entity.Location,
                               RollingFurler = entity.RollingFurling,
                               SailTypeID = entity.SailType.ID,
                               Colors = entity.Colors,
                               SailArea = entity.SailArea,
                               SailLettering = entity.SailLettering
                           };
            }
            // if it's null, we return null.
            return null;
        }

        /// <summary>
        /// The create sheet sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="SheetSail"/>.
        /// </returns>
        public static New.SheetSail CreateSheetSail(Entities_Vessels_Sails entity)
        {
            if (entity != null)
            {
                return new New.SheetSail()
                           {
                               ID = entity.ID,
                               Location = entity.Location,
                               RollingFurling = entity.RollingFurler,
                               Colors= entity.Colors,
                               SailArea= entity.SailArea,
                               SailType = entity.Entities_Config == null ? null : CreateConfiguration(entity.Entities_Config),
                               Vessel = Factories.FleetFactory.CreateVesselFoundation(entity.Entities_Vessel),
                               SailLettering = entity.SailLettering
                           };
            }
            return null;
        }

        /// <summary>
        /// The create comment.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Comment"/>.
        /// </returns>
        public static New.Comment CreateComment(Entities_Comment entity)
        {
            var result = CreateCommentBase(entity).ToComment();
            result.IsTest = entity.TestData == true;
            result.UpdatebyID = entity.ModifiedCrewID;
            result.Leeway = entity.Leeway;
            result.TrackKey = entity.TrackKey;
            if (entity.Bearings_LandMark != null && entity.Bearings_LandMark.Any())
            {
                result.LandMarkBearings = entity.Bearings_LandMark.Select(CreateLandMarkBearing);
            }

            result.Updated = entity.DateModified;
            result.SheetSailLog = (entity.Logs_Comments_Vessel_Sails == null || !entity.Logs_Comments_Vessel_Sails.Any())
                                        ? null
                                        : entity.Logs_Comments_Vessel_Sails.Select(Factories.CommonFactory.CreateSheetSailLog);
            result.Animals = (entity.Entities_Animals_Names == null || !entity.Entities_Animals_Names.Any())
                                 ? null
                                 : entity.Entities_Animals_Names.Select(AnimalFactory.CreateAnimal);
            result.Crew = (entity.Entities_Crew5 == null || !entity.Entities_Crew5.Any())
                              ? null
                              : entity.Entities_Crew5.Select(CrewFactory.CreateCrewFoundation);

            result.LoggedSail = (entity.Logs_Sails == null) ? null : SailFactory.CreateSailbase(entity.Logs_Sails);

            result.Maneuvers = (entity.Entities_Maneuvers != null && entity.Entities_Maneuvers.Any())
                                   ? entity.Entities_Maneuvers.Select(CommonFactory.CreateManeuver)
                                   : null;
            result.Animals = (entity.Entities_Animals_Names != null && entity.Entities_Animals_Names.Any())
                                 ? entity.Entities_Animals_Names.Select(AnimalFactory.CreateAnimal).ToArray()
                                 : null;

            result.Weather = (entity.Entities_Weather == null || !entity.Entities_Weather.Any())
                                 ? null
                                 : entity.Entities_Weather.Select(CreateWeatherBase);
            
            result.Vessels = (entity.Entities_Vessel == null || !entity.Entities_Vessel.Any())
                                 ? null
                                 : entity.Entities_Vessel.Select(FleetFactory.CreateVesselBase);

            result.Tags = (entity.Entities_Tags == null || !entity.Entities_Tags.Any())
                              ? null
                              : entity.Entities_Tags.Select(CreateTag);
            result.Photos = (entity.Entities_Photos == null || !entity.Entities_Photos.Any())
                                ? null
                                : entity.Entities_Photos.Select(PhotoFactory.CreatePhotoBase);

            result.Locations = (entity.Entities_Locations == null || !entity.Entities_Locations.Any())
                                   ? null
                                   : entity.Entities_Locations.Select(CommonFactory.CreateLocationBase);

            result.ModifiedBy = entity.Entities_Crew2 == null
                                    ? null
                                    : Factories.CrewFactory.CreateCrewFoundation(entity.Entities_Crew2);

            return result;
        }
    }
}