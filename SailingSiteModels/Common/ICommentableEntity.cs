﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Common
{
    interface ICommentableEntity
    {
        IEnumerable<New.CommentBase> Comments { get; set; }
    }
}
