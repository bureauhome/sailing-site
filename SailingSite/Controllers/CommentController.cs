﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.Net;

    using Microsoft.Ajax.Utilities;

    using SailingSite.App_GlobalResources;
    using SailingSite.Common;
    using SailingSite.Services;

    using SailingSiteModels.New;

    public class CommentController : BaseController
    {
        // GET: Comment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSailReport(int trackId)
        {
            var result = this.CommentService.GetSailReport(trackId);
            return this.PartialView("SailReport/SailReportPartial", result);
        }

        [HttpPost]
        public ActionResult Create(SailingSiteModels.New.Comment entity, string tags)
        {
            if (entity != null && !string.IsNullOrEmpty(entity.Message))
            {
                this.Session[Resources.SessionVariables.TemporaryComment] = entity;
                this.GetSessionVariables();

                if (this.CurrentUser == null)
                {
                    return new HttpStatusCodeResult(
                        HttpStatusCode.BadRequest,
                        "You must be logged in to create or edit a comment.");
                }

                if (entity.ID > 0)
                {
                    entity.Updated = DateTime.Now;
                    entity.ModifiedBy = this.CurrentUser;
                    this.CommentService.Update(entity);

                }
                else
                {
                    entity.Created = DateTime.Now;
                    entity.CreatedBy = this.CurrentUser;
                    entity.CreatedbyID = this.CurrentUser.ID;
                    try
                    {
                        entity.ID = this.CommentService.Create(entity);
                    }
                    catch (Exception ex)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message);
                    }
                }

                if (tags != null)
                {
                    this.CommentService.AddTags(entity.ID, tags);
                }
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest , SharedStrings.EmptyOrNullCommentWarning);
           
        }

        public ActionResult Get(int id)
        {
            this.GetSessionVariables();
            var data = CommentService.Get(id);
            IsCreator(data);
            return View(data);
        }

        public ActionResult New(int? id = null)
        {
            this.GetSessionVariables();
            SailingSiteModels.New.Comment data=new SailingSiteModels.New.Comment();
            if (id != null&&id>0)
            {
                data = CommentService.Get((int)id);
            }
            return View(data);
        }

        [HttpPost]
        public ActionResult AddTags(int id, string tags)
        {
            CommentService.AddTags(id, tags);
            var result = CommentService.GetTags(id).ToArray();
            ViewData["controller"] = "comment";
            ViewData["entityID"] = id;
            return PartialView("TagList", result);
        }

        public ActionResult Delete(int id, string controller, int entityID)
        {
            if (CurrentUser != null)
            {
                CommentService.Delete(id, base.CurrentUser.ID);
                return RedirectToAction(Resources.ActionVerbs.GetCommentsPartial, controller, new { id = entityID });
            }
            else
            {
                throw new HttpException(500, "Authentication error.  You must log in to perform this action.");
            }
        }

        public ActionResult NewComment(DateTime? date, string message=null)
        {
            SailingSiteModels.New.Comment result=new Comment();
            if (date != null)
            {
                result.Created = (DateTime)date;
            }
            else
            {
                result.Created = DateTime.Now;
            }
            if (message != null)
            {
                result.Subject = message;
            }
            return this.PartialView("_Comment,", result);

        }

        [HttpPost]
        public ActionResult DeleteTag(int id, int entityId)
        {
            CommentService.DeleteTag(id, entityId);
            var result = CommentService.GetTags(id);
            ViewData["controller"] = "comment";
            ViewData["entityID"] = id;
            return PartialView("TagList", result);
        }
    }
}