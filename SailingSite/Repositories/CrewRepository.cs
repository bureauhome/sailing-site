﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using System.Data.Entity.Validation;
    using System.EnterpriseServices;
    using System.Net.Configuration;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.WebPages;

    using Microsoft.SqlServer.Server;

    using SailingSite.Factories;

    public class CrewRepository :ICrewRepository
    {
        public readonly static string TestUsername = "test";

        public readonly static string TestEmailAddress = "test@test.try";

        public IEnumerable<New.Tags> GetTags(int id)
        {
            IEnumerable<New.Tags> result = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Crew.Include(j => j.Entities_Tags) where i.CrewID == id select i.Entities_Tags.Where(k=>k.DateDeleted==null)).FirstOrDefault();
                result = query.ToArray().Select(Factories.CommonFactory.CreateTag);
            }
            return result;
        }

        public New.Crew.SIDEnum GetUserSID(int id)
        {
            var result = New.Crew.SIDEnum.None;

            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.FirstOrDefault(i => i.CrewID == id);
                if (user != null && user.SID != null)
                {
                    result = (New.Crew.SIDEnum)user.SID;
                }
            }
            return result;
        }

        public async Task<string> CreateToken(int userId)
        {
            using (var context = new SailingEntities())
            {
                var token = new Debug_SecurityTokens() { CrewID = userId, Date = DateTime.Now,Token = Guid.NewGuid().ToString()};
                context.Debug_SecurityTokens.Add(token);
                await context.SaveChangesAsync();
                return token.Token;
            }
        }

        public async Task<bool> VerifyToken(int userId, string token)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                var dbToken =
                    await
                    context.Debug_SecurityTokens.FirstOrDefaultAsync(
                        i => i.Token == token && i.CrewID == userId);
                if (dbToken != null)
                {
                    result = dbToken.Date > DateTime.Now.AddHours(-2);
                    context.Debug_SecurityTokens.Remove(dbToken);
                    await context.SaveChangesAsync();
                }

            }
            return result;

        }


        public bool UserExists(string username)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.FirstOrDefault(i => i.UserName == username && i.DateDeleted==null);
                if (user != null)
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// The is user locked.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUserLocked(string username)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.FirstOrDefault(i => i.UserName == username);
                if (user != null)
                {
                    result = this.IsUserLocked(user.CrewID);
                }
            }
            return result;
        }

        /// <summary>
        /// The is user locked.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUserLocked(int id)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.FirstOrDefault(i => i.CrewID == id);
                if (user != null && user.LastBagLogin != null && user.BadLoginCount > 10)
                {
                    var span = (((DateTime)user.LastBagLogin).Subtract(DateTime.Now));
                    if (span < new TimeSpan(0, 0, 5, 0))
                    {
                        result = true;
                    }

                }
            }
            return result;
        }

        /// <summary>
        /// The increment user bad login count.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        public void IncrementUserBadLoginCount(string username)
        {
            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.FirstOrDefault(i => i.UserName == username);
                if (user != null)
                {
                    user.LastBagLogin = DateTime.Now;
                    ;
                    user.BadLoginCount++;
                    context.SaveChanges();
                }
            }
        }

        public void IncrementBadLoginCount(int userId)
        {
            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.FirstOrDefault(i => i.CrewID==userId);
                if (user != null)
                {
                    user.LastBagLogin = DateTime.Now;
                    ;
                    user.BadLoginCount++;
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// The null user bad login count.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        public void NullUserBadLoginCount(string username)
        {
            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.FirstOrDefault(i => i.UserName == username);
                if (user != null)
                {
                    user.LastBagLogin = null;
                    ;
                    user.BadLoginCount = null;
                    context.SaveChanges();
                }
            }
        }

        public async Task UpdateQuestion(New.SecurityQuestion entity)
        {
            if (entity != null)
            {
                using (var context = new SailingEntities())
                {
                    var dbResult =
                        await context.Debug_SecutityQuestions.FirstOrDefaultAsync(i => i.QuestionID == entity.ID);
                    dbResult.Answer = entity.Answer;
                    dbResult.Question = entity.Question;
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<int> InsertQuestion(New.SecurityQuestion entity)
        {
            int result = 0;
            if (entity != null)
            {
                using (var context = new SailingEntities())
                {
                    var dbResult = CommonFactory.CreateQuestion(entity);
                    context.Debug_SecutityQuestions.Add(dbResult);
                    await context.SaveChangesAsync();
                    result = dbResult.QuestionID;
                }
            }
            return result;
        }

        public async Task<bool> IsQuestionDuplicate(int userId, string question)
        {
            using (var context = new SailingEntities())
            {
                return context.Debug_SecutityQuestions.Any(
                        i => i.CrewID == userId && i.Question.Trim().ToLower() == question.Trim().ToLower());

            }
           
        }

        public IEnumerable<int> GetFleetIDsForCrew(int crewId)
        {
            List<int> result = new List<int>();
            using (var context = new SailingEntities())
            {
                var vessels = context.Logs_Sails.Where(i => i.Entities_Crew3.Any(j => j.CrewID == crewId)).Select(i => i.Entities_Vessel);
                if (vessels.Any())
                {
                    vessels = vessels.Distinct();
                    var temp = vessels.SelectMany(i => i.Logs_Fleet_Vessel.Select(j => j.FleetID)).Distinct();
                    result = temp.ToList();
                }

            }
            return result;
        }

        public bool MatchPW(string username, string password)
        {
            var result = false;
            using (var context = new SailingEntities())
            {
                var user =
                    context.Entities_Crew.Where(i => i.UserName == username).FirstOrDefault();

                if (user != null && user.PassWord != "null")
                {
                    //user = this.getCrewSecurity(user.CrewID);
                    var computedPW = RenderPassword(password, user.Salt);
                    var comparePW = user.PassWord;
                    return comparePW == computedPW;

                }
            }
            return result;
        }


        public New.CrewSecurity GetSecurity(string username, string password)
        {
            New.CrewSecurity result = null;
         using (var context = new SailingEntities())
            {                      
                        var user = context.Entities_Crew.Include(i => i.Entities_Vessel).Include(i => i.Entities_Vessel.Select(j => j.Entities_Crew)).Include(i => i.Entities_Vessel2).Include(i => i.Entities_Vessel2.Select(j => j.Entities_Crew)).Where(i => i.UserName == username && i.DateDeleted == null).FirstOrDefault();
                        //string guid = Guid.NewGuid().ToString();
                        //user.Session_Tokens.Clear();
                        //context.SaveChanges();
                        //var token = new Session_Tokens { GUID = guid, Time = DateTime.Now.AddHours(9) };
                        //// TODO ADD TIME
                        //user.Session_Tokens.Add(token);

                        
                        //    context.SaveChanges();

                if (user != null)
                {
                    result = Factories.CrewFactory.CreateCrewSecurity(user);
                    //entity.Token = token.GUID;
                }
            }
            return result;
        }

        public void SetUserSession(int userId, string guid)
        {
            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.Include(i => i.Entities_Vessel).Include(i => i.Entities_Vessel.Select(j => j.Entities_Crew)).Include(i => i.Entities_Vessel2).Include(i => i.Entities_Vessel2.Select(j => j.Entities_Crew)).Where(i => i.CrewID==userId).FirstOrDefault();
                var length = guid.Length;
                user.Session_Tokens.Clear();
                context.SaveChanges();
                var token = new Session_Tokens { GUID = guid, Time = DateTime.Now.AddHours(9) };
                user.Session_Tokens.Add(token);

                context.SaveChanges();


                var entity = Factories.CrewFactory.CreateCrewSecurity(user);
            }
        }

        public static string RenderPassword(string password, string salt)
        {
            // Convert the salt string and the password string to a single
            // array of bytes. Note that the password string is Unicode and
            // therefore may or may not have a zero in every other byte.
            var shash = salt.GetHashCode();
            var phash = password.GetHashCode();

            return string.Concat(phash, shash);

        }

        private Entities_Crew getCrewSecurity(int id)
        {
            using (var context = new SailingEntities())
            {
                var user = context.Entities_Crew.Where(i => i.CrewID==id&&i.DateDeleted==null).Include(i=>i.Entities_Vessel).Include(i=>i.Entities_Vessel1).Include(i=>i.Entities_Fleet).FirstOrDefault();
                if (user != null)
                {
                    return user;
                }
            }
            return null;
        }

        public New.CrewSecurity VerifyToken(string token)
        {
            using (var context = new SailingEntities())
            {
                var entityToken = context.Session_Tokens.Where(i => i.GUID == token).FirstOrDefault();
                if (entityToken != null&&entityToken.Entities_Crew!=null&&entityToken.Time>DateTime.Now)
                {
                    var user = entityToken.Entities_Crew;
                    if (user != null)
                    {
                        entityToken.Time = DateTime.Now.AddHours(9);
                        context.SaveChanges();
                        var result=Factories.CrewFactory.CreateCrewSecurity(user);
                        result.Token = token;
                        return result;
                    }
                }
                else if (entityToken != null && entityToken.Time < DateTime.Now)
                {
                    context.Session_Tokens.Remove(entityToken);
                    context.SaveChanges();
                }
            }
            return null;
        }

        public IEnumerable<Entities_Crew> GetAllCrew()
        {
            Entities_Crew[] result = null;
            using (var context = new SailingEntities())
            {
                result =
                    context.Entities_Crew.Where(i => i.UserName.ToLower() != "sysadmin" && i.DateDeleted == null)
                        .OrderBy(i => i.LastName)
                        .ToArray();
            }
            return result;
        }

        public IEnumerable<New.CrewFoundation> CrewSearch(GeneralSearchRequest query)
        {
            IEnumerable<New.CrewFoundation> result = null;
            using (var context = new SailingEntities())
            {
                var entities = context.Entities_Crew.Where(i => i.UserName != null);
                if (query.SelectDeleted != true)
                {
                     entities = entities.Where(i => i.DateDeleted == null);
                }
                if (!string.IsNullOrEmpty(query.EmailAddress))
                {
                    entities = entities.Where(i => query.EmailAddress.ToLower().Contains(i.EmailAddress.Trim().ToLower()));
                }
                if (query.MaxHours >= 0)
                {
                    entities = entities.Where(i => i.Logs_Sails.Sum(j => j.Hours) >= query.MaxHours);
                }

                if (query.FirstInitial != null)
                {
                    entities.Where(i => i.LastName.StartsWith(query.FirstInitial));
                }

                if (query.MaxMiles>0)
                {
                    entities.Where(i => i.Logs_Sails.Sum(j => j.Miles) >= query.MaxMiles);
                }
                if (query.MaxWind>0)
                {
                    entities.Where(i => i.Logs_Sails.Max(j => j.MaxWind) >= query.MaxWind);
                }
                if (!string.IsNullOrEmpty(query.FirstName))
                {
                    entities = entities.Where(i => query.FirstName.ToLower().Contains(i.FirstName.Trim().ToLower()));
                }
                if (query.FleetID != null&&query.FleetID>=0)
                {
                    entities = entities.Where(i => i.Entities_Fleet.Any(j => j.FleetID == query.FleetID));
                }
                if (query.ID != null && query.ID >= 0)
                {
                    entities = entities.Where(i => i.CrewID == query.ID);
                }
                if (!string.IsNullOrEmpty(query.LastName))
                {
                    entities = entities.Where(i => query.LastName.Trim().ToLower().Contains(i.LastName)); 
                }
                if (query.TagIDS != null && query.TagIDS.Any())
                {
                    entities = entities.Where(i=>i.Entities_Tags.Any()&&i.Entities_Tags.Any(j=>query.TagIDS.Contains(j.TagID)));
                }
                if (query.VesselID != null && query.VesselID >= 0)
                {
                    entities = entities.Where(i => i.Logs_Sails.All(j => j.Entities_Vessel.VesselID == query.VesselID));
                }
                result = entities.ToArray().Select(Factories.CrewFactory.CreateCrewFoundation);
            }
            return result;

        }

        public void AddTags(List<int> tagIds, int crewID)
        {
            using (var context = new SailingEntities())
            {
                List<Entities_Tags> tags = new List<Entities_Tags>();
                foreach (var tagId in tagIds)
                {
                    var tag = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                    if (tag != null && tag.Name != null)
                    {
                        tags.Add(tag);
                    }
                }

                var crew = (from i in context.Entities_Crew where i.CrewID == crewID select i).FirstOrDefault();
                if (crew != null && crew.CrewID != null)
                {
                    foreach (var tag in tags)
                    {
                        var match = crew.Entities_Tags.Where(i => i.TagID == tag.TagID).FirstOrDefault();
                        if (match == null || match.TagID <= 0)
                        {
                            crew.Entities_Tags.Add(tag);
                        }
                    }
                }
                context.SaveChanges();

            }

        }

        public New.Crew GetDetailedCrew(int id)
        {
            New.Crew result = null;
            using (var context=new SailingEntities())
            {

                var query =
                    from i in
                        context.Entities_Crew.Where(i => i.CrewID == id && i.DateDeleted == null)
                        .Include(k => k.Entities_Photos2)
                        .Include(
                            j =>
                            j.Entities_Photos)
                        .Include(k => k.Entities_Tags)
                        .Include(k => k.Entities_Comment5)
                        .Include(k=>k.Entities_Comment5.Select(l=>l.Entities_Crew))
                        .Include(k => k.Entities_Comment5.Select(l => l.Entities_Crew3))
                        .Include(k => k.Logs_Sails3)
                        .Include(l => l.Logs_Sails3.Select(o => o.Entities_Crew2))
                    select i;
                var entity = query.FirstOrDefault();
                entity.Entities_Photos3 = entity.Entities_Photos3.Where(i => i.DeleteDate == null).ToArray();
                entity.Entities_Comment5 = entity.Entities_Comment5.Where(i => i.DateDeleted == null).ToArray();
                entity.Logs_Sails3 = entity.Logs_Sails3.Where(i => i.DateDeleted == null).ToArray();
                var dbEntity = query.FirstOrDefault();
                if (dbEntity!=null)
                {
                    result=Factories.CrewFactory.CreateCrew(dbEntity);
                }
            }
            return result;
        }

        public static New.Crew GetSystemID()
        {
            New.Crew result = null;
            using (var context = new SailingEntities())
            {
                var query = from i in context.Entities_Crew where i.UserName == "sysadmin" select i;
                var dbentity = query.FirstOrDefault();
                if (dbentity != null)
                {
                    result = Factories.CrewFactory.CreateCrew(dbentity);
                }
            }
            return result;
        }


        public IEnumerable<New.PhotoBase> GetCrewPhotoLog(int id, int limit=10)
        {
            IEnumerable<New.PhotoBase> result = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Photos_Details.Include(j => j.Entities_Photos).Include(j => j.Entities_Photos.Logs_Photos_Likes).Include(j => j.Entities_Photos.Entities_Photos_Details) where i.CrewID ==id select i.Entities_Photos).Where(i=>i.DeleteDate==null).Take(limit);
                var dbEntity = query.ToArray();
                if (dbEntity != null)
                {
                    result = dbEntity.Select(Factories.PhotoFactory.CreatePhotoBase);

                }
            }
            return result;
        }

        public int GetCrewSailCount(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Crew where i.CrewID == id  &&i.DateDeleted==null select i.Logs_Sails.Count).FirstOrDefault();
                if (query != null)
                {
                    result = query;
                }
            }
            return result;
        
        }

        public New.SailPeriod GetStatsByPeriod(int year, int? month)
        {
            New.SailPeriod result = null;
            result.Year = year;

            using (var context = new SailingEntities())
            {

                var sails = context.Logs_Sails.Where(i=>i.Date.Year==year);
                if (month!=null)
                {
                    sails = sails.Where(i => i.Date.Month == month);
                    result.Month = month;
                }
                var dbcrew = sails.Select(i => i.Entities_Crew3.Where(j=>j==null)).FirstOrDefault().ToArray();
                New.Crew[] crew=null;
                if (dbcrew!=null&&dbcrew.Any())
                {
                 crew = dbcrew.Select(Factories.CrewFactory.CreateCrew).ToArray();
                }
                var dbVessel = sails.Select(i => i.Entities_Vessel).ToArray();
                New.VesselBase[] vessels = null;
                if (dbVessel != null)
                {
                    vessels = dbVessel.Select(Factories.FleetFactory.CreateVesselBase).ToArray();
                }
                if (crew != null)
                {
                    foreach (var member in crew)
                    {
                        var hours = (from i in sails where i.Entities_Crew3.Any(j => j.CrewID == member.ID) select i.Hours).Sum();
                        var miles = (from i in sails where i.Entities_Crew3.Any(j => j.CrewID == member.ID) select i.Miles).Sum();
                        var wind = (from i in sails where i.Entities_Crew3.Any(j => j.CrewID == member.ID) select i.MaxWind).Average();
                        var sailCount = (from i in sails where i.Entities_Crew3.Any(j => j.CrewID == member.ID) select i).Count();
                        result.CrewHours.Add(member, hours);
                        result.CrewAvgHighWinds.Add(member, wind);
                        result.CrewMiles.Add(member, miles);
                        result.CrewSails.Add(member, sailCount);
                    }
                }
                if (vessels != null)
                {
                    foreach (var vessel in vessels)
                    {
                        var hours = (from i in sails where i.Entities_Vessel.VesselID == vessel.ID select i.Hours).Sum();

                        var miles = (from i in sails where i.Entities_Vessel.VesselID == vessel.ID select i.Miles).Sum();
                        var wind = (from i in sails where i.Entities_Vessel.VesselID == vessel.ID select i.MaxWind).Average();
                        var sailCount = (from i in sails where i.Entities_Vessel.VesselID == vessel.ID select i).Count();
                        result.VesselHours.Add(vessel, hours);
                        result.VesselAvgHighWinds.Add(vessel, wind);
                        result.VesselMiles.Add(vessel, miles);
                        result.VesselSails.Add(vessel, sailCount);
                    }
                }

                
            }
            return result;
        }

        public decimal GetCrewMileCount(int id)
        {
            decimal result = 0;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Crew where i.CrewID == id select i.Logs_Sails.Sum(j=>j.Miles)).FirstOrDefault();
                if (query != null)
                {
                    result = query;
                }
            }
            return result;
        }
        public decimal GetCrewHourCount(int id)
        {
            decimal result = 0;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Crew
                             where i.CrewID == id
                             select i.Logs_Sails.Where(k => k.DateDeleted == null).Sum(j => j.Hours)).FirstOrDefault();
                if (query != null)
                {
                    result = query;
                }
            }
            return result;
        }

        public IEnumerable<New.CrewFoundation> GetCrewFoundationForVessel(IEnumerable<int> vesselIds)
        {
            IEnumerable<New.CrewFoundation> result=null;
            using (var context = new SailingEntities())
            {
                var crew = context.Logs_Sails.Where(i => vesselIds.Contains(i.Entities_Vessel.VesselID)).Distinct().Select(i => i.Entities_Crew3.Where(j=>j.DateDeleted==null)).Distinct().FirstOrDefault().ToArray();
                if (crew != null)
                {
                    result = crew.Select(Factories.CrewFactory.CreateCrewFoundation);

                }
                return result;
            }
            
        }



        public IEnumerable<int> GetSailIDSForCrew(IEnumerable<int> crewIDs)
        {
            IEnumerable<int> results = null;
            using (var context = new SailingEntities())
            {
                var query = context.Logs_Sails.Where(i => i.DateDeleted==null&& i.Entities_Crew3.Any(j => crewIDs.Contains(j.CrewID)));
                results = query.Select(j => j.SailID).ToArray();
            }
            return results;

        }

        public IEnumerable<SailingSiteModels.New.Crew> Get(GeneralSearchRequest query)
        {
            using (var context = new SailingEntities())
            {
                var dbEntity = from i in context.Entities_Crew.Include(j=>j.Logs_Fleet_Crew).Include(j=>j.Entities_Photos2).Include(j=>j.Entities_Photos1).Include(j=>j.Entities_Comment) select i;
                if (query.ID != null)
                { 
                    dbEntity=from i in dbEntity where i.CrewID==query.ID select i;
                }
                if (query.FirstName!=null)
                {
                    dbEntity=from i in dbEntity where i.FirstName.ToLower().Contains(query.FirstName.ToLower()) select i;
                }
                if (query.LastName != null)
                {
                    dbEntity = from i in dbEntity where i.LastName.ToLower().Contains(query.LastName.ToLower()) select i;
                }
                if (query.EmailAddress != null)
                {
                    dbEntity = from i in dbEntity where i.EmailAddress.ToLower().Contains(query.EmailAddress.ToLower()) select i;
                }
                if (query.VesselID != null)
                {
                    dbEntity = dbEntity.Where(i => i.Logs_Sails.Any(j => j.VesselID == query.VesselID));
                }
                if (query.FleetID != null)
                {
                    dbEntity = dbEntity.Where(i => i.Logs_Fleet_Crew.Any(j => j.FleetID == query.FleetID));
                }
                if (query.TagIDS != null && query.TagIDS.Any())
                {
                    dbEntity = dbEntity.Where(i => i.Entities_Tags1.Any(j => query.TagIDS.Contains(j.TagID)));
                }
                if (query.FirstInitial != null)
                {
                    dbEntity = dbEntity.Where(i => i.FirstName.StartsWith(query.FirstInitial));
                }
                return dbEntity.Select(Factories.CrewFactory.CreateCrew).ToArray();
            }
        }

        public bool CheckUserExists(string username, string email=null)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Crew.Where(i => i.UserName == username&&i.DateDeleted==null);
                if (email!=null&&!string.IsNullOrEmpty(email.Trim()))
                {
                    query=query.Where(i=>i.EmailAddress==email);
                }
                var entity=query.FirstOrDefault();
                if (entity != null)
                {
                    result = true;
                }
                return result;
            }
        }

        public int Create(SailingSiteModels.New.Crew entity)
        {
            using (var context = new SailingEntities())
            {
                var dbEntity = Factories.CrewFactory.CreateCrew(entity);
                context.Entities_Crew.Add(dbEntity);
                context.SaveChanges();
                return dbEntity.CrewID;
            }
  
        }

        public bool Update(SailingSiteModels.New.Crew entity)
        {
            using (var context = new SailingEntities())
            {
                var result = context.Entities_Crew.FirstOrDefault(i => i.CrewID == entity.ID);
                if (result != null)
                {
                    result.EmailAddress = entity.EmailAddress;
                    result.FirstName = entity.FirstName;
                    result.LastName = entity.LastName;
                    result.UserName = entity.UserName;
                    if (entity.Password != "Change Password")
                    {
                        result.PassWord = entity.Password;
                    }
                    if (entity.UserName != result.UserName)
                    {
                        if (CheckUserExists(entity.UserName))
                        {
                            throw new Exception("Username already exists in system.");
                        }
                    }
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool Delete(int id, int crewId)
        {
            using (var context = new SailingEntities())
            {
                var entity = context.Entities_Crew.Where(i => i.CrewID == id).FirstOrDefault();
                if (entity != null)
                {
                    entity.DateDeleted = DateTime.Today;
                    //TODO: ADD DELETED BY
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public void DeleteTag(int id, int crewID)
        {
            using (var context = new SailingEntities())
            {
                var crew = context.Entities_Crew.Where(i => i.CrewID == crewID).FirstOrDefault();
                var tag = crew.Entities_Tags.Where(i => i.TagID == id).FirstOrDefault();
                if (tag != null)
                {
                    crew.Entities_Tags.Remove(tag);
                    context.SaveChanges();
                }
            }
        }


        public void AddPhoto(int photoID, int entityId)
        {
            using (var context = new SailingEntities())
            {
                var dbEntity = context.Entities_Crew.FirstOrDefault(i => i.CrewID == entityId);
                var dbPhoto = context.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoID);
                if (dbEntity != null && dbPhoto != null)
                {
                    dbEntity.Entities_Photos1.Add(dbPhoto);
                    context.SaveChanges();
                }
            }
        }

        public void DeletePhoto(int photoID, int entityId)
        {
            using (var context = new SailingEntities())
            {
                var dbEntity = context.Entities_Crew.FirstOrDefault(i => i.CrewID == entityId);
                var dbPhoto = context.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoID);
                if (dbEntity != null && dbPhoto != null)
                {
                    dbEntity.Entities_Photos1.Remove(dbPhoto);
                    context.SaveChanges();
                }
            }
        }

        public void AddComment(int commentID, int entityID)
        {
            using (var context = new SailingEntities())
            {
                var dbentity = context.Entities_Crew.FirstOrDefault(i => i.CrewID == entityID);
                var dbcomment = context.Entities_Comment.FirstOrDefault(i => i.CommentID == commentID);
                if (dbcomment != null && dbentity != null)
                {
                    dbentity.Entities_Comment.Add(dbcomment);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<int> GetCommentsMade(int[] entityID)
        {
            List<int> result = new List<int>();
            using (var context = new SailingEntities())
            {
                foreach (var id in entityID)
                {
                    var sub =
                        context.Entities_Comment.Where(i => i.CrewID == id && i.DateDeleted == null)
                            .Select(j => j.CommentID)
                            .ToArray();
                    if (sub != null && sub.Any())
                    {
                        result.AddRange(sub);
                    }
                }
            }
            return result;
        }

        public New.CrewBase GetTestUser()
        {
            New.CrewBase result = null;
            using (var context = new SailingEntities())
            {
                var dbResult =
                    context.Entities_Crew.FirstOrDefault(
                        i => i.UserName == CrewRepository.TestUsername);
                if (dbResult == null)
                {
                    dbResult = new Entities_Crew()
                                      {
                                          DateDeleted = DateTime.Now,
                                          UserName = TestUsername,
                                          PassWord = "123FakeSt",
                                          EmailAddress = TestEmailAddress,
                                          FirstName = "Bruce",
                                          LastName = "TheTester"
                                      };
                    context.Entities_Crew.Add(dbResult);
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        var a = 0;
                    }
                }
                result = Factories.CrewFactory.CreateCrewBase(dbResult);
                return result;
            }
        }

        public int? GetSystemUserID()
        {
            int? result = null;
            using (var context = new SailingEntities())
            {
                result = context.Entities_Crew.FirstOrDefault(i => i.UserName == "sysadmin").CrewID;
                return result;
            }
        }

        public IEnumerable<int> GetCommentIDsForSail(IEnumerable<int> entityID)
        {
            List<int> result=new List<int>();
            using (var context = new SailingEntities())
            {
                foreach (var id in entityID)
                {
                    var sub =
                        context.Entities_Crew.Where(i => i.CrewID == id)
                            .Select(i => i.Entities_Comment3.Where(k => k.DateDeleted == null).Select(j => j.CommentID)).FirstOrDefault();
                    if (sub != null && sub.Any())
                    {
                        result.AddRange(sub);
                    }
                }
            }
            return result;
        }


        public List<string> SelectStartsWithIndex()
        {
            List<string> result=new List<string>();
            using (var context = new SailingEntities())
            {
                result =
                    context.Entities_Crew.Where(i => !string.IsNullOrEmpty(i.LastName))
                        .Select(i => i.LastName.Substring(0, 1).ToUpper()).Distinct()
                        .ToList();
            }

            return result;
        }

        public async Task UpdatePassword(int id, string salt, string password)
        {
            using (var context = new SailingEntities())
            {
                var user = await context.Entities_Crew.FirstOrDefaultAsync(i => i.CrewID == id);
                if (user != null)
                {
                    user.PassWord = password;
                    user.Salt = salt;
                    await context.SaveChangesAsync();
                }
            }

        }


        public async Task<bool> MatchAnswer(int id, string answer)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                var query = await context.Debug_SecutityQuestions.FirstOrDefaultAsync(i => i.QuestionID == id);
                if (query != null)
                {
                    result = query.Answer.Equals(answer, StringComparison.InvariantCultureIgnoreCase);
                }
            }
            return result;
        }

        public async Task<IEnumerable<New.SecurityQuestion>> GetQuestions(int crewId)
        {
            IEnumerable<New.SecurityQuestion> result = null;
            using (var context = new SailingEntities())
            {
                var dbEntries = await context.Debug_SecutityQuestions.Where(i => i.CrewID == crewId).ToArrayAsync();
                if (dbEntries != null && dbEntries.Any())
                {
                    result=dbEntries.Select(CommonFactory.CreateQuestion);

                }
            }
            return result;
        }

        public async Task CreateQuestions(IEnumerable<New.SecurityQuestion> questions)
        {
            if (questions!=null&&questions.Any())
            using (var context = new SailingEntities())
            {
                var dbEntities = questions.Select(CommonFactory.CreateQuestion);
                context.Debug_SecutityQuestions.AddRange(dbEntities);
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteQuestion(int id)
        {
            using (var context = new SailingEntities())
            {
                var dbEntity = await context.Debug_SecutityQuestions.FirstOrDefaultAsync(i => i.QuestionID == id);
                if (dbEntity != null)
                {
                    context.Debug_SecutityQuestions.Remove(dbEntity);
                    await context.SaveChangesAsync();
                }
            }
        }


        public async Task<int?> GetUserId(string username)
        {
            int? result = null;
            using (var context = new SailingEntities())
            {
                var dbEntity = await context.Entities_Crew.FirstOrDefaultAsync(i => i.UserName.ToLower() == username);
                if (dbEntity != null)
                {
                    result = dbEntity.CrewID;
                }
            }
            return result;
        }
    }
}