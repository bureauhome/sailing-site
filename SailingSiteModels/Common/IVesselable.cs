﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IVesselable.cs" company="">
//   
// </copyright>
// <summary>
//   The Vesselable interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The Vesselable interface.
    /// </summary>
    public interface IVesselable : IEntityModel
    {
        /// <summary>
        /// Gets or sets the vessels.
        /// </summary>
        IEnumerable<VesselFoundation> Vessels { get; set; }
    }
}
