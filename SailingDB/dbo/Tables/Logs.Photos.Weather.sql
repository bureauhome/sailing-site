﻿CREATE TABLE [dbo].[Logs.Photos.Weather] (
    [PhotoID]   INT NOT NULL,
    [WeatherID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Photos.Weather] PRIMARY KEY CLUSTERED ([PhotoID] ASC, [WeatherID] ASC),
    CONSTRAINT [FK_Logs.Photos.Weather_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]),
    CONSTRAINT [FK_Logs.Photos.Weather_Entities.Weather] FOREIGN KEY ([WeatherID]) REFERENCES [dbo].[Entities.Weather] ([WeatherID])
);

