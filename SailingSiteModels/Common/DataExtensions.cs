﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Common
{
    using System.Data.Entity.Spatial;

    public static class DataExtensions
    {

        public static DbGeography ToGPSCoordinates(this IGpsContainer entity)
        {
            DbGeography result = null;
            if (entity.Longitude != null && entity.Latitude != null)
            {
                string wkt = String.Format("POINT({0} {1})", entity.Longitude, entity.Latitude);

                result= DbGeography.PointFromText(wkt, 4326);
            }
            return result;
        }

    }
}
