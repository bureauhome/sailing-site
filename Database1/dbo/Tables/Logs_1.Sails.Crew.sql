﻿CREATE TABLE [dbo].[Logs.Sails.Crew] (
    [SailID] INT NOT NULL,
    [CrewID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Sails.Crew] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Sails.Crew] PRIMARY KEY CLUSTERED ([SailID] ASC, [CrewID] ASC),
    CONSTRAINT [FK_Logs.Sails.Crew_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Sails.Crew_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID]) ON DELETE CASCADE
);

