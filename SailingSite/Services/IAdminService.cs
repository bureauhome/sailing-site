﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAdminService.cs" company="">
//   
// </copyright>
// <summary>
//   The AdminService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using System;
    using System.Collections.Generic;

    using SailingSite.Models;

    /// <summary>
    /// The AdminService interface.
    /// </summary>
    public interface IAdminService
    {
        /// <summary>
        /// The get admin model.
        /// </summary>
        /// <returns>
        /// The <see cref="AdminModel"/>.
        /// </returns>
        AdminModel GetAdminModel();

        /// <summary>
        /// The get search return object.
        /// </summary>
        /// <returns>
        /// The <see cref="SearchReturnModel"/>.
        /// </returns>
        SearchReturnModel<T> GetSearchReturnObject<T>(
            Func<GeneralSearchRequest, T> funcMain,
            Func<List<String>> funcIndex);

    }
}
