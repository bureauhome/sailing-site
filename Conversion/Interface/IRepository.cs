﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SailingSiteModels.Common;

namespace Conversion
{
    interface IRepository<T> where T:IDatabaseModel
    {
        T get(object id);
        IEnumerable<T> GetAll(object id);
        int Insert(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}
