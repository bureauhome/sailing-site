﻿CREATE TABLE [dbo].[Logs.Sails.Weather] (
    [WeatherID] INT NOT NULL,
    [SailID]    INT NOT NULL,
    CONSTRAINT [PK_Logs.Sails.Weather] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Sails.Weather] PRIMARY KEY CLUSTERED ([WeatherID] ASC, [SailID] ASC),
    CONSTRAINT [FK_Logs.Sails.Weather_Entities.Weather] FOREIGN KEY ([WeatherID]) REFERENCES [dbo].[Entities.Weather] ([WeatherID]),
    CONSTRAINT [FK_Logs.Sails.Weather_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID]) ON DELETE CASCADE
);

