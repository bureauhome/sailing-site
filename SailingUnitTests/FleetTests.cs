﻿using System;
using SailingSiteModels.New;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SailingUnitTests
{
    using System.Linq;

    using SailingSite.Common;

    [TestClass]
    public class FleetTests : TestBase
    {
        /// <summary>
        /// The sterilize test.
        /// </summary>
        [TestInitialize]
        [TestCleanup]
        public void SterilizeTest()
        {
            this.SailRepository.Sterilize();
            this.LoggingRepository.PurgeTestData();
        }

        [TestMethod]
        [TestCategory("IntegrationTest")]
        public void TestVesselAdditionAndRemoval()
        {
            var context = this.Context;
            context.ActiveCrew.ID = 7;
            var fleet = GenerateRandomFleet();
            fleet.ID=this.SailService.CreateFleet(fleet, context);
            var vessel = this.GenerateVessel();
            vessel.ID=this.SailService.CreateVessel(vessel, context);
            this.SailService.AddVesselToFleet(this.GenerateVesselEntry(vessel), fleet.ID, context);
            var ids = this.SailService.VesselRepository.GetFleetIDSForVessel(vessel.ID);
            Assert.IsTrue(ids.Contains(fleet.ID));
            this.SailService.RemoveVesselFromFleet(vessel.ID,fleet.ID,context);
            var newId=this.SailService.VesselRepository.GetFleetIDSForVessel(vessel.ID);
            Assert.IsFalse(newId.Contains(fleet.ID));
            // verify it's still in the default list.
            Assert.IsTrue(newId.Contains(StaticTokens.Defaults.DefaultFleetId));
        }

        public static Fleet GenerateRandomFleet()
        {
            return new Fleet()
                       {
                           Name = UnitTestHelpers.GetRandomString(14),
                           Description = UnitTestHelpers.GetRandomString(12),
                           FoundedDate = DateTime.Today,
                           IsTest = true
                       };
        }
    }
}
