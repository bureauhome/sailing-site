﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSite.Services
{
    using System.Security.Cryptography.X509Certificates;

    interface ICommonService<Model, BaseModel>
    {
        int Create(Model entity);

        void Delete(int id, int userID);

        Model Get(int id);

        IEnumerable<BaseModel> GetAll(object search=null);
    }
}
