﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISingleLocationEntity.cs" company="">
//   
// </copyright>
// <summary>
//   The SingleLocationEntity interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using SailingSiteModels.New;

    /// <summary>
    /// The SingleLocationEntity interface.
    /// </summary>
    public interface ISingleLocationEntity
    {
        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        LocationBase Location { get; set; } 
    }
}
