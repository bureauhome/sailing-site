﻿CREATE TABLE [dbo].[Photos.Configuration](
	[PhotoID] [int] NOT NULL,
	[ConfigurationID] [int] NOT NULL,
 CONSTRAINT [PK_Photos.Configuration] PRIMARY KEY CLUSTERED 
(
	[PhotoID] ASC,
	[ConfigurationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Photos.Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Photos.Configuration_Entities.Config] FOREIGN KEY([ConfigurationID])
REFERENCES [dbo].[Entities.Config] ([ConfigID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Photos.Configuration] CHECK CONSTRAINT [FK_Photos.Configuration_Entities.Config]
GO

ALTER TABLE [dbo].[Photos.Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Photos.Configuration_Entities.Photos] FOREIGN KEY([PhotoID])
REFERENCES [dbo].[Entities.Photos] ([PhotoID])
ON DELETE CASCADE
GO