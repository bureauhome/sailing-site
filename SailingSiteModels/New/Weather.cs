﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Weather.cs" company="">
//   
// </copyright>
// <summary>
//   The weather.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;

    using SailingSiteModels.Common;

    /// <summary>
    /// The weather.
    /// </summary>
    [Serializable]
    public class Weather :WeatherBase, IDatabaseModel, ITaggableModel, IPhotographedEntity, ICommentableEntity
    {
        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return "Weather";
            }

        }

        /// <summary>
        /// Gets or sets the photos.
        /// </summary>
        public IEnumerable<PhotoBase> Photos { get; set; }

        /// <summary>
        /// Gets or sets the tags.
        /// </summary>
        public IEnumerable<Tags> Tags { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public IEnumerable<CommentBase> Comments { get; set; }

        /// <summary>
        /// Gets the primary key.
        /// </summary>
        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }


    }
}
