﻿CREATE TABLE [dbo].[Logs.Tags.Crew] (
    [CrewID] INT NOT NULL,
    [TagID]  INT NOT NULL,
    CONSTRAINT [PK_Logs.Tags.Crew] PRIMARY KEY CLUSTERED ([CrewID] ASC, [TagID] ASC),
    CONSTRAINT [FK_Logs.Tags.Crew_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Tags.Crew_Entities.Tags] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Entities.Tags] ([TagID]) ON DELETE CASCADE
);

