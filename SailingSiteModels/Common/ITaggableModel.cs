﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SailingSiteModels.New;

namespace SailingSiteModels.Common
{
    public interface ITaggableModel : IEntityModel
    {
        IEnumerable<Tags> Tags { get; set; }
    }
}
