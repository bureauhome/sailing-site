﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMetadataService.cs" company="">
//  
// </copyright>
// <summary>
//   The MetadataService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using SailingSite.Models;

    /// <summary>
    /// The MetadataService interface.
    /// </summary>
    public interface IMetadataService
    {
        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string Get(string key);

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Process(MetaData entity);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        void Delete(string key);
    }
}
