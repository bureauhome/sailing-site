﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.ComponentModel.Design;
    using System.Net;

    using SailingSite.Common;
    using SailingSite.ViewModels;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    public class VesselController : BaseController
    {
        //
        // GET: /Vessel/
        public ActionResult Index(int? fleetId=null)
        {
            this.GetSessionVariables();
            ViewData[Resources.SessionVariables.FleetNameViewBagVariable] = fleetId;
            var data = Sailservice.GetVesselBases(fleetId);
            
            return View(data);
        }

        [HttpGet]
        public ActionResult Get(int id)
        {
            var data=Sailservice.GetVessel(id);
            if (data != null)
            {
                return View(data);
            }
            else return this.View("NullObject");
        }

        public ActionResult GetSheetSail(int id)
        {
            var data = Sailservice.GetSheetSail(id);
            return this.PartialView("EditSheetSail", data);
        }

        public ActionResult Delete(int id, int? fleetId = null)
        {

            if (this.CurrentUser == null
                || (this.CurrentUser != null
                    && !this.Sailservice.isUserVesselCrewOrSecurity(id, this.CurrentUser.ID, true)))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "You must be logged in as a verified member to perform that action.");
            }

            var data = Sailservice.GetVesselBases(fleetId);
            this.FlagCacheUpdate(StaticTokens.CachingTokens.VesselEnum);
            return View("_VesselUL", data);
        }

        [HttpGet]
        public ActionResult New(int? id)
        {

            if (this.CurrentUser == null || this.CurrentUser.SID < Crew.SIDEnum.PowerUser)
            {
                this.LoggingService.GenerateLog("Fail-User attempted to create a vessel with no permissions.", Log.LogTypes.AuditAdmin, this.Context);
                var redirectModel = new RedirectModel() { ActionName = "Index", ControllerName = "Admin", UserBlocked = true, Description = "You do not have permission to access this page." };
                return this.View("NoPermissionPage");
            }
            this.GetSessionVariables();
            Vessel data = null;
            if (id == null || id < 1)
            {
                data = this.Sailservice.JiffyVessel(this.Context);
            }
            else
            {
                data=this.Sailservice.GetVessel((int)id);
            }

            
            
            return View(data);
        }

        /// <summary>
        /// The add sheet sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult AddSheetSail(SheetSail entity)
        {
            // adds the sheetsail then 
             this.Sailservice.AddSheetSailToExistingVessel(entity, this.Context);
            var result = this.Sailservice.GetVessel(entity.Vessel.ID);
            return this.PartialView("ExistingSheetSail", result);
        }

        /// <summary>
        /// The update sheet sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPatch]
        public ActionResult UpdateSheetSail(SheetSail entity)
        {
            this.Sailservice.UpdateSheetSail(entity, this.Context);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpDelete]
        public ActionResult DeleteSheetSail(int id)
        {
            this.Sailservice.DeleteSheetSail(id, this.Context);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult SearchByLetter(string letter)
        {
            var request = new GeneralSearchRequest();
            if (string.IsNullOrEmpty(letter) || !char.IsLetter(letter[0]))
            {
                this.Sailservice.SearchVessel(request);
            }
            request.FirstInitial = letter;
            var data = this.Sailservice.SearchVessel(request);
            return this.View("_VesselSearch", data);

        }

        /// <summary>
        /// The edit sheet sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpGet]
        public ActionResult EditSheetSail(int id)
        {
            var result = this.Sailservice.GetSheetSail(id);
            return this.View(result);
        }

        /// <summary>
        /// The create new sheet sail.
        /// </summary>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpGet]
        public ActionResult CreateNewSheetSail(int parentId)
        {
            var result = new SheetSail() { Vessel = new Vessel() { ID = parentId } };
            return this.PartialView("EditSheetSail", result);
        }

        [HttpPost]
        public ActionResult AddTags(int id, string tags)
        {
            Sailservice.AddVesselTags(id,tags);
            var result = Sailservice.GetVesselTag(id).ToArray();
            this.ViewData["controller"] = "vessel";
            this.ViewData["entityID"] = id;
            base.FlagCacheUpdate(StaticTokens.CachingTokens.TagEnum);
            return this.PartialView("TagList", result);
        }

        [HttpPost]
        public ActionResult DeleteTag(int id, int entityId)
        {
            Sailservice.DeleteVesselTag(id, entityId);
            var result = Sailservice.GetVesselTag(id).ToArray();
            ViewData["controller"] = "animal";
            ViewData["entityID"] = id;
            return PartialView("TagList", result);
        }

        public void AddPhoto(int entityid, int photodId)
        {
            Sailservice.AddVesselPhoto(photodId, entityid);
        }

        public void DeletePhoto(int photoId, int entityId)
        {
            Sailservice.DeleteVesselPhoto(photoId, entityId);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="tagList">
        /// The tag list.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        [HttpPost]
        public ActionResult Create(SailingSiteModels.New.Vessel entity, string tagList, HttpPostedFileBase file = null)
        {
            if (this.CurrentUser == null
                || (this.CurrentUser != null
                    && this.Sailservice.GetCrewSID(this.CurrentUser.ID) < Crew.SIDEnum.StandardUser))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "You must be logged in as a verified member to perform that action.");
            }

            if (file != null&&entity!=null)
            {
                var photo = new SailingSiteModels.New.Photo() { Name = entity.Name, FileName = file.FileName, EntityName = entity.Name, Subdirectory = Resources.SessionVariables.ImageFolderVessel };
                photo.ID = this.PhotoService.SavePhoto(file, photo, this.Context, Server.MapPath("~/imagedir"));
                entity.Photo = photo;
            }
            if (entity.ID > 0)
            {
                if (this.Sailservice.isUserVesselCrewOrSecurity(entity.ID,this.CurrentUser.ID, true))
                Sailservice.UpdateVessel(entity);
            }
            else
            {
                entity.ID = Sailservice.CreateVessel(entity, this.Context);
            }

            base.FlagCacheUpdate(StaticTokens.CachingTokens.VesselEnum);

            Sailservice.AddVesselTags(entity.ID, tagList);
            this.FlagCacheUpdate(StaticTokens.CachingTokens.VesselEnum);
            return this.RedirectToAction("Index");
        }

	}
}