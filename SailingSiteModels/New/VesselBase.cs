﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VesselBase.cs" company="">
//   
// </copyright>
// <summary>
//   The vessel base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    using SailingSiteModels.Common;

    /// <summary>
    /// The vessel base.
    /// </summary>
    public class VesselBase : VesselFoundation, IEntityModel
    {
        // TODO: Add eqpt. here!

        /// <summary>
        /// The length over all.
        /// </summary>
        private decimal? lengthOverAll;

        /// <summary>
        /// Gets or sets the mile total.
        /// </summary>
        public decimal MileTotal { get; set; }


        /// <summary>
        /// Gets or sets the crew total.
        /// </summary>
        public int CrewTotal { get; set; }

        /// <summary>
        /// The name of the model.
        /// </summary>
        [StringLength(50)]
        public string Model { get; set; }

        /// <summary>
        /// A list of photos.
        /// </summary>
        public IEnumerable<Photo> Photos { get; set; }

        /// <summary>
        /// Gets or sets the sail total.
        /// </summary>
        public int NumberOfMasts { get; set; }

        /// <summary>
        /// The fleets.
        /// </summary>
        public IEnumerable<FleetBase> Fleets { get; set; }

        /// <summary>
        /// The beam length of the vessel.
        /// </summary>
        public decimal Beam { get; set; }

        /// <summary>
        /// The manufactury date of the vessel.
        /// </summary>
        public DateTime? ManufacturedDate { get; set; }

        /// <summary>
        /// Gets or sets the sail locker.
        /// </summary>
        public IEnumerable<SheetSail> SailLocker { get; set; }

        /// <summary>
        /// Gets or sets the length over all.
        /// </summary>
        public decimal? LengthOverAll
        {
            get
            {
                if (this.lengthOverAll != null)
                {
                    return this.lengthOverAll;
                }

                if (this.Length != null)
                {
                    return (decimal)this.Length;
                }

                return null;
            }

            set
            {
                this.lengthOverAll = value;
                if (value != null)
                {
                    this.Length = (double)value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the length water line.
        /// </summary>
        public decimal? LengthWaterLine { get; set; }

        /// <summary>
        /// The get random photo.
        /// </summary>
        /// <returns>
        /// The <see cref="Photo"/>.
        /// </returns>
        public Photo GetRandomPhoto()
        {
            Photo result = null;
            if (Photos != null && Photos.Any())
            {
                result = Photos.ElementAtOrDefault(Helpers.Rand.Next(0, Photos.Count() - 1));
            }

            return result;
        }

        public Vessel ToVessel()
        {
            return new Vessel()
                       {
                           Beam = this.Beam,
                           SailLocker = this.SailLocker,
                           ManufacturedDate = this.ManufacturedDate,
                           Fleets = this.Fleets,
                           NumberOfMasts = this.NumberOfMasts,
                           Photos = this.Photos,
                           Model = this.Model,
                           CrewTotal = this.CrewTotal,
                           MileTotal = this.MileTotal,
                           Length = this.Length,
                           LengthOverAll = this.LengthOverAll,
                           LengthWaterLine = this.LengthWaterLine,
                           ID = this.ID,
                           Rig = this.Rig,
                           Name = this.Name,
                           Description = this.Description,
                           Captain = this.Captain,
                           Hull = this.Hull
                       };
        }


    }
}
