﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using Newtonsoft.Json.Linq;

    using SailingSite.Factories;

    /// <summary>
    /// The animal repository.
    /// </summary>
    public class AnimalRepository : IAnimalRepository
    {
        public List<SailingSiteModels.New.PhotoBase> GetAnimalPhotosForCategory(string categoryId)
        {
            List<New.PhotoBase> result = null;
            using (var context = new SailingEntities())
            {
                var animals = context.Entities_Animals_Names.Where(i => i.AnimalCategory == categoryId);
                if (animals.Any())
                {
                    var photos =
                        context.Entities_Photos.Where(i => i.Entities_Animals_Names.Any(j => animals.Contains(j)));
                    if (photos != null && photos.Any())
                    {
                        result = photos.Select(PhotoFactory.CreatePhotoBase).ToList();
                    }
                }
                
            }
            return result;
        }

        public AnimalRepository()
        { 
        
        }

        public void AddAnimalPhoto(int animalId, int photoId)
        {
            using (var context = new SailingEntities())
            {
                var animal = context.Entities_Animals_Names.FirstOrDefault(i => i.AnimalID == animalId);
                if (animal != null)
                {
                    var photo = context.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoId);
                    if (photo != null)
                    {
                        animal.Entities_Photos1.Add(photo);
                        context.SaveChanges();
                    }
                }
            }
        }

        public bool CheckDuplicate(New.Animal entity)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                result =
                    context.Entities_Animals_Names.Any(
                        i => i.AnimalName == entity.Name && i.AnimalCategory == entity.Category);
            }

            return result;
        }

        public New.AnimalDetails GetDetailedAnimal(int id)
        {
            New.AnimalDetails result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Animals_Names.Where(i => i.AnimalID == id).Include(i => i.Entities_Tags).Include(i => i.Entities_Photos).Include(i => i.Logs_Animals_Alias);
                var dbEntities = query.FirstOrDefault();
                if (dbEntities != null)
                { 
                    result=Factories.AnimalFactory.CreateAnimalDetails(dbEntities);
                    var commentquery=(from i in query.Include(i => i.Entities_Comment) select i.Entities_Comment.OrderByDescending(k=>k.DateCreated).Take(10).Select(j => j.CommentID));
                    result.CommentIDs =commentquery.FirstOrDefault().ToArray();
                    var sailquery=(from i in query.Include(i => i.Logs_Sails) select i.Logs_Sails.OrderByDescending(k=>k.Date).Take(10).Select(k => k.SailID));
                    result.SailIDs = sailquery.FirstOrDefault().ToArray();
                }
            }
            return result;
        }

        public int[] GetAnimalComments(int id)
        {
            using (var context = new SailingEntities())
            {
                int[] result = null;
                var query = from i in context.Entities_Animals_Names where i.AnimalID == id select i.Entities_Comment.Select(j => j.CommentID);
                result = query.FirstOrDefault().Take(10).ToArray();
                return result;
            }
        }

        public void DeleteTag(int id, int animalID)
        {
            using (var context = new SailingEntities())
            {
                var trial = (from i in context.Entities_Tags where i.TagID == id select i).FirstOrDefault();
                if (trial != null)
                {
                    var animal = (from i in trial.Entities_Animals_Names where i.AnimalID == animalID select i).FirstOrDefault();
                    if (animal != null)
                    {
                        trial.Entities_Animals_Names.Remove(animal);
                        context.SaveChanges();
                    }


                }
            }
        }

        public void AddAnimalComment(int commentId, int animalId)
        {
            using (var context = new SailingEntities())
            {
                var comment =
                    (from i in context.Entities_Comment where i.CommentID == commentId select i).FirstOrDefault();
                if (comment != null)
                {
                    var animal = (from i in context.Entities_Animals_Names where i.AnimalID == animalId select i).FirstOrDefault();
                    if (animal != null)
                    {
                        animal.Entities_Comment.Add(comment);
                        context.SaveChanges();
                    }
                }
            }
        }

        public void AddTags(List<int> tagIds, int animalId)
        {
            using (var context = new SailingEntities())
            {
                List <Entities_Tags> tags= new List<Entities_Tags>();
                foreach (var tagId in tagIds)
                {
                    var tag = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                    if (tag != null && tag.Name != null)
                    {
                        tags.Add(tag);
                    }
                }

                    var animal = (from i in context.Entities_Animals_Names where i.AnimalID == animalId select i).FirstOrDefault();
                    if (animal != null && animal.AnimalName != null)
                    {
                        foreach (var tag in tags)
                        {
                            var match = animal.Entities_Tags.Where(i => i.TagID == tag.TagID).FirstOrDefault();
                            if (match == null || match.TagID <= 0)
                            {
                                animal.Entities_Tags.Add(tag);
                            }
                        }
                    }
                    context.SaveChanges();
                
            }

        }

        public IEnumerable<New.Tags> GetTags(int animalID)
        {
            IEnumerable<New.Tags> results = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Animals_Names where i.AnimalID==animalID select i.Entities_Tags.Where(k=>k.DateDeleted==null)).FirstOrDefault();
                if (query != null)
                {
                    results = query.Select(Factories.CommonFactory.CreateTag);
                }
            }
            return results;
        }

        public IEnumerable<New.AnimalCategory> GetAllCategories()
        {
            IEnumerable<New.AnimalCategory> retVal=null;
            using (var context = new SailingEntities())
            {
                retVal =
                    (from i in
                         context.Entities_Animals_Categories.Include(i => i.Entities_Animals_Names)
                         .Include(i => i.Entities_Animals_Categories1)
                         .Include(i => i.Entities_Animals_Names.Select(j => j.Entities_Photos1))
                         .Include(i => i.Entities_Animals_Names.Select(j => j.Logs_Animals_Alias))
                     where i.DateToDelete == null
                     select i).Select(Factories.AnimalFactory.CreateAnimalCategory).ToArray();
            }
            return retVal;
        }
        public bool CreateAnimalCategory(SailingSiteModels.New.AnimalCategory entity)
        {
            using (var context = new SailingEntities())
            {
                if (entity != null)
                {
                    var dcEntity = Factories.AnimalFactory.CreateAnimalCategory(entity);
                    context.Entities_Animals_Categories.Add(dcEntity);
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public int CreateAnimal(SailingSiteModels.New.Animal entity, Entities_Animals_Categories category)
        {
            using (var context = new SailingEntities())
            {
                var dcEntity = Factories.AnimalFactory.CreateAnimal(entity);

                context.Entities_Animals_Names.Add(dcEntity);
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                }
                return dcEntity.AnimalID;
            }
            

        }

        public New.Animal GetAnimal(int id)
        {
            New.Animal result = null;
            using (var context = new SailingEntities())
            {
                var dbResult =
                    context.Entities_Animals_Names.Include(i => i.Entities_Tags)
                        .Include(i => i.Entities_Comment)
                        .Include(i => i.Entities_Comment.Select(t => t.Entities_Crew))
                        .Include(i => i.Entities_Photos)
                        .Include(i => i.Logs_Animals_Alias)
                        .Include(i=>i.Logs_Sails).Include(i=>i.Logs_Sails.Select(j=>j.Entities_Crew2))
                        .FirstOrDefault(i => i.AnimalID == id);
                if (dbResult != null && dbResult.Entities_Comment != null)
                {
                    dbResult.Entities_Comment = dbResult.Entities_Comment.Where(i => i.DateDeleted == null).ToArray();
                }
                if (dbResult != null)
                {
                    result = Factories.AnimalFactory.CreateAnimal(dbResult);
                }

            }
            return result;

        }



        public IEnumerable<New.Animal> GetAnimals(GenericQuery query)
        {
            IEnumerable<New.Animal> result = null;
            using (var context = new SailingEntities())
            {
                var retVal = context.Entities_Animals_Names.Include(i=>i.Entities_Comment).Include(i=>i.Entities_Photos1).Include(i=>i.Logs_Animals_Alias).Where(i => i.DateToDelete == null);
                if (query != null)
                {
                    if (query.AnimalID != null)
                    {
                        retVal = retVal.Where(i => i.AnimalID == query.AnimalID);
                    }
                    if (query.Name != null)
                    {
                        retVal =
                            retVal.Where(
                                i => i.AnimalName.Equals(query.Name, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (query.Name != null)
                    {
                        retVal =
                            retVal.Where(
                                i => i.AnimalName.Equals(query.Name, StringComparison.CurrentCultureIgnoreCase));
                    }
                    if (query.Category != null)
                    {
                        retVal =
                            retVal.Where(
                                i => i.AnimalCategory.Equals(query.Category, StringComparison.CurrentCultureIgnoreCase));
                    }
                }
                var entities = retVal.ToArray();
                if (entities != null)
                {
                    result = entities.Select(Factories.AnimalFactory.CreateAnimal);
                }
                return result;
            }
            //TODO convert to animalbase

        }


        public New.AnimalCategory GetCategory(string name)
        {
            using (var context = new SailingEntities())
            {
                var retVal = (from i in context.Entities_Animals_Categories.Include(i => i.Entities_Animals_Names).Include(i => i.Entities_Animals_Names.Select(j => j.Entities_Photos1)).Include(i => i.Entities_Animals_Names.Select(j => j.Logs_Animals_Alias)) where i.AnimalCategory==name select i).FirstOrDefault();
                
                var result= Factories.AnimalFactory.CreateAnimalCategory(retVal);
                result.Subcategories = GetSubCategories(name);
                return result;
            }
        }

        public IEnumerable<New.AnimalCategory> GetSiblingCategories(string name)
        {
            using (var context = new SailingEntities())
            {
                IEnumerable<New.AnimalCategory> result=null;
                string id = (from i in context.Entities_Animals_Categories where i.AnimalCategory==name select i.ParentCategory).FirstOrDefault();
               
                    result = (from i in context.Entities_Animals_Categories where i.ParentCategory == id select i).Select(Factories.AnimalFactory.CreateAnimalCategory).ToArray();
                
                return result;
            }
           
        }

        public IEnumerable<SailingSiteModels.New.AnimalCategory> GetSubCategories(string name)
        {
            using (var context = new SailingEntities())
            {
                var result = from i in context.Entities_Animals_Categories where i.ParentCategory == name select i;
                return result.Select(Factories.AnimalFactory.CreateAnimalCategory).ToList();
            }
        }


        public New.AnimalCategory GetTopLevelCategories()
        {
            SailingSiteModels.New.AnimalCategory result = null;
            using (var context = new SailingEntities())
            { 
                
                var categories=(from i in context.Entities_Animals_Categories where i.ParentCategory==null select i).Select(Factories.AnimalFactory.CreateAnimalCategory).ToArray();
                result = new New.AnimalCategory { Name = "Animals", Subcategories = categories==null||!categories.Any()?null:categories };
                return result;
            }
        }


        public IEnumerable<New.AnimalCategory> GetParentCategories()
        {
            using (var context = new SailingEntities())
            {
                var result= (from i in context.Entities_Animals_Categories where i.ParentCategory == null && i.DateToDelete==null select i);
                return (IEnumerable<New.AnimalCategory>)result.ToList();
            }
        }

        public string UpdateCategory(New.AnimalCategory entity)
        {
            using (var context = new SailingEntities())
            {
                var dbContext = (from i in context.Entities_Animals_Categories where i.AnimalCategory == entity.Name select i).FirstOrDefault();
                if (dbContext != null)
                {
                    dbContext.AnimalCategory = entity.Name;
                    dbContext.Entities_Animals_Categories2 = (from i in context.Entities_Animals_Categories where i.AnimalCategory == entity.ParentName select i).FirstOrDefault();
                    dbContext.Description = entity.Description;
                    context.SaveChanges();
                    return dbContext.ParentCategory;
                }
                else
                {
                    throw new Exception("Category Not Found");
                }
            }
            
            return null;
        }

        public void UpdateAnimal(New.Animal entity)
        {
            using (var context = new SailingEntities())
            {
                var dbContext = context.Entities_Animals_Names.FirstOrDefault(i => i.AnimalID == entity.ID);

                if (dbContext != null)
                {
                    dbContext.Description = entity.Description;
                    dbContext.AnimalName = entity.Name;
                    dbContext.AnimalCategory = entity.Category;
                    if (entity.Photo!=null&&entity.Photo.ID != null)
                    {
                        dbContext.PhotoID = entity.Photo.ID;
                    }
                    if (entity.Photo != null)
                    {
                        dbContext.Entities_Photos = Factories.PhotoFactory.CreatePhoto(entity.Photo.ToPhoto());
                    }


                    if (entity.Aliases.Any() || dbContext.Logs_Animals_Alias.Any())
                    {
                        dbContext.Logs_Animals_Alias.Clear();
                        foreach (var alias in entity.Aliases)
                        {
                            var dbAlias = new Logs_Animals_Alias() { Alias = alias, AnimalID = dbContext.AnimalID, Entities_Animals_Names = dbContext };
                            dbContext.Logs_Animals_Alias.Add(dbAlias);
                        }
                    }
                }

                context.SaveChanges();
            }
        }

        
        public void DeleteAnimalCategory(string id)
        {
            using (var context = new SailingEntities())
            {
                var entity = (from i in context.Entities_Animals_Categories where i.AnimalCategory == id select i).FirstOrDefault();
                if (entity != null)
                {
                    entity.DateToDelete = DateTime.Now.AddMonths(1);
                }
                context.SaveChanges();
            }
        }

        public string DeleteAnimal(int id)
        {
            using (var context = new SailingEntities())
            {
                var entity = (from i in context.Entities_Animals_Names where i.AnimalID == id select i).FirstOrDefault();
                if (entity != null)
                {
                    entity.DateToDelete = DateTime.Now;
                }
                context.SaveChanges();
                return entity.AnimalCategory;
            }
        }

        public string DeleteCategory(string name)
        {
            using (var context = new SailingEntities())
            {
                var entity = (from i in context.Entities_Animals_Categories where i.AnimalCategory == name select i).FirstOrDefault();
                if (entity != null)
                {
                    context.Entities_Animals_Categories.Remove(entity);
                }
                context.SaveChanges();
                return entity.ParentCategory;
            }
        }

        public void NukeTempData()
        {
            using (var context = new SailingEntities())
            {
                var data =
                    context.Entities_Animals_Names.Where(
                        i =>
                        i.AnimalName=="Gullywump").ToArray();

                foreach (var entity in data)
                {
                    entity.Entities_Tags.Clear();
                    entity.Entities_Comment.Clear();
                    entity.Entities_Photos1.Clear();
                    entity.Logs_Animals_Alias.Clear();
                    context.Entities_Animals_Names.Remove(entity);
                }
                context.SaveChanges();
            }
        }


        public IEnumerable<New.Animal> Search(GenericQuery request)
        {
            IEnumerable<New.Animal> result = null;
            using (var context = new SailingEntities())
            {
                var query =
                    context.Entities_Animals_Names.Where(i => i.AnimalName != null);

                if (!string.IsNullOrEmpty(request.FirstInitial))
                {
                    query = query.Where(i => i.AnimalName.StartsWith(request.FirstInitial));
                }

                if (!string.IsNullOrEmpty(request.Name))
                {
                    query = query.Where(i => i.AnimalName == request.Name);
                }

                if (query != null)
                {
                    result = query.Select(AnimalFactory.CreateAnimal).ToArray();
                }
            }

            return result;
        }

        public List<string> GetFirstInitials()
        {
            List<string> result = new List<string>();
            using (var context = new SailingEntities())
            {
                result = context.Entities_Animals_Names.Where(i => !string.IsNullOrEmpty(i.AnimalName)).Select(i => i.AnimalName.Substring(0, 1).ToUpper()).Distinct().ToList();
            }
            return result;
        }
    }
}