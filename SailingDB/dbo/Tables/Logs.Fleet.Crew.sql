﻿CREATE TABLE [dbo].[Logs.Fleet.Crew] (
    [FleetID]     INT          NOT NULL,
    [CrewID]      INT          NOT NULL,
    [Description] VARCHAR (50) NULL,
    [Ordinal]     INT          NULL,
    CONSTRAINT [PK_Logs.Fleet.Crew] PRIMARY KEY CLUSTERED ([FleetID] ASC, [CrewID] ASC),
    CONSTRAINT [FK_Logs.Fleet.Crew_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Fleet.Crew_Entities.Fleet] FOREIGN KEY ([FleetID]) REFERENCES [dbo].[Entities.Fleet] ([FleetID]) ON DELETE CASCADE
);

