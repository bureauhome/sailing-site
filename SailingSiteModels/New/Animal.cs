﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    using System.Collections;

    using SailingSiteModels.Common;

    public class Animal : AnimalBase, IPhotographedEntity
    {
        public Animal()
        {
            Aliases = new List<string>();
        }

        public Animal(Animal patchAnimal, string aliases)
        {
            ID = patchAnimal.ID;
            Name = patchAnimal.Name;
            Description = patchAnimal.Description;
            Category = patchAnimal.Category;
            processAliases(aliases);
        }

        public bool IsAssociated { get; set; }

        /// A list of photos.
        /// </summary>
        public IEnumerable<PhotoBase> Photos { get; set; }

        /// <summary>
        /// A list of comments.
        /// </summary>
        public IEnumerable<CommentBase> Comments { get; set; }
    }

}
