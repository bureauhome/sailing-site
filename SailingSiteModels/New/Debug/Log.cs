﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;


namespace SailingSiteModels.New.Debug
{
    using System.IO;
    using System.Runtime.CompilerServices;

    using Newtonsoft.Json;

    /// <summary>
    /// The log.
    /// </summary>
    public class Log
    {
        /// <summary>
        /// The log types.
        /// </summary>
        public enum LogTypes
        {
            /// <summary>
            /// The audit.
            /// </summary>
            Audit,

            /// <summary>
            /// The audit create.
            /// </summary>
            AuditCreate,

            /// <summary>
            /// The audit delete.
            /// </summary>
            AuditDelete,

            AuditAdmin,

            AuditLogin,

            AuditLoginError,

            AuditLogout,

            AuditLockOut,

            AuditLog,

            /// <summary>
            /// The audit update.
            /// </summary>
            AuditUpdate,

            /// <summary>
            /// The user error.
            /// </summary>
            UserError,

            /// <summary>
            /// The error.
            /// </summary>
            SystemError,

            /// <summary>
            /// The maintenance.
            /// </summary>
            Maintenance
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public LogTypes Type { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets or sets the correlation id.
        /// </summary>
        public string CorrelationID { get; set; }

        /// <summary>
        /// Gets or sets the exception id.
        /// </summary>
        public string ExceptionID { get; set; }

        /// <summary>
        /// Gets or sets the exception message.
        /// </summary>
        public string ExceptionMessage { get; set; }

        /// <summary>
        /// Gets or sets the active user.
        /// </summary>
        public string ActiveUser { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the view name.
        /// </summary>
        public string ViewName { get; set; }

        /// <summary>
        /// Gets or sets the request.
        /// </summary>
        public string EntryPoint { get; set; }

        /// <summary>
        /// Gets or sets the verb.
        /// </summary>
        public string Verb { get; set; }

        /// <summary>
        /// Gets the JSON store of the entity.  Use attach entity to attach an entity.
        /// </summary>
        public string AttachedEntity { get; set; }

        /// <summary>
        /// Attached an entity via JSON
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void AttachEntity(object entity)
        {
            try
            {
                this.AttachedEntity = JsonConvert.SerializeObject(
                    entity,
                    new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                if (this.AttachedEntity.Length > 4000)
                {
                    this.AttachedEntity = this.AttachedEntity.Substring(0, 3998);
                }
            }
            catch (Exception ex)
            {
                this.ExceptionMessage = string.Format(
                    "{0}... Error attaching entity - {1}",
                    this.ExceptionMessage,
                    ex.Message);
                // if anything happens here, skip it.
            }
        }
    }
}
