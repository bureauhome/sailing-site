﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    using SailingSiteModels.Common;

    /// <summary>
    /// A concrete model of a ship's bearing.  DECREMENTED
    /// </summary>
    [Serializable]
    [Obsolete("Use BearingTaken", true)]

    public class CompassBearing 
    {
        /// <summary>
        /// The primary key.
        /// </summary>
        public int ID {get; set;}
        /// <summary>
        /// The animal name.
        /// </summary>
        private CardinalDirections cardinalDirection;
        private int bearing;
        /// <summary>
        /// The bearing, returned in integer format.
        /// </summary>
        public decimal Bearing { get; set; }

        /// <summary>
        /// The time/date of the reading.
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Our empty constructor
        /// </summary>
        public CompassBearing()
        {}
        
        public string GPSCoords { get; set; }


        /// <summary>
        /// Our populated constructor
        /// <param name="input">the bearing in integer format</param>
        /// <param name="time">the time of the reading</param>
        /// </summary>
        public CompassBearing(int input, DateTime time)
        {
            Bearing = input;
            Date = time;
        }

        /// <summary>
        /// Our populated constructor
        /// <param name="input">the bearing in cardinal enum format.</param>
        /// <param name="time
        public CompassBearing(CardinalDirections input, DateTime time)
        {
            Bearing = input.ToCompassBearing();
            Date = time;
        }


        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        } 
    }
}
