﻿CREATE TABLE [dbo].[Entities.Vessel] (
    [VesselID]               INT            IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (50)  NOT NULL,
    [Length]                 DECIMAL (5, 2) NOT NULL,
    [Beam]                   DECIMAL (5, 2) NOT NULL,
    [CreatedDate]            DATETIME       NULL,
    [Model]                  NVARCHAR (50)  NULL,
    [Manufacturer]           NVARCHAR (50)  NULL,
    [HullColor]              NCHAR (10)     NULL,
    [LetterColor]            NCHAR (10)     NULL,
    [SailColors]             NVARCHAR (50)  NULL,
    [SailNumbers]            NVARCHAR (50)  NULL,
    [HullNumbers]            NVARCHAR (50)  NULL,
    [DistinguishingFeatures] NVARCHAR (100) NULL,
    [Rig]                    NVARCHAR (50)  NULL,
    CONSTRAINT [pk_VesselID] PRIMARY KEY CLUSTERED,
    CONSTRAINT [pk_VesselID] PRIMARY KEY CLUSTERED ([VesselID] ASC)
);

