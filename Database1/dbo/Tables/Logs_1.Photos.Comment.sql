﻿CREATE TABLE [dbo].[Logs.Photos.Comment] (
    [CommentID] INT NOT NULL,
    [PhotoID]   INT NOT NULL,
    CONSTRAINT [PK_Logs.Photos.Comment] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Photos.Comment] PRIMARY KEY CLUSTERED ([CommentID] ASC, [PhotoID] ASC),
    CONSTRAINT [FK_Logs.Photos.Comment_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]),
    CONSTRAINT [FK_Logs.Photos.Comment_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE
);

