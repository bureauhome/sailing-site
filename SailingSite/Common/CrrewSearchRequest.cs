﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CrrewSearchRequest.cs" company="">
//   
// </copyright>
// <summary>
//   The general search request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite
{
    /// <summary>
    /// The general search request.
    /// </summary>
    public class GeneralSearchRequest : GenericQuery
    {
        /// <summary>
        /// Gets or sets the max wind.
        /// </summary>
        public int MaxWind { get; set; }

        /// <summary>
        /// Gets or sets the max miles.
        /// </summary>
        public int MaxMiles { get; set; }

        /// <summary>
        /// Gets or sets the max hours.
        /// </summary>
        public int MaxHours { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the vessel id.
        /// </summary>
        public int? VesselID { get; set; }

        /// <summary>
        /// Gets or sets the fleet id.
        /// </summary>
        public int? FleetID { get; set; }
    }
}