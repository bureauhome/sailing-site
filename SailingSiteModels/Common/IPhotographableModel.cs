﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPhotographableModel.cs" company="">
//   
// </copyright>
// <summary>
//   The PhotographableModel interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The PhotographableModel interface.
    /// </summary>
    public interface IPhotographableModel : IEntityModel
    {
        /// <summary>
        /// Gets or sets the photos.
        /// </summary>
        IEnumerable<PhotoBase> Photos { get; set; }
    }
}
