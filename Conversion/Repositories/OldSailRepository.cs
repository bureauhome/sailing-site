﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Old = SailingSiteModels.Old;
using New = SailingSiteModels.New;
using OldDataConnection;

namespace Conversion.Repositories
{
    class OldSailRepository
    {
        public static List<Old.Sail> OldSailList = new List<Old.Sail>();
        private Repositories.DateRepository daterepo = new DateRepository();

        public OldSailRepository()
        {
            foreach (var date in Repositories.DateRepository.SailDates)
            {
                Console.WriteLine("Searching old database for date: {0}",date.ToShortDateString());
                try {
                    var oldSail=OldSQL.SQLStuff.GetSailingDate(date).FirstOrDefault().ToOldSail();
                    Console.WriteLine("Sail found: Maxwind: {0}, MinWind: {1}, Comments: {1}",oldSail.WindHigh,oldSail.WindLow, oldSail.Comment);
                     var exists=from x in OldSailList where x.Date==date select x;
                     if (exists!=null)
                     {
                        OldSailList.Add(oldSail);
                         Console.WriteLine("Sail logged!");
                     }
                }
                catch (Exception e)
                {
                    string ex=e.InnerException==null? e.Message:e.InnerException.Message;
                    Console.WriteLine("An error occured! {0}",ex);
                    return;

                }
            }
        }
    }
}
