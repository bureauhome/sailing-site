﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Repositories.Interfaces
{
    using SailingSiteModels.New;

    public interface ILocationRepository: ICommentableRepository, IPhotographableRepository, ITaggableRepository, ICommonRepository<Entities_Locations, SailingSiteModels.New.Location, SailingSiteModels.New.LocationBase>
    {
        IEnumerable<LandMarkBase> GetLandMarks();

        LandMarkBase GetLandMarkBase(int id);

        IEnumerable<LandMarkBase> SearchLandmarks(GenericQuery request);

        List<string> GetLandMarkFirstInitials();

        IEnumerable<LocationBase> SearchLocations(GenericQuery request);

        List<string> GetLocationFirstInitials();

        void DeleteLandmark(int id, int crewid);

        int CreateLandmark(LandMarkBase entity);

        void UpdateLandmark(LandMarkBase entity);

        void Update(Location entity);
    }
}