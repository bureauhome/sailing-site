﻿CREATE TABLE [dbo].[Entities.Entity] (
    [EntityID]   INT           IDENTITY (1, 1) NOT NULL,
    [EntityName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [pk_EntityID] PRIMARY KEY CLUSTERED,
    CONSTRAINT [pk_EntityID] PRIMARY KEY CLUSTERED ([EntityID] ASC)
);

