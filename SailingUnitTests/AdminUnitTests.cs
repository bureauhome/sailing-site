﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SailingUnitTests
{
    using System.Linq;

    using SailingSite.Models;
    using SailingSite.Services;

    [TestClass]
    public class AdminUnitTests
    {
        [TestMethod]
        public void TestAdminModel()
        {
            var AdminService = new AdminService();
            var result = AdminService.GetAdminModel();
            Assert.IsNotNull(result);
            this.TestSearchReturnModel(result.AnimalList);
            this.TestSearchReturnModel(result.CrewList);
            this.TestSearchReturnModel(result.FleetList);
            this.TestSearchReturnModel(result.LandMarkList);
            this.TestSearchReturnModel(result.VesselList);
            this.TestSearchReturnModel(result.LocationList);
        }

        private void TestSearchReturnModel<T>(SearchReturnModel<T> entity)
        {
            Assert.IsNotNull(entity);
            Assert.IsNotNull(entity.Result);
            Assert.IsNotNull(entity.StartsWithIndex);
            Assert.IsTrue(entity.StartsWithIndex.Any());
            Assert.IsInstanceOfType(entity.Result, typeof(T));
        }
    }
}
