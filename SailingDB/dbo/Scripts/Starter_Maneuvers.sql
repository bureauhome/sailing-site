﻿USE [Sailing]
GO
DELETE FROM [dbo].[Entities.Maneuvers];

SET IDENTITY_INSERT [dbo].[Entities.Maneuvers] ON;

INSERT INTO [dbo].[Entities.Maneuvers]
           ([ManeuverID]
		   ,[Name]
           ,[Description])
     VALUES
           (0,'Tacking'
           ,'The process of crossing the wind.'),
 (1,'Wearing/Jibing'
           ,'The process of going around the wind.'),
 (2,'Crew Overboard'
           ,'The practice of rescuing crewmembers.'),
		    (3,'Teaching'
           ,'Teaching or demonstrating sailing skills.'),
		    (4,'Reefing'
           ,'Preparing a sail for higher winds.'),
	 (5,'Bouy Slalom'
           ,'Runnig a vessel around one of more bouys.'),
		   (6,'Anchoring'
           ,'Anchoring a vessel.'),
 (7,'Mooring'
           ,'Mooring a vessel.')
GO


