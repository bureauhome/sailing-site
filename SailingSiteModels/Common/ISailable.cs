﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Common
{
    using System.Security.Cryptography.X509Certificates;

    using SailingSiteModels.New;

    public interface ISailable
    {
        IEnumerable<SailBase> Sails { get; set; }
    }
}
