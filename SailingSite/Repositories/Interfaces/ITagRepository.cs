﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    public interface ITagRepository
    {

        New.TagHydrated GetHydratedTag(int id);
        int? SaveTag(New.Tags entity, Entities_Tags tags=null);
        IEnumerable<string> GetAllTagsByString();
        IEnumerable<New.Tags> GetAllTags();
        New.Tags GetTag(TagRequest request);

        void DeleteTag(int id);
    }
}