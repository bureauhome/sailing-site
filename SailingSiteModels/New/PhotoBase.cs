﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PhotoBase.cs" company="">
//   
// </copyright>
// <summary>
//   The photo base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System.Web.Configuration;

    using SailingSiteModels.Common;

    /// <summary>
    /// The photo base.
    /// </summary>
    public class PhotoBase : IEntityModel, IFileBase
    {
        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return "Photo";
            }
        }

        public bool IsTest { get; set; }

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the subdirectory.
        /// </summary>
        public string Subdirectory { get; set; }

        /// <summary>
        /// Gets or sets the entity name.
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the thumb path.
        /// </summary>
        public string ThumbPath { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The details.
        /// </summary>
        public PhotoDetails Details;

        public int Width { get; set; }

        public int Height { get; set; }

        public string CarouselPath { get; set; }

        public Photo ToPhoto()
        {
            Photo result = new Photo
                               {
                                   Path = this.Path,
                                   CarouselPath = this.Path,
                                   Width=this.Width,
                                   Height = this.Height,
                                   Name = this.Name,
                                   Description = this.Description,
                                   ThumbPath = this.ThumbPath,
                                   EntityName = this.EntityName,
                                   Subdirectory = this.Subdirectory,
                                   FileName = this.FileName,
                                   ID = this.ID
                               };
            return result;
        }

        public string RenderedMainPath()
        {
            var result= this.Path.Replace(System.IO.Directory.GetCurrentDirectory(), "");
            result = result.Replace(
                "C:\\Users\\bbureau\\Documents\\sailing-site\\SailingSite\\",
                WebConfigurationManager.AppSettings["Url"]);
            result=result.Replace("c:\\",WebConfigurationManager.AppSettings["Url"]);
            return result;
        }

        public string RenderedThumbpath()
        {

            var result = this.ThumbPath.Replace(System.IO.Directory.GetCurrentDirectory(), "");
            result = result.Replace(
                "C:\\Users\\bbureau\\Documents\\sailing-site\\SailingSite\\",
                WebConfigurationManager.AppSettings["Url"]);
            result = result.Replace("c:\\", WebConfigurationManager.AppSettings["Url"]);
            return result;
        }

        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
    }
}
