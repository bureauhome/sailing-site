﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    using System.ComponentModel.DataAnnotations;

    using SailingSiteModels.Common;

    public class SecurityQuestion: IEntityModel
    {

        public int ID { get; set; }

        public string ControllerName
        {
            get { return "Login"; }
        }

        public string DefaultDelete
        {
            get
            {
                return "DeleteQuestion";
            }
        }

        public string DefaultSet
        {
            get
            {
                return "GetQuestion";
            }
        }

        //[Required]
        //[StringLength(255, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string Question { get; set; }

        public int CrewID { get; set; }
        
        //[Required]
        //[StringLength(255, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string Answer { get; set; }
 

    }
}
