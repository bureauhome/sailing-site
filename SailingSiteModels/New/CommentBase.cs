﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommentBase.cs" company="">
//   
// </copyright>
// <summary>
//   The comment base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using SailingSiteModels.Common;
    using SailingSiteModels.GPX;

    /// <summary>
    /// The comment base.
    /// </summary>
    public class CommentBase : IUserCreatable, IEntityModel, ITargetable, IMetadata
    {

                public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }

        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return "Comment";
            }
        }

        /// <summary>
        /// Gets or sets the target controller.
        /// </summary>
        public string TargetController { get; set; }

        /// <summary>
        /// Gets the wind color.
        /// </summary>
        public string WindColor
        {
            get
            {
                decimal result = 0;
                if (this.WindSpeed != null)
                {
                    result = (decimal)this.WindSpeed;
                }
                return result.ToWindColorClass();
            }
        }
        // TODO: Eventually break this out!  Have wind be its own model!!
        /// <summary>
        /// The contains sail readings.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ContainsSailReadings()
        {
            return (Longitude != null && Latitude != null) || Leeway != null || SpeedMph != null
                   || CompassBearing != null;
        }

        /// <summary>
        /// The contains weather readings.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ContainsWeatherReadings()
        {
            return WindSpeed != null || CloudCover != null || RainSeverity != null || SeaState != null
                   || Barometer != null;
        }

        /// <summary>
        /// The contains ancillary observations.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ContainsAncillaryObservations()
        {
            return Sounding != null;
        }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        [Required(ErrorMessage = "Please add a valid date and time.")]
        [DataType(DataType.DateTime)]
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets the createdby id.
        /// </summary>
        public int CreatedbyID { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        public CrewFoundation ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        public CrewFoundation CreatedBy { get; set; }

        /// <summary>
        /// The subject css.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SubjectCSS()
        {
            string result = "commentSubject";
            if (this.IsLog)
            {
                if (this.IsCoordinateOnly)
                {
                    result = "trackSubject";
                }
                else
                {
                    result = this.Longitude == null && this.Latitude == null ? "logSubject" : "logAndTrackSubject";
                }
            }

            return result;
        }

        /// <summary>
        /// The potential header.
        /// </summary>
        internal class PotentialHeader
        {
            /// <summary>
            /// The value.
            /// </summary>
            public string Value;

            /// <summary>
            /// The prefix.
            /// </summary>
            public string Prefix;

            /// <summary>
            /// Initializes a new instance of the <see cref="PotentialHeader"/> class.
            /// </summary>
            public PotentialHeader()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="PotentialHeader"/> class.
            /// </summary>
            /// <param name="val">
            /// The val.
            /// </param>
            /// <param name="prefix">
            /// The prefix.
            /// </param>
            public PotentialHeader(string val, string prefix)
            {
                Value = val;
                Prefix = prefix;
            }
        }

        /// <summary>
        /// The rendered subject line.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string RenderedSubjectLine()
        {
            string result = "No Subject";
            if (this.Subject != null)
            {
                result = this.Subject;
            }
            else
            {
                List<PotentialHeader> rankedSubjects = new List<PotentialHeader>()
                                                           {
                                                               new PotentialHeader(
                                                                   this.Message, 
                                                                   string.Empty), 
                                                               new PotentialHeader(
                                                                   this.Title, 
                                                                   string.Empty), 
                                                               new PotentialHeader(
                                                                   this.GeoCoordinates(), 
                                                                   "Coordinates:"), 
                                                               new PotentialHeader(
                                                                   this.WindSpeed.ToString(), 
                                                                   "Wind logged:"), 
                                                               new PotentialHeader(
                                                                   this.SeaState.ToString(), 
                                                                   "Sea state logged:"), 
                                                               new PotentialHeader(
                                                                   this.RainSeverity.ToString(), 
                                                                   "Rain logged:"), 
                                                               new PotentialHeader(
                                                                   this.CloudCover.ToString(), 
                                                                   "Cloud cover logged:")
                                                           };
                result = GetResultFromStrings(rankedSubjects, "No Subject", 50);
            }

            var renderedDate = this.Created.ToString("yyyy/MM/dd HH:mm");
            return string.Format("{0}:{1}", result, renderedDate);
        }

        /// <summary>
        /// The get result from strings.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <param name="defaultResult">
        /// The default result.
        /// </param>
        /// <param name="chars">
        /// The chars.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetResultFromStrings(List<PotentialHeader> entities, string defaultResult, int chars)
        {
            foreach (var pair in entities)
            {
                if (!string.IsNullOrWhiteSpace(pair.Value))
                {
                    return string.Format("{0}{1}", pair.Prefix, pair.Value.Substring(0, chars));
                }
            }

            return defaultResult;
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether log flag.
        /// </summary>
        public bool LogFlag { get; set; }

        /// <summary>
        /// Gets or sets the log id.
        /// </summary>
        public int? LogId { get; set; }

        /// <summary>
        /// Gets or sets the updateby id.
        /// </summary>
        public int? UpdatebyID { get; set; }

        /// <summary>
        /// Gets or sets the updated.
        /// </summary>
        public DateTime? Updated { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        [Range(-180, 180)]
        public double? Longitude { get; set; }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        [Range(-90, 90)]
        public double? Latitude { get; set; }

        /// <summary>
        /// Gets or sets the elevation.
        /// </summary>
        public double? Elevation { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        [Required(ErrorMessage = "Please add a subject.")]
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the gps coords.
        /// </summary>
        public string GPSCoords { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is coordinate only.
        /// </summary>
        public bool IsCoordinateOnly { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentBase"/> class.
        /// </summary>
        public CommentBase()
        {
        }

        /// <summary>
        /// The geo coordinates.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GeoCoordinates()
        {
            if (Longitude == null || Latitude == null)
            {
                return null;
            }

            var latSuffix = Latitude < 0 ? "S" : "N";
            var longSuffix = Longitude < 0 ? "W" : "E";
            var result = string.Format(
                "{0}{1} {2}{3}", 
                latSuffix, 
                Math.Abs((double)Latitude), 
                Math.Abs((double)Longitude), 
                longSuffix);
            return result;
        }

        public string RenderGoogleCoords()
        {
            string result = null;
            if (this.Longitude != null && this.Latitude != null)
            {
                result = string.Format("{{lat: {0}, lng: {1}}}", this.Latitude, this.Longitude);
            }
            return result;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentBase"/> class.
        /// </summary>
        /// <param name="entry">
        /// The entry.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        public CommentBase(GpxTrackPoint entry, string key)
        {
            this.Created = entry.Date;
            this.IsLog = true;
            this.CompassBearing = (decimal)entry.Coordinate.Course;
            this.Elevation = entry.Coordinate.Altitude;
            this.Latitude = entry.Coordinate.Latitude;
            this.MilesLogged = (decimal)entry.TotalMiles;
            this.Longitude = entry.Coordinate.Longitude;
            this.TrackKey = key;
            this.SpeedMph = entry.SpeedMPH;
        }

        /// <summary>
        /// The merge trackwith log.
        /// </summary>
        /// <param name="entry">
        /// The entry.
        /// </param>
        public void MergeTrackPoint(GpxTrackPoint entry)
        {
            this.Elevation = entry.Coordinate.Altitude;
            this.Latitude = entry.Coordinate.Latitude;
            this.Longitude = entry.Coordinate.Longitude;
        }

            /// <summary>
        /// Average temperature in Farenheit.
        /// </summary>
        [Range(-25, 180, ErrorMessage = "Please input a valid temperature.")]
        public decimal? TempF { get; set; }

        /// <summary>
        /// Bearing.
        /// </summary>
        [Range(0, 360, ErrorMessage = "Bearing Must Be Between 0 and 360.")]
        public decimal? CompassBearing
        {
            get
            {
                return this.compassBearing;
            }

            set
            {
                if (value < 0 || compassBearing > 360)
                {
                    this.compassBearing = 0;
                }
                else
                {
                    this.compassBearing = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the sounding.
        /// </summary>
        [Range(0, 40000)]
        public decimal? Sounding { get; set; }

        /// <summary>
        /// The maximum wind recorded.
        /// </summary>
        [Range(0, 180, ErrorMessage = "Please input a valid maximum wind.")]
        public decimal? WindSpeed { get; set; }

        /// <summary>
        /// Percentage of heel.
        /// </summary>
        [Range(0, 360, ErrorMessage = "Heel % Must Be Between 0 and 360.")]
        public decimal? HeelPct { get; set; }

        /// <summary>
        /// Gets or sets the speed mph.
        /// </summary>
        public double? SpeedMph { get; set; }

        /// <summary>
        /// Percentage the centerboard was dropped.
        /// </summary>
        [Range(0, 100, ErrorMessage = "Centerboard % Must Be Between 0 and 100.")]
        public decimal? CenterboardPct { get; set; }

        /// <summary>
        /// Gets or sets the wind direction.
        /// </summary>
        public decimal? WindDirection { get; set; }

        /// <summary>
        /// The average barometer reading
        /// </summary>
        [Range(0, 33, ErrorMessage = "Please enter a valid barometric pressure reading (in inches hygrometer.)")]
        public decimal? Barometer { get; set; }

        /// <summary>
        /// The total number of miles sailed.
        /// </summary>
        [Range(0, 1800, ErrorMessage = "Please input a valid number for miles sailed.")]
        public decimal? MilesLogged { get; set; }

        /// <summary>
        /// The contains weather info.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ContainsWeatherInfo()
        {
            return WindSpeed != null || WindDirection != null || RainSeverity != null || CloudCover != null
                   || Barometer != null || SeaState != null;
        }

        /// <summary>
        /// The contains secondary info.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ContainsSecondaryInfo()
        {
            return HeelPct != null || CenterboardPct != null || Sounding != null;
        }

        /// <summary>
        /// Gets or sets the percentage of cloud cover.
        /// </summary>
        [Range(0, 100, ErrorMessage = "Please input a number between 0 and 100 for cloud cover.")]
        public int? CloudCover
        {
            get
            {
                return cloudCover;
            }

            set
            {
                if (value > 100)
                {
                    cloudCover = 100;
                }
                else if (value < 0)
                {
                    cloudCover = 0;
                }
                else
                {
                    cloudCover = value;
                }
            }
        }

        /// <summary>
        /// The severity of the rain at time of recording..
        /// </summary>
        [Range(0, 10, ErrorMessage = "Please input a number between 0 and 10 for rain.")]
        public int? RainSeverity
        {
            get
            {
                return rainSeverity;
            }

            set
            {
                if (value < 0)
                {
                    rainSeverity = 0;
                }
                else if (value > 10)
                {
                    rainSeverity = 10;
                }
                else
                {
                    rainSeverity = value;
                }
            }
        }

        public CrewFoundation HelmsPerson { get; set; }

        /// <summary>
        /// The rain severity.
        /// </summary>
        private int? rainSeverity;

        /// <summary>
        /// The sea state.
        /// </summary>
        private int? seaState;

        /// <summary>
        /// The cloud cover.
        /// </summary>
        private int? cloudCover;

        /// <summary>
        /// The compass bearing.
        /// </summary>
        private decimal? compassBearing;



        /// <summary>
        /// The maximum wind recorded.
        /// </summary>
        [Range(0, 9, ErrorMessage = "Please input a valid number for sea state (1-9).")]
        public int? SeaState
        {
            get
            {
                return seaState;
            }

            set
            {
                if (value < 0)
                {
                    seaState = 0;
                }
                else if (value > 9)
                {
                    seaState = 9;
                }
                else
                {
                    seaState = value;
                }
            }
        }

        // TODO: IMPLEMENT SEA STATE, CLOUD COVER, RAIN SEVERITY from DB CONNECT WIND SPEED, SEA STATE W/AJAX! REFACTOR READINGS INTO CLASSES
        /// <summary>
        /// Gets or sets the track key.
        /// </summary>
        public string TrackKey { get; set; }

        /// <summary>
        /// Gets or sets the leeway.
        /// </summary>
        public int? Leeway { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is log.
        /// </summary>
        public bool IsLog { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public IEnumerable<CommentBase> Comments { get; set; }

        /// <summary>
        /// The to comment.
        /// </summary>
        /// <returns>
        /// The <see cref="Comment"/>.
        /// </returns>
        public Comment ToComment()
        {
            return new Comment
                       {
                           Created = this.Created, 
                           CreatedBy = this.CreatedBy, 
                           ModifiedBy = this.ModifiedBy, 
                           ID = this.ID, 
                           CreatedbyID = this.CreatedbyID, 
                           Message = this.Message, 
                           Title = this.Title, 
                           Subject = this.Subject, 
                           WindSpeed = this.WindSpeed, 
                           WindDirection = this.WindDirection, 
                           Barometer = this.Barometer, 
                           CompassBearing = this.CompassBearing, 
                           CloudCover = this.CloudCover, 
                           HeelPct = this.HeelPct, 
                           Sounding = this.Sounding, 
                           CenterboardPct = this.CenterboardPct, 
                           RainSeverity = this.RainSeverity, 
                           MilesLogged = this.MilesLogged, 
                           TempF = this.TempF, 
                           Longitude = this.Longitude, 
                           Latitude = this.Latitude, 
                           LogId = this.LogId, 
                           Elevation = this.Elevation, 
                           SpeedMph = this.SpeedMph, 
                           SeaState = this.SeaState, 
                           IsLog = this.IsLog, 
                           HelmsPerson = this.HelmsPerson,
                           IsCoordinateOnly = this.IsCoordinateOnly, 
                           TrackKey = this.TrackKey, 
                           Leeway = this.Leeway
                       };
        }
    }
}
