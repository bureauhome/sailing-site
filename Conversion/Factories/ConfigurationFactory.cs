﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using New = SailingSiteModels.New;
using Old = SailingSiteModels.Old;
using OldDataConnection;

namespace Conversion.Factories
{
    
    class ConfigurationFactory
    {
        public static New.Configuration CreateConfiguration(Old.Configuration entity)
        {
            return CreateConfiguration(entity.Name);
        }
    

        public static New.Configuration CreateConfiguration(string entity)
        {
            Old.Configuration old = new Old.Configuration() { Name = entity };
            New.Configuration retVal= new New.Configuration(old);
            int id=OldSQL.SQLStuff.AddNewConfig(retVal);
            switch (id)
            { 
                case -4:
                    Console.WriteLine("Duplicate detected.");
                    break;
                case -1:
                    Console.WriteLine("Error occured.");
                    throw new Exception("SQL ERROR OCCURED.  EXITING.");
                    break;
                default:
                    Console.WriteLine("New SQL entity created with ID: {0}", id);
                    break;
            }
            retVal.SetID(id);
            return retVal;

        }
    }
}
