﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conversion.Service
{
    class SailingService
    {
        Repositories.ConfigurationRepository ConfigRepo=new Repositories.ConfigurationRepository();
        Repositories.LocationRepository LocationRepo = new Repositories.LocationRepository();
        Repositories.ManeuverRepository ManeuverRepo = new Repositories.ManeuverRepository();
        Repositories.CrewRepository CrewRepo = new Repositories.CrewRepository();
        Repositories.VesselRepository VesselRepo = new Repositories.VesselRepository();
        Repositories.OldSailRepository SailRepo = new Repositories.OldSailRepository();
        public SailingService()
        {
            Console.WriteLine("Background work completed.  Logging sails in new database.");
            try
            {
                foreach (var sail in Repositories.OldSailRepository.OldSailList)
                {
                    Factories.SailingFactory.CreateSail(sail, VesselRepo, CrewRepo);
                }
                Console.WriteLine("Work completed.");
            }
            catch (Exception ex)
            { 
                string e=ex.InnerException==null?ex.Message:ex.InnerException.Message;
                Console.WriteLine("Error writing new entities to database. {0}", e);

            }
        }
    }
}
