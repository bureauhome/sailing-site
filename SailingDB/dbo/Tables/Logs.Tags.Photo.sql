﻿CREATE TABLE [dbo].[Logs.Tags.Photo] (
    [PhotoID] INT NOT NULL,
    [TagID]   INT NOT NULL,
    CONSTRAINT [PK_Logs.Tags.Photo] PRIMARY KEY CLUSTERED ([PhotoID] ASC, [TagID] ASC),
    CONSTRAINT [FK_Logs.Tags.Photo_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Tags.Photo_Entities.Tags] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Entities.Tags] ([TagID]) ON DELETE CASCADE
);

