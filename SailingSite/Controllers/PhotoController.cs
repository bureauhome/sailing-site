﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.Net;
    using System.Threading.Tasks;

    public class PhotoController : BaseController
    {
        // GET: Photo
        public ActionResult Index()
        {
            throw new NotImplementedException();
        }

        public ActionResult NewPhoto()
        {
            var result = new SailingSiteModels.New.Photo();
            return this.PartialView("_PhotoNew", result);
        }

        public async Task<ActionResult> GetBase(int id)
        {
            var data= await this.PhotoService.GetPhotoBase(id);
            return this.PartialView("PhotoControls/ViewPhoto", data);
        }

        public async Task<ActionResult> PurgePhoto(int id)
        {
            if (this.CurrentUser != null)
            {
                await this.PhotoService.PurgePhoto(id);
                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden,"You must be logged in to delete a photo.");
            }
        }

    }
}