﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Repositories
{
    using System.Data.Entity;

    public class StatRepository
    {

        public async void CalculateStats(int id)
        {
            using (var context = new SailingEntities())
            {
                decimal HighWindMean = await context.Logs_Sails.AverageAsync(i => i.MaxWind);
                int AverageTilt = (int)await context.Logs_Sails.Where(i => i.MaxTilt > 0).AverageAsync(i => i.MaxTilt);
                decimal AverageMPH = await context.Logs_Sails.Where(i => i.Miles > 0 && i.Hours > 0).AverageAsync(i => i.Miles / i.Hours);
                List<decimal> highwindSDvalues = new List<decimal>();
                List<decimal> mphSDvalues = new List<decimal>();
                List<double> tiltSDvalues = new List<double>();
                await context.Logs_Sails.ForEachAsync(
                    i =>
                    {
                        if (i.MaxWind > 0)
                        {
                            var a = Math.Pow((double)(i.MaxWind - HighWindMean), 2);
                            highwindSDvalues.Add((decimal)a);
                        }
                    });
                await context.Logs_Sails.ForEachAsync(
                    i =>
                    {
                        if (i.Miles > 0 && i.Hours > 0)
                        {
                            var a = Math.Pow((double)((i.Miles / i.Hours) - AverageMPH), 2);
                            mphSDvalues.Add((decimal)a);
                        }
                    });
                await context.Logs_Sails.ForEachAsync(
                    i =>
                    {
                        if (i.MaxTilt > 0)
                        {
                            var a = Math.Pow((double)(i.MaxTilt - AverageTilt), 2);
                            tiltSDvalues.Add(a);
                        }
                    });
                decimal HighWindStandardDeviation = 0;
                if (highwindSDvalues != null && highwindSDvalues.Any())
                {
                    HighWindStandardDeviation = (decimal)Math.Sqrt((double)highwindSDvalues.Average(i => i));
                }
                decimal TiltStandardDeviation = 0;
                if (tiltSDvalues != null && tiltSDvalues.Any())
                {
                    TiltStandardDeviation = (decimal)Math.Sqrt((double)tiltSDvalues.Average(i => i));
                }
                decimal MPHStandardDeviation = 0;
                if (mphSDvalues != null && mphSDvalues.Any())
                {
                    MPHStandardDeviation = (decimal)Math.Sqrt((double)mphSDvalues.Average(i => i));
                }



            }
        }
    }
}