﻿CREATE TABLE [dbo].[Logs.Photos.Animal] (
    [PhotoID]  INT NOT NULL,
    [AnimalID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Photos.Animal] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Photos.Animal] PRIMARY KEY CLUSTERED ([PhotoID] ASC, [AnimalID] ASC),
    CONSTRAINT [FK_Logs.Photos.Animal_Entities.Animals.Names] FOREIGN KEY ([AnimalID]) REFERENCES [dbo].[Entities.Animals.Names] ([AnimalID]),
    CONSTRAINT [FK_Logs.Photos.Animal_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE
);

