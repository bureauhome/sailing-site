﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite
{
    using SailingSite.Common;
    using SailingSite.Models;

    public interface IAnimalService
    {
        void AddTags(int animalID, string tagStr);
        IEnumerable<New.CommentBase> GetAnimalComments(int animalID);
        void AddAnimalComment(New.Comment comment, int animalID);
        void DeleteAnimalTag(int id, int animalID);
        IEnumerable<New.Tags> GetAnimalTags(int animalID);
        New.AnimalDetails GetDetailedAnimal(int id);
        IEnumerable<New.AnimalCategory> GetSiblingCategories(string name);
        New.AnimalCategory GetTopLevelCategories();
        IEnumerable<SailingSiteModels.New.AnimalCategory> GetSubCategories(string name);
        IEnumerable<New.AnimalCategory> GetParentCategories();
        void CreateCategory(SailingSiteModels.New.AnimalCategory entity);
        SailingSiteModels.New.Photo GetRandomCategoryPhoto(string categoryId);
        int CreateAnimal(SailingSiteModels.New.Animal entity, AuditContext context);
         IEnumerable<New.Animal> GetAllAnimals();

        void AddAnimalPhoto(int animalId, int photoId);
         IEnumerable<New.AnimalCategory> GetAllCategories();
         New.Animal GetAnimal(int id);
         New.AnimalCategory GetCategory(string name);
         void UpdateAnimal(New.Animal entity, AuditContext context);
         string UpdateCategory(New.AnimalCategory entity, AuditContext context);
         string DeleteAnimal(int id);

        SearchReturnModel<IEnumerable<New.Animal>> Search(GenericQuery search);
        bool CheckDuplicate(New.Animal entity);
        IEnumerable<New.Animal> GetAnimals(GenericQuery query);
         string DeleteCategory(string name);
    }
}