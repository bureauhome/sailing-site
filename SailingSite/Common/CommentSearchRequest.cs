﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    public class PagingRequest
    {
        public PagingRequest()
        {
            Page = 0;
            Offset = 12;
        }

        public int Page { get; set; }
        public int Items { get; set; }
        public int Offset { get; set; }
    }

    public class DateRange
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }

    public class CommentSearchRequest
    {
        PagingRequest PageRequest
        {
            get; set; 
        }

        public int Skip()
        {
            return PageRequest.Page * PageRequest.Offset;
        }

        public int Take()
        {
            return PageRequest.Offset;
        }

        public List<string> KeyWords { get; set; }
    }
}