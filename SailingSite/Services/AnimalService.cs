﻿
namespace SailingSite.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using SailingSite.Common;
    using SailingSite.Models;
    using SailingSite.Repositories;

    using SailingSiteModels.New.Debug;

    using New = SailingSiteModels.New;

    public class AnimalService : ServiceBase, IAnimalService
    {
        private ITagService tagservice;
        private Repositories.ICommentRepository commentRepository;
        public IAnimalRepository animalRepository;
        private Repositories.ITagRepository tagRepository;
        private Repositories.ISailRepository sailRepository;
        public AnimalService()
        {
            tagservice = new TagService();
            tagRepository = new Repositories.TagRepository();
            sailRepository = new Repositories.SailRepository();
            commentRepository = new Repositories.CommentRepository();
        }

        public void AddAnimalPhoto(int animalId, int photoId)
        {
            this.animalRepository.AddAnimalPhoto(animalId,photoId);
        }

        public IAnimalRepository AnimalRepository
        {
            get
            {
                if (this.animalRepository == null)
                {
                    this.animalRepository=new AnimalRepository();
                }
                return this.animalRepository;
            }

            set
            {
                this.animalRepository = value;
            }
        }

        public void DeleteAnimalTag(int id, int animalID) {
            this.AnimalRepository.DeleteTag(id, animalID);
        }

        public void AddAnimalComment(New.Comment comment, int animalID)
        {
           int? commentId=commentRepository.Create(comment);
           if (commentId != null)
           {
               this.AnimalRepository.AddAnimalComment((int)commentId, animalID);
           }
        }
        public IEnumerable<New.CommentBase> GetAnimalComments(int animalID)
        {
            IEnumerable<New.CommentBase> result = null;
            var ids = this.AnimalRepository.GetAnimalComments(animalID);
            if (ids != null && ids.Any())
            {
                result = commentRepository.GetCommentBases(ids);
            }
            return result;
        }

        public Models.SearchReturnModel<IEnumerable<New.Animal>> Search(GenericQuery search)
        {
            var result = new SearchReturnModel<IEnumerable<New.Animal>>();
            result.StartsWithIndex = this.AnimalRepository.GetFirstInitials();
            result.Result = this.AnimalRepository.Search(search);
            return result;
        }

        public IEnumerable<New.Tags> GetAnimalTags(int animalID)
        {
            return this.AnimalRepository.GetTags(animalID);
        }

        public bool CheckDuplicate(New.Animal entity)
        {
            return this.AnimalRepository.CheckDuplicate(entity);
        }

        public void AddTags(int animalID, string tagStr)
        {
            var tags = tagservice.GenerateTagsFromString(tagStr);
            List<int> tagIds = new List<int>();
            foreach (var tag in tags)
            {

                var id = tagservice.SaveTag(tag, null);
                if (id!=null&&(int)id>0)
                {
                    tagIds.Add((int)id);
                }
            }
            if (tagIds.Count > 0)
            {
                this.AnimalRepository.AddTags(tagIds, animalID);
            }
        }

        public IEnumerable<New.AnimalCategory> GetSiblingCategories(string name)
        {
            return this.AnimalRepository.GetSiblingCategories(name);
        }
        public New.AnimalCategory GetTopLevelCategories()
        {
            return this.AnimalRepository.GetTopLevelCategories();
        }

        public New.AnimalDetails GetDetailedAnimal(int id)
        {
            var result= this.AnimalRepository.GetDetailedAnimal(id);
            if (result.CommentIDs != null && result.CommentIDs.Count() > 0)
            {
                result.Comments = commentRepository.GetCommentBases(result.CommentIDs);
            }
            if (result.SailIDs != null && result.SailIDs.Count() > 0)
            {
                result.Spottings = sailRepository.GetSailBases(result.SailIDs).ToList();
            }
            return result;
        }
        public IEnumerable<SailingSiteModels.New.AnimalCategory> GetSubCategories(string name)
        {
            var result = this.AnimalRepository.GetSubCategories(name);
            foreach (var category in result)
            {
                category.Photo = GetRandomCategoryPhoto(category.Name);
            }
            return result;

        }

        public int CreateAnimal(SailingSiteModels.New.Animal entity, AuditContext context)
        {
            int result = 0;
            this.LoggingService.GenerateLog("Creating animal", Log.LogTypes.AuditCreate, context, entity);
            var category = this.AnimalRepository.GetCategory(entity.Category);
            if (category == null)
            {
                throw new EntityNotFoundException("Category does not exist");
            }
            var animal = this.AnimalRepository.GetAnimals(new GenericQuery() { Name = entity.Name }).FirstOrDefault();
            if (animal == null || (animal != null && animal.Category != entity.Category))
            {
                result = this.AnimalRepository.CreateAnimal(entity, Factories.AnimalFactory.CreateAnimalCategory(category));
            }
            else
            {
                throw new EntityAlreadyInDatabaseException("This entity already exists");
            }

            return result;
        }

        public IEnumerable<SailingSiteModels.New.Animal> GetAllAnimals()
        {
            return this.AnimalRepository.GetAnimals(null);
        }


        public IEnumerable<SailingSiteModels.New.AnimalCategory> GetAllCategories()
        {
            var result = this.AnimalRepository.GetAllCategories();
            foreach (var category in result)
            {
                category.Photo = GetRandomCategoryPhoto(category.Name);
            }
            return result;
        }

        public New.Animal GetAnimal(int id)
        {
            return this.AnimalRepository.GetAnimal(id);
        }

        public SailingSiteModels.New.AnimalCategory GetCategory(string name)
        {
            var result=this.AnimalRepository.GetCategory(name);
            result.Photo = GetRandomCategoryPhoto(name);
            return result;
        }

        public void UpdateAnimal(SailingSiteModels.New.Animal entity, AuditContext context)
        {
            Entities_Animals_Names dbEntityFound = null;
            bool categoryFound = false;
            this.LoggingService.GenerateLog("Animal updated", Log.LogTypes.AuditUpdate, context, entity);
            if (entity.Category != null)
            {
                if (this.AnimalRepository.GetCategory(entity.Category) != null)
                {
                    categoryFound = true;
                }
                else throw new Exception("Animal category is invalid.");
            }
            else throw new Exception("Animal must have an assigned category!");

            try
            {
             
                this.AnimalRepository.UpdateAnimal(entity);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                List<string> Errors = new List<string>();
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Errors.Add(String.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
            }

        }

        public void CreateCategory(SailingSiteModels.New.AnimalCategory entity)
        {
            try {
                this.AnimalRepository.CreateAnimalCategory(entity);
            }
            catch {}
        }

        public string UpdateCategory(SailingSiteModels.New.AnimalCategory entity, AuditContext context)
        {
            this.LoggingService.GenerateLog("Category updated", Log.LogTypes.AuditUpdate, context, entity);
            try
            {
                // WTF is this shit??
                return this.AnimalRepository.UpdateCategory(entity);
            }
            catch { 
            }
            return null;
        }

        public IEnumerable<SailingSiteModels.New.AnimalCategory> GetParentCategories()
        {
            return this.AnimalRepository.GetParentCategories();
        }

        public string DeleteAnimal(int id)
        {
            return this.AnimalRepository.DeleteAnimal(id);
        }

        public string DeleteCategory(string name)
        {
            return this.AnimalRepository.DeleteCategory(name);
        }

        public IEnumerable<New.Animal> GetAnimals(GenericQuery query)
        {
            return this.AnimalRepository.GetAnimals(query);
        }

        public SailingSiteModels.New.Photo GetRandomCategoryPhoto(string id)
        {
            SailingSiteModels.New.Photo photo = null;
            var photos=this.AnimalRepository.GetAnimalPhotosForCategory(id);
            if (photos != null && photos.Any())
            {
                var i = Common.Helpers.Rand.Next(0, photos.Count - 1);
                photo = photos[i].ToPhoto();
            }
            return photo;
        }


        string IAnimalService.DeleteAnimal(int id)
        {
           return this.AnimalRepository.DeleteAnimal(id);
        }
    }
}