﻿CREATE TABLE [dbo].[Entities.Animals.Names] (
    [AnimalCategory]    NVARCHAR (50)  NOT NULL,
    [AnimalName]        NCHAR (64)     NOT NULL,
    [AnimalID]          INT            IDENTITY (1, 1) NOT NULL,
    [Description]       NVARCHAR (255) NULL,
    [PhotoID]           INT            NULL,
    [DateToDelete]      DATETIME       NULL,
    [TimesSpottedMonth] INT            NULL,
    [CreatedByID] INT NULL, 
    CONSTRAINT [PK_Entities.Animals.Names_1] PRIMARY KEY CLUSTERED ([AnimalID] ASC),
    CONSTRAINT [FK_Entities.Animals.Names_Entities.Animals.Categories] FOREIGN KEY ([AnimalCategory]) REFERENCES [dbo].[Entities.Animals.Categories] ([AnimalCategory]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FKPhotoID] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]), 
    CONSTRAINT [FK_Entities.Animals.Names_ToTable] FOREIGN KEY ([CreatedByID]) REFERENCES [dbo].[Entities.Crew] ([CrewId])
);

