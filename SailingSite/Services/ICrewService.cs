﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite.Services
{
    using System.Threading.Tasks;

    using SailingSite.Common;
    using SailingSite.Models;

    public interface ICrewService : ICommentableService
    {
        IEnumerable<New.Tags> GetCrewTag(int id);
        void AddCrewTags(int crewId, string tagStr);
        SailingSiteModels.New.Crew GetSingle(int id);
        bool CheckUsername(string username);
        IEnumerable<SailingSiteModels.New.CrewFoundation> GetAll_Foundation();
        New.CrewSecurity Login(string username, string password, AuditContext context);
        New.Crew HydrateCrewStats(New.Crew entity);
        IEnumerable<SailingSiteModels.New.Crew> Get(GeneralSearchRequest query);
        int Create(SailingSiteModels.New.Crew entity, AuditContext context);
        bool Update(SailingSiteModels.New.Crew entity, AuditContext context);
        bool Delete(int id, int crewId, AuditContext context);
        void DeleteCrewTags(int tagId, int crewId);
        void DeletePhoto(int entityId, int photoID);
        void AddPhoto(int entityId, int photoID);
        New.CrewSecurity VerifyToken(string token);

        Task ProcessQuestion(New.SecurityQuestion entity);
        Task ResetPassword(int id, string token, string password, AuditContext context);
        Task<string> MatchAnswersAndSetToken(int id, IEnumerable<New.SecurityQuestion> entity, AuditContext context);
        Task DeleteQuestion(int id, AuditContext context);
        Task<IEnumerable<New.SecurityQuestion>> GetQuestions(string userName, AuditContext context);
        Task<IEnumerable<New.SecurityQuestion>> GetQuestions(int userId, AuditContext context); 
        SearchReturnModel<IEnumerable<SailingSiteModels.New.CrewBase>> GetSearchModel(GeneralSearchRequest query);
        IEnumerable<New.SailBase> GetSailsForCrew(IEnumerable<int> ids);
    }
}