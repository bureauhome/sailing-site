﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdminModel.cs" company="">
//   
// </copyright>
// <summary>
//   The admin model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Models
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The admin model.
    /// </summary>
    public class AdminModel
    {
        /// <summary>
        /// Gets or sets the crew list.
        /// </summary>
        public SearchReturnModel<IEnumerable<CrewFoundation>> CrewList { get; set; }

        /// <summary>
        /// Gets or sets the vessel list.
        /// </summary>
        public SearchReturnModel<IEnumerable<VesselBase>> VesselList { get; set; }

        /// <summary>
        /// Gets or sets the fleet list.
        /// </summary>
        public SearchReturnModel<IEnumerable<FleetBase>> FleetList { get; set; }

        /// <summary>
        /// Gets or sets the land mark list.
        /// </summary>
        public SearchReturnModel<IEnumerable<LandMarkBase>> LandMarkList { get; set; }

        /// <summary>
        /// Gets or sets the location list.
        /// </summary>
        public SearchReturnModel<IEnumerable<LocationBase>> LocationList { get; set; }

        /// <summary>
        /// Gets or sets the animal list.
        /// </summary>
        public SearchReturnModel<IEnumerable<Animal>> AnimalList { get; set; }
    }
}