﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    public sealed class CrewDetails
    {
        public int NumberOfSails { get; set; }
        public double MilesSailed { get; set; }
        public double AvgMiles { get; set; }
        public List<Maneuver> Maneuvers { get; set; }
        public List<Vessel> Vessels { get; set; } 
        public Sail LongestSail { get; set; }
        public Sail WindiestSail { get; set; }
        public List<Sail> FavoriteSails { get; set; } 



    }
}
