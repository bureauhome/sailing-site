﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite.Services
{
    public interface ITagService
    {
        int? SaveTag(New.Tags entity, Entities_Tags tag);
        New.TagHydrated GetHydratedTag(int id);
        List<New.Tags> GenerateTagsFromString(string tagStr);
        IEnumerable<string> GetAllTagsByString();
        IEnumerable<New.Tags> GetAllTags();
        New.Tags GetTag(Repositories.TagRequest request);
    }
}