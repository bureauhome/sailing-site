﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The column [dbo].[Entities.Photos].[Height] on table [dbo].[Entities.Photos] must be added, but the column has no
         default value and does not allow NULL values. If the table contains data, the ALTER script will not work. To avoid this
         issue you must either: add a default value to the column, mark it as allowing NULL values, or enable the generation of
         smart-defaults as a deployment option.
       The column [dbo].[Entities.Photos].[PhotoCarouselPath] on table [dbo].[Entities.Photos] must be added, but the column
         has no default value and does not allow NULL values. If the table contains data, the ALTER script will not work. To
         avoid this issue you must either: add a default value to the column, mark it as allowing NULL values, or enable the
         generation of smart-defaults as a deployment option.
       The column [dbo].[Entities.Photos].[Width] on table [dbo].[Entities.Photos] must be added, but the column has no default
         value and does not allow NULL values. If the table contains data, the ALTER script will not work. To avoid this issue
         you must either: add a default value to the column, mark it as allowing NULL values, or enable the generation of
         smart-defaults as a deployment option.

** User actions
     Alter
       [dbo].[Entities.Photos] (Table)

** Supporting actions

The column [dbo].[Entities.Photos].[Height] on table [dbo].[Entities.Photos] must be added, but the column has no default value and does not allow NULL values. If the table contains data, the ALTER script will not work. To avoid this issue you must either: add a default value to the column, mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
The column [dbo].[Entities.Photos].[PhotoCarouselPath] on table [dbo].[Entities.Photos] must be added, but the column has no default value and does not allow NULL values. If the table contains data, the ALTER script will not work. To avoid this issue you must either: add a default value to the column, mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
The column [dbo].[Entities.Photos].[Width] on table [dbo].[Entities.Photos] must be added, but the column has no default value and does not allow NULL values. If the table contains data, the ALTER script will not work. To avoid this issue you must either: add a default value to the column, mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.

