﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models
{
    using DotNet.Highcharts;

    public class ChartCollection
    {
        public IEnumerable<ChartEntry> Collection { get; set; }
    }

    public class ChartEntry
    {
        public Highcharts Chart { get; set; }
        public string Descriptor { get; set; }
        public string UID { get; set; }
        public ChartEntry(Highcharts chart, string decriptor)
        {
            this.Descriptor = decriptor;
            this.Chart = chart;
            this.UID = Guid.NewGuid().ToString();
        }
    }
}