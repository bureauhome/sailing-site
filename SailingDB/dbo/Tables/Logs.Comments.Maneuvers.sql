﻿CREATE TABLE [dbo].[Logs.Comments.Maneuvers](
	[CommentID] [int] NOT NULL,
	[ManeuverID] [int] NOT NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC,
	[ManeuverID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Logs.Comments.Maneuvers]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Maneuver] FOREIGN KEY([ManeuverID])
REFERENCES [dbo].[Entities.Maneuvers] ([ManeuverID])
GO

ALTER TABLE [dbo].[Logs.Comments.Maneuvers] CHECK CONSTRAINT [FK_Comment_Maneuver]
GO

ALTER TABLE [dbo].[Logs.Comments.Maneuvers]  WITH CHECK ADD  CONSTRAINT [FK_Table_1_Entities.Comment] FOREIGN KEY([CommentID])
REFERENCES [dbo].[Entities.Comment] ([CommentID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Logs.Comments.Maneuvers] CHECK CONSTRAINT [FK_Table_1_Entities.Comment]
GO