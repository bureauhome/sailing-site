﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationService.cs" company="">
//   
// </copyright>
// <summary>
//   The location service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using System.Collections.Generic;

    using SailingSite.Models;

    using SailingSiteModels.New;

    /// <summary>
    /// The location service.
    /// </summary>
    public class LocationService : ServiceBase, ILocationService
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int Create(Location entity)
        {
            if (entity.ID == 0)
            {
                return this.LocationRepository.Create(entity);
            }

            LocationRepository.Update(entity);
            return entity.ID;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="crewId">
        /// The crew id.
        /// </param>
        public void Delete(int id, int crewId)
        {
            LocationRepository.Delete(id, crewId);
        }

        /// <summary>
        /// The delete land mark.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="crewId">
        /// The crew id.
        /// </param>
        public void DeleteLandMark(int id, int crewId)
        {
            LocationRepository.DeleteLandmark(id, crewId);
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Update(Location entity)
        {
            LocationRepository.Update(entity);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Location"/>.
        /// </returns>
        public Location Get(int id)
        {
            return LocationRepository.Get(id);
        }

        /// <summary>
        /// The get land mark base.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="LandMarkBase"/>.
        /// </returns>
        public LandMarkBase GetLandMarkBase(int id)
        {
            return this.LocationRepository.GetLandMarkBase(id);
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="search">
        /// The search.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<LocationBase> GetAll(object search = null)
        {
            return LocationRepository.GetAll(search);
        }

        /// <summary>
        /// The get tags.
        /// </summary>
        /// <param name="entityID">
        /// The entity id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Tags> GetTags(int entityID)
        {
            return LocationRepository.GetTags(entityID);
        }

        /// <summary>
        /// The add tags.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <param name="tagString">
        /// The tag string.
        /// </param>
        public void AddTags(int entityId, string tagString)
        {
            var tags = TagService.GenerateTagsFromString(tagString);
            List<int> tagIds = new List<int>();
            foreach (var tag in tags)
            {

                var id = TagService.SaveTag(tag, null);
                if (id!=null&&(int)id>0)
                {
                    tagIds.Add((int)id);
                }
            }

            if (tagIds.Count > 0)
            {
                LocationRepository.AddTags(tagIds, entityId);
            }
        }

        /// <summary>
        /// The delete tag.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <param name="tagId">
        /// The tag id.
        /// </param>
        public void DeleteTag(int entityId, int tagId)
        {
            LocationRepository.DeleteTag(entityId, tagId);
        }

        /// <summary>
        /// The add comment.
        /// </summary>
        /// <param name="entityID">
        /// The entity id.
        /// </param>
        /// <param name="comment">
        /// The comment.
        /// </param>
        public void AddComment(int entityID, Comment comment)
        {
            var commentID=CommentRepository.Create(comment);
            LocationRepository.AddComment(commentID, entityID);
        }

        /// <summary>
        /// The get comments.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<CommentBase> GetComments(int[] id)
        {
            return CommentRepository.GetCommentBases(LocationRepository.GetCommentIDsForSail(id));
        }

        /// <summary>
        /// The is unique.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool isUnique(Location entity)
        {
            List<string> list = null;
            return LocationRepository.IsUnique(entity, out list);
        }

        /// <summary>
        /// The get land marks.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<LandMarkBase> GetLandMarks()
        {
            return this.LocationRepository.GetLandMarks();
        }

        /// <summary>
        /// The search landmarks.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="SearchReturnModel"/>.
        /// </returns>
        public SearchReturnModel<IEnumerable<LandMarkBase>> SearchLandmarks(GenericQuery request)
        {
            var result = new SearchReturnModel<IEnumerable<LandMarkBase>>();
            result.StartsWithIndex = this.LocationRepository.GetLandMarkFirstInitials();
            result.Result = this.LocationRepository.SearchLandmarks(request);
            return result;
        }

        /// <summary>
        /// The get land mark first initials.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<string> GetLandMarkFirstInitials()
        {
            return this.LocationRepository.GetLandMarkFirstInitials();
        }

        /// <summary>
        /// The search locations.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="SearchReturnModel"/>.
        /// </returns>
        public SearchReturnModel<IEnumerable<LocationBase>> SearchLocations(GenericQuery request)
        {
            var result = new SearchReturnModel<IEnumerable<LocationBase>>();
            result.StartsWithIndex = this.LocationRepository.GetLocationFirstInitials();
            result.Result = this.LocationRepository.SearchLocations(request);
            return result;
        }

        /// <summary>
        /// The get location first initials.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<string> GetLocationFirstInitials()
        {
            return this.LocationRepository.GetLocationFirstInitials();
        }

        /// <summary>
        /// The get locations and landmarks.
        /// </summary>
        /// <returns>
        /// The <see cref="MergedLocationIndex"/>.
        /// </returns>
        public MergedLocationIndex GetLocationsAndLandmarks()
        {
            var result=new MergedLocationIndex();
            result.Locations = this.AdminService.GetSearchReturnObject(
    this.LocationRepository.SearchLocations, 
    this.LocationRepository.GetLocationFirstInitials);
            result.LandMarks = this.AdminService.GetSearchReturnObject(this.LocationRepository.SearchLandmarks, this.LocationRepository.GetLandMarkFirstInitials);
            return result;
        }

        /// <summary>
        /// The create land mark.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int CreateLandMark(LandMarkBase entity)
        {
            if (entity.ID == 0)
            {
                return this.LocationRepository.CreateLandmark(entity);
            }
            else
            {
                this.LocationRepository.UpdateLandmark(entity);
                return entity.ID;
            }
        }
    }
}