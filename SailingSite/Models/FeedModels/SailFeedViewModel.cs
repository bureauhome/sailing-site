﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models
{
    using SailingSiteModels.Common;
    using SailingSiteModels.New;

    public class SailFeedViewModel : SailBase, INewsFeedable
    {
        public PhotoBase RandomlyChosenPhoto { get; set; }

        public int Order { get; set; }

        public string BlurbPartial
        {
            get
            {
                return "_Sail_Feed";
            }
        }

        public string Blurb
        {
            get
            {
                if (this.GeneralObservations != null && this.GeneralObservations.Length > 128)
                {
                    return string.Format("{0}...", this.GeneralObservations.Substring(0, 127));
                }
                return this.GeneralObservations;
            }
        }

        public override bool Equals(object obj)
        {
            var item = obj as SailFeedViewModel;
            if (item == null)
            {
                return false;
            }
            return this.ID.Equals(item.ID);
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode();
        }
    }
}