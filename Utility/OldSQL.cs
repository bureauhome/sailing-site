﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading.Tasks;
namespace Utility
{
    class OldSQL
    {
        #region Helper Classes
        public class HelperYear
        {
            public int Year;
            public List<int> Months = new List<int>();

        }
        public class AnniversaryMessage
        {
            public string Name;
            public string Message;
            public int Index;
        }
        public class SailingStat
        {
            public DateTime Date;
            public float WindMPHHigh;
            public float WindMPHLow;
            public float TempFar;
            public float DistanceMiles;
        }
        public class Vessel
        {
            public static List<Vessel> VesselList = new List<Vessel>();
            public string Name { get; set; }
            public bool Default = false;
            public static void Add(Vessel vessel)
            {
                if (vessel.Default)
                {
                    foreach (Vessel v in VesselList)
                    {
                        if (v.Default)
                        {
                            v.Default = false;
                        }
                    }
                }
                VesselList.Add(vessel);
            }
        }
        public class SailingDate
        {
            public DateTime Date;
            public string Location;
            public float Barometer;
            public float Temp;
            public float MinWind;
            public float MaxWind;
            public float Distance;
            public float Hours;
            public string Notes;
            public string Vessel;
            public List<string> Crew = new List<string>();
            public List<string> Maneuvers = new List<string>();
            public List<string> Configurations = new List<string>();
            public SailingDate(DateTime date, string location, string vessel, float barometer, float temp, float minwind, float maxwind, float distance, float hours, string notes)
            {
                Vessel = vessel;
                Date = date;
                Barometer = barometer;
                Temp = temp;
                MinWind = minwind;
                MaxWind = maxwind;
                Location = location;
                Distance = distance;
                Notes = notes;
            }
            public void AddCrew(string name)
            {
                Crew.Add(name);
            }
            public void AddConfiguration(string name)
            {
                Configurations.Add(name);
            }
            public void AddManeuver(string name)
            {
                Maneuvers.Add(name);
            }
        }
        public class SailingComment
        {
            public string Message;
            public string User;
            public DateTime EntryDate;
        }

        #endregion



        public class SQLStuff
        { 
            #region Private Fields
            private static Random rdm = new Random();
            private static List<int> _messagesCalled = new List<int>();
            private static string connection = @"Data Source=192.168.254.34\SQLEXPRESS;Initial Catalog=bureau;User id=inquire;Password=inquire";
            private static string sailingcon = @"Data Source=192.168.254.34\SQLEXPRESS;Initial Catalog=bureau;User id=inquire;Password=inquire";
            private static string testconnection = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=bureau;User id=inquire;Password=inquire";
            #endregion
            #region TestFunctions
            public static int TestSQLByIPAddy()
            {
                SqlConnection con = new SqlConnection(connection);
                int output = 0;
                using (con)
                {
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select count(name) from Messages", con);
                        string t = com.ExecuteScalar().ToString();
                        int.TryParse(t, out output);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            public static int TestSQLByLocalHost()
            {
                SqlConnection con = new SqlConnection(testconnection);
                int output = 0;
                using (con)
                {
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select count(name) from Messages", con);
                        string t = com.ExecuteScalar().ToString();
                        int.TryParse(t, out output);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            #endregion
            # region Sailing Functions
            #region Logging Functions

            public static int DeleteSail(DateTime date, string vessel)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("DELETE from Sailing_Log WHERE Date='" + date.ToString() + "' AND Vessel='" + vessel + "'", con);
                        SqlCommand com2 = new SqlCommand("DELETE from Sailing_ManeuverNames WHERE Date='" + date.ToString() + "' AND Vessel='" + vessel + "'", con);
                        SqlCommand com3 = new SqlCommand("DELETE from Sailing_CrewLog WHERE Date='" + date.ToString() + "' AND Vessel='" + vessel + "'", con);
                        SqlCommand com4 = new SqlCommand("DELETE from Sailing_SailConfig WHERE Date='" + date.ToString() + "' AND Vessel='" + vessel + "'", con);
                        output += com.ExecuteNonQuery();
                        output += com2.ExecuteNonQuery();
                        output += com3.ExecuteNonQuery();
                        output += com4.ExecuteNonQuery();

                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            public static int LogSail(DateTime date, string location, float temp, float barometer, float distance, float hours, float minWind, float maxWind, string vessel, List<string> crew, string winddir, string notes = "", List<string> maneuvers = null, List<string> configs = null)
            {
                // Add wind direction!!
                int output = 0;
                string extendedNotes = "";
                notes = notes.Replace("'", "''");
                if (notes.Length > 255)
                {
                    extendedNotes = notes;
                    notes = notes.Substring(0, 254);
                }
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    con.Open();

                    SqlCommand comm = new SqlCommand("Insert into Sailing_Log (Date, Location, Vessel,WindMPHMin,WindMPHMax, WindDir, DistanceMiles,Barometer, TempFar, Notes,ExtendedNotes) VALUES ('" + date.ToString() + "','" + location + "','" + vessel + "','" + minWind.ToString() + "','" + maxWind.ToString() + "','" + winddir + "','" + distance.ToString() + "','" + barometer.ToString() + "','" + temp + "','" + notes + "','" + extendedNotes + "')", con);
                    output += comm.ExecuteNonQuery();

                    foreach (string c in crew)
                    {
                        output += LogCrew(c, vessel, date);
                    }
                    if (maneuvers.Count > 0)
                    {
                        foreach (string m in maneuvers)
                        {
                            // Log
                            output += LogManeuver(m, vessel, date);
                        }
                    }
                    if (configs.Count > 0)
                    {
                        foreach (string c in configs)
                        {
                            output += LogConfiguration(c, vessel, date);
                        }
                    }
                }
                return output;
            }

            public static int LogCrew(string name, string vessel, DateTime date)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO SAILING_CREWLOG (CrewName, Vessel, Date) Values ('" + name + "','" + vessel + "','" + date.ToString() + "')", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch { }
                }
                return output;
            }



            public static int LogConfiguration(string name, string vessel, DateTime date)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO SAILING_SAILCONFIG (SailConfig, Vessel, Date) Values ('" + name + "','" + vessel + "','" + date.ToString() + "')", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch { }
                }
                return output;

            }


            public static int LogManeuver(string name, string vessel, DateTime date)
            {
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO SAILING_MANEUVERS (Maneuvername, Vessel, Date) Values ('" + name + "','" + vessel + "','" + date.ToString() + "')", con);
                        return com.ExecuteNonQuery();
                    }
                    catch { return 0; }
                }
            }
            #endregion
            #region Sailing Page Default Values
            // TODO: Make links to delete or edit entries into LINKS that send PARAMS!
            public static List<SailingDate> GetSailingDate(DateTime date)
            {
                List<SailingDate> list = new List<SailingDate>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Vessel, Location, WindMPHMin, WindMPHMax, Barometer, TempFar, DistanceMiles, Notes FROM Sailing_log where DATE='" + date.ToShortDateString() + "'", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            string location = dr["Location"].ToString();
                            string vessel = dr["Vessel"].ToString();
                            float barometer;
                            float.TryParse(dr["Barometer"].ToString(), out barometer);
                            float temp;
                            float.TryParse(dr["TempFar"].ToString(), out temp);
                            float windMin;
                            float.TryParse(dr["WindMPHMin"].ToString(), out windMin);
                            float windMax;
                            float.TryParse(dr["WindMPHMax"].ToString(), out windMax);
                            float distance;
                            float.TryParse(dr["DistanceMiles"].ToString(), out distance);
                            float hrs = 0;
                            SailingDate sail = new SailingDate(date, location, vessel, barometer, temp, windMin, windMax, distance, hrs, dr["Notes"].ToString());
                            SqlConnection con4 = new SqlConnection(connection);
                            using (con4)
                            {
                                con4.Open();
                                SqlCommand commCrew = new SqlCommand("Select CrewName from Sailing_CrewLog where Date='" + date.ToShortDateString() + "' AND Vessel='" + sail.Vessel + "'", con4);
                                SqlDataReader dr2 = commCrew.ExecuteReader();
                                while (dr2.Read())
                                {
                                    sail.AddCrew(dr2[0].ToString());
                                }
                            }
                            SqlConnection con2 = new SqlConnection(connection);
                            using (con2)
                            {
                                con2.Open();
                                SqlCommand commManeuvers = new SqlCommand("Select ManeuverName from Sailing_Maneuvers where  Date='" + date.ToShortDateString() + "' AND Vessel='" + sail.Vessel + "'", con2);
                                SqlDataReader dr3 = commManeuvers.ExecuteReader();
                                while (dr3.Read())
                                {
                                    sail.AddManeuver(dr3[0].ToString());
                                }
                            }

                            SqlConnection con3 = new SqlConnection(connection);
                            using (con3)
                            {
                                con3.Open();
                                SqlCommand commSailConfig = new SqlCommand("Select Sailconfig from Sailing_SailConfig where  Date='" + date.ToShortDateString() + "' AND Vessel='" + sail.Vessel + "'", con3);
                                SqlDataReader dr4 = commSailConfig.ExecuteReader();
                                while (dr4.Read())
                                {
                                    sail.AddConfiguration(dr4[0].ToString());
                                }
                            }
                            list.Add(sail);
                        }
                    }
                    catch { }
                    return list;
                }

            }

            public static List<DateTime> LoadDates()
            {
                List<DateTime> list = new List<DateTime>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Date from Sailing_Log ORDER BY Date DESC", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            DateTime date;
                            DateTime.TryParse(dr[0].ToString(), out date);
                            list.Add(date);
                        }
                    }
                    catch { }
                }
                return list;
            }
            public static void LoadVessels()
            {
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Vessel, IsDefault from Sailing_Vessels ORDER BY Vessel", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            Vessel v = new Vessel();
                            v.Name = dr[0].ToString();
                            bool.TryParse(dr[1].ToString(), out v.Default);
                            Vessel.Add(v);
                        }
                    }
                    catch { }
                }
            }

            public static List<string> LoadLocations()
            {
                List<string> list = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Location from Sailing_Location", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                    catch { }
                }
                return list;
            }


            public static List<string> LoadSailCrew()
            {
                List<string> list = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select CrewName from Sailing_CrewName", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                    catch { }
                }
                return list;
            }

            public static List<string> LoadSailConfigs()
            {
                List<string> list = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select SailConfig from Sailing_SailConfigNames", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                    catch { }
                }
                return list;
            }

            public static List<string> LoadSailingManeuvers()
            {
                List<string> list = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select ManeuverName from Sailing_ManeuverNames", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                    catch { }
                }
                return list;
            }
            #endregion
            #region Comment Functions
            public static int LogSailingComment(string vessel, string message, string user, DateTime date)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO Sailing_Comments (Vessel, Date, UserName, Entry, Entrydate) VALUES=('" + vessel + "','" + date.ToString() + "','" + user + "','" + message + "','" + System.DateTime.Now + "'", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch { }
                    return output;
                }


            }

            public static List<SailingComment> LoadSailingMessages(string vessel, DateTime date)
            {
                List<SailingComment> messages = new List<SailingComment>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("SELECT EntryDate, Entry, UserName from Sailing_Comments WHERE Vessel='" + vessel + "' AND Date='" + date.ToShortDateString() + "' ORDER BY EntryDate", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            SailingComment message = new SailingComment();
                            message.EntryDate = (DateTime)dr[0];
                            message.Message = dr[1].ToString();
                            message.User = dr[2].ToString();
                            messages.Add(message);
                        }
                    }
                    catch { }
                }
                return messages;
            }


            #endregion
            # region Chart Functions
            public static Dictionary<string, int> GetCrewCount()
            {
                Dictionary<string, int> dic = new Dictionary<string, int>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("SELECT TOP 7 CrewName, Count from Sailing_CrewCount ORDER BY Count DESC", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            string name = dr[0].ToString();
                            int count;
                            int.TryParse(dr[1].ToString(), out count);
                            dic.Add(name, count);
                        }
                    }
                    catch { }

                }
                return dic;
            }

            public static List<SailingStat> GetAllSailingStats()
            {
                List<SailingStat> list = new List<SailingStat>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("select WindMPHMin, WindMPHMax, Date,DistanceMiles, TempFar from Sailing_Log", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            SailingStat stat = new SailingStat();
                            float.TryParse(dr[0].ToString(), out stat.WindMPHLow);
                            float.TryParse(dr[1].ToString(), out stat.WindMPHHigh);
                            DateTime.TryParse(dr[2].ToString(), out stat.Date);
                            float.TryParse(dr[3].ToString(), out stat.DistanceMiles);
                            float.TryParse(dr[4].ToString(), out stat.TempFar);
                            list.Add(stat);
                        }
                    }
                    catch { }
                }
                return list;
            }
            #endregion
            #endregion
            #region Message Functions

            public static int[] GetMessagesVSBadMessages()
            {
                List<int> list = new List<int>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    con.Open();
                    int bad = 0;
                    int good = 0;
                    SqlCommand com = new SqlCommand("select Count(*) from Messages", con);
                    string i = "0";
                    i = com.ExecuteScalar().ToString();
                    int.TryParse(i, out good);
                    i = "0";
                    SqlCommand com2 = new SqlCommand("Select Count(*) from BadMessages", con);
                    i = com2.ExecuteScalar().ToString();
                    int.TryParse(i, out bad);
                    list.Add(good);
                    list.Add(bad);
                }
                return list.ToArray();
            }

            public static AnniversaryMessage GetMessageByID(int id)
            {
                SqlConnection con = new SqlConnection(connection);
                AnniversaryMessage output = new AnniversaryMessage();
                using (con)
                {
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select name, message, rowid from Messages where rowid='" + id + "'", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            string name = dr[0].ToString();
                            string message = dr[1].ToString();
                            string t = dr[2].ToString();
                            int.TryParse(t, out output.Index);
                        }
                    }
                    catch
                    {

                    }
                }
                return output;
            }

            public static int GetMessageCount()
            {
                SqlConnection con = new SqlConnection(connection);
                int output = 0;
                using (con)
                {
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select count(name) from Messages", con);
                        string t = com.ExecuteScalar().ToString();
                        int.TryParse(t, out output);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }
            public static List<AnniversaryMessage> GetAllMessages()
            {
                List<AnniversaryMessage> output = new List<AnniversaryMessage>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("SELECT name, message, rowid from Messages", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            AnniversaryMessage message = new AnniversaryMessage();
                            message.Name = dr[0].ToString();
                            message.Message = dr[1].ToString();
                            string t = dr[2].ToString();
                            int.TryParse(t, out message.Index);
                            output.Add(message);
                        }
                    }
                    catch { }
                    return output;
                }

            }



            public static List<AnniversaryMessage> GetAllBadMessages()
            {
                List<AnniversaryMessage> output = new List<AnniversaryMessage>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("SELECT name, message, rowid from BadMessages", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            AnniversaryMessage message = new AnniversaryMessage();
                            message.Name = dr[0].ToString();
                            message.Message = dr[1].ToString();
                            string t = dr[2].ToString();
                            int.TryParse(t, out message.Index);
                            output.Add(message);
                        }
                    }
                    catch { }
                    return output;
                }

            }

            public static int DeleteMessage(int rowid)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("DELETE FROM Messages where RowID='" + rowid + "'", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch { return -1; }
                }
                return output;
            }

            public static string GetMessage()
            {
                int hours = System.DateTime.Now.Hour;
                if (hours > 12)
                    hours -= 12;
                int mins = System.DateTime.Now.Minute;
                int noMsgs = 0;
                int selectN = 0;
                int timesCalled = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select count(name) from Messages", con);
                        object obj = com.ExecuteScalar();
                        noMsgs = Convert.ToInt32(obj.ToString());

                        // If it's half-past or top of the hour, return number of messages
                        if (mins == 0 || mins == 30)
                        {
                            return "It is " + hours + ":" + mins + " ... " + noMsgs + " messages in queue.";
                        }

                        //Randomly selects an element by Row ID (an auto incremented row.)
                        selectN = rdm.Next(1, ++noMsgs);  // Add 1 here as you're going from 1-n, not 0-n
                        // Determine how many times this number has been called
                        if (_messagesCalled.Count > 0)
                        {
                            timesCalled = _messagesCalled.Select(i => i == selectN).Count();
                        }
                        // Add the number to messages called

                        if (_messagesCalled.Count > 0) // Don't div by 0!
                        {
                            if (timesCalled > (_messagesCalled.Count() / noMsgs))  // If the number has been called a higher number of times than is statistically probable, reroll
                            {
                                selectN = rdm.Next(1, noMsgs);
                            }
                        }
                        _messagesCalled.Add(selectN);

                        // call that item
                        return CallMessage(selectN, con, noMsgs + 1);
                    }
                    catch (Exception e)
                    {
                        return e.Message + " " + e.InnerException;
                    }
            }


            private static string CallMessage(int messageNo, SqlConnection con, int messageCount, bool errorDir = false, int errorNo = -1, int errorcount = -1)
            {
                int errorInitial;
                bool error = false;
                if (errorNo != -1)
                {
                    errorInitial = errorNo;
                    error = true;
                }
                else errorInitial = messageCount;
                bool errorDown = errorDir;
                int errorCount = -1;
                if (errorcount != -1)
                    errorCount = errorcount;
                string[] wordsForWrite = new string[] { "writes", "says", "types" };  // adds a bit of flavor
                string name = "";
                string message = "";
                try
                {
                    // Get the message
                    SqlCommand com = new SqlCommand("Select Name, Message from Messages where RowID='" + messageNo.ToString() + "'", con);
                    SqlDataReader dr = com.ExecuteReader();
                    while (dr.Read())
                    {
                        name = dr[0].ToString();
                        message = dr[1].ToString();
                    }
                    if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(message))
                    {
                        Exception e = new Exception("Selection statement returned no results using:" + com.CommandText);
                        throw e;
                    }
                }
                // if it fails, scan up and down the numbers 40 times.
                catch
                {
                    int x;
                    // initialize the initial variables
                    if (error == false)
                    {
                        if (errorCount < messageCount / 2)
                            // if the error occured in the lower half.
                            errorDown = true;
                        // It should iterate over each message.
                        errorCount = messageCount;
                    }
                    else
                    {
                        // If you've gone to either extreme, start at the  initial error number and switch the direction.
                        if (messageNo < 0)
                        {
                            messageNo = errorInitial;
                            errorDown = false;
                        }
                        if (messageNo >= messageCount)
                        {
                            messageNo = errorInitial;
                            errorDown = true;
                        }
                    }
                    // If we're going up, raise by one, if we're going down decrement one
                    if (errorDown == true)
                        x = messageNo - 1;
                    else
                        x = messageNo + 1;

                    // Decrement errorcount
                    errorCount--;
                    if (errorCount > -1)
                    {
                        // Call the message.  If you've run out of errors, call 2.
                        CallMessage(x, con, messageCount, errorDown, errorInitial, errorCount);
                    }
                    else
                    {
                        // When errorcount reaches 0, return an error.
                        return "Error.  Could not load any messages.";
                    }
                }

                return name + " " + wordsForWrite[rdm.Next(0, wordsForWrite.Count())] + ": " + message;

            }



            // Message Posting Methods

            public static int PostMessage(string name, string message)
            {
                int i = 0;
                SqlConnection con = new SqlConnection(connection);
                if (name.Length > 50)
                {
                    name = name.Substring(0, 50);
                }
                if (message.Length > 150)
                {
                    message = message.Substring(0, 150);
                }
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Insert into Messages (name, message, dateentered) values ('" + name + "','" + message + "', GetDate())", con);
                        i = comm.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                    return i;
                }

            }

            public static int PostBadMessage(string name, string message)
            {
                int i = 0;
                SqlConnection con = new SqlConnection(connection);
                if (name.Length > 50)
                {
                    name = name.Substring(0, 50);
                }
                if (message.Length > 150)
                {
                    message = message.Substring(0, 150);
                }
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Insert into BadMessages (name, message) values ('" + name + "','" + message + "')", con);
                        i = comm.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                    return i;
                }
            }
            #endregion
            #region Reminder Functions
            public static int LogReminder(string emailAddress)
            {
                // Todo: Test, then put dev
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                try
                {
                    using (con)
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO AnniversaryReminders (EmailAddress, Type) VALUES ('" + emailAddress + "','Test')", con);
                        output = com.ExecuteNonQuery();
                    }
                }
                catch { }
                return output;

            }
            #endregion
            #region RSVP Functions
            public static int LogExcuse(string name, string excuse)
            {
                int output = 0;
                // Cleans excuse
                excuse = excuse.Replace("'", "//");
                excuse = excuse.Replace("\"", "//");
                if (excuse.Length > 255)
                    excuse = excuse.Substring(0, 250);
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("INSERT INTO AnniversaryExcuse (Name, Excuse) VALUES ('" + name + "','" + excuse + "')", con);
                        output = comm.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                }
                // Todo: fill this fucking class
                return output;
            }
            public static int LoadGuestNumbers(string firstName, string lastName)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    con.Open();
                    try
                    {
                        SqlCommand com = new SqlCommand("Select TotalAllowed from AnniversaryGuests where LastName='" + lastName + "' and FirstName='" + firstName + "'", con);
                        string i = com.ExecuteScalar().ToString();
                        int.TryParse(i, out output);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            public static List<string> LoadNames(string lastName)
            {
                List<string> output = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {

                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("SELECT FirstName from AnniversaryGuests where LastName='" + lastName.ToLower() + "'", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            output.Add(dr[0].ToString());
                        }

                    }
                    catch
                    {
                        return null;
                    }
                }

                return output;
            }

            public static int LogRSVP(int guests, string lastname, string firstname, bool web = false, string excuse = "")
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        string vattend = web ? "1" : "0";
                        SqlCommand com = new SqlCommand("UPDATE AnniversaryGuests SET TotalAttending='" + guests + "', VirtualAttendance='" + vattend + "' WHERE LastName='" + lastname + "' AND FirstName='" + firstname + "'", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            #endregion
            #region Guest Functions
            public static int GetGuestsAttending()
            {
                int guests = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Sum(TotalAttending) from AnniversaryGuests WHERE TotalAttending IS NOT NULL", con);
                        string t = comm.ExecuteScalar().ToString();
                        int.TryParse(t, out guests);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return guests;
            }

            public static int GetGuestsVirtual()
            {
                int guests = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select COUNT(FirstName) from  AnniversaryGuests WHERE VirtualAttendance IS NOT NULL AND VirtualAttendance!=0", con);
                        string t = comm.ExecuteScalar().ToString();
                        int.TryParse(t, out guests);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return guests;
            }

            public static int GetGuestsCheckedIn()
            {
                int guests = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Count(LastName) from AnniversaryGuests WHERE TotalAttending IS NOT NULL", con);
                        string t = comm.ExecuteScalar().ToString();
                        int.TryParse(t, out guests);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return guests;
            }

            public static int GetTotalGuests()
            {
                int guests = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Count(LastName) from AnniversaryGuests", con);
                        string t = comm.ExecuteScalar().ToString();
                        int.TryParse(t, out guests);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return guests;
            }

            public static List<string> GetGuestListAttending()
            {
                List<string> guests = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select FirstName,LastName, TotalAttending FROM AnniversaryGuests where TotalAttending>0 ORDER BY LastName", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            string first = dr[0].ToString();
                            string last = dr[1].ToString();
                            string attending = dr[2].ToString();
                            attending = attending == "1" ? "" : "+" + attending;

                            guests.Add(first + " " + last + attending);

                        }
                    }
                    catch (Exception e) { guests.Add(e.Message + " " + e.InnerException); }
                }
                return guests;
            }

            public static Dictionary<string, int> GetPhysicalVSVirtualGuests()
            {
                Dictionary<string, int> dic = new Dictionary<string, int>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("SELECT Physically,Virtually,Declined,NoResponse FROM Anniversary_GuestCount", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            int physicalAt;
                            int virtualAt;
                            int declined;
                            int noresp;
                            int.TryParse(dr[0].ToString(), out physicalAt);
                            int.TryParse(dr[1].ToString(), out virtualAt);
                            int.TryParse(dr[2].ToString(), out declined);
                            int.TryParse(dr[3].ToString(), out noresp);

                            dic.Add("Physically", physicalAt);
                            dic.Add("Virtually", virtualAt);
                            dic.Add("Declined", declined);
                            dic.Add("No Response", noresp);

                        }


                    }
                    catch { }
                }
                return dic;
            }

            public static List<string> GetGuestListAttendingVirtually()
            {
                List<string> guests = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select FirstName,LastName FROM AnniversaryGuests where VirtualAttendance='1' ORDER BY LastName", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            string first = dr[0].ToString();
                            string last = dr[1].ToString();

                            guests.Add(first + " " + last);

                        }
                    }
                    catch (Exception e) { guests.Add(e.Message + " " + e.InnerException); }
                }
                return guests;
            }

            #endregion

        }

    }
}
