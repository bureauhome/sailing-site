﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Old=SailingSiteModels.Old;
using New=SailingSiteModels.New;
using OldDataConnection;


namespace Conversion.Repositories
{
    class LocationRepository
    {
        public static Dictionary<Old.Location, New.Location> Locations = new Dictionary<Old.Location, New.Location>();
        public LocationRepository()
        {
            var list = OldSQL.SQLStuff.LoadLocations();
            foreach (var location in list)
            {
                Console.WriteLine("Processing Location: {0}", location);
                var oldLocation = new Old.Location() { Name = location };
                var newLocation = new New.Location() { Name = location };
                int id=OldSQL.SQLStuff.AddNewLocation(newLocation);
                newLocation.ID=id;
                var count = Locations.Where(i => i.Key.Name == location && i.Value.ID == newLocation.ID).Count();
                if (count < 1)
                {
                    Locations.Add(oldLocation, newLocation);
                    Console.WriteLine("Tying: {0} to ID:{1}", location, newLocation.ID);
                }
                else Console.WriteLine("Location ID:{0} detected in list.", newLocation.ID);
            }
        }

        public static New.Location Get(Old.Location location)
        {
            return Locations.Where(i => i.Key.Name == location.Name).Select(i => i.Value).FirstOrDefault();

        }
    }
}
