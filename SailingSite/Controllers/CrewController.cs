﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.Collections;
    using System.Net;
    using System.Threading.Tasks;

    using SailingSite.App_GlobalResources;
    using SailingSite.Common;
    using SailingSite.ViewModels;

    using SailingSiteModels.New;

    using SessionVariables = Resources.SessionVariables;

    public class CrewController : BaseController
    {
        
        //
        // GET: /Crew/
        public ActionResult Index()
        {
            var data=GetAll();
            return View(data);
        }

        public ActionResult Edit(int id)
        {
            return RedirectToAction("New", new { id = id });
        }

        [HttpPost]
        public ActionResult AddTags(int id, string tags)
        {
            this.CrewService.AddCrewTags(id, tags);
            var result = this.CrewService.GetCrewTag(id).ToArray();
            ViewData["controller"] = "crew";
            ViewData["entityID"] = id;
            return PartialView("TagList", result);
        }

      [HttpPost]
        public ActionResult Login(string username, string password, string redirecturl=null)
        {
            SailingSiteModels.New.CrewFoundation entity = null;
            try
            {
                this.LoginController.Login(username, password);
                
            }
            catch (Exception ex)
            {
                ViewBag[SessionVariables.Error] = SharedStrings.IncorrectUsernamePassword;
            }
            return PartialView(entity);

        }

        public ActionResult VerifyUserName(string userName)
      {
            bool result=false;
            result=this.CrewService.CheckUsername(userName);
            if (result)
            {
                throw new HttpException("This user already exists");
            }
            return new EmptyResult();

      }

        public ActionResult AddComment(int entityid, SailingSiteModels.New.Comment comment)
        {
            if (CurrentUser != null)
            {
                if (comment.ID < 1)
                {
                    comment.Created = DateTime.Now;
                    comment.CreatedbyID = CurrentUser.ID;
                }
                else
                {
                    comment.Updated = DateTime.Now;
                    comment.UpdatebyID = CurrentUser.ID;
                }
                this.CrewService.AddComment(entityid, comment);
            }
            return null;
        }

        [HttpPost]
        public ActionResult Create(SailingSiteModels.New.Crew entity, string tagList, HttpPostedFileBase file)
        {
            if (this.CurrentUser != null || (this.CurrentUser != null && this.CurrentUser.SID < Crew.SIDEnum.Admin))
            {
                var model = new RedirectModel() { Description = "You must be an admin to complete this task." };

                return this.View("NoPermissionPage", model);
            }

            int result = 0;
            try
            {
                if (file != null)
                {
                    var photo = new SailingSiteModels.New.Photo() { Name = entity.UserName, FileName = file.FileName, EntityName = entity.FullName, Subdirectory = @Resources.SessionVariables.ImageFolderCrew };
                    photo.ID = this.PhotoService.SavePhoto(file, photo, this.Context, Server.MapPath("~/imagedir"));
                    entity.ProfilePhoto = photo;
                }
                
                if (entity.ID > 0)
                {
                    result = entity.ID;
                    this.CrewService.Update(entity, this.Context);
                }
                else
                {
                    result=this.CrewService.Create(entity, this.Context);
                }
                if (tagList != null)
                {
                    this.CrewService.AddCrewTags(result, tagList);
                }
            }
            catch (Exception e) { 
                throw new HttpException(500,e.Message);
            }
            if (result == 0)
            {
                throw new HttpException(500, "Entity could not be created.");
            }
            base.FlagCacheUpdate(StaticTokens.CachingTokens.CrewEnum);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            if (this.CurrentUser != null || (this.CurrentUser != null && this.CurrentUser.SID < Crew.SIDEnum.Admin))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Authentication error.  You must log in to perform this action.");
            }
            if (CurrentUser != null)
            {
                this.CrewService.Delete(id, CurrentUser.ID, this.Context);
                var data = GetAll();
                base.FlagCacheUpdate(StaticTokens.CachingTokens.CrewEnum);
                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }
            else
            {
                throw new HttpException(500, "Authentication error.  You must log in to perform this action.");
            }

        }

        public async Task<ActionResult> VerifyIdentity(string username)
        {
            this.GetSessionVariables();
            IEnumerable<SecurityQuestion> data = null;
            try
            {
                data = await this.CrewService.GetQuestions(username, this.Context);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict, "User not found");
            }
            return this.PartialView("SecQuestions", data);
        }

        [HttpPost]
        public async Task<ActionResult> PostAnswers(IEnumerable<SecurityQuestion> data, int id)
        {
            //Next
            var token = await this.CrewService.MatchAnswersAndSetToken(id, data, this.Context);
            Dictionary<int, string> result = new Dictionary<int, string>();
            result.Add(id, token);
            return this.View(result);
        }

        public async Task<ActionResult> CheckPassword(string password)
        {
            if (Crew.VerifyPasswordRequirements(password))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }
            else
            {
                return new HttpStatusCodeResult(
                    HttpStatusCode.ExpectationFailed,
                    "Password does not meet requirements.  Please verify your password is between 7 and 28 characters and has an uppercase, lowercase and number.");
            }
        }

        [HttpPost]
        public async Task<ActionResult> ProcessAnswer(SecurityQuestion securityQuestion)
        {
            var crew = Session[StaticTokens.CachingTokens.UserSessionResources] as CrewSecurity;
            if (securityQuestion == null || !this.ModelState.IsValid)
            {
                return new HttpStatusCodeResult(
                    HttpStatusCode.BadRequest,
                    "Both question and answer must be 7-255 characters in length.");
            }
            
            if (crew == null || crew.ID != securityQuestion.CrewID)
            {
                return new HttpStatusCodeResult(
                    HttpStatusCode.Forbidden,
                    "It appears you are not logged in under the correct credentials.  Please log back in and try again.");
            }
            if (this.ModelState.IsValid && securityQuestion != null)
            {
                try
                {
                    await this.CrewService.ProcessQuestion(securityQuestion);
                    
                }
                catch (Exception ex)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Conflict, ex.Message);
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        public async Task<ActionResult> NewQuestion()
        {
            var crew = Session[StaticTokens.CachingTokens.UserSessionResources] as CrewSecurity;
            if (crew == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Sorry.  You must be logged in to do that.");
            }
            var question = new SecurityQuestion() { CrewID = crew.ID };
            return this.PartialView("_QuestionPartial", question);
        }

        public async Task<ActionResult> UpdatePassword(string token, string password, int id)
        {
            await this.CrewService.ResetPassword(id, token, password, this.Context);
            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        public async Task<ActionResult> LoadQuestions()
        {
            IEnumerable<SecurityQuestion> result= null;
            if (this.CurrentUser != null)
            {
                result = await this.CrewService.GetQuestions(this.CurrentUser.ID, this.Context);
            }
            return this.View(result);
        }

        public ActionResult New(int? id=null)
        {
            this.GetSessionVariables();
            SailingSiteModels.New.Crew result = new SailingSiteModels.New.Crew();
            result.Password = "null";
            if (id != null)
            {
                var entity = this.CrewService.Get(new GeneralSearchRequest
                {
                    ID = id
                });
                result = entity.FirstOrDefault();
            }
            return View(result);
        }

        public ActionResult Get(int id)
        {
            this.GetSessionVariables();
            SailingSiteModels.New.Crew data = this.CrewService.GetSingle(id);
            if (data != null)
            {
                return View(data);
            }
            else return this.View("NullObject");
        }

        public ActionResult SearchByLetter(string letter)
        {
            var request = new GeneralSearchRequest();
            if (string.IsNullOrEmpty(letter) || !char.IsLetter(letter[0]))
            {
                this.Search(request);
            }

            request.FirstInitial = letter;
            return this.Search(request);
        }

        public ActionResult Search(GeneralSearchRequest query)
        {
            this.Session[StaticTokens.Security.IsAdminToken] = this.CurrentUser.SID >= Crew.SIDEnum.Admin;
            IEnumerable<SailingSiteModels.New.Crew> result = null;
            result = this.CrewService.Get(query);
            return View("_CrewUL",result);
        }

        public ActionResult Select(int id)
        {
            return null;
        }

        [HttpPost]
        public ActionResult DeleteTag(int id, int entityId)
        {
            this.CrewService.DeleteCrewTags(id,entityId);
            var result = this.CrewService.GetCrewTag(id);
            ViewData["controller"] = "crew";
            ViewData["entityID"] = id;
            return PartialView("TagList", result);
        }

        public void AddPhoto(int entityid, int photoId)
        {
            this.CrewService.AddPhoto(entityid,photoId);
        }

        public void DeletePhoto(int entityid, int photoid)
        {
            this.CrewService.DeletePhoto(entityid,photoid);
        }

        private IEnumerable<SailingSiteModels.New.CrewFoundation> GetAll()
        {
            IEnumerable<SailingSiteModels.New.CrewFoundation> data = null;
            data = this.CrewService.GetAll_Foundation();
            return data;
        }
	}
}