﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    public class AnimalDetails :Animal
    {
        public List<SailBase> Spottings;
        public int[] CommentIDs;
        public int[] SailIDs;
       
    }
}
