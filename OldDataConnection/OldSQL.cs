﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading.Tasks;
namespace OldDataConnection
{
    public static class OldSQL
    {
        #region Helper Classes
        public class HelperYear
        {
            public int Year;
            public List<int> Months = new List<int>();

        }
        public class AnniversaryMessage
        {
            public string Name;
            public string Message;
            public int Index;
        }
        public class SailingStat
        {
            public DateTime Date;
            public float WindMPHHigh;
            public float WindMPHLow;
            public float TempFar;
            public float DistanceMiles;
        }
        public class Vessel
        {
            public static List<Vessel> VesselList = new List<Vessel>();
            public string Name { get; set; }
            public bool Default = false;
            public static void Add(Vessel vessel)
            {
                if (vessel.Default)
                {
                    foreach (Vessel v in VesselList)
                    {
                        if (v.Default)
                        {
                            v.Default = false;
                        }
                    }
                }
                VesselList.Add(vessel);
            }
        }
        public class SailingDate
        {
            public DateTime Date;
            public string Location;
            public float Barometer;
            public float Temp;
            public float MinWind;
            public float MaxWind;
            public float Distance;
            public float Hours;
            public string Notes;
            public string Vessel;
            public string WindDirection;
            public List<string> Crew = new List<string>();
            public List<string> Maneuvers = new List<string>();
            public List<string> Configurations = new List<string>();
            public SailingSiteModels.Old.Sail ToOldSail()
            {   
                List<SailingSiteModels.Old.Configuration> configs=new List<SailingSiteModels.Old.Configuration>();
                foreach (var c in Configurations)
                {
                    configs.Add(new SailingSiteModels.Old.Configuration() {Name=c});
                }

                List<SailingSiteModels.Old.Crew> crew=new List<SailingSiteModels.Old.Crew>();
                foreach (var c in Crew)
                {
                    crew.Add(new SailingSiteModels.Old.Crew{  Name=c});
                
                }

                List<SailingSiteModels.Old.Maneuvers> maneuvers=new List<SailingSiteModels.Old.Maneuvers>();
                foreach (var m in Maneuvers)
                {
                    maneuvers.Add(new SailingSiteModels.Old.Maneuvers() {Name=m});
                }

                return new SailingSiteModels.Old.Sail()
                {
                    Barometer = (decimal)this.Barometer,
                    WindLow = (decimal)MinWind,
                    WindHigh = (decimal)MaxWind,
                    Comment = Notes,
                    Hours = (decimal)this.Hours,
                    Temperature = (decimal)this.Temp,
                    Miles = (decimal)this.Distance,
                    Vessel = new SailingSiteModels.Old.Vessel() { Name = this.Vessel },
                    Configurations = configs,
                    Maneuvers = maneuvers,
                    Date = this.Date,
                    Location = new SailingSiteModels.Old.Location() { Name = this.Location },
                    SailCrew = crew,
                    Direction="W",
                    WindDirection=this.WindDirection

                };
            }

            public SailingDate(DateTime date, string location, string vessel, float barometer, float temp, float minwind, float maxwind, float distance, float hours, string notes, string windDirection)
            {
                Vessel = vessel;
                Date = date;
                Barometer = barometer;
                Temp = temp;
                MinWind = minwind;
                MaxWind = maxwind;
                Location = location;
                Distance = distance;
                Notes = notes;
                WindDirection=windDirection;
            }
            public void AddCrew(string name)
            {
                Crew.Add(name);
            }
            public void AddConfiguration(string name)
            {
                Configurations.Add(name);
            }
            public void AddManeuver(string name)
            {
                Maneuvers.Add(name);
            }
        }
        public class SailingComment
        {
            public string Message;
            public string User;
            public DateTime EntryDate;
        }

        #endregion



        public class SQLStuff
        {
            #region Conversion Methods

            private static bool CheckNewSailExists(SailingSiteModels.New.Sail entity)
            {
                SqlConnection con = new SqlConnection(newconn);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Count(SailID) from Logs.Sail Where SailID=@ID", con);
                        com.Parameters.AddWithValue("@ID", entity.ID);
                        int count = com.ExecuteNonQuery();
                        if (count > 0)
                        {
                            return true;
                        }
                    }
                    catch { }
                }
                return false;
            }

            private static void LogNewConfiguration (int SailID, int EntityID)
            {
                var retVal=0;
                SqlConnection con=new SqlConnection(newconn);
                using (con)
                {
                    con.Open();
                    SqlCommand com=new SqlCommand("Insert into Log.Sails.Crew (SailID,CrewID) Values (@SailID, @CrewID)",con);
                    com.Parameters.AddWithValue("@SailID", SailID);
                    com.Parameters.AddWithValue("@ConfigID",EntityID);
                    com.ExecuteNonQuery();
                }
            }

            private static void LogNewCrew (int SailID, int EntityID)
            {
                var retVal=0;
                SqlConnection con=new SqlConnection(newconn);
                using (con)
                {
                    con.Open();
                    SqlCommand com=new SqlCommand("Insert into Log.Sails.Crew (SailID,CrewID) Values (@SailID, @CrewID)",con);
                    com.Parameters.AddWithValue("@SailID", SailID);
                    com.Parameters.AddWithValue("@ConfigID",EntityID);
                    com.ExecuteNonQuery();
                }
            }

            private static void LogNewManeuver (int SailID, int EntityID)
            {
                var retVal=0;
                SqlConnection con=new SqlConnection(newconn);
                using (con)
                {
                    con.Open();
                    SqlCommand com=new SqlCommand("Insert into Log.Sails.Maneuver (SailID,ManeuverID) Values (@SailID, @EntityID)",con);
                    com.Parameters.AddWithValue("@SailID", SailID);
                    com.Parameters.AddWithValue("@EntityID",EntityID);
                    com.ExecuteNonQuery();
                }
            }


            private static void LogConfig (int SailID, int EntityID)
            {
                var retVal=0;
                SqlConnection con=new SqlConnection(newconn);
                using (con)
                {
                    con.Open();
                    SqlCommand com=new SqlCommand("Insert into Log.Sails.Config (SailID,ConfigID) Values (@SailID, @ConfigID)",con);
                    com.Parameters.AddWithValue("@SailID", SailID);
                    com.Parameters.AddWithValue("@ConfigID",EntityID);
                    com.ExecuteNonQuery();
                }
            }

            public static int LogNewSail(SailingSiteModels.New.Sail entity)
            {
                
                 var retVal=0;
                 SqlConnection con = new SqlConnection(newconn);

                Console.WriteLine("Sail bearing found: {0}",bearing);
                using (con)
                {
                    con.Open();
                        SqlCommand com = new SqlCommand("Insert into Log.Sail Output SailId (Date, VesselID, LocationID, TempF, Barometer, MinWind,MaxWind,WindDirection, SailDirection,Hours,Miles,MilesMotored,ShipLog) VALUES (@Date,@VesselID,@LocationID,@Temp,@Barometer,@Minwind,@Maxwind,@WindDirection,@SailDirection,@Hours,@Miles,@MilesMotored,'1')", con);
                        com.Parameters.AddWithValue("@Date",entity.Date);
                        com.Parameters.AddWithValue("@VesselID",entity.Vessel.ID);
                        com.Parameters.AddWithValue("@LocationID",entity.Location.ID);
                        com.Parameters.AddWithValue("@Temp",entity.TempF);
                        com.Parameters.AddWithValue("@Barometer",entity.Barometer);
                        com.Parameters.AddWithValue("@Minwind",entity.MinWind);
                        com.Parameters.AddWithValue("@Maxwind",entity.MaxWind);
                        com.Parameters.AddWithValue("@WindDirection",entity.WindDirection);
                        com.Parameters.AddWithValue("@Hours",entity.HoursSailed);
                        com.Parameters.AddWithValue("@SailDirection",bearing);
                        com.Parameters.AddWithValue("@Miles",entity.MilesSailed);
                        com.Parameters.AddWithValue("@MilesMotored",entity.MilesMotored);
                    try {
                        SqlParameter param = new SqlParameter("@SailID", System.Data.SqlDbType.Int, 4);
                        param.Direction = System.Data.ParameterDirection.Output;
                        com.ExecuteNonQuery();
                        int.TryParse(com.Parameters["@VesselID"].ToString(), out retVal);
                        Console.WriteLine("Sail succesfully logged as SailID:{0}",retVal);
                    }
                    catch (Exception ex) {
                        LogSQLException(ex);
                        return -4;
                    }
                        
                }

                foreach (var crew in entity.SailCrew)
                {
                    LogNewCrew(retVal,crew.ID);
                }

                foreach (var config in entity.Configurations)
                {
                    LogNewConfiguration(retVal, config.ID);
                }

                foreach(var maneuver in entity.Maneuvers)
                {
                    LogNewManeuver(retVal,maneuver.ID);
                }

                if (entity.Comments!=null&&entity.Comments.Any())
                {
                    Console.WriteLine("Comments detected processing comment.");
                    StringBuilder sb=new StringBuilder();
                    foreach (var comment in entity.Comments)
                    {
                        sb.Append(comment.Message+" ");
                    }
                    string comm=sb.ToString();
                    if (!string.IsNullOrWhiteSpace(comm.Trim()))
                    {
                        Console.WriteLine("Comment detected.  Parsing...");
                        ParseComment(comm,retVal);
                    }
                }

                return retVal;
            }

          
            private static Dictionary<int, string> Animals=new Dictionary<int,string>();
            private static void ParseComment(string comment, int sailId)
            {
                if (!Animals.Any())
                {
                    LoadAnimals();
                }
                Console.WriteLine("Comment is {0}",comment);
                List<int> animalIDs=new List<int>();
                int? heel=null;
                foreach (var animal in Animals)
                {
                    if (comment.Contains(animal.Value))
                    {
                        Console.WriteLine("{0} detected in comment.",animal.Value);
                        if (!animalIDs.Contains(animal.Key))
                        {
                            Console.WriteLine("Logging {0} to sail!",animal.Value);
                            animalIDs.Add(animal.Key);
                        }
                        else Console.WriteLine("{0} already in list.",animal.Value);
                    
                    }
                }

                string heelStr=@"\bheel[a-z]{0,5}\s\w*\s*\d+";
                string numRegex=@"\d+";

                var heelRegex=Regex.Matches(comment.ToLower(), heelStr);
                if (heelRegex.Count>0)
                {
                    foreach (var match in heelRegex)
                    {
                        Console.WriteLine("Heeling detected in comment line: {0}",match.ToString());
                        foreach (var num in Regex.Matches(numRegex,match.ToString()))
                        {
                            Console.WriteLine("Angle detected was {0}",num.ToString());
                            int possibleHeel;
                            int.TryParse(num.ToString(), out possibleHeel);
                            if (possibleHeel>0)
                            {
                                if (possibleHeel<60)
                                {
                                    if (heel==null||possibleHeel>heel)
                                    {
                                        Console.WriteLine("Highest heel value set to {0}",possibleHeel.ToString());
                                        heel=possibleHeel;
                                    }
                                    else Console.WriteLine("Higher heel of {0} already set for this sail.", heel);
                                }
                                else Console.WriteLine("Boat heel above 60 unlikely without capsize.  Manual override required to set this value.");
                            }
                            else Console.WriteLine("Could not parse number.");
                        }
                    }
                }

                if (animalIDs.Any())
                {
                    Console.WriteLine("Processing animals spotted.");
                    foreach (var id in animalIDs)
                    {
                        LogAnimals(sailId,id);
                    }
                }
                if (heel!=null)
                {
                    UpdateHeel(sailId,(int)heel);
                }

                LogNewComment(comment,sailId);
                
               
            }

            private static void UpdateHeel(int sailID, int heel)
            {
                Console.WriteLine("Updating SailID{0}'s heel value.",sailID);
                SqlConnection con=new SqlConnection(newconn);
                using (con)
                {
                    con.Open();
                    SqlCommand comm=new SqlCommand("UPDATE Logs.Sail where [SailID]=@SailID SET [MaxTilt]=@Heel",con);
                    comm.Parameters.AddWithValue("@SailID",sailID);
                    comm.Parameters.AddWithValue("@Heel",heel);
                    try {
                        comm.ExecuteNonQuery();
                        Console.WriteLine("Setting SailID {0} to have a maximum heel value of {1}",sailID.ToString(),heel.ToString());
                    }
                    catch (Exception ex)
                    {
                        LogSQLException(ex);
                    }
                }
            }

            private static void LogSQLException(Exception ex)
            {
                string output=ex.InnerException==null? ex.Message:ex.InnerException.Message;
                Console.WriteLine("Small issue!  SQL threw a fit.  Here's the details: {0}", output);
            }

            public static void LogAnimals (int sailID, int animalId)
            {
                SqlConnection con=new SqlConnection(newconn);
                using (con)
                {
                    con.Open();
                    SqlCommand comm=new SqlCommand("INSERT INTO Logs.Sails.Animal (SailID, AnimalID) VALUES (@SailID, @AnimalID)",con);
                    comm.Parameters.AddWithValue("@SailID",sailID);
                    comm.Parameters.AddWithValue("@AnimalID",animalId);
                    try {
                        comm.ExecuteNonQuery();
                        Console.WriteLine("Added AnimalID{0} to SailID{1}",animalId,sailID);
                    }
                    catch (Exception ex) {
                      LogSQLException(ex);
                    }

                }
            }

            private static int? sysAdminID=null;
            private static int GetSysAdminID()
            {
             
                if (sysAdminID!=null) return (int)sysAdminID;
                else {
                    Console.WriteLine("Finding Sysadmin ID.");
                    SqlConnection con=new SqlConnection(newconn);
                    using(con)
                    {
                        con.Open();
                        SqlCommand comm=new SqlCommand("SELECT [CrewID] from Entities.Crew where [FirstName]='SysAdmin'",con);
                        int i=0;
                        
                        try {
                            SqlDataReader dr=comm.ExecuteReader();
                        
                        while (dr.Read())
                        {
                           
                            int.TryParse(dr[0].ToString(), out i);
                           
                        }
                        }
                        catch (Exception ex) {
                         LogSQLException(ex);
                        }
                        if (i!=0)
                        {
                            sysAdminID=i;
                            return i;
                        }
                    }
                }
                return 0;
            }
            public static void LogNewComment(string comment, int SailID)
            {
                int commentID=0;
                SqlConnection con=new SqlConnection(newconn);
                using (con)
                {
                    con.Open();
                    Console.WriteLine("Adding comment {0} to entities.",comment);
                    SqlCommand comm=new SqlCommand("INSERT INTO Logs.Comments.Sail Output Inserted.CommentID (CrewID, Comment, Subject, DateModified, DateCreated) Values (@CrewID, @Comment, @Subject, @DateModified, @DateCreated)",con);
                    comm.Parameters.AddWithValue("@CrewID",GetSysAdminID());
                    comm.Parameters.AddWithValue("@Comment",comment);
                    comm.Parameters.AddWithValue("@Subject","Ship's Log");
                    comm.Parameters.AddWithValue("@DateModified",DateTime.Today);
                    comm.Parameters.AddWithValue("@DateCreated",DateTime.Today);
                    SqlParameter param = new SqlParameter("@CommentID", System.Data.SqlDbType.Int, 4);
                    param.Direction = System.Data.ParameterDirection.Output;
                    try {
                        comm.Parameters.Add(param);
                        int id = comm.ExecuteNonQuery();
                        int.TryParse(comm.Parameters["@VesselID"].ToString(), out commentID);
                        Console.WriteLine("Comment assigned ID {0}",commentID);
                    }
                    catch (Exception ex)
                    {
                        LogSQLException(ex);
                    }
                    con.Close();

                    Console.WriteLine("Adding Comment to SailID{0}",SailID);

                    con.Open();
                    SqlCommand comm2=new SqlCommand("Insert into Logs.Comments.Sail ([commentid],[sailid]) VALUES (@SailID,@CommentID)",con);
                    comm2.Parameters.AddWithValue("@SailID",SailID);
                    comm2.Parameters.AddWithValue("@CommentID",commentID);
                    try {
                        comm2.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        LogSQLException(ex);
                    }
                }
            }

            private static void LoadAnimals()
            {
                SqlConnection con=new SqlConnection(newconn);
                using (con)
                {
                    con.Open();
                    SqlCommand comm=new SqlCommand("Select AnimalName, AnimalID from [Entities.Animals.Names]",con);
                    SqlDataReader dr=comm.ExecuteReader();
                    while (dr.Read())
                    {
                        int id;
                        string name=dr[0].ToString();
                        int.TryParse(dr[1].ToString(), out id);
                        try {
                        Animals.Add(id, name);
                        Console.WriteLine("Adding {0} to dictionary of animals.", name);
                        }
                        catch {
                            Console.WriteLine("Could not load {0} to dictionary of animals.  It may have already been added.", name);
                        }
                    }

                    con.Close();
                    con.Open();
                    SqlCommand comm2=new SqlCommand("Select Alias, AnimalID from [Logs.Animals.Alias]",con);
                    SqlDataReader dr2=comm2.ExecuteReader();
                    while (dr2.Read())
                    {
                        int id;
                        string name=dr[0].ToString();
                        int.TryParse(dr[1].ToString(), out id);
                        try {
                        Animals.Add(id, name);
                        Console.WriteLine("Adding {0} to dictionary of animals.", name);
                        }
                        catch {
                            Console.WriteLine("Could not load {0} to dictionary of animals.  It may have already been added.", name);
                        }
                    }
                }
            }

            private static bool CheckNewConfigExists(SailingSiteModels.New.Configuration entity)
            {
                SqlConnection con = new SqlConnection(newconn);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Count(ConfigID) from Entity.Config Where Name='" + entity.Name + "'", con);
                        int count = com.ExecuteNonQuery();
                        if (count > 0)
                        {
                            return true;
                        }
                    }
                    catch { }
                }
                return false;
            }

            public static int AddNewConfig(SailingSiteModels.New.Configuration entity)
            {
                SqlConnection con = new SqlConnection(newconn);
                if (CheckNewConfigExists(entity))
                {
                    return -4;
                }
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Insert into Entities.Config Output Inserted.ConfigID (Name) VALUES (@Name)", con);
                        com.Parameters.AddWithValue("@Name", entity.Name);
                        SqlParameter param = new SqlParameter("@ConfigID", System.Data.SqlDbType.Int, 4);
                        param.Direction = System.Data.ParameterDirection.Output;
                        com.Parameters.Add(param);
                        int id = com.ExecuteNonQuery();
                        int retVal;
                        int.TryParse(com.Parameters["@VesselID"].ToString(), out retVal);
                        if (retVal > 0)
                        {
                            return retVal;
                        }
                        return -1;

                    }
                    catch { return -1; }
                }
            }


            private static bool CheckNewVesselExists(SailingSiteModels.New.Vessel entity)
            {
                SqlConnection con = new SqlConnection(newconn);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Count(VesselID) from Entity.Vessel Where Name='" + entity.Name + "'", con);
                        int count = com.ExecuteNonQuery();
                        if (count > 0)
                        {
                            return true;
                        }
                    }
                    catch { }
                }
                return false;
            }

            public static int AddNewVessel(SailingSiteModels.New.Vessel entity)
            {
                SqlConnection con = new SqlConnection(newconn);
                if (CheckNewVesselExists(entity))
                {
                    return -4;
                }
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Insert into Entities.Vessel Output Inserted.VesselID (Name) VALUES (@Name)", con);
                        com.Parameters.AddWithValue("@Name", entity.Name);
                        SqlParameter param = new SqlParameter("@VesselID", System.Data.SqlDbType.Int, 4);
                        param.Direction = System.Data.ParameterDirection.Output;
                        com.Parameters.Add(param);
                        int id = com.ExecuteNonQuery();
                        int retVal;
                        int.TryParse(com.Parameters["@VesselID"].ToString(), out retVal);
                        if (retVal > 0)
                        {
                            return retVal;
                        }
                        return -1;

                    }
                    catch { return -1; }
                }
            }

            private static bool CheckNewCrewExists(SailingSiteModels.New.Crew crew)
            {
                SqlConnection con = new SqlConnection(newconn);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Count(CrewID) from Entity.Crew Where FirstName='" + crew.FirstName + "' and LastName='" + crew.LastName + "'", con);
                        int count = com.ExecuteNonQuery();
                        if (count > 0)
                        {
                            return true;
                        }
                    }
                    catch { }
                }
                return false;
            }

            public static int AddNewCrew(SailingSiteModels.New.Crew crew)
            {
                SqlConnection con = new SqlConnection(newconn);
                if (CheckNewCrewExists(crew))
                {
                    return -4;
                }
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Insert into Entities.Crew Output Inserted.CrewID (FirstName, LastName, Username, Password, Salt) VALUES (@FirstName, @LastName, @Username, @Password, @Salt)", con);
                        com.Parameters.AddWithValue("@FirstName", crew.FirstName);
                        com.Parameters.AddWithValue("@LastName", crew.LastName);
                        com.Parameters.AddWithValue("@UserName", crew.UserName);
                        com.Parameters.AddWithValue("@Password", crew.Password);
                        com.Parameters.AddWithValue("@Salt", crew.Salt);
                        SqlParameter param = new SqlParameter("@CrewID", System.Data.SqlDbType.Int, 4);
                        param.Direction = System.Data.ParameterDirection.Output;
                        com.Parameters.Add(param);
                        int id = com.ExecuteNonQuery();
                        int retVal;
                        int.TryParse(com.Parameters["@CrewID"].ToString(), out retVal);
                        if (retVal > 0)
                        {
                            return retVal;
                        }
                        return -1;

                    }
                    catch { return -1; }
                }
            }

            private static bool CheckNewManeuverExists(SailingSiteModels.New.Maneuver entity)
            {
                SqlConnection con = new SqlConnection(newconn);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Count(Name) from Entity.Maneuvers Where Name='" + entity.Name + "'", con);
                        int count = com.ExecuteNonQuery();
                        if (count > 0)
                        {
                            return true;
                        }
                    }
                    catch { }
                }
                return false;
            }


            public static int AddNewLocation(SailingSiteModels.New.Location entity)
            {
                if (!CheckLocationExists(entity))
                {
                    try
                    {
                        SqlConnection con = new SqlConnection(newconn);
                        SqlCommand com = new SqlCommand("Insert into Entities.Vessel Output Inserted.VesselID (Name) VALUES (@Name)", con);
                        com.Parameters.AddWithValue("@Name", entity.Name);
                        SqlParameter param = new SqlParameter("@VesselID", System.Data.SqlDbType.Int, 4);
                        param.Direction = System.Data.ParameterDirection.Output;
                        com.Parameters.Add(param);
                        int id = com.ExecuteNonQuery();
                        int retVal;
                        int.TryParse(com.Parameters["@VesselID"].ToString(), out retVal);
                        if (retVal > 0)
                        {
                            return retVal;
                        }
                        return -1;

                    }
                    catch { return -1; }
                }
                else return -4;
            }


            public static bool CheckLocationExists(SailingSiteModels.New.Location entity)
            { 
                SqlConnection con = new SqlConnection(newconn);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Count(Name) from Entity.Vessel Where Name='" + entity.Name + "'", con);
                        int count = com.ExecuteNonQuery();
                        if (count > 0)
                        {
                            return true;
                        }
                    }
                    catch { }
                }
                return false;
            }   
            

            public static int AddNewManeuver(SailingSiteModels.New.Maneuver entity)
            {
                SqlConnection con = new SqlConnection(newconn);
                if (CheckNewManeuverExists(entity))
                {
                    return -4;
                }
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Insert into Entities.Maneuvers Output Inserted.ManeuverID (Name) VALUES (@Name)", con);
                        com.Parameters.AddWithValue("@Name", entity.Name);
                        SqlParameter param = new SqlParameter("@ManeuverID", System.Data.SqlDbType.Int, 4);
                        param.Direction = System.Data.ParameterDirection.Output;
                        com.Parameters.Add(param);
                        int id = com.ExecuteNonQuery();
                        int retVal;
                        int.TryParse(com.Parameters["@ManeuverID"].ToString(), out retVal);
                        if (retVal > 0)
                        {
                            return retVal;
                        }
                        return -1;

                    }
                    catch { return -1; }
                }
            }

            #endregion 
            #region Private Fields
            private static Random rdm = new Random();
            private static List<int> _messagesCalled = new List<int>();
            private static string connection = @"Data Source=192.168.254.34\SQLEXPRESS;Initial Catalog=bureau;User id=inquire;Password=inquire";
            private static string sailingcon = @"Data Source=192.168.254.34\SQLEXPRESS;Initial Catalog=bureau;User id=inquire;Password=inquire";
            private static string testconnection = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=bureau;User id=inquire;Password=inquire";
            private static string newconn = @"Data Source=192.168.254.34\SQLEXPRESS;Initial Catalog=sailing;User id=inquire;Password=inquire";
            #endregion
            #region TestFunctions
            public static int TestSQLByIPAddy()
            {
                SqlConnection con = new SqlConnection(connection);
                int output = 0;
                using (con)
                {
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select count(name) from Messages", con);
                        string t = com.ExecuteScalar().ToString();
                        int.TryParse(t, out output);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            public static int TestSQLByLocalHost()
            {
                SqlConnection con = new SqlConnection(testconnection);
                int output = 0;
                using (con)
                {
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select count(name) from Messages", con);
                        string t = com.ExecuteScalar().ToString();
                        int.TryParse(t, out output);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            #endregion
            # region Sailing Functions
            #region Logging Functions

            public static int DeleteSail(DateTime date, string vessel)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("DELETE from Sailing_Log WHERE Date='" + date.ToString() + "' AND Vessel='" + vessel + "'", con);
                        SqlCommand com2 = new SqlCommand("DELETE from Sailing_ManeuverNames WHERE Date='" + date.ToString() + "' AND Vessel='" + vessel + "'", con);
                        SqlCommand com3 = new SqlCommand("DELETE from Sailing_CrewLog WHERE Date='" + date.ToString() + "' AND Vessel='" + vessel + "'", con);
                        SqlCommand com4 = new SqlCommand("DELETE from Sailing_SailConfig WHERE Date='" + date.ToString() + "' AND Vessel='" + vessel + "'", con);
                        output += com.ExecuteNonQuery();
                        output += com2.ExecuteNonQuery();
                        output += com3.ExecuteNonQuery();
                        output += com4.ExecuteNonQuery();

                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            public static int LogSail(DateTime date, string location, float temp, float barometer, float distance, float hours, float minWind, float maxWind, string vessel, List<string> crew, string winddir, string notes = "", List<string> maneuvers = null, List<string> configs = null)
            {
                // Add wind direction!!
                int output = 0;
                string extendedNotes = "";
                notes = notes.Replace("'", "''");
                if (notes.Length > 255)
                {
                    extendedNotes = notes;
                    notes = notes.Substring(0, 254);
                }
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    con.Open();

                    SqlCommand comm = new SqlCommand("Insert into Sailing_Log (Date, Location, Vessel,WindMPHMin,WindMPHMax, WindDir, DistanceMiles,Barometer, TempFar, Notes,ExtendedNotes) VALUES ('" + date.ToString() + "','" + location + "','" + vessel + "','" + minWind.ToString() + "','" + maxWind.ToString() + "','" + winddir + "','" + distance.ToString() + "','" + barometer.ToString() + "','" + temp + "','" + notes + "','" + extendedNotes + "')", con);
                    output += comm.ExecuteNonQuery();

                    foreach (string c in crew)
                    {
                        output += LogCrew(c, vessel, date);
                    }
                    if (maneuvers.Count > 0)
                    {
                        foreach (string m in maneuvers)
                        {
                            // Log
                            output += LogManeuver(m, vessel, date);
                        }
                    }
                    if (configs.Count > 0)
                    {
                        foreach (string c in configs)
                        {
                            output += LogConfiguration(c, vessel, date);
                        }
                    }
                }
                return output;
            }

            public static int LogCrew(string name, string vessel, DateTime date)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO SAILING_CREWLOG (CrewName, Vessel, Date) Values ('" + name + "','" + vessel + "','" + date.ToString() + "')", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch { }
                }
                return output;
            }



            public static int LogConfiguration(string name, string vessel, DateTime date)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO SAILING_SAILCONFIG (SailConfig, Vessel, Date) Values ('" + name + "','" + vessel + "','" + date.ToString() + "')", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch { }
                }
                return output;

            }


            public static int LogManeuver(string name, string vessel, DateTime date)
            {
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO SAILING_MANEUVERS (Maneuvername, Vessel, Date) Values ('" + name + "','" + vessel + "','" + date.ToString() + "')", con);
                        return com.ExecuteNonQuery();
                    }
                    catch { return 0; }
                }
            }
            #endregion
            #region Sailing Page Default Values
            // TODO: Make links to delete or edit entries into LINKS that send PARAMS!
            public static List<SailingDate> GetSailingDate(DateTime date)
            {
                List<SailingDate> list = new List<SailingDate>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Vessel, Location, WindMPHMin, WindMPHMax, Barometer, TempFar, DistanceMiles, WindDir, Notes FROM Sailing_log where DATE='" + date.ToShortDateString() + "'", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            string location = dr["Location"].ToString();
                            string vessel = dr["Vessel"].ToString();
                            float barometer;
                            float.TryParse(dr["Barometer"].ToString(), out barometer);
                            float temp;
                            float.TryParse(dr["TempFar"].ToString(), out temp);
                            float windMin;
                            float.TryParse(dr["WindMPHMin"].ToString(), out windMin);
                            float windMax;
                            float.TryParse(dr["WindMPHMax"].ToString(), out windMax);
                            float distance;
                            float.TryParse(dr["DistanceMiles"].ToString(), out distance);
                            float hrs = 0;
                            string windDir=dr["WindDir"].ToString();
                            SailingDate sail = new SailingDate(date, location, vessel, barometer, temp, windMin, windMax, distance, hrs, dr["Notes"].ToString(), windDir);
                            SqlConnection con4 = new SqlConnection(connection);
                            using (con4)
                            {
                                con4.Open();
                                SqlCommand commCrew = new SqlCommand("Select CrewName from Sailing_CrewLog where Date='" + date.ToShortDateString() + "' AND Vessel='" + sail.Vessel + "'", con4);
                                SqlDataReader dr2 = commCrew.ExecuteReader();
                                while (dr2.Read())
                                {
                                    sail.AddCrew(dr2[0].ToString());
                                }
                            }
                            SqlConnection con2 = new SqlConnection(connection);
                            using (con2)
                            {
                                con2.Open();
                                SqlCommand commManeuvers = new SqlCommand("Select ManeuverName from Sailing_Maneuvers where  Date='" + date.ToShortDateString() + "' AND Vessel='" + sail.Vessel + "'", con2);
                                SqlDataReader dr3 = commManeuvers.ExecuteReader();
                                while (dr3.Read())
                                {
                                    sail.AddManeuver(dr3[0].ToString());
                                }
                            }

                            SqlConnection con3 = new SqlConnection(connection);
                            using (con3)
                            {
                                con3.Open();
                                SqlCommand commSailConfig = new SqlCommand("Select Sailconfig from Sailing_SailConfig where  Date='" + date.ToShortDateString() + "' AND Vessel='" + sail.Vessel + "'", con3);
                                SqlDataReader dr4 = commSailConfig.ExecuteReader();
                                while (dr4.Read())
                                {
                                    sail.AddConfiguration(dr4[0].ToString());
                                }
                            }
                            list.Add(sail);
                        }
                    }
                    catch { }
                    return list;
                }

            }

            public static List<DateTime> LoadDates()
            {
                List<DateTime> list = new List<DateTime>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Date from Sailing_Log ORDER BY Date DESC", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            DateTime date;
                            DateTime.TryParse(dr[0].ToString(), out date);
                            list.Add(date);
                        }
                    }
                    catch { }
                }
                return list;
            }
            public static List<SailingSiteModels.Old.Vessel> LoadVessels()
            {
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Vessel, IsDefault from Sailing_Vessels ORDER BY Vessel", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            Vessel v = new Vessel();
                            v.Name = dr[0].ToString();
                            bool.TryParse(dr[1].ToString(), out v.Default);
                            Vessel.Add(v);
                        }
                    }
                    catch { }
                }
                List<SailingSiteModels.Old.Vessel> retVal = new List<SailingSiteModels.Old.Vessel>();
                foreach (var vessel in Vessel.VesselList)
                {
                    var output = new SailingSiteModels.Old.Vessel()
                    {
                        Name = vessel.Name
                    };

                }
                return retVal;
            }

            public static List<string> LoadLocations()
            {
                List<string> list = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select Location from Sailing_Location", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                    catch { }
                }
                return list;
            }


            public static List<string> LoadSailCrew()
            {
                List<string> list = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select CrewName from Sailing_CrewName", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                    catch { }
                }
                return list;
            }

  
            public static List<string> LoadSailConfigs()
            {
                List<string> list = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select SailConfig from Sailing_SailConfigNames", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                    catch { }
                }
                return list;
            }

            public static List<string> LoadSailingManeuvers()
            {
                List<string> list = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Select ManeuverName from Sailing_ManeuverNames", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                    catch { }
                }
                return list;
            }
            #endregion
            #region Comment Functions
            public static int LogSailingComment(string vessel, string message, string user, DateTime date)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO Sailing_Comments (Vessel, Date, UserName, Entry, Entrydate) VALUES=('" + vessel + "','" + date.ToString() + "','" + user + "','" + message + "','" + System.DateTime.Now + "'", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch { }
                    return output;
                }


            }

            public static List<SailingComment> LoadSailingMessages(string vessel, DateTime date)
            {
                List<SailingComment> messages = new List<SailingComment>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("SELECT EntryDate, Entry, UserName from Sailing_Comments WHERE Vessel='" + vessel + "' AND Date='" + date.ToShortDateString() + "' ORDER BY EntryDate", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            SailingComment message = new SailingComment();
                            message.EntryDate = (DateTime)dr[0];
                            message.Message = dr[1].ToString();
                            message.User = dr[2].ToString();
                            messages.Add(message);
                        }
                    }
                    catch { }
                }
                return messages;
            }


            #endregion
            # region Chart Functions
            public static Dictionary<string, int> GetCrewCount()
            {
                Dictionary<string, int> dic = new Dictionary<string, int>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("SELECT TOP 7 CrewName, Count from Sailing_CrewCount ORDER BY Count DESC", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            string name = dr[0].ToString();
                            int count;
                            int.TryParse(dr[1].ToString(), out count);
                            dic.Add(name, count);
                        }
                    }
                    catch { }

                }
                return dic;
            }

            public static List<SailingStat> GetAllSailingStats()
            {
                List<SailingStat> list = new List<SailingStat>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("select WindMPHMin, WindMPHMax, Date,DistanceMiles, TempFar from Sailing_Log", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            SailingStat stat = new SailingStat();
                            float.TryParse(dr[0].ToString(), out stat.WindMPHLow);
                            float.TryParse(dr[1].ToString(), out stat.WindMPHHigh);
                            DateTime.TryParse(dr[2].ToString(), out stat.Date);
                            float.TryParse(dr[3].ToString(), out stat.DistanceMiles);
                            float.TryParse(dr[4].ToString(), out stat.TempFar);
                            list.Add(stat);
                        }
                    }
                    catch { }
                }
                return list;
            }
            #endregion
            #endregion
            #region Message Functions

            public static int[] GetMessagesVSBadMessages()
            {
                List<int> list = new List<int>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    con.Open();
                    int bad = 0;
                    int good = 0;
                    SqlCommand com = new SqlCommand("select Count(*) from Messages", con);
                    string i = "0";
                    i = com.ExecuteScalar().ToString();
                    int.TryParse(i, out good);
                    i = "0";
                    SqlCommand com2 = new SqlCommand("Select Count(*) from BadMessages", con);
                    i = com2.ExecuteScalar().ToString();
                    int.TryParse(i, out bad);
                    list.Add(good);
                    list.Add(bad);
                }
                return list.ToArray();
            }

            public static AnniversaryMessage GetMessageByID(int id)
            {
                SqlConnection con = new SqlConnection(connection);
                AnniversaryMessage output = new AnniversaryMessage();
                using (con)
                {
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select name, message, rowid from Messages where rowid='" + id + "'", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            string name = dr[0].ToString();
                            string message = dr[1].ToString();
                            string t = dr[2].ToString();
                            int.TryParse(t, out output.Index);
                        }
                    }
                    catch
                    {

                    }
                }
                return output;
            }

            public static int GetMessageCount()
            {
                SqlConnection con = new SqlConnection(connection);
                int output = 0;
                using (con)
                {
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select count(name) from Messages", con);
                        string t = com.ExecuteScalar().ToString();
                        int.TryParse(t, out output);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }
            public static List<AnniversaryMessage> GetAllMessages()
            {
                List<AnniversaryMessage> output = new List<AnniversaryMessage>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("SELECT name, message, rowid from Messages", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            AnniversaryMessage message = new AnniversaryMessage();
                            message.Name = dr[0].ToString();
                            message.Message = dr[1].ToString();
                            string t = dr[2].ToString();
                            int.TryParse(t, out message.Index);
                            output.Add(message);
                        }
                    }
                    catch { }
                    return output;
                }

            }



            public static List<AnniversaryMessage> GetAllBadMessages()
            {
                List<AnniversaryMessage> output = new List<AnniversaryMessage>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("SELECT name, message, rowid from BadMessages", con);
                        SqlDataReader dr = com.ExecuteReader();
                        while (dr.Read())
                        {
                            AnniversaryMessage message = new AnniversaryMessage();
                            message.Name = dr[0].ToString();
                            message.Message = dr[1].ToString();
                            string t = dr[2].ToString();
                            int.TryParse(t, out message.Index);
                            output.Add(message);
                        }
                    }
                    catch { }
                    return output;
                }

            }

            public static int DeleteMessage(int rowid)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("DELETE FROM Messages where RowID='" + rowid + "'", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch { return -1; }
                }
                return output;
            }

            public static string GetMessage()
            {
                int hours = System.DateTime.Now.Hour;
                if (hours > 12)
                    hours -= 12;
                int mins = System.DateTime.Now.Minute;
                int noMsgs = 0;
                int selectN = 0;
                int timesCalled = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                    try
                    {
                        con.Open();
                        // Gets the total number of elements
                        SqlCommand com = new SqlCommand("select count(name) from Messages", con);
                        object obj = com.ExecuteScalar();
                        noMsgs = Convert.ToInt32(obj.ToString());

                        // If it's half-past or top of the hour, return number of messages
                        if (mins == 0 || mins == 30)
                        {
                            return "It is " + hours + ":" + mins + " ... " + noMsgs + " messages in queue.";
                        }

                        //Randomly selects an element by Row ID (an auto incremented row.)
                        selectN = rdm.Next(1, ++noMsgs);  // Add 1 here as you're going from 1-n, not 0-n
                        // Determine how many times this number has been called
                        if (_messagesCalled.Count > 0)
                        {
                            timesCalled = _messagesCalled.Select(i => i == selectN).Count();
                        }
                        // Add the number to messages called

                        if (_messagesCalled.Count > 0) // Don't div by 0!
                        {
                            if (timesCalled > (_messagesCalled.Count() / noMsgs))  // If the number has been called a higher number of times than is statistically probable, reroll
                            {
                                selectN = rdm.Next(1, noMsgs);
                            }
                        }
                        _messagesCalled.Add(selectN);

                        // call that item
                        return CallMessage(selectN, con, noMsgs + 1);
                    }
                    catch (Exception e)
                    {
                        return e.Message + " " + e.InnerException;
                    }
            }


            private static string CallMessage(int messageNo, SqlConnection con, int messageCount, bool errorDir = false, int errorNo = -1, int errorcount = -1)
            {
                int errorInitial;
                bool error = false;
                if (errorNo != -1)
                {
                    errorInitial = errorNo;
                    error = true;
                }
                else errorInitial = messageCount;
                bool errorDown = errorDir;
                int errorCount = -1;
                if (errorcount != -1)
                    errorCount = errorcount;
                string[] wordsForWrite = new string[] { "writes", "says", "types" };  // adds a bit of flavor
                string name = "";
                string message = "";
                try
                {
                    // Get the message
                    SqlCommand com = new SqlCommand("Select Name, Message from Messages where RowID='" + messageNo.ToString() + "'", con);
                    SqlDataReader dr = com.ExecuteReader();
                    while (dr.Read())
                    {
                        name = dr[0].ToString();
                        message = dr[1].ToString();
                    }
                    if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(message))
                    {
                        Exception e = new Exception("Selection statement returned no results using:" + com.CommandText);
                        throw e;
                    }
                }
                // if it fails, scan up and down the numbers 40 times.
                catch
                {
                    int x;
                    // initialize the initial variables
                    if (error == false)
                    {
                        if (errorCount < messageCount / 2)
                            // if the error occured in the lower half.
                            errorDown = true;
                        // It should iterate over each message.
                        errorCount = messageCount;
                    }
                    else
                    {
                        // If you've gone to either extreme, start at the  initial error number and switch the direction.
                        if (messageNo < 0)
                        {
                            messageNo = errorInitial;
                            errorDown = false;
                        }
                        if (messageNo >= messageCount)
                        {
                            messageNo = errorInitial;
                            errorDown = true;
                        }
                    }
                    // If we're going up, raise by one, if we're going down decrement one
                    if (errorDown == true)
                        x = messageNo - 1;
                    else
                        x = messageNo + 1;

                    // Decrement errorcount
                    errorCount--;
                    if (errorCount > -1)
                    {
                        // Call the message.  If you've run out of errors, call 2.
                        CallMessage(x, con, messageCount, errorDown, errorInitial, errorCount);
                    }
                    else
                    {
                        // When errorcount reaches 0, return an error.
                        return "Error.  Could not load any messages.";
                    }
                }

                return name + " " + wordsForWrite[rdm.Next(0, wordsForWrite.Count())] + ": " + message;

            }



            // Message Posting Methods

            public static int PostMessage(string name, string message)
            {
                int i = 0;
                SqlConnection con = new SqlConnection(connection);
                if (name.Length > 50)
                {
                    name = name.Substring(0, 50);
                }
                if (message.Length > 150)
                {
                    message = message.Substring(0, 150);
                }
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Insert into Messages (name, message, dateentered) values ('" + name + "','" + message + "', GetDate())", con);
                        i = comm.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                    return i;
                }

            }

            public static int PostBadMessage(string name, string message)
            {
                int i = 0;
                SqlConnection con = new SqlConnection(connection);
                if (name.Length > 50)
                {
                    name = name.Substring(0, 50);
                }
                if (message.Length > 150)
                {
                    message = message.Substring(0, 150);
                }
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Insert into BadMessages (name, message) values ('" + name + "','" + message + "')", con);
                        i = comm.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                    return i;
                }
            }
            #endregion
            #region Reminder Functions
            public static int LogReminder(string emailAddress)
            {
                // Todo: Test, then put dev
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                try
                {
                    using (con)
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO AnniversaryReminders (EmailAddress, Type) VALUES ('" + emailAddress + "','Test')", con);
                        output = com.ExecuteNonQuery();
                    }
                }
                catch { }
                return output;

            }
            #endregion
            #region RSVP Functions
            public static int LogExcuse(string name, string excuse)
            {
                int output = 0;
                // Cleans excuse
                excuse = excuse.Replace("'", "//");
                excuse = excuse.Replace("\"", "//");
                if (excuse.Length > 255)
                    excuse = excuse.Substring(0, 250);
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("INSERT INTO AnniversaryExcuse (Name, Excuse) VALUES ('" + name + "','" + excuse + "')", con);
                        output = comm.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                }
                // Todo: fill this fucking class
                return output;
            }
            public static int LoadGuestNumbers(string firstName, string lastName)
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    con.Open();
                    try
                    {
                        SqlCommand com = new SqlCommand("Select TotalAllowed from AnniversaryGuests where LastName='" + lastName + "' and FirstName='" + firstName + "'", con);
                        string i = com.ExecuteScalar().ToString();
                        int.TryParse(i, out output);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            public static List<string> LoadNames(string lastName)
            {
                List<string> output = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {

                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("SELECT FirstName from AnniversaryGuests where LastName='" + lastName.ToLower() + "'", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            output.Add(dr[0].ToString());
                        }

                    }
                    catch
                    {
                        return null;
                    }
                }

                return output;
            }

            public static int LogRSVP(int guests, string lastname, string firstname, bool web = false, string excuse = "")
            {
                int output = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        string vattend = web ? "1" : "0";
                        SqlCommand com = new SqlCommand("UPDATE AnniversaryGuests SET TotalAttending='" + guests + "', VirtualAttendance='" + vattend + "' WHERE LastName='" + lastname + "' AND FirstName='" + firstname + "'", con);
                        output = com.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return output;
            }

            #endregion
            #region Guest Functions
            public static int GetGuestsAttending()
            {
                int guests = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Sum(TotalAttending) from AnniversaryGuests WHERE TotalAttending IS NOT NULL", con);
                        string t = comm.ExecuteScalar().ToString();
                        int.TryParse(t, out guests);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return guests;
            }

            public static int GetGuestsVirtual()
            {
                int guests = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select COUNT(FirstName) from  AnniversaryGuests WHERE VirtualAttendance IS NOT NULL AND VirtualAttendance!=0", con);
                        string t = comm.ExecuteScalar().ToString();
                        int.TryParse(t, out guests);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return guests;
            }

            public static int GetGuestsCheckedIn()
            {
                int guests = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Count(LastName) from AnniversaryGuests WHERE TotalAttending IS NOT NULL", con);
                        string t = comm.ExecuteScalar().ToString();
                        int.TryParse(t, out guests);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return guests;
            }

            public static int GetTotalGuests()
            {
                int guests = 0;
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select Count(LastName) from AnniversaryGuests", con);
                        string t = comm.ExecuteScalar().ToString();
                        int.TryParse(t, out guests);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return guests;
            }

            public static List<string> GetGuestListAttending()
            {
                List<string> guests = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select FirstName,LastName, TotalAttending FROM AnniversaryGuests where TotalAttending>0 ORDER BY LastName", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            string first = dr[0].ToString();
                            string last = dr[1].ToString();
                            string attending = dr[2].ToString();
                            attending = attending == "1" ? "" : "+" + attending;

                            guests.Add(first + " " + last + attending);

                        }
                    }
                    catch (Exception e) { guests.Add(e.Message + " " + e.InnerException); }
                }
                return guests;
            }

            public static Dictionary<string, int> GetPhysicalVSVirtualGuests()
            {
                Dictionary<string, int> dic = new Dictionary<string, int>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("SELECT Physically,Virtually,Declined,NoResponse FROM Anniversary_GuestCount", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            int physicalAt;
                            int virtualAt;
                            int declined;
                            int noresp;
                            int.TryParse(dr[0].ToString(), out physicalAt);
                            int.TryParse(dr[1].ToString(), out virtualAt);
                            int.TryParse(dr[2].ToString(), out declined);
                            int.TryParse(dr[3].ToString(), out noresp);

                            dic.Add("Physically", physicalAt);
                            dic.Add("Virtually", virtualAt);
                            dic.Add("Declined", declined);
                            dic.Add("No Response", noresp);

                        }


                    }
                    catch { }
                }
                return dic;
            }

            public static List<string> GetGuestListAttendingVirtually()
            {
                List<string> guests = new List<string>();
                SqlConnection con = new SqlConnection(connection);
                using (con)
                {
                    try
                    {
                        con.Open();
                        SqlCommand comm = new SqlCommand("Select FirstName,LastName FROM AnniversaryGuests where VirtualAttendance='1' ORDER BY LastName", con);
                        SqlDataReader dr = comm.ExecuteReader();
                        while (dr.Read())
                        {
                            string first = dr[0].ToString();
                            string last = dr[1].ToString();

                            guests.Add(first + " " + last);

                        }
                    }
                    catch (Exception e) { guests.Add(e.Message + " " + e.InnerException); }
                }
                return guests;
            }

            #endregion

        }

    }
}
