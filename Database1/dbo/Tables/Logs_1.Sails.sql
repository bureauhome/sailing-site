﻿CREATE TABLE [dbo].[Logs.Sails] (
    [SailID]         INT            IDENTITY (1, 1) NOT NULL,
    [Date]           DATETIME       NOT NULL,
    [VesselID]       INT            NOT NULL,
    [LocationID]     INT            NOT NULL,
    [TempF]          DECIMAL (3, 2) NOT NULL,
    [Barometer]      DECIMAL (4, 3) NOT NULL,
    [MinWind]        DECIMAL (4, 2) NOT NULL,
    [MaxWind]        DECIMAL (4, 2) NOT NULL,
    [WinDirection]   NCHAR (10)     NOT NULL,
    [SailDirection]  NCHAR (10)     NOT NULL,
    [Hours]          DECIMAL (4, 2) NOT NULL,
    [MilesMotored]   DECIMAL (8, 2) NULL,
    [Miles]          DECIMAL (8, 2) NOT NULL,
    [MaxTilt]        INT            NULL,
    [AvgWind]        DECIMAL (4, 2) NULL,
    [HoursMotored]   DECIMAL (4, 2) NULL,
    [CenterBoardPct] DECIMAL (2, 2) NULL,
    CONSTRAINT [PK_Logs.Sails] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Sails] PRIMARY KEY CLUSTERED ([SailID] ASC),
    CONSTRAINT [FK_Logs.Sails_Entities.Locations] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Entities.Locations] ([LocationID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Sails_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]) ON DELETE CASCADE
);

