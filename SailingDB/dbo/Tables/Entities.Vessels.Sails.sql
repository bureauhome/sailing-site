﻿CREATE TABLE [dbo].[Entities.Vessels.Sails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SailTypeID] [int] NOT NULL,
	[VesselID] [int] NOT NULL,
	[RollingFurler] [bit] NULL,
	[Location] [nvarchar](50) NULL,
 [Colors] NVARCHAR(128) NULL, 
    [SailArea] DECIMAL(18, 2) NULL, 
    [SailLettering] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_Entities.Vessels.Sails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Entities.Vessels.Sails]  WITH CHECK ADD  CONSTRAINT [FK_Entities.Vessels.Sails_Entities.Config] FOREIGN KEY([SailTypeID])
REFERENCES [dbo].[Entities.Config] ([ConfigID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Entities.Vessels.Sails] CHECK CONSTRAINT [FK_Entities.Vessels.Sails_Entities.Config]
GO

ALTER TABLE [dbo].[Entities.Vessels.Sails]  WITH CHECK ADD  CONSTRAINT [FK_Entities.Vessels.Sails_Entities.Vessel] FOREIGN KEY([VesselID])
REFERENCES [dbo].[Entities.Vessel] ([VesselID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Entities.Vessels.Sails] CHECK CONSTRAINT [FK_Entities.Vessels.Sails_Entities.Vessel]
GO
