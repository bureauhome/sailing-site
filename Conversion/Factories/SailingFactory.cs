﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using New = SailingSiteModels.New;
using Old = SailingSiteModels.Old;
using OldDataConnection;

namespace Conversion.Factories
{
    class SailingFactory
    {

        public static New.Sail CreateSail(Old.Sail entity, Repositories.VesselRepository vesselRepo, Repositories.CrewRepository crewRepo)
            {
                if (!entity.ValidateModel())
                {
                    Console.WriteLine("Model was not validated.");
                    return null;
                }

                var newSail=new New.Sail(){
                    Vessel=vesselRepo.Get(entity.Vessel),
                    Date=entity.Date,
                    MaxWind=entity.WindHigh,
                    MinWind=entity.WindLow,
                    Barometer=entity.Barometer,
                    MilesSailed=entity.Miles,
                    HoursSailed=entity.Hours,
                    TempF=entity.Temperature,
                    SailCrew=Repositories.CrewRepository.Get(from i in entity.SailCrew select i.Name).ToList(),
                    Configurations=Repositories.ConfigurationRepository.Get(from i in entity.Configurations select i.Name).ToList(),
                    Maneuvers=Repositories.ManeuverRepository.Get(from i in entity.Maneuvers select i.Name).ToList(),
                    Bearing=new List<SailingSiteModels.New.CompassBearing>(){new SailingSiteModels.New.CompassBearing(270,entity.Date)},
                    Location=Repositories.LocationRepository.Get(entity.Location)
                };
                
               
                newSail.SetWindDirection(entity.WindDirection);
                int id=OldSQL.SQLStuff.LogNewSail(newSail);
                newSail.SetID(id);
                return newSail;
            }

        private static New.Sail GetMilesMotored(New.Sail entity)
        {
            var motored = entity.Configurations.Where(i => i.Name.ToLower().Contains("motor only"));
            if (motored.Any()||(entity.MilesSailed < (decimal).6&&entity.Date.Year<2015))
            {
                entity.MilesMotored = entity.MilesSailed;
                entity.MilesSailed = 0;
            }
            else {
                if (entity.Date.Year < 2015)
                {
                    entity.MilesSailed -=(decimal) .6;
                    entity.MilesMotored += (decimal).6;
                }
            }
            return entity;

        }
    }
}
