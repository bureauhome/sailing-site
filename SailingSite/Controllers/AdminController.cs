﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.Net;

    using SailingSite.Common;
    using SailingSite.Models;
    using SailingSite.ViewModels;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    /// <summary>
    /// The admin controller.
    /// </summary>
    public class AdminController : BaseController
    {
        // GET: Admin
        public ActionResult Index()
        {

            if (this.CurrentUser == null)
            {
                var redirectModel = new RedirectModel() { ActionName = "Index", ControllerName = "Admin" };
                return this.View("_AdminRedirect", redirectModel);
            }

            if (this.CurrentUser.SID != Crew.SIDEnum.Admin)
            {
                this.LoggingService.GenerateLog("Fail-User attempted to access admin site with no permissions.", Log.LogTypes.AuditAdmin, this.Context);
                var redirectModel = new RedirectModel() { ActionName = "Index", ControllerName = "Admin" , UserBlocked = true, Description = "You do not have permission to access this page."};
                return this.View("NoPermissionPage");
            }
            this.LoggingService.GenerateLog("User logged into admin site.", Log.LogTypes.AuditAdmin, this.Context);
            var data = this.AdminService.GetAdminModel();
            this.ViewData[StaticTokens.Admin.AdminBanner] = this.GetBanner();
            return this.View(data);
        }

        public ActionResult NukeCache()
        {
            if (this.CurrentUser.SID == Crew.SIDEnum.Admin || this.CurrentUser.SID == Crew.SIDEnum.PowerUser)
            {
                this.LoggingService.GenerateLog("User nuked cache.", Log.LogTypes.AuditAdmin,this.Context);
                this.Cache.Clear();
            }
            else
            {
                this.LoggingService.GenerateLog("Fail-User requested cache nuke but lacked permissions.", Log.LogTypes.AuditAdmin, this.Context);
                return new HttpStatusCodeResult(
                        HttpStatusCode.BadRequest,
                        "Only admins may perform this action.");
            }
            return new HttpStatusCodeResult(
                        HttpStatusCode.OK);

        }

        /// <summary>
        /// The set admin banner message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        [HttpPost]
        public ActionResult SetAdminBannerMessage(string message)
        {
            if (CurrentUser == null || CurrentUser.UserName == null || CurrentUser.SID!=Crew.SIDEnum.Admin)
            {
                var redirectModel = new RedirectModel() { ActionName = "Index", ControllerName = "Admin", UserBlocked = true, Description = "You do not have permission to access this page." };
                this.LoggingService.GenerateLog("Fail-User attempted to change admin banner message but had no permissions.", Log.LogTypes.AuditAdmin, this.Context, message);
                return this.View("NoPermissionPage", redirectModel);
            }
            var msg = string.Format("{0} </br><span> {1}, {2}</span>", message, CurrentUser.UserName, DateTime.Now.ToString("f"));
            var metadata = new MetaData() { Key = StaticTokens.Admin.AdminBanner, Value = msg };
            this.LoggingService.GenerateLog("User changed admin banner message.", Log.LogTypes.AuditAdmin, this.Context, message);
            this.MetadataService.Process(metadata);
            return this.RedirectToAction("Index");
        }

        /// <summary>
        /// The get banner.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetBanner()
        {
            string result = string.Empty;
            var entity = this.MetadataService.Get(StaticTokens.Admin.AdminBanner);
            if (entity != null)
            {
                result = entity;
            }
            return result;
        }
    }
}