﻿CREATE TABLE [dbo].[Logs.Comments.Location] (
    [LocationID] INT NOT NULL,
    [CommentID]  INT NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Location] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Comments.Location] PRIMARY KEY CLUSTERED ([LocationID] ASC, [CommentID] ASC),
    CONSTRAINT [FK_Logs.Comments.Location_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]),
    CONSTRAINT [FK_Logs.Comments.Location_Entities.Locations] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Entities.Locations] ([LocationID])
);

