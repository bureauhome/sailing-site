﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using SailingSite.Common;
    using SailingSite.Models;
    using SailingSite.Repositories.Interfaces;

    public interface ICommentRepository :  ITaggableRepository, ICommonRepository<Entities_Comment, SailingSiteModels.New.Comment, SailingSiteModels.New.CommentBase>, ICommentableRepository
    {
        IEnumerable<New.CommentBase> GetCommentBases(IEnumerable<int> ids);

        IEnumerable<New.Comment> Get(IEnumerable<int> id);

        IEnumerable<int> GetTrackIDs(int sailID);

        IEnumerable<int> GetSailLogIDS(int sailID);

        IEnumerable<CommentNewsFeedViewModel> GetFeedItems(FeedSearch search);

        SailReport GetSailReport(int trackId);
        int DeleteTrack(string trackKey);

        void ProcessLandmarkBearings(New.Comment comment);

        New.Comment GetOverlappingTrack(New.Comment comment, int minuteThreshhold);
    }
}