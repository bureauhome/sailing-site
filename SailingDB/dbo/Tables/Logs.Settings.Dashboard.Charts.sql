﻿CREATE TABLE [dbo].[Logs.Settings.Dashboard.Charts] (
    [ChartType] NVARCHAR (50) NOT NULL,
    [ChartName] NVARCHAR (50) NOT NULL,
    [CrewID]    INT           NOT NULL,
    [Ordinal]   INT           NOT NULL,
    CONSTRAINT [PK_Logs.Settings.Dashboard.Charts] PRIMARY KEY CLUSTERED ([CrewID] ASC, [Ordinal] ASC),
    CONSTRAINT [FK_Logs.Settings.Dashboard.Charts_Entities.Chart] FOREIGN KEY ([ChartName]) REFERENCES [dbo].[Entities.Chart] ([ChartName]),
    CONSTRAINT [FK_Logs.Settings.Dashboard.Charts_Entities.Chart.Type] FOREIGN KEY ([ChartType]) REFERENCES [dbo].[Entities.Chart.Type] ([ChartType]),
    CONSTRAINT [FK_Logs.Settings.Dashboard.Charts_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID])
);

