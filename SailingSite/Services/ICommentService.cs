﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommentService.cs" company="">
//   
// </copyright>
// <summary>
//   The CommentService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using System.Collections.Generic;

    using SailingSite.Models;

    using SailingSiteModels.GPX;
    using SailingSiteModels.New;

    /// <summary>
    /// The CommentService interface.
    /// </summary>
    interface ICommentService : ITaggableService, ICommonService<Comment, CommentBase>
    {
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Update(Comment entity);

        SailReport GetSailReport(int trackId);

        /// <summary>
        /// The parse logs.
        /// </summary>
        /// <param name="track">
        /// The track.
        /// </param>
        /// <param name="sailId">
        /// The sail id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        ParsedLogResult ParseLogs(SailingSiteModels.GPX.GpxTrack track, int sailId, int userId);

        /// <summary>
        /// The delete track.
        /// </summary>
        /// <param name="trackKey">
        /// The track key.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int DeleteTrack(string trackKey);
    }
}
