﻿CREATE TABLE [dbo].[Entities.Photos.Details] (
    [PhotoID]      INT  NOT NULL,
    [CreatedDate]  DATE NOT NULL,
    [ModifiedDate] DATE NULL,
    [CrewID]       INT  NOT NULL,
    [ModifiedBy]   INT  NULL,
    CONSTRAINT [PK_Entities.Photos.Details_1] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Entities.Photos.Details_1] PRIMARY KEY CLUSTERED ([PhotoID] ASC),
    CONSTRAINT [FK_Entities.Photos.Details_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Entities.Photos.Details_Entities.ModifiedCrew] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Entities.Photos.Details_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE
);

