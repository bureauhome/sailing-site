﻿namespace SailingUnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using SailingSite.Common;

    /// <summary>
    /// The comment test.
    /// </summary>
    [TestClass]
    public class CommentTest : TestBase
    {
        private List<string> Profanities = new List<string>()
                                               {
                                                   "f*ck",
                                                   "bitch",
                                                   "cunt",
                                                   "whore",
                                                   "buttcrack",
                                                   "shit",
                                                   "sh1t",
                                                   "weiner",
                                                   "wiener",
                                                   "weener",
                                                   "cocaine",
                                                   "shat",
                                                   "shyt",
                                                   "sh_t",
                                                   "sh-t",
                                                   "gonad",
                                                   "sexually",
                                                   "pedophile",
                                                   "paederast",
                                                   "paedophile",
                                                   "paedo",
                                                   "fuck",
                                                   "fondle",
                                                   "semen",
                                                   "spoog",
                                                   "uterus",
                                                   "masturbat",
                                                   "gangbang",
                                                   "gang bang",
                                                   "astroglide",
                                                   "cocaine",
                                                   "marijuana",
                                                   "jack off",
                                                   "wank",
                                                   "rape",
                                                   "bukake",
                                                   "shlong",
                                                   "schlong",
                                                   "coitus",
                                                   "bukkake",
                                                   "teat",
                                                   "gtfo",
                                                   "boner",
                                                   "titties",
                                                   "t tties",
                                                   "cl toris",
                                                   "clitoris",
                                                   "dildo",
                                                   "penis",
                                                   "dmn",
                                                   "fck",
                                                   "fk",
                                                   "shit",
                                                   "f you",
                                                   "damn",
                                                   "sphincter",
                                                   "bust a nut",
                                                   "rimjob",
                                                   "sixty nine",
                                                   "sixtynine",
                                                   "69",
                                                   "420",
                                                   "dirty sanchez",
                                                   "blowjob",
                                                   "cleavage",
                                                   "vagina",
                                                   "boob",
                                                   "asshole",
                                                   "ass hole",
                                                   "grope",
                                                   "breasticle",
                                                   "nipple",
                                                   "stripper",
                                                   "hooters",
                                                   "asswhole",
                                                   "ass whole",
                                                   "anal",
                                                   "bugger",
                                                   "shat",
                                                   "rape",
                                                   "dig up dirt",
                                                   "xxx",
                                                   "pre approved",
                                                   "visit our website",
                                                   "direct email",
                                                   "email harvest",
                                                   "horny",
                                                   "xanax",
                                                   "prostitute",
                                                   "bangtail",
                                                   "dollymop",
                                                   "call girl",
                                                   "callgirl",
                                                   "sensual",
                                                   "suicide",
                                                   "massag",
                                                   "orgasm",
                                                   "semen",
                                                   "analingus",
                                                   "nigger",
                                                   "baby batter",
                                                   "cannibal",
                                                   "funbags",
                                                   "fun bags",
                                                   "intercourse"
                                               };

        private List<string> Safewords = new List<string>()
                                             {
                                                 "happy bunny",
                                                 "meep",
                                                 "i went to the store",
                                                 "ipsum dolor",
                                                 "call me ishmael",
                                                 "proceeded north by northwest to destination",
                                                 String.Empty
                                             };

    /// <summary>
            /// The crud test.
            /// </summary>
            [TestMethod]
            public void CommentCrudTest()
            {
                SailRepository.Sterilize();
                var sailId = SailService.JiffySail(this.Context, null, true).ID;
                var originalComment = this.GenerateSailLog();
                originalComment.ID = CommentService.Create(originalComment);
                SailService.AddSailComment(originalComment, sailId, this.Context);
                var dbComment = CommentService.Get(originalComment.ID);
                Assert.IsTrue(this.MatchComment(originalComment, dbComment));
                // Add the comment to the list
                var commentList = SailService.GetCommentBasesForSail(new List<int>() { sailId });
                Assert.IsTrue(commentList.Any(i => i.ID == originalComment.ID));
                commentList = SailService.GetSail(sailId).Comments;
                Assert.IsTrue(commentList.Any(i => i.ID == originalComment.ID));
                // Update the comment.
                ModComment(originalComment);
                CommentService.Update(originalComment);
                dbComment = CommentService.Get(originalComment.ID);
                Assert.IsTrue(MatchComment(originalComment, dbComment));
                // Test deletion
                CommentService.Delete(originalComment.ID, 7);
                commentList = SailService.GetCommentBasesForSail(new List<int>() { sailId });
                Assert.IsFalse(commentList.Any(i => i.ID == originalComment.ID));
                commentList = SailService.GetSail(sailId).Comments;
                Assert.IsFalse(commentList.Any(i => i.ID == originalComment.ID));
                SailRepository.Sterilize();
            }
        [TestMethod]
        public void GetVesselForComment_Success()
        {
            SailRepository.Sterilize();
           
            var sail = this.generateSail();
            sail.ID=SailRepository.CreateSail(sail);
            
            var commment=this.GenerateSailLog(sail);
            SailService.AddSailComment(commment, sail.ID, this.Context);
            var baseComment = SailService.GetSail(sail.ID).Logbook.FirstOrDefault();
            Assert.IsNotNull(baseComment);
            var comment = CommentService.Get(baseComment.ID);
            Assert.IsNotNull(comment.Vessel);
            Assert.IsTrue(comment.Vessel.ID == sail.Vessel.ID);
            SailRepository.Sterilize();
        }

        [TestMethod]
            public void TestProfanityFilter()
            {
                foreach (string swear in Profanities)
                {
                    var comment = this.GenerateSailLog();
                    comment.Message = swear;
                    Exception exception = null;
                    int i = 0;
                    i = StringFunctions.ProcessSwears(comment.Message);

                    Assert.IsTrue(i>0);
                }

                foreach (string s in Safewords)
                {
                    int i = 0;
                    i = StringFunctions.ProcessSwears(s);
                    Assert.IsTrue(i<50);
                }


            }
        }
    }
