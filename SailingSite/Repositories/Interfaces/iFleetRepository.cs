﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="iFleetRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The FleetRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Repositories
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The FleetRepository interface.
    /// </summary>
    public interface iFleetRepository:ITaggableRepository
    {
        /// <summary>
        /// The does fleet name exist.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool DoesFleetNameExist(string name);

        /// <summary>
        /// The get fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Fleet"/>.
        /// </returns>
        Fleet GetFleet(int id);

        /// <summary>
        /// The get all fleet bases.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<FleetBase> GetAllFleetBases();

        /// <summary>
        /// The hydrate fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Fleet"/>.
        /// </returns>
        Fleet HydrateFleet(int id);

        /// <summary>
        /// The select fleet base.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="FleetBase"/>.
        /// </returns>
        FleetBase SelectFleetBase(int id);

        /// <summary>
        /// The add vessel to fleet.
        /// </summary>
        /// <param name="entry">
        /// The entry.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        void AddVesselToFleet(VesselEntry entry, int fleetId);

        /// <summary>
        /// The create fleet.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int CreateFleet(Fleet entity);

        /// <summary>
        /// The delete fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        void DeleteFleet(int id);

        /// <summary>
        /// The is user fleet crew.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsUserFleetCrew(int userId, int fleetId);

        /// <summary>
        /// The update fleet.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void UpdateFleet(Fleet entity);

        /// <summary>
        /// The get first initials.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<string> GetFirstInitials();

        /// <summary>
        /// The search.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<FleetBase> Search(GenericQuery query); 

        /// <summary>
        /// The select crew sum.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        int SelectCrewSum(int id);

        /// <summary>
        /// The select mile sum.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        decimal SelectMileSum(int id);

        /// <summary>
        /// The select fleet sail count.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int SelectFleetSailCount(int id);

    }
}