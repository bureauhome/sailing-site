﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISailService.cs" company="">
//   
// </copyright>
// <summary>
//   The SailService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading.Tasks;

    using SailingSite.Common;
    using SailingSite.Models;
    using SailingSite.Repositories;

    using SailingSiteModels.New;

    /// <summary>
    /// The SailService interface.
    /// </summary>
    public interface ISailService
    {
        /// <summary>
        /// The get vessel id for sail.
        /// </summary>
        /// <param name="sailId">
        /// The sail id.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        int? GetVesselIDForSail(int sailId);

        Task<IEnumerable<VesselEntry>> GetVesselEntriesForFleet(int fleetId);
        IEnumerable<SailingSiteModels.New.SheetSail> GetSheetSailLocker(int id);

        /// <summary>
        /// The add sail tags.
        /// </summary>
        /// <param name="sailID">
        /// The sail id.
        /// </param>
        /// <param name="tagStr">
        /// The tag str.
        /// </param>
        void AddSailTags(int sailID, string tagStr);

        /// <summary>
        /// The is user fleet crew or security.
        /// </summary>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsUserFleetCrewOrSecurity(int vesselId, int userId);

        /// <summary>
        /// The is user fleet crew or security.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <param name="adminOnly">
        /// The admin only.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsUserFleetCrewOrSecurity(int userId, int fleetId, bool adminOnly);

        /// <summary>
        /// The add vessel photo.
        /// </summary>
        /// <param name="photoID">
        /// The photo id.
        /// </param>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        void AddVesselPhoto(int photoID, int vesselId);

        /// <summary>
        /// The delete vessel photo.
        /// </summary>
        /// <param name="photoID">
        /// The photo id.
        /// </param>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        void DeleteVesselPhoto(int photoID, int vesselId);

        /// <summary>
        /// The delete fleet tag.
        /// </summary>
        /// <param name="tagId">
        /// The tag id.
        /// </param>
        /// <param name="fleetID">
        /// The fleet id.
        /// </param>
        void DeleteFleetTag(int tagId, int fleetID);

        /// <summary>
        /// The add fleet tags.
        /// </summary>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <param name="tagStr">
        /// The tag str.
        /// </param>
        void AddFleetTags(int fleetId, string tagStr);

        /// <summary>
        /// The create sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int CreateSail(Sail entity);

        /// <summary>
        /// The get sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Sail"/>.
        /// </returns>
        Sail GetSail(int id);

        /// <summary>
        /// The get fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Fleet"/>.
        /// </returns>
        Fleet GetFleet(int id);

        /// <summary>
        /// The is user vessel crew or security.
        /// </summary>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="adminOnly">
        /// The admin only.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool isUserVesselCrewOrSecurity(int vesselId, int userId, bool adminOnly);

        /// <summary>
        /// The is user vessel crew or security.
        /// </summary>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool isUserVesselCrewOrSecurity(int vesselId, int userId);

        /// <summary>
        /// The check fleet name.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CheckFleetName(string input);

        /// <summary>
        /// The get fleet tags.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Tags> GetFleetTags(int id);

        /// <summary>
        /// The update vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void UpdateVessel(Vessel entity);

        /// <summary>
        /// The get sheet sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="SheetSail"/>.
        /// </returns>
        SheetSail GetSheetSail(int id);

        /// <summary>
        /// The derive compass rose.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string DeriveCompassRose(int input);

        /// <summary>
        /// The derive compass rose.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="decimal?"/>.
        /// </returns>
        decimal? DeriveCompassRose(string input);

        /// <summary>
        /// The remove vessel from fleet.
        /// </summary>
        /// <param name="vesselID">
        /// The vessel id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        void RemoveVesselFromFleet(int vesselID, int fleetId, AuditContext context);

        /// <summary>
        /// The add vessel to fleet.
        /// </summary>
        /// <param name="entry">
        /// The entry.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void AddVesselToFleet(VesselEntry entry, int fleetId, AuditContext context);

        SearchReturnModel<IEnumerable<FleetBase>> SearchFleet(GenericQuery search);

        SearchReturnModel<IEnumerable<VesselBase>> SearchVessel(GeneralSearchRequest search);

        /// <summary>
        /// The get sails for vessel.
        /// </summary>
        /// <param name="ids">
        /// The ids.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<SailBase> GetSailsForVessel(IEnumerable<int> ids);

        /// <summary>
        /// The add sail comment.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="sailID">
        /// The sail id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void AddSailComment(Comment entity, int sailID, AuditContext context);

        /// <summary>
        /// The update sail.
        /// </summary>
        /// <param name="newEntity">
        /// The new entity.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void UpdateSail(Sail newEntity, AuditContext context);

        /// <summary>
        /// The get configurations.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Configuration> GetConfigurations();

        /// <summary>
        /// The get vessel bases.
        /// </summary>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<VesselBase> GetVesselBases(int? fleetId = null);

        /// <summary>
        /// The create vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int CreateVessel(Vessel entity, AuditContext context);

        /// <summary>
        /// The delete vessel.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void DeleteVessel(int id, int userId, AuditContext context);

        /// <summary>
        /// The delete vessel tag.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        void DeleteVesselTag(int id, int vesselId);

        /// <summary>
        /// The get vessel tag.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Tags> GetVesselTag(int id);

        /// <summary>
        /// The add vessel tags.
        /// </summary>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <param name="tagStr">
        /// The tag str.
        /// </param>
        void AddVesselTags(int vesselId, string tagStr);

        /// <summary>
        /// The delete fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void DeleteFleet(int id, AuditContext context);

        /// <summary>
        /// The create fleet.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int CreateFleet(Fleet entity, AuditContext context);

        /// <summary>
        /// The update fleet.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void UpdateFleet(Fleet entity, AuditContext context);

        /// <summary>
        /// The jiffy vessel.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="Vessel"/>.
        /// </returns>
        Vessel JiffyVessel(AuditContext context);

        /// <summary>
        /// The get vessel.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Vessel"/>.
        /// </returns>
        Vessel GetVessel(int id);

        /// <summary>
        /// The get all fleets.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<FleetBase> GetAllFleets();

        /// <summary>
        /// The get weather base.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<WeatherBase> GetWeatherBase();

        /// <summary>
        /// The get maneuvers.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Maneuver> GetManeuvers();

        /// <summary>
        /// The get locations.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<LocationBase> GetLocations();

        /// <summary>
        /// The get vessel foundation.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="VesselFoundation"/>.
        /// </returns>
        VesselFoundation GetVesselFoundation(int id);

        /// <summary>
        /// The get crew names.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<CrewFoundation> GetCrewNames();

        /// <summary>
        /// The get vessel names.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<VesselFoundation> GetVesselNames();

        /// <summary>
        /// The hydrate vessel stats.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="VesselBase"/>.
        /// </returns>
        VesselBase HydrateVesselStats(VesselBase entity);

        /// <summary>
        /// The hydrate fleet stats.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="FleetBase"/>.
        /// </returns>
        FleetBase HydrateFleetStats(FleetBase entity);

        /// <summary>
        /// The get crew sid.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Crew.SIDEnum"/>.
        /// </returns>
        Crew.SIDEnum GetCrewSID(int userId);

        /// <summary>
        /// The get comment bases for sail.
        /// </summary>
        /// <param name="ids">
        /// The ids.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<CommentBase> GetCommentBasesForSail(IEnumerable<int> ids);

        /// <summary>
        /// The delete sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void DeleteSail(int id, AuditContext context);

        /// <summary>
        /// The jiffy sail.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="creatorID">
        /// The creator id.
        /// </param>
        /// <param name="test">
        /// The test.
        /// </param>
        /// <returns>
        /// The <see cref="Sail"/>.
        /// </returns>
        Sail JiffySail(AuditContext context, int? creatorID, bool test=false);

        /// <summary>
        /// The add sail photo.
        /// </summary>
        /// <param name="photoID">
        /// The photo id.
        /// </param>
        /// <param name="sailId">
        /// The sail id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool AddSailPhoto(int photoID, int sailId);

        /// <summary>
        /// The add sheet sail to existing vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int AddSheetSailToExistingVessel(SheetSail entity, AuditContext context);

        /// <summary>
        /// The add sheet sail to existing vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int AddSheetSailToExistingVessel(SheetSail entity, int parentId, AuditContext context);

        /// <summary>
        /// The update sheet sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void UpdateSheetSail(SheetSail entity, AuditContext context);

        /// <summary>
        /// The delete sheet sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void DeleteSheetSail(int id, AuditContext context);

    }
}