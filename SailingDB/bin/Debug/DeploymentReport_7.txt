﻿** Highlights
     Tables that will be rebuilt
       [dbo].[Debug.Logs]
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Table rebuild
       [dbo].[Debug.Logs] (Table)

** Supporting actions
