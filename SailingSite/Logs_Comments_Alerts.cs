//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SailingSite
{
    using System;
    using System.Collections.Generic;
    
    public partial class Logs_Comments_Alerts
    {
        public int AlertID { get; set; }
        public int CommentID { get; set; }
        public System.DateTime RelaxDate { get; set; }
        public Nullable<bool> ReRun { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<byte> ReRunType { get; set; }
    
        public virtual Entities_Comment Entities_Comment { get; set; }
    }
}
