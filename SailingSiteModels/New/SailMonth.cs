﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    public abstract class SailPeriodAbst
    {
        public Dictionary<CrewBase, int> CrewSails {get; set;}
        public Dictionary<CrewBase, decimal> CrewMiles {get; set;}
        public Dictionary<CrewBase, decimal> CrewHours {get; set;}
        public Dictionary<CrewBase, decimal> CrewAvgHighWinds {get; set;}

        public Dictionary<VesselBase, int> VesselSails {get; set;}
        public Dictionary<VesselBase, decimal> VesselMiles {get; set;}
        public Dictionary<VesselBase, decimal> VesselHours {get; set;}
        public Dictionary<VesselBase, decimal> VesselAvgHighWinds {get; set;}
    }

    public class SailPeriod:SailPeriodAbst
    {
        public int Year {get; set;}
        public int? Month { get; set; }
    }
}
