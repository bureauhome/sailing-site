﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite.Services
{
    public class TagService:ITagService
    {
        private Repositories.ITagRepository repository;
        private Repositories.ITagRepository Repository
        {
            get {
                if (repository == null)
                { 
                    repository=new Repositories.TagRepository();
                }
                return repository; 
            }
            set {
                repository = value;
            }
        }
        private Services.ICrewService crewService;
        private ICrewService CrewService {
            get {
            if (crewService==null)
            {
                crewService=new CrewService();
            }
                return crewService;
            }
            set {
                crewService=value;
            }
        }
        private ISailService SailService{
            get {
                if (sailservice==null)
                {
                    sailservice=new SailingService();
                }
                return sailservice;
            }
            set {
                sailservice=value;
            }
        }

        public TagService()
        {

        }

        public int? SaveTag(New.Tags entity, Entities_Tags tag=null)
        {
            if (entity.Creator == null)
            {
                // TODO: GET THIS TO ALSO WORK WITH REGULAR USERS
                entity.Creator = Repositories.CrewRepository.GetSystemID();
             
            }
            return Repository.SaveTag(entity, tag);
        }
        private Services.ISailService sailservice;
        public New.TagHydrated GetHydratedTag(int id)
        {
            var result = Repository.GetHydratedTag(id);
            if (result.Vessels != null && result.Vessels.Any())
            {

                foreach (var vessel in result.Vessels)
                {
                    SailService.HydrateVesselStats(vessel);
                }
            }
            if (result.Crews != null && result.Crews.Any())
            {
                foreach (var member in result.Crews)
                {
                    CrewService.HydrateCrewStats(member.ToCrew());
                }
            }
            if (result.Vessels != null && result.Vessels.Any())
            {
                foreach (var fleet in result.Fleets)
                {
                    SailService.HydrateFleetStats(fleet);
                }
            }
            return result;
        }

        public List<New.Tags> GenerateTagsFromString(string tagStr)
        {
            List<New.Tags> result=new List<New.Tags>();
            string[] tags = null;
            if (tagStr.Contains(","))
            {
                tags = tagStr.Split(',');
            }
            else
            {
                tags = new string[1] { tagStr };
            }
            for (int i=0; i<=tags.Count()-1;i++)
            {
                if (Common.StringFunctions.ProcessSwears(tags[i]) < 100)
                {
                    tags[i] = processTagName(tags[i]);
                }
                else {
                    tags[i] = "";
                }
            }

            for (int i=0; i<=tags.Count()-1;i++)
            {
                if (!string.IsNullOrEmpty(tags[i]))
                {
                    var tag = new New.Tags { Name = tags[i] };
                    result.Add(tag);
                }
            }
            return result;
        }


        private string processTagName(string tag)
        {
            tag = tag.Trim();

            if (tag != null && !string.IsNullOrEmpty(tag))
            {
                string result = "";
                
                    if (tag.Length > 12)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (char i in tag)
                         {
                            
                            if (char.IsLetterOrDigit(i) || char.IsWhiteSpace(i)||i=='-')
                            {
                                sb.Append(i);
                            }
                            
                        }
                    
                    if (sb.Length > 3)
                            {
                                result = sb.ToString();
                            }
                    }

                    else
                    {
                        foreach (char i in tag)
                        {
                            if (char.IsLetterOrDigit(i) || char.IsWhiteSpace(i) || i == '-')
                            {
                                result += i;
                            }
                        }
                    }
                if (result.Length > 50)
                {
                    result = result.Substring(0, 49);
                }

                return result;
                }
            return ""; 
            }
        
            
        

        public IEnumerable<string> GetAllTagsByString()
        {
            return Repository.GetAllTagsByString();
        }
        public IEnumerable<New.Tags> GetAllTags()
        {
            return Repository.GetAllTags();
        }
       public  New.Tags GetTag(Repositories.TagRequest request)
        {
            return Repository.GetTag(request);
        }
    }
}