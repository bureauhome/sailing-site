﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.SqlClient;
using System.Web;
using System.Data.Entity;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using System.CodeDom;
    using System.Data.Entity.SqlServer;
    using System.Data.Entity.Validation;
    using System.Data.Linq.Mapping;
    using System.Drawing;
    using System.Security.Cryptography.X509Certificates;

    using New;

    using SailingSite.Common;
    using SailingSite.Factories;
    using SailingSite.Models;
    using SailingSite.Repositories.Interfaces;

    using SailingSiteModels.New.Bearing;

    using WebGrease.Css.Extensions;

    /// <summary>
    /// The comment repository.
    /// </summary>
    public class CommentRepository : ICommentRepository
    {

        public IEnumerable<New.CommentBase> GetAll(object search)
        {
            IEnumerable<New.CommentBase> result = null;
            Common.CommentSearchRequest request = search as Common.CommentSearchRequest;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Comment.Where(i=>i.CommentID!=0);
                if (request != null)
                {   
                    if (request.KeyWords != null && request.KeyWords.Any())
                    {
                        query = query.Where(i => request.KeyWords.Contains(i.Comment)||request.KeyWords.Contains(i.Subject)||request.KeyWords.Contains(i.Title));
                    }
                    var dbEntities = query.Skip(request.Skip()).Take(request.Take());
                    if (dbEntities != null && dbEntities.Any())
                    {
                        result = dbEntities.Select(Factories.CommonFactory.CreateCommentBase);
                    }
                }
            }
            return result;
        }

        public IEnumerable<New.CommentBase> GetCommentBases(IEnumerable<int> IDs)
        {
            IEnumerable < New.CommentBase > result= null;
            using (var context = new SailingEntities())
            {
                var query = from i in context.Entities_Comment.Include(i=>i.Entities_Crew).Include(i=>i.Entities_Crew3).Include(i=>i.Logs_Comments_Comments) where i.DateDeleted==null&&IDs.Contains(i.CommentID) select i;
                var dbentity = query.ToArray();
                result = dbentity.Select(Factories.CommonFactory.CreateCommentBase);
            }
            return result;
        }

        public IEnumerable<int> GetSailLogIDS(int sailID)
        {
            IEnumerable<int> result = null;
            using (var context = new SailingEntities())
            {
                var comment =
                    context.Entities_Comment.Where(i => i.BearingOnly != true && i.LogId != null && sailID==i.LogId)
                        .Select(i => i.CommentID);
                if (comment != null)
                {
                    result = comment.ToArray();
                }


            }
            return result;
        }

        public IEnumerable<int> GetTrackIDs(int sailID)
        {
            IEnumerable<int> result = null;
            using (var context = new SailingEntities())
            {
                var comment =
                    context.Entities_Comment.Where(i => i.BearingOnly == true && i.LogId != null && sailID == i.LogId)
                        .Select(i => i.CommentID);
                if (comment != null)
                {
                    result = comment.ToArray();
                }


            }
            return result;
        }

        public SailReport GetSailReport(int trackId)
        {
            SailReport result = null;
            if (trackId > 0)
            {
                using (var context = new SailingEntities())
                {
                    var time = context.Entities_Comment.FirstOrDefault(i => i.CommentID == trackId).DateCreated;
                    var comments =
                        context.Logs_Sails.Where(i => i.Entities_Comment.Any(j => j.CommentID == trackId))
                            .Select(i => i.Entities_Comment.Where(k=>k.DateDeleted==null&&(k.WindDirection!=null||k.WindSpeed!=null||k.Comment!=null)))
                            .FirstOrDefault();
                    if (comments != null)
                    {
                        result=new SailReport();
                        result.ReportTime = time;
                        comments = comments.OrderBy(i => time.Subtract(i.DateCreated));
                        var comment = comments.FirstOrDefault(i => !string.IsNullOrEmpty(i.Comment));
                        var wind = comments.Where(i => i.WindSpeed != null).OrderBy(i => time.Subtract(i.DateCreated)).ThenByDescending(i => i.WindDirection).Take(3).ToArray();
                        var heel = comments.FirstOrDefault(i => i.HeelPct != null);
                        if (wind != null && wind.Any())
                        {
                            result.Wind = wind.Select(i=>new SailReading<WindReading>(new WindReading(){WindDirection =i.WindDirection,WindSpeed = i.WindSpeed.Value},i.DateCreated));
                        }
                        if (heel != null)
                        {
                            result.Heel = new SailReading<decimal>((decimal)heel.HeelPct, heel.DateCreated);
                        }
                        if (comment != null)
                        {
                            result.Comment = new SailReading<string>(comment.Comment, comment.DateCreated);
                        }
                    }
                }
            }
            return result;
        }

        public void ProcessLandmarkBearings(New.Comment comment)
        {
            if (comment.ID > 0 && comment.LandMarkBearings != null && comment.LandMarkBearings.Any())
            {
                using (var context = new SailingEntities())
                {
                    foreach (var bearing in comment.LandMarkBearings)
                    {
                        var dbComment = context.Entities_Comment.FirstOrDefault(i => i.CommentID == comment.ID);
                        if (bearing.Bearing != null && bearing.LandMark != null)
                        {
                            var dbBearing = new Entities_Bearing() { CompassReading = bearing.Bearing.CompassReading };
                            context.Entities_Bearing.Add(dbBearing);
                            context.SaveChanges();

                            var dblmBrg = new Bearings_LandMark()
                                              {
                                                  BearingID = dbBearing.Id,
                                                  LandMarkID = bearing.LandMark.ID
                                              };
                            dbComment.Bearings_LandMark.Add(dblmBrg);
                            context.SaveChanges();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The parse sheet sail configurations.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="entries">
        /// The entries.
        /// </param>
        private void ParseSheetSailLog(int id, IEnumerable<New.SheetSailLog> entries)
        {
            if (entries != null && entries.Any())
            {
                using (var context = new SailingEntities())
                {
                    var comment = context.Entities_Comment.FirstOrDefault(i => i.CommentID == id);
                    foreach (var entry in entries)
                    {
                        if (entry != null && entry.SheetSail != null)
                        {
                            var log = CommonFactory.CreateSheetSailLog_Comment(entry);
                            comment.Logs_Comments_Vessel_Sails.Add(log);
                            context.SaveChanges();
                            
                        }
                    }
                    //TODO: Break out into base that could cover both sails and comments.
                }
            }
        }

        public int Create(New.Comment comment) {
            int result=0;
            using (var context = new SailingEntities())
            {
                int obscenityFilter = StringFunctions.ProcessSwears(comment.Message);
                if (obscenityFilter >= 100)
                {
                    Common.ObscenityFilterException exception=new ObscenityFilterException("You are a vulgar individual.");
                    exception.ObscenityLevel = obscenityFilter;
                    throw exception;
                }

                var dbcomment = Factories.CommonFactory.CreateComment(comment);

                dbcomment.Entities_Crew = context.Entities_Crew.FirstOrDefault(i => i.CrewID == comment.CreatedbyID);

                if (comment.HelmsPerson != null)
                {
                    dbcomment.Entities_Crew3 =
                        context.Entities_Crew.FirstOrDefault(i => i.CrewID == comment.HelmsPerson.ID);
                }
                if (comment.Crew != null && comment.Crew.Any())
                {
                    var p = comment.Crew.Select(i => i.ID).ToArray();
                    dbcomment.Entities_Crew5 = context.Entities_Crew.Where(i => p.Contains(i.CrewID)).ToArray();
                }


                if (comment.Weather != null && comment.Weather.Any())
                {
                    var p = comment.Weather.Select(i => i.ID);
                    dbcomment.Entities_Weather = context.Entities_Weather.Where(i => p.Contains(i.WeatherID)).ToArray();
                }

                if (comment.Vessels != null && comment.Vessels.Any())
                {
                    var p = comment.Vessels.Select(i => i.ID);
                    dbcomment.Entities_Vessel = context.Entities_Vessel.Where(i => p.Contains(i.VesselID)).ToArray();
                }

                if (comment.Animals != null && comment.Animals.Any())
                {
                    var p = comment.Animals.Select(i => i.ID);
                    dbcomment.Entities_Animals_Names =
                        context.Entities_Animals_Names.Where(i => p.Contains(i.AnimalID)).ToArray();
                }

                if (comment.Maneuvers != null && comment.Maneuvers.Any())
                {
                    var p = comment.Maneuvers.Select(i => i.ID);
                    dbcomment.Entities_Maneuvers =
                        context.Entities_Maneuvers.Where(i => p.Contains(i.ManeuverID)).ToArray();
                }
                //TODO: Add configuration and DRY.

                context.Entities_Comment.Add(dbcomment);
                try
                {
                    context.SaveChanges();
                    // We have the id.  Now parse the sheetsail logs.
                    this.ParseSheetSailLog(dbcomment.CommentID, comment.SheetSailLog);

                }
                catch (DbEntityValidationException ex)
                {
                    // TODO: Do something with this.
                }
                
                result = dbcomment.CommentID;
            }

            return result;
        }

        /// <summary>
        /// The delete track.
        /// </summary>
        /// <param name="trackKey">
        /// The track key.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int DeleteTrack(string trackKey)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                var entities = context.Entities_Comment.Where(i => i.TrackKey == trackKey);
                result = entities.Count();
                context.Entities_Comment.RemoveRange(entities);
            }
            return result;
        }

        public bool IsUnique(SailingSiteModels.New.Comment entity, out List<string> errorString)
        {
            bool result = true;
            errorString = new List<string>();
            var swearCount = Common.StringFunctions.ProcessSwears(entity.Message);
            
            if (swearCount > 75)
            {
                errorString.Add("Tone it down there, Popeye.  Mothers and nieces visit this site.");
                result = false;
            }
            else if (swearCount > 50)
            {
                errorString.Add("Careful there, Swearing Suzy.  Even sailors have limits!");
            }
            else if (swearCount > 20)
            {
                errorString.Add("Possible obscenities detected by Sailnet.  We'll let it slide... for now.");
            }
            using (var context = new SailingEntities())
            {
                var tooSimilar =
                    context.Entities_Comment.Where(
                        i => Math.Abs(SqlMethods.DateDiffSecond(i.DateCreated, DateTime.Now)) < 5&&entity.ID!=i.CommentID).Count();
                if (tooSimilar > 1)
                {
                    errorString.Add(
                        string.Format(
                            "Look, we're not saying you're a bot, but you've posted {0} comments in the past 5 seconds, so you'll understand why we're suspicious.  Slow down.  Chew and digest your data.",
                            tooSimilar));
                }
                var similar =
                    context.Entities_Comment.Where(
                        i =>
                        SqlMethods.DateDiffHour(i.DateCreated, DateTime.Now) < 3 && i.CrewID == entity.CreatedbyID
                        && Math.Abs(i.Comment.Length - entity.Message.Length) < 3
                        && matchCompare(i.Comment, entity.Message) > .5 && entity.ID!=i.CommentID).Count();
                if (similar > 4)
                {
                    errorString.Add(string.Format("Sailnet's spam senses are all tingly!  You have made {0} very similar comments in the past day.", result));
                    result = false;
                }
                else if (similar > 2)
                {
                    errorString.Add(string.Format("Suspicious!  You have made {0} very similar comments in the past day.  We're not saying you're a bot, but try to be more unique.", result));
                }
                
            }
            return result;
        }

        public void Delete(int id, int userId)
        {
            using (var context = new SailingEntities())
            {
                var entity = context.Entities_Comment.FirstOrDefault(i => i.CommentID == id);
                if (entity  !=null)
                {
                    entity.DateDeleted=DateTime.Now;
                    entity.DeletedByID = userId;
                    context.SaveChanges();
                }
            }
        }

        public void Update(SailingSiteModels.New.Comment entity)
        {

            int obscenityFilter = StringFunctions.ProcessSwears(entity.Message);
            if (obscenityFilter > 100)
            {
                Common.ObscenityFilterException exception = new ObscenityFilterException("You are a vulgar individual.");
                exception.ObscenityLevel = obscenityFilter;
                throw exception;
            }
         

            using (var context = new SailingEntities())
            {
                var dbentity = context.Entities_Comment.FirstOrDefault(i => i.CommentID == entity.ID);
                var helmsPerson = entity.HelmsPerson == null
                                      ? null
                                      : context.Entities_Crew.FirstOrDefault(i => i.CrewID == entity.HelmsPerson.ID);
                if (dbentity != null)
                {
                    dbentity.Entities_Crew2 = entity.ModifiedBy != null
                                                  ? context.Entities_Crew.FirstOrDefault(i => i.CrewID == entity.ID)
                                                  : null;
                    dbentity.DateModified = entity.Updated == null ? DateTime.Now : (DateTime)entity.Updated;
                    dbentity.Comment = entity.Message;
                    dbentity.Subject = entity.Subject;
                    dbentity.Title = entity.Title;
                    dbentity.Barometer = entity.Barometer;
                    dbentity.Elevation = entity.Elevation;
                    dbentity.Latitude = entity.Latitude;
                    dbentity.Entities_Crew3 = helmsPerson;
                    dbentity.Longitude = entity.Longitude;
                    dbentity.CenterboardPct = (int?)entity.CenterboardPct;
                    dbentity.CloudCoverPerc = entity.CloudCover;
                    dbentity.CompassBearing = entity.CompassBearing;
                    dbentity.Soundings = entity.Sounding;
                    dbentity.DateModified=DateTime.Now;
                    dbentity.Entities_Crew2 =entity.ModifiedBy==null?null: context.Entities_Crew.FirstOrDefault(i=>i.CrewID==entity.ModifiedBy.ID);
                    dbentity.GPSCoords = entity.GPSCoords == null
                                             ? null
                                             : System.Data.Entity.Spatial.DbGeography.FromText(entity.GPSCoords);
                    dbentity.HeelPct = entity.HeelPct;
                    dbentity.MilesRun = entity.MilesLogged;
                    dbentity.RainPerc = entity.RainSeverity;
                    dbentity.SeaState = entity.SeaState;
                    dbentity.Leeway = entity.Leeway;
                    dbentity.TrackKey = entity.TrackKey;
                    dbentity.TemperatureF = entity.TempF;
                    dbentity.ShipLog = entity.IsLog;
                    dbentity.WindDirection = entity.WindDirection;
                    dbentity.WindSpeed = entity.WindSpeed;
                    dbentity.Longitude = entity.Longitude;
                    dbentity.Latitude = entity.Latitude;
                    dbentity.BearingOnly = entity.IsCoordinateOnly;
                    dbentity.MilesRun = entity.MilesLogged;
                    if (entity.Crew != null && entity.Crew.Any())
                    {
                        dbentity.Entities_Crew5.Clear();
                        var p = entity.Crew.Select(i => i.ID).ToArray();
                        dbentity.Entities_Crew5 = context.Entities_Crew.Where(i => p.Contains(i.CrewID)).ToList();
                    }
                    else
                    {
                        dbentity.Entities_Crew5.Clear();
                    }

                    if (entity.LandMarkBearings != null && entity.LandMarkBearings.Any())
                    {
                        var brgs = dbentity.Bearings_LandMark.Select(i => i.Entities_Bearing);
                        dbentity.Bearings_LandMark.Clear();
                        context.Entities_Bearing.RemoveRange(brgs);
                        context.SaveChanges();
                        this.ProcessLandmarkBearings(entity);
                    }

                    dbentity.Logs_Comments_Vessel_Sails.Clear();
                    context.SaveChanges();
                    if (entity.SheetSailLog != null && entity.SheetSailLog.Any())
                    {
                        this.ParseSheetSailLog(entity.ID, entity.SheetSailLog);
                    }

                    if (entity.Weather != null && entity.Weather.Any())
                    {
                        var p = entity.Weather.Select(i => i.ID);
                        dbentity.Entities_Weather.Clear();
                        dbentity.Entities_Weather =
                            context.Entities_Weather.Where(i => p.Contains(i.WeatherID)).ToList();
                    }
                    else
                    {
                        dbentity.Entities_Weather.Clear();
                    }

                    if (entity.Vessels != null && entity.Vessels.Any())
                    {
                        var p = entity.Vessels.Select(i => i.ID);
                        dbentity.Entities_Vessel.Clear();
                        dbentity.Entities_Vessel = context.Entities_Vessel.Where(i => p.Contains(i.VesselID)).ToList();
                    }
                    else
                    {
                        dbentity.Entities_Vessel.Clear();
                    }

                    if (entity.Animals != null && entity.Animals.Any())
                    {
                        var p = entity.Animals.Select(i => i.ID);
                        dbentity.Entities_Animals_Names.Clear();
                        dbentity.Entities_Animals_Names =
                            context.Entities_Animals_Names.Where(i => p.Contains(i.AnimalID)).ToList();
                    }
                    else
                    {
                        dbentity.Entities_Vessel.Clear();
                    }

                    if (entity.Maneuvers != null && entity.Maneuvers.Any())
                    {
                        var p = entity.Maneuvers.Select(i => i.ID);
                        dbentity.Entities_Maneuvers.Clear();
                        dbentity.Entities_Maneuvers =
                            context.Entities_Maneuvers.Where(i => p.Contains(i.ManeuverID)).ToList();
                    }
                    else
                    {
                        dbentity.Entities_Maneuvers.Clear();
                    }


                    context.SaveChanges();
                  
                }
            }
        }

        public void AddTags(List<int> tagIds, int id)
        {
            using (var context = new SailingEntities())
            {
                List<Entities_Tags> tags = new List<Entities_Tags>();
                foreach (var tagId in tagIds)
                {
                    var tag = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                    if (tag != null && tag.Name != null)
                    {
                        tags.Add(tag);
                    }
                }

                var comment = (from i in context.Entities_Comment where i.CommentID == id select i).FirstOrDefault();
                if (comment != null && comment.CommentID >0)
                {
                    foreach (var tag in tags)
                    {
                        var match = comment.Entities_Tags.Where(i => i.TagID == tag.TagID).FirstOrDefault();
                        if (match == null || match.TagID <= 0)
                        {
                            comment.Entities_Tags.Add(tag);
                        }
                    }
                }
                context.SaveChanges();

            }

        }

        public void DeleteTag(int tagId, int id)
        {
            using (var context = new SailingEntities())
            {
                var trial = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                if (trial != null)
                {
                    var comment = (from i in trial.Entities_Comment where i.CommentID == id select i).FirstOrDefault();
                    if (comment != null)
                    {
                        trial.Entities_Comment.Remove(comment);
                        context.SaveChanges();
                    }


                }
            }
        }

        public IEnumerable<New.Tags> GetTags(int id)
        {
            IEnumerable<New.Tags> results = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Comment where i.CommentID == id select i.Entities_Tags.Where(k => k.DateDeleted == null)).FirstOrDefault();
                if (query != null)
                {
                    results = query.Select(Factories.CommonFactory.CreateTag);
                }
            }
            return results;
        }

        public IEnumerable<New.Comment> Get(IEnumerable<int> id)
        {
            IEnumerable<New.Comment> result = null;
            using (var context = new SailingEntities())
            {
                var comment =
                    (from i in context.Entities_Comment where id.Contains(i.CommentID) select i).Include(
                        i => i.Entities_Crew)
                        .Include(i => i.Entities_Maneuvers)
                        .Include(i => i.Entities_Tags)
                        .Include(i => i.Entities_Vessel)
                        .Include(i => i.Entities_Vessel.Select(j => j.Entities_Vessels_Sails))
                        .Include(i => i.Entities_Photos)
                        .Include(i => i.Logs_Comments_Vessel_Sails)
                        .Include(i => i.Logs_Comments_Vessel_Sails.Select(j => j.Entities_Vessels_Sails))
                        .Include(i => i.Logs_Comments_Vessel_Sails.Select(j => j.Entities_Vessels_Sails).Select(k => k.Entities_Config))
                        .Include(i => i.Entities_Locations)
                        .Include(i => i.Entities_Animals_Names)
                        .Include(i => i.Entities_Weather)
                        .Include(i => i.Logs_Sails1)
                        .Include(i => i.Logs_Sails.Entities_Crew)
                        .Include(i => i.Logs_Sails.Entities_Crew1)
                        .Include(i => i.Entities_Crew)
                        .Include(i=>i.Entities_Crew2)
                        .Include(i=>i.Entities_Crew3)
                        .Include(i=>i.Entities_Crew5)
                        .Include(i => i.Logs_Sails1)
                        .Include(i => i.Entities_Vessel)
                        .ToArray();
                // TODO this is noob shit.  Merge these two methods togeter!!
                // TODO deal with other deletions
                if (comment != null)
                {
                    result = comment.Select(CommonFactory.CreateComment).ToArray();
                }
            }
            return result;
        }

        public New.Comment Get(int id) {
            New.Comment result = null;
            using (var context = new SailingEntities())
            {
                var comment =
                    (from i in context.Entities_Comment where i.CommentID == id select i).Include(i => i.Entities_Crew)
                        .Include(i => i.Entities_Maneuvers)
                        .Include(i => i.Entities_Tags)
                        .Include(i => i.Entities_Vessel)
                        .Include(i => i.Entities_Vessel.Select(j => j.Entities_Vessels_Sails))
                        .Include(i => i.Entities_Photos)
                        .Include(i => i.Logs_Comments_Vessel_Sails)
                        .Include(i => i.Logs_Comments_Vessel_Sails.Select(j => j.Entities_Vessels_Sails))
                        .Include(i => i.Logs_Comments_Vessel_Sails.Select(j => j.Entities_Vessels_Sails).Select(k=>k.Entities_Config))
                        .Include(i => i.Logs_Comments_Vessel_Sails.Select(j => j.Entities_Vessels_Sails.Entities_Vessel))
                       .Include(i => i.Logs_Comments_Vessel_Sails.Select(j => j.Entities_Vessels_Sails.Entities_Vessel.Entities_Crew))
                        .Include(i=>i.Entities_Locations)
                        .Include(i=>i.Entities_Animals_Names)
                        .Include(i => i.Entities_Weather)
                        .Include(i => i.Logs_Sails)
                        .Include(i => i.Logs_Sails.Entities_Crew)
                        .Include(i => i.Entities_Crew)
                        .Include(i=>i.Entities_Crew3)
                        .Include(i => i.Entities_Crew2)
                        .Include(i => i.Bearings_LandMark)
                        .Include(i => i.Bearings_LandMark.Select(j => j.Entities_Bearing))
                        .Include(i => i.Bearings_LandMark.Select(j => j.Entities_LandMark))
                        .Include(i => i.Entities_Crew3)
                        .Include(i => i.Entities_Crew5)
                        .Include(i => i.Logs_Sails.Entities_Crew1)
                        .Include(i=>i.Logs_Sails.Entities_Locations)
                        .Include(i=>i.Entities_Vessel).FirstOrDefault();
                comment.Logs_Sails1 = new List<Logs_Sails>() { comment.Logs_Sails1.FirstOrDefault() };
                comment.Entities_Crew5 = comment.Entities_Crew5.Where(i => i.DateDeleted == null).ToArray();
                // TODO deal with other deletions
                if (comment != null)
                {
                    result = Factories.CommonFactory.CreateComment(comment);
                }
            }
            return result;
        }

        private double matchCompare(string a, string b)
        {

            if (a != null && b != null)
            {
                a = a.ToLower().Replace(" ", "");
                b = b.ToLower().Replace(" ", "");
                int counter = 0;
                int max = a.Length < b.Length ? b.Length : a.Length;
                int min = a.Length > b.Length ? b.Length : a.Length;
                if (max < 15 || (Math.Abs(a.Length - b.Length) < max / 2))
                {

                    for (int i = 0; i < min; i++)
                    {
                        if (a[i] == b[i])
                        {
                            counter++;
                        }
                    }
                }
                return counter / max;
            }
            return 0;
        }

        public void AddComment(int commentID, int entityID)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<int> GetCommentIDsForSail(IEnumerable<int> entityID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get overlapping logs.
        /// </summary>
        /// <param name="comment">
        /// The comment.
        /// </param>
        /// <param name="minuteThreshhold">
        /// The minute threshhold.
        /// </param>
        /// <returns>
        /// The <see cref="Comment"/>.
        /// </returns>
        public New.Comment GetOverlappingTrack(New.Comment comment, int minuteThreshhold)
        {
            New.Comment result = null;
            if (comment!=null&& comment.LogId != null)
            {
                int? cid = null;
                using (var context = new SailingEntities())
                {
                    // We only want unmodified tracks.
                    cid = context.Entities_Comment.Where(
                        i => !string.IsNullOrEmpty(i.TrackKey) && i.DateModified == null && i.LogId!=null&&
                            i.LogId == (int)comment.LogId && DbFunctions.DiffMinutes(i.DateCreated, comment.Created) < minuteThreshhold)
                            .OrderBy(i => DbFunctions.DiffMinutes(i.DateCreated, comment.Created))
                            .Select(i => i.CommentID).FirstOrDefault();
                if (cid != 0)
                                {
                                    this.Get((int)cid);
                                }
                }
            }
            return result;

        }


        public IEnumerable<Models.CommentNewsFeedViewModel> GetFeedItems(FeedSearch search)
        {
            IEnumerable<CommentNewsFeedViewModel> result = new List<CommentNewsFeedViewModel>();
            if (search == null)
            {
                return null;
            }
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Comment.Where(i => i.DateDeleted == null);
                if (search.DaysBack > 0 && search.StartDate != null)
                {
                    var minDate = ((DateTime)search.StartDate).AddDays(-1 * (Math.Abs((int)search.DaysBack)));
                    query = query.Where(i => i.DateCreated >= minDate);
                }
                if (search.CommentCrewIDs != null && search.CommentCrewIDs.Any())
                {
                    query = query.Where(i => i.Entities_Crew5.Any(j => search.CommentCrewIDs.Contains(j.CrewID)));
                }
                if (search.AnniversaryDate != null)
                {
                    query = query.Where(i => (SqlFunctions.DateDiff("DAY", i.DateCreated, search.AnniversaryDate) % 365) == 0);
                }
                if (search.CrewIDs != null && search.CrewIDs.Any())
                {
                    query = query.Where(i => search.CrewIDs.Contains(i.Entities_Crew.CrewID));
                }
                if (search.FilterSearch != null && search.FilterSearch.Any())
                {
                    query = query.Where(i => !search.FilterSearch.Contains(i.Logs_Sails.SailID));
                }
                query = query.OrderByDescending(i => i.DateCreated).Take(search.NumberToGrab);
                var queryResult = query.ToArray();
                if (queryResult.Any())
                {
                    result = queryResult.Select(Factories.CommonFactory.CreateNewsFeed);
                }
                result.ForEach(i=>i.CreatedBy=(Factories.CrewFactory.CreateCrewFoundation((context.Entities_Crew.FirstOrDefault(j=>j.CrewID==i.CreatedbyID)))));
            }
            if (result != null && result.Any() && search.Priority != null)
            {
                result.ForEach(i => i.Order = (int)search.Priority);
                
            }

            return result;
        }
    }
}