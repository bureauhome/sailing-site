﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VesselRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The vessel repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using SailingSite.Factories;
    using SailingSite.Models;

    using SailingSiteModels.New;

    /// <summary>
    /// The vessel repository.
    /// </summary>
    public class VesselRepository : IVesselRepository
    {
        /// <summary>
        /// The jiffy save vessel.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Vessel"/>.
        /// </returns>
        public Vessel JiffySaveVessel()
        {
            using (var context = new SailingEntities())
            {
                var result = new Entities_Vessel() { TestData = true, Name = "New Vessel", Length = 0, Beam = 0 };
                context.Entities_Vessel.Add(result);
                context.SaveChanges();
                return FleetFactory.CreateVessel(result);
            }
        }

        /// <summary>
        /// The get crew for vessel.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<CrewBase> GetCrewForVessel(int id)
        {
            IEnumerable<CrewBase> result = null;
            using (var context=new SailingEntities())
            {
                var query =
                    context.Entities_Crew.Where(i => i.Logs_Sails.Any(j => j.VesselID == id)).Distinct().ToArray();
                result = query.Select(CrewFactory.CreateCrewBase);
                       }

            return result;
        }

        /// <summary>
        /// The get vessel id for sail.
        /// </summary>
        /// <param name="sailId">
        /// The sail id.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? GetVesselIDForSail(int sailId)
        {
            using (var context = new SailingEntities())
            {
                int? result = context.Logs_Sails.Where(i => i.SailID == sailId).Select(i => i.VesselID).FirstOrDefault();
                return result;
            }
        }

        public IEnumerable<int> GetVesselIDsForFleet(IEnumerable<int> fleetIds)
        {
            List<int> result=new List<int>();
            using (var context = new SailingEntities())
            {
                result =
                    context.Entities_Vessel.Where(i => i.Logs_Fleet_Vessel.Any(j => fleetIds.Contains(j.FleetID)))
                        .Select(j => j.VesselID)
                        .ToList();
            }
            return result;
        }

        /// <summary>
        /// The is user in crew.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool isUserVesselCrewOrSecurity(int userId, int vesselId)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                var ship = context.Entities_Vessel.FirstOrDefault(i => i.VesselID == vesselId);
                var crew = context.Entities_Crew.FirstOrDefault(i => i.CrewID==userId);
                if (ship != null && crew != null)
                {
                    // if the crew SID is higher than 2, we've got an admin.
                    if (crew.SID > 2)
                    {
                        result = true;
                    }

                    // if the crew in question is the captain, we're OK
                    if (ship.CaptainID == userId)
                    {
                        result = true;
                    }

                    // if the crew is found among the listed crew, we're OK.
                    if (ship.Entities_Crew2.Any(i => i.CrewID == userId))
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="crewId">
        /// The crew id.
        /// </param>
        public void Delete(int id, int crewId)
        {
            using (var context = new SailingEntities())
            {
                var entity = context.Entities_Vessel.Where(i => i.VesselID == id).FirstOrDefault();
                if (entity != null)
                {
                    entity.DateDeleted = DateTime.Now;
                    entity.DeletedbyID = crewId;
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int Create(Vessel entity)
        {
            var vessel=FleetFactory.CreateVessel(entity);

                var editorIds = entity.Editors.Select(i => i.ID).ToArray();
                using (var context = new SailingEntities())
                {
                    vessel.Entities_Crew = entity.Captain == null
                                               ? null
                                               : context.Entities_Crew.FirstOrDefault(
                                                   i => i.CrewID == entity.Captain.ID);
                    vessel.Entities_Crew2 = editorIds.Count() < 1
                                                ? null
                                                : context.Entities_Crew.Where(i => editorIds.Contains(i.CrewID)).Distinct().
                                                      ToArray();
                    vessel.Entities_Photos = entity.Photo == null
                                                 ? null
                                                 : context.Entities_Photos.FirstOrDefault(i => i.PhotoID == entity.Photo.ID);
                    if (entity.SailLocker != null && entity.SailLocker.Any())
                    {
                        foreach (var sheet in entity.SailLocker)
                        {
                            if (sheet.SailType != null)
                            {
                                var sail = CommonFactory.CreateSheetSail(sheet);
                                vessel.Entities_Vessels_Sails.Add(sail);
                            }
                        }
                    }

                    context.Entities_Vessel.Add(vessel);
                    context.SaveChanges();

                }
           
            return vessel.VesselID;
        }

        /// <summary>
        /// The add vessel to fleet.
        /// </summary>
        /// <param name="entry">
        /// The entry.
        /// </param>
        /// <param name="fleetID">
        /// The fleet id.
        /// </param>
        public void AddVesselToFleet(VesselEntry entry, int fleetID)
        {
            if (entry != null && entry.Vessel != null&&entry.Vessel.ID>0)
            {

            }
        }

        /// <summary>
        /// The get sail id sfor vessel.
        /// </summary>
        /// <param name="ids">
        /// The ids.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<int> GetSailIDSforVessel(IEnumerable<int> ids)
        {
            IEnumerable<int> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Logs_Sails.Where(i => i.DateDeleted==null&&ids.Contains(i.Entities_Vessel.VesselID));
                result = query.Select(i => i.SailID).ToArray();
            }

            return result;
        }

        /// <summary>
        /// The get tags.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Tags> GetTags(int id)
        {
            IEnumerable<Tags> results = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Vessel where i.VesselID == id select i.Entities_Tags).FirstOrDefault();
                if (query != null)
                {
                    results = query.Select(CommonFactory.CreateTag);
                }
            }

            return results;
        }

        /// <summary>
        /// The add tags.
        /// </summary>
        /// <param name="tagIds">
        /// The tag ids.
        /// </param>
        /// <param name="vesselId">
        /// The vessel id.
        /// </param>
        public void AddTags(List<int> tagIds, int vesselId)
        {
            using (var context = new SailingEntities())
            {
                List<Entities_Tags> tags = new List<Entities_Tags>();
                foreach (var tagId in tagIds)
                {
                    var tag = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                    if (tag != null && tag.Name != null)
                    {
                        tags.Add(tag);
                    }
                }

                var vessel = (from i in context.Entities_Vessel where i.VesselID == vesselId select i).FirstOrDefault();
                if (vessel != null && vessel.VesselID != null)
                {
                    foreach (var tag in tags)
                    {
                        if (vessel.Entities_Tags.Where(i => i.TagID == tag.TagID).FirstOrDefault() == null)
                        {
                            vessel.Entities_Tags.Add(tag);
                        }
                    }
                }

                context.SaveChanges();

            }

        }

        /// <summary>
        /// The delete tag.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="vesselID">
        /// The vessel id.
        /// </param>
        public void DeleteTag(int id, int vesselID)
        {
            using (var context = new SailingEntities())
            {
                var trial = (from i in context.Entities_Tags where i.TagID == id select i).FirstOrDefault();
                if (trial != null)
                {
                    var vessel = (from i in trial.Entities_Vessel where i.VesselID == vesselID select i).FirstOrDefault();
                    if (vessel != null)
                    {
                        trial.Entities_Vessel.Remove(vessel);
                        context.SaveChanges();
                    }


                }
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <exception cref="EntityNotFoundException">
        /// </exception>
        public void Update(Vessel entity)
        {

            using (var context = new SailingEntities())
            { 
                var captain = (entity.Captain != null) ? context.Entities_Crew.FirstOrDefault(i=>i.CrewID==entity.Captain.ID) : null;
                var cids = entity.Editors.Select(j => j.ID);
                var fleetIds = (entity.Fleets == null || entity.Fleets.Any(i => i != null))
                                 ? null
                                 : entity.Fleets.Select(i => i.ID);
                var crew = context.Entities_Crew.Where(i => cids.Contains(i.CrewID))
                                     .ToArray();
                var currentEntity = context.Entities_Vessel.FirstOrDefault(i => i.VesselID == entity.ID);
                if (currentEntity != null)
                {
                    currentEntity.Beam = entity.Beam;
                    currentEntity.CaptainID = entity.Captain == null ? 0 : entity.Captain.ID;
                    currentEntity.Entities_Crew = captain;
                    currentEntity.DistinguishingFeatures = entity.DistinguishingFeatures;
                    currentEntity.HullColor = entity.HullColors;
                    currentEntity.HullNumbers = entity.HullNumbers;
                    currentEntity.Keel = entity.Keel == null ? null : entity.Keel.ToString();
                    currentEntity.Length = (decimal)entity.Length;
                    currentEntity.LetterColor = entity.LetteringColors;
                    currentEntity.Manufacturer = entity.Manufacturer;
                    currentEntity.Model = entity.Model;
                    currentEntity.Name = entity.Name;
                    currentEntity.NumberofMasts = entity.Masts;
                    currentEntity.LengthOverall = entity.LengthOverAll;
                    currentEntity.Length = (decimal)entity.Length;
                    currentEntity.LengthWaterline = entity.LengthWaterLine;
                    currentEntity.Rig = entity.Rig == null ? null : entity.Rig.ToString();
                    currentEntity.SailColors = entity.SailColors;
                    currentEntity.SailNumbers = entity.SailNumbers;
                    currentEntity.HullType = entity.Hull.ToString();
                    currentEntity.Entities_Crew2.Clear();
                    currentEntity.Entities_Crew2 = crew;

                    context.SaveChanges();
                }
                else
                {
                    throw new EntityNotFoundException(string.Format("Vessel ID {0} not located.", entity.ID));
                }

            }
        }



        /// <summary>
        /// The vessel sail count.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int VesselSailCount(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                result = context.Logs_Sails.Count(i => i.VesselID == id);
            }

            return result;
        }

        /// <summary>
        /// The vessel mile count.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal VesselMileCount(int id)
        {
            decimal result = 0;
            using (var context = new SailingEntities())
            {
                result = context.Logs_Sails.Where(i => i.VesselID == id).Sum(i => i.Miles);
            }

            return result;
        }

        /// <summary>
        /// The vessel hour count.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal VesselHourCount(int id)
        {
            decimal result = 0;
            using (var context = new SailingEntities())
            {
                result = context.Logs_Sails.Where(i => i.VesselID == id).Sum(i => i.Hours);
            }

            return result;
        }

        /// <summary>
        /// The get top ten sail for vessel.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<SailBase> GetTopTenSailForVessel(int id)
        {
            IEnumerable<SailBase> result = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Vessel.Include(j => j.Logs_Sails).Include(k=>k.Logs_Sails.Select(i=>i.Entities_Crew2)) select i.Logs_Sails.OrderByDescending(j=>j.Date)).Take(10);
                var entities = query.ToArray() as IEnumerable<Logs_Sails>;
                result = entities.Select(SailFactory.CreateSailbase);
            }

            return result;

        }

        /// <summary>
        /// The get tags for vessel.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Tags> GetTagsForVessel(int id)
        {
            IEnumerable<Tags> result = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Vessel.Include(j => j.Entities_Tags) where i.VesselID == id select i.Entities_Tags.Where(k=>k.DateDeleted==null)).FirstOrDefault();
                result = query.ToArray().Select(CommonFactory.CreateTag);
            }

            return result;
        }

        /// <summary>
        /// The get crew for sail.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<CrewBase> GetCrewForSail(int id)
        {
            IEnumerable<CrewBase> result = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Logs_Sails.Include(j => j.Entities_Vessel).Include(j => j.Entities_Crew)
                             where i.Entities_Vessel.Logs_Fleet_Vessel.Any(k => k.FleetID == id)
                             select i.Entities_Crew3.Distinct()).FirstOrDefault();
                var dbentity = query.ToArray();
                if (dbentity != null)
                {
                
                    result = dbentity.Select(CrewFactory.CreateCrewBase);
                }
                
            }

            return result;
        }

        /// <summary>
        /// The get crew count.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetCrewCount(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Logs_Sails.Include(j => j.Entities_Vessel).Include(j => j.Entities_Crew) where i.VesselID == id select i.Entities_Crew3.Distinct()).Count();
                result = query;

            }

            return result;
        }

        /// <summary>
        /// The sail total.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int SailTotal(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            { 
                var query=(from i in context.Logs_Sails where i.VesselID==id select i).Count();
                result = query;
            }

            return result;
         
        }

        /// <summary>
        /// The sail total for fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int SailTotalForFleet(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Logs_Sails where i.Entities_Vessel.Logs_Fleet_Vessel.Any(k => k.FleetID == id) select i).Count();
                result = query;
            }

            return result;

        }

        /// <summary>
        /// The get mile total for fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal GetMileTotalForFleet(int id)
        {
            decimal result = 0;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Logs_Sails where i.Entities_Vessel.Logs_Fleet_Vessel.Any(k => k.FleetID == id) select i.Miles).Sum();
                result = query;
            }

            return result;

        }

        /// <summary>
        /// The get vessel total for fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetVesselTotalForFleet(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Fleet where i.FleetID==id select i.Logs_Fleet_Vessel.Select(k=>k.Entities_Vessel)).Count();
                result = query;
            }

            return result;
        }

        /// <summary>
        /// The get crew total for fleet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetCrewTotalForFleet(int id)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Logs_Sails.Include(j => j.Entities_Vessel).Include(j => j.Entities_Crew) where i.Entities_Vessel.Logs_Fleet_Vessel.Any(k => k.FleetID == id) select i.Entities_Crew3.Distinct()).Count();
                result = query;
            }

            return result;
        }

        /// <summary>
        /// The get animals for vessel.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Animal> GetAnimalsForVessel(int id)
        {
            IEnumerable<Animal> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Animals_Names.Where(i => i.Logs_Sails.Any(j => j.VesselID == id)).ToArray();
                if (query!=null)
                {
                    result = query.Select(AnimalFactory.CreateAnimal).ToArray();
                }
            }

            return result;
        }

        /// <summary>
        /// The remove vessel from fleet.
        /// </summary>
        /// <param name="vesselID">
        /// The vessel id.
        /// </param>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        public void RemoveVesselFromFleet(int vesselID, int fleetId)
        {
            using (var context = new SailingEntities())
            {
                var fleet = context.Entities_Fleet.FirstOrDefault(i => i.FleetID == fleetId);
                var vessel = context.Entities_Vessel.FirstOrDefault(i => i.VesselID == vesselID);
                var entry = fleet.Logs_Fleet_Vessel.First(i => i.Entities_Vessel.VesselID == vesselID);
                if (fleet != null && vessel != null && entry != null)
                {
                    fleet.Logs_Fleet_Vessel.Remove(entry);
                    vessel.Logs_Fleet_Vessel.Remove(entry);
                    context.Logs_Fleet_Vessel.Remove(entry);
                    context.SaveChanges();
                }
                
            }
        }

        public bool IsVesselInFleet(int vesselId, int fleetId)
        {
            bool result = false;
            using (var context = new SailingEntities())
            {
                var vessel =
                    context.Entities_Vessel.FirstOrDefault(i => i.Logs_Fleet_Vessel.Any(x => x.FleetID == fleetId));
                result = vessel != null;

            }
            return result;
        }

        /// <summary>
        /// The get first initial index.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<string> GetFirstInitialIndex()
        {
            List<string> result = new List<string>();
            using (var context = new SailingEntities())
            {
                result = context.Entities_Vessel.Where(i => !string.IsNullOrEmpty(i.Name)).Select(i => i.Name.Substring(0, 1).ToUpper()).Distinct().ToList();
            }
            return result;
        }

        /// <summary>
        /// The search.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<VesselBase> Search(GenericQuery request)
        {
            IEnumerable<VesselBase> result = null;
            using (var context = new SailingEntities())
            {
                var query =
                    context.Entities_Vessel.Include(i => i.Entities_Crew1)
                        .Include(i => i.Entities_Vessels_Sails)
                        .Include(i => i.Entities_Vessels_Sails.Select(j => j.Entities_Config))
                        .Where(i => i.Name != null);
                if (!request.SelectDeleted)
                {
                    query = query.Where(i => i.DateDeleted == null);
                }

                if (!string.IsNullOrEmpty(request.FirstInitial))
                {
                    query = query.Where(i => i.Name.StartsWith(request.FirstInitial));
                }

                if (!string.IsNullOrEmpty(request.Name))
                {
                    query = query.Where(i => i.Name == request.Name);
                }

                if (query != null)
                {
                    result = query.Select(FleetFactory.CreateVesselBase).ToArray();
                }
            }

            return result;
        }

        /// <summary>
        /// The get vessel bases.
        /// </summary>
        /// <param name="fleetId">
        /// The fleet id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<VesselBase> GetVesselBases(int? fleetId = null)
        {
            IEnumerable<VesselBase> data=null;
            using (var context=new SailingEntities())
            {
                var query=context.Entities_Vessel.Where(i=>i.DateDeleted==null&&i.TestData==null);
                if (fleetId!=null&&fleetId!=0)
                {
                    query=query.Where(i=>i.Logs_Fleet_Vessel.Any(n=>n.FleetID==fleetId));
                }
                else if (fleetId!=null&&fleetId==0)
                {
                    query=query.Where(i=>!i.Logs_Fleet_Vessel.Any());
                }

                var entities=query.ToArray();
                if (entities!=null)
                {
                    data=query.Select(FleetFactory.CreateVesselBase).ToArray();
                }

                return data;
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Vessel"/>.
        /// </returns>
        public Vessel Get(int id)
        {
            Vessel result = null;
            using (var context = new SailingEntities())
            {
                var query =
                    context.Entities_Vessel.Include(a => a.Entities_Crew1)
                        .Include(a => a.Entities_Crew2)
                        .Include(a => a.Entities_Crew)
                        .Include(a => a.Entities_Crew1)
                        .Include(a => a.Logs_Fleet_Vessel)
                        .Include(i => i.Entities_Vessels_Sails)
                        .Include(i => i.Entities_Vessels_Sails.Select(j => j.Entities_Config))
                        .Include(i => i.Entities_Vessels_Sails.Select(j => j.Entities_Vessel))
                        .Include(i => i.Entities_Vessels_Sails.Select(j => j.Entities_Vessel.Entities_Crew))
                        .Include(k => k.Entities_Photos1)
                        .Include(i => i.Entities_Photos)
                        .Include(i => i.Entities_Comment)
                        .Include(i => i.Entities_Comment.Select(j => j.Entities_Crew))
                        .Include(i => i.Entities_Tags)
                        .Where(i => i.VesselID == id)
                        .Include(i => i.Logs_Sails)
                        .FirstOrDefault();
                query.Entities_Comment = query.Entities_Comment.Where(i => i.DateDeleted == null).ToArray();
                query.Entities_Photos1 = query.Entities_Photos1.Where(i => i.DeleteDate == null).ToArray();
                query.Logs_Sails = query.Logs_Sails.Where(i => i.DateDeleted == null).ToArray();

                if (query != null)
                {
                    result = FleetFactory.CreateVessel(query);
                }

                // Crew1 is permissions
                var animals =
                    context.Entities_Animals_Names.Where(i => i.Logs_Sails.Any(j => j.VesselID == id && j.DateDeleted==null))
                        .Distinct()
                        .ToArray();
                if (animals != null && animals.Any())
                {
                    result.AnimalsSpotted = animals.Select(AnimalFactory.CreateAnimal).ToArray();
                }

                var fleets =
                    context.Logs_Fleet_Vessel.Where(i => i.Entities_Vessel.VesselID == id)
                        .Select(i => i.Entities_Fleet)
                        .ToArray();
                if (fleets.Any())
                {
                    result.Fleets = fleets.Select(FleetFactory.CreateFleetBase);
                }

                var crew = context.Entities_Crew.Where(i => i.Logs_Sails3.Any(j => j.VesselID == id && j.DateDeleted == null))
                        .Distinct()
                        .ToArray();

                if (crew != null && crew.Any())
                {
                    result.Crew = crew.Select(CrewFactory.CreateCrewBase);
                }
            }

            return result;
        }

        /// <summary>
        /// The get vessel foundation.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="VesselFoundation"/>.
        /// </returns>
        public VesselFoundation GetVesselFoundation(int id)
        {
            VesselFoundation result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Vessel.Include(i=>i.Entities_Crew).FirstOrDefault(i => i.VesselID == id);
                if (query != null)
                {
                    result = FleetFactory.CreateVesselFoundation(query);
                }
            }

            return result;
        }

        /// <summary>
        /// The add photo.
        /// </summary>
        /// <param name="photoID">
        /// The photo id.
        /// </param>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        public void AddPhoto(int photoID, int entityId)
        {
            using (var context = new SailingEntities())
            {
                var dbEntity = context.Entities_Vessel.FirstOrDefault(i => i.VesselID == entityId);
                var dbPhoto = context.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoID);
                if (dbEntity != null && dbPhoto != null)
                {
                    dbEntity.Entities_Photos1.Add(dbPhoto);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// The delete photo.
        /// </summary>
        /// <param name="photoID">
        /// The photo id.
        /// </param>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        public void DeletePhoto(int photoID, int entityId)
        {
            using (var context = new SailingEntities())
            {
                var dbEntity = context.Entities_Vessel.FirstOrDefault(i => i.VesselID == entityId);
                var dbPhoto = context.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoID);
                if (dbEntity != null && dbPhoto != null)
                {
                    dbEntity.Entities_Photos1.Remove(dbPhoto);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="search">
        /// The search.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<VesselBase> GetAll(object search=null)
        {
            IEnumerable<VesselBase> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Vessel.Where(i=>i.DateDeleted==null);
                if (search != null && search is VesselSearch)
                {
                    var filter = search as VesselSearch;
                    if (filter.SelectDeleted != true)
                    {
                        query = query.Where(i => i.DateDeleted == null);
                    }
                    if (filter.Name != null)
                    {
                        query = query.Where(i => i.Name.Equals(filter.Name, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (filter.fleet != null)
                    {
                        query = query.Where(i => i.Logs_Fleet_Vessel.Any(j => j.FleetID == filter.fleet));
                    }

                    var dbentities = query.ToArray();

                    if (dbentities!=null&&dbentities.Any())
                    {
                        result = dbentities.Select(FleetFactory.CreateVesselBase).ToArray();
                    }
                }
                
            }

            return result;
        }

        /// <summary>
        /// The is unique.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="errorMessage">
        /// The error message.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUnique(Vessel entity, out List<string> errorMessage)
        {
            bool result = false;
            errorMessage = null;
            if (entity.HullNumbers!=null)
            {
                using (var context = new SailingEntities())
                {
                    var dbentity =
                        context.Entities_Vessel.FirstOrDefault(
                            i => (i.HullNumbers == entity.HullNumbers));
                    if (dbentity != null)
                    {
                        result = true;
                        errorMessage=new List<string>();
                        var rig = dbentity.Rig;
                        var length = dbentity.Length;
                        var description = "n unspecified vessel";
                        if (length > 0 && rig != null)
                        {
                            description = string.Format(" {0}' {1}", length, rig);
                        }

                        var captain = dbentity.Entities_Crew == null
                                          ? "<no skipper listed?"
                                          : dbentity.Entities_Crew.FirstName + " " + dbentity.Entities_Crew.LastName;
                        errorMessage.Add(string.Format("The {0}, a{1} with a listed skipper of {2} already has hull numbers {3}.", dbentity.Name, description, captain, dbentity.HullNumbers));
                    }
                }
                }

            return result;
        }

        /// <summary>
        /// The add sheet sail to existing vessel.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="parentID">
        /// The parent id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        /// <exception cref="EntityNotFoundException">
        /// </exception>
        public int AddSheetSailToExistingVessel(SheetSail entity, int parentID)
        {
            int result = 0;
            if (entity != null)
            {
                using (var context = new SailingEntities())
                {
                    var vessel = context.Entities_Vessel.FirstOrDefault(i => i.VesselID == parentID);
                    if (vessel != null)
                    {
                        
                        var sheetSail = CommonFactory.CreateSheetSail(entity);
                        vessel.Entities_Vessels_Sails.Add(sheetSail);
                        context.SaveChanges();
                        result = sheetSail.ID;
                    }
                    else
                    {
                        throw new EntityNotFoundException("The vessel referenced does not exist");
                    }
                }
            }
            else
            {
                throw new ArgumentNullException("The sheet sail was blank.");
            }
            return result;
        }

        /// <summary>
        /// The update sheet sail.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        /// <exception cref="EntityNotFoundException">
        /// </exception>
        public void UpdateSheetSail(SheetSail entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity cannot be null");
            }
            using (var context = new SailingEntities())
            {
                var dbEntity = context.Entities_Vessels_Sails.FirstOrDefault(i => i.ID == entity.ID);
                if (dbEntity == null)
                {
                    throw new EntityNotFoundException("sail not found");
                }
                if (entity.SailType != null)
                {
                    dbEntity.SailTypeID = entity.SailType.ID;
                }
                // We always step through a new entity, setting all fields to their new values.  Tedious but necessary.
                dbEntity.Location = entity.Location;
                dbEntity.SailLettering = entity.SailLettering;
                dbEntity.SailArea = entity.SailArea;
                dbEntity.RollingFurler = entity.RollingFurling;
                context.SaveChanges();
            }
        }

        public void DeleteSheetSail(int id)
        {
            using (var context = new SailingEntities())
            {
                var entity = context.Entities_Vessels_Sails.FirstOrDefault(i => i.ID == id);
                if (entity == null)
                {
                    throw new EntityNotFoundException("That sail does not exist.");
                }
                context.Entities_Vessels_Sails.Remove(entity);
                context.SaveChanges();
            }
        }

        public SheetSail GetSheetSail(int id)
        {
            SheetSail result = null;
            using (var context = new SailingEntities())
            {
                var entity =
                    context.Entities_Vessels_Sails.Include(i => i.Entities_Config)
                        .Include(i => i.Entities_Vessel)
                        .Include(i => i.Entities_Vessel.Entities_Crew)
                        .FirstOrDefault(i => i.ID == id);
                result = Factories.CommonFactory.CreateSheetSail(entity);
            }
            return result;
        }

        public async Task<IEnumerable<VesselEntry>> GetVesselEntriesForFleet(int fleetId)
        {
            IEnumerable<VesselEntry> result = null;
            using (var context = new SailingEntities())
            {
                var dbEntities = await context.Logs_Fleet_Vessel.Where(i => i.FleetID == fleetId).ToArrayAsync();
                if (dbEntities != null && dbEntities.Any(i => i != null))
                {
                    result = dbEntities.Select(FleetFactory.CreateVesselEntry);
                }

            }
            return result;
        }

        public IEnumerable<int> GetFleetIDSForVessel(int vesselId)
        {
            IEnumerable<int> result = null;
            using (var context = new SailingEntities())
            {
                var fleets =
                    context.Entities_Vessel.Where(i => i.VesselID == vesselId)
                        .Select(i => i.Logs_Fleet_Vessel)
                        .ToArray().FirstOrDefault();
                if (fleets != null && fleets.Any())
                {
                    result = fleets.Select(i => i.FleetID).ToArray();
                }
            }

            return result;
        }
    }
}