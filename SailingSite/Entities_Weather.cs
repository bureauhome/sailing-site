//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SailingSite
{
    using System;
    using System.Collections.Generic;
    
    public partial class Entities_Weather
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Entities_Weather()
        {
            this.Entities_Comment = new HashSet<Entities_Comment>();
            this.Entities_Photos = new HashSet<Entities_Photos>();
            this.Logs_Sails = new HashSet<Logs_Sails>();
        }
    
        public int WeatherID { get; set; }
        public string Description { get; set; }
        public string IconPath { get; set; }
        public Nullable<int> Value { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Entities_Comment> Entities_Comment { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Entities_Photos> Entities_Photos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Logs_Sails> Logs_Sails { get; set; }
    }
}
