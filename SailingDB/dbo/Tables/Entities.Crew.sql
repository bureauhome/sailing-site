﻿CREATE TABLE [dbo].[Entities.Crew] (
    [CrewID]         INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]      NVARCHAR (128) NOT NULL,
    [LastName]       NVARCHAR (128) NOT NULL,
    [UserName]       NVARCHAR (50)  NOT NULL,
    [PassWord]       TEXT           NOT NULL,
    [EmailAddress]   NVARCHAR (128) NULL,
    [Salt]           NVARCHAR (128) NULL,
    [SID]            INT            CONSTRAINT [DF_Entities.Crew_SID] DEFAULT ((0)) NULL,
    [DateDeleted]    DATETIME       NULL,
    [PrimaryPhotoID] INT            NULL,
    [TestData] BIT NULL DEFAULT 0, 
    [BadLoginCount] INT NULL, 
    [LastBagLogin] DATETIME2 NULL, 
    CONSTRAINT [pk_CrewID] PRIMARY KEY CLUSTERED ([CrewID] ASC),
    CONSTRAINT [FK_Entities.Crew_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Entities.Crew_Entities.Crew1] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Entities.Crew_Entities.Crew2] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Entities.Crew_Entities.PrimaryPhoto] FOREIGN KEY ([PrimaryPhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID])
);

