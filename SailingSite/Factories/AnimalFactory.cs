﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Factories
{
    public static class AnimalFactory
    {
        public static Entities_Animals_Categories CreateAnimalCategory(New.AnimalCategory entity)
        {
            if (entity.ParentName==null||entity.ParentName.ToLower() == "null")
            {
                entity.ParentName = null;
            }
            return new Entities_Animals_Categories()
            {
               AnimalCategory=entity.Name==null?"":entity.Name.Trim(),
               ParentCategory=entity.ParentName==null?null:entity.ParentName.Trim(),
               Description=entity.Description==null?null:entity.Description.Trim(),
               Entities_Photos=entity.Photo==null?null:PhotoFactory.CreatePhoto(entity.Photo.ToPhoto())
            };
        }

        public static New.AnimalCategory CreateAnimalCategory(Entities_Animals_Categories entity)
        {
            return new New.AnimalCategory()
            {
                Name = entity.AnimalCategory==null?null:entity.AnimalCategory.Trim(),
                ParentName=entity.ParentCategory==null?null:entity.AnimalCategory.Trim(),
                Description=entity.Description==null?null:entity.Description.Trim(),
                Subcategories=entity.Entities_Animals_Categories1==null||!entity.Entities_Animals_Categories1.Any()?null:entity.Entities_Animals_Categories1.Select(Factories.AnimalFactory.CreateAnimalCategory),
                Animals = entity.Entities_Animals_Names.Select(Factories.AnimalFactory.CreateAnimal),
                Photo = entity.Entities_Photos == null ? null : PhotoFactory.CreatePhotoBase(entity.Entities_Photos)
            };
        }




        public static Entities_Animals_Names CreateAnimal (New.Animal entity)
        {
            if (entity.Name.Length>20)
            {
                entity.Name=entity.Name.Substring(0,19);
            }
            
            var dcContext= new Entities_Animals_Names()
            {
                AnimalName = entity.Name == null ? null : entity.Name.Trim(),
                AnimalID = entity.ID,
                CreatedByID = entity.CreatedBy.ID,
                AnimalCategory = entity.Category==null?null:entity.Category.Trim(),
                Description=entity.Description==null?null:entity.Description.Trim(),
                Entities_Photos= entity.Photo==null? null:PhotoFactory.CreatePhoto(entity.Photo.ToPhoto())
            };
            if (entity.Aliases != null && entity.Aliases.Any())
            {
                foreach (var alias in entity.Aliases)
                {
                    var dbAlias = new Logs_Animals_Alias();
                    dbAlias.AnimalID = dcContext.AnimalID;
                    dbAlias.Entities_Animals_Names = dcContext;
                    dbAlias.Alias = alias.Trim();
                    dcContext.Logs_Animals_Alias.Add(dbAlias);
                }
            }
            
            return dcContext;
        }

        public static New.Animal CreateAnimal(Entities_Animals_Names entity)
        {
            List<string> aliases = new List<string>();
            if (entity.Logs_Animals_Alias.Any())
            {
                aliases = (from s in entity.Logs_Animals_Alias select s.Alias.Trim()).ToList();
            }

            return new New.Animal()
            {
                Name = entity.AnimalName==null?null:entity.AnimalName.Trim(),
                ID = entity.AnimalID,
                Category = entity.AnimalCategory==null?null:entity.AnimalCategory.Trim(),
                Aliases=aliases,
                DateDeleted = entity.DateToDelete,
                Description=entity.Description==null?null:entity.Description.Trim(),
                Comments=entity.Entities_Comment==null&&!entity.Entities_Comment.Any()?null:entity.Entities_Comment.Select(CommonFactory.CreateCommentBase),
                Photo=entity.Entities_Photos==null?null:PhotoFactory.CreatePhotoBase(entity.Entities_Photos)
            };
        }

        public static New.AnimalDetails CreateAnimalDetails(Entities_Animals_Names entity)
        {
            List<string> aliases = new List<string>();
            if (entity.Logs_Animals_Alias.Any())
            {
                aliases = (from s in entity.Logs_Animals_Alias select s.Alias.Trim()).ToList();
            }
            List<New.PhotoBase> photos = new List<New.PhotoBase>();
            if (entity.Entities_Photos1.Any())
            {
                photos=entity.Entities_Photos1.Select(PhotoFactory.CreatePhotoBase).ToList();
            }
            List<New.Tags> tags=new List<New.Tags>();
            if (entity.Entities_Tags!=null&&entity.Entities_Tags.Any())
            {
                tags=entity.Entities_Tags.Select(CommonFactory.CreateTag).ToList();
            }
            List<New.SailBase> spottings=new List<New.SailBase>();
            if (entity.Logs_Sails!=null&&entity.Logs_Sails.Any())
            {
                spottings=entity.Logs_Sails.Select(SailFactory.CreateSailbase).ToList();
            }
            List<New.CommentBase> comment=new List<New.CommentBase>();
            if (entity.Entities_Comment!=null&&entity.Entities_Comment.Any())
            {
                comment=entity.Entities_Comment.Select(CommonFactory.CreateCommentBase).ToList();
            }

            return new New.AnimalDetails()
            {
                Name = entity.AnimalName == null ? null : entity.AnimalName.Trim(),
                ID = entity.AnimalID,
                Category = entity.AnimalCategory == null ? null : entity.AnimalCategory.Trim(),
                Aliases = aliases,
                Description = entity.Description == null ? null : entity.Description.Trim(),
                Spottings = spottings,
                Comments = comment,
                Tags = tags,
                Photos=photos,
                Photo = entity.Entities_Photos == null ? null : PhotoFactory.CreatePhotoBase(entity.Entities_Photos)
            };
        }
    }
}