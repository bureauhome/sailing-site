﻿CREATE TABLE [dbo].[Comments.Bearings.LandMark](
	[CommentID] [int] NOT NULL,
	[LandMarkBearingID] [int] NOT NULL,
 CONSTRAINT [PK_CommentsBearingsLandMark] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC,
	[LandMarkBearingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Comments.Bearings.LandMark]  WITH CHECK ADD  CONSTRAINT [FK_CommentsBearingsLandMark_Entities.Comment] FOREIGN KEY([CommentID])
REFERENCES [dbo].[Entities.Comment] ([CommentID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Comments.Bearings.LandMark] CHECK CONSTRAINT [FK_CommentsBearingsLandMark_Entities.Comment]
GO

ALTER TABLE [dbo].[Comments.Bearings.LandMark]  WITH CHECK ADD  CONSTRAINT [FK_CommentsBearingsLandMark_Entities.LandMark] FOREIGN KEY([LandMarkBearingID])
REFERENCES [dbo].[Bearings.LandMark] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Comments.Bearings.LandMark] CHECK CONSTRAINT [FK_CommentsBearingsLandMark_Entities.LandMark]
GO