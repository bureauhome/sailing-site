﻿CREATE TABLE [dbo].[Logs.Tags.Fleet] (
    [FleetID] INT NOT NULL,
    [TagID]   INT NOT NULL,
    CONSTRAINT [PK_Logs.Tags.Fleet] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Tags.Fleet] PRIMARY KEY CLUSTERED ([FleetID] ASC, [TagID] ASC),
    CONSTRAINT [FK_Logs.Tags.Fleet_Entities.Fleet] FOREIGN KEY ([FleetID]) REFERENCES [dbo].[Entities.Fleet] ([FleetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Tags.Fleet_Entities.Tags] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Entities.Tags] ([TagID])
);

