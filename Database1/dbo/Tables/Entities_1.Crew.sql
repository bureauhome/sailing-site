﻿CREATE TABLE [dbo].[Entities.Crew] (
    [CrewID]       INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]    NVARCHAR (128) NOT NULL,
    [LastName]     NVARCHAR (128) NOT NULL,
    [UserName]     NVARCHAR (50)  NOT NULL,
    [PassWord]     TEXT           NOT NULL,
    [EmailAddress] NVARCHAR (128) NULL,
    [Salt]         NVARCHAR (128) NULL,
    [SID]          INT            CONSTRAINT [DF_Entities.Crew_SID] DEFAULT ((0)) NULL,
    CONSTRAINT [pk_CrewID] PRIMARY KEY CLUSTERED ([CrewID] ASC),
    CONSTRAINT [pk_CrewID] PRIMARY KEY CLUSTERED ([CrewID] ASC)
);

