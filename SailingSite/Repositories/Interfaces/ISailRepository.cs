﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using SailingSite.Common;
    using SailingSite.Models;

    public interface ISailRepository:ITaggableRepository,IPhotographableRepository, ICommentableRepository
    {
        New.SailBase GetLongestSail(int crewID);
        New.SailBase GetWindiestSail(int crewID);
        int CreateSail(New.Sail data, bool jiffy=false);
        IEnumerable<New.WeatherBase> GetWeather();
        IEnumerable<New.FleetBase> GetAllFleets();
        IEnumerable<New.Maneuver> GetManeuvers();
        IEnumerable<New.LocationBase> GetLocations();
        IEnumerable<New.CrewFoundation> GetCrewNames();

        void PurgeSail(IEnumerable<int> ids);
        IEnumerable<New.SheetSail> GetSheetSailLocker(int id);
        IEnumerable<SailingSiteModels.New.VesselFoundation> GetVesselNames();
        IEnumerable<New.SailBase> GetSailBases(IEnumerable<int> ids);
        New.Sail GetSail(int id);
        void UpdateSail(New.Sail newEntity);
        IEnumerable<New.Configuration> GetConfigurations();

        IEnumerable<int> GetExpiredSailIds();
        IEnumerable<SailFeedViewModel> GetFeedItems(FeedSearch search);
        New.VesselBase GetVesselForSail(int sailId);
        string GetCompassRose(int input);
        decimal? GetCompassRose(string input);
        void UpdateComment(New.Comment entity);
        IEnumerable<int> GetCommentIDsForSail(IEnumerable<int> entityID);
        void DeleteSail(int id);
        int JiffySave(Logs_Sails entity);
        int CreateLandMark(New.LandMarkBase entity);
        DateTime? GetSailDate(int entityId);
    }
}