﻿CREATE TABLE [dbo].[Entities.Comment] (
    [CommentID]      INT            IDENTITY (1, 1) NOT NULL,
    [Comment]        NVARCHAR (MAX) NOT NULL,
    [Subject]        NVARCHAR (55)  NULL,
    [CrewID]         INT            NOT NULL,
    [DateCreated]    DATETIME       NOT NULL,
    [DateModified]   DATETIME       NOT NULL,
    [ModifiedCrewID] INT            NULL,
    [Title]          NVARCHAR (50)  NULL,
    [ShipLog]        BIT            NULL,
    CONSTRAINT [pk_CommentID] PRIMARY KEY CLUSTERED,
    CONSTRAINT [pk_CommentID] PRIMARY KEY CLUSTERED ([CommentID] ASC),
    CONSTRAINT [FK__Entities.__CrewI__6C190EBB] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Entities.Comment_Entities.Crew] FOREIGN KEY ([ModifiedCrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Entities.Comment_Entities.Likes] FOREIGN KEY ([ModifiedCrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID])
);

