﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    public static class GlobalVariables
    {
        public static Dictionary<string, string> SessionVariables()
        {
            var output = new Dictionary<string, string>();
            output.Add("descriptiontitle", "Description:");
            output.Add("categorytitle", "Category:");
            output.Add("user", "User:");
            return output;
        }
    }
}