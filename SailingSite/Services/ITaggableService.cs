﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSite.Services
{
    interface ITaggableService
    {
        IEnumerable<SailingSiteModels.New.Tags> GetTags(int entityID);

        void AddTags(int entityId, string tagString);

        void DeleteTag(int entityId, int tagId);
    }
}
