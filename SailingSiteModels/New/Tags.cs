﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tags.cs" company="">
//   
// </copyright>
// <summary>
//   The tags.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;

    using SailingSiteModels.Common;

    /// <summary>
    /// The tags.
    /// </summary>
    [Serializable]
    public class Tags :IDatabaseModel, IEntityModel
    {
        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName {
            get
            {
                return "Tag";
            }
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The creator.
        /// </summary>
        public Crew Creator;

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets the primary key.
        /// </summary>
        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }

        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
    }
}
