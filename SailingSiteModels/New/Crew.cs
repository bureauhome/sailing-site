﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace SailingSiteModels.New
{
    using Newtonsoft.Json;

    using SailingSiteModels.Common;

    [Serializable]
    public class Crew : CrewBase, Common.IDatabaseModel, Common.ITaggableModel, Common.IPhotographedEntity, ISailable
    {
        [Flags]
        public enum SIDEnum { 
            None=0,
            StandardUser=1,
            PowerUser=2,
            Admin=3
        }
                public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";

            }
        }

        public Crew(CrewFoundation entity) : this()
        {
            UserName = entity.UserName;
            FirstName = entity.FirstName;
            LastName = entity.LastName;
            ID = entity.ID;
        }
        public Crew(CrewBase entity) : this()
        {
            FirstName = entity.FirstName;
            ID = entity.ID;
            LastName = entity.LastName;
            UserName = entity.UserName;

        }

        public IEnumerable<CommentBase> CommentsMade { get; set; }
        public PhotoBase ProfilePhoto { get; set; }
        public string Token { get; set; }
        public Stats SailStats { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        private string password="Return Password";
        public string Password
        {
            
            get {
                return password;
            }
            set {
                if (value != "null"&&value!="Change Password")
                {
                    if (!VerifyPasswordRequirements(value)) throw new Exceptions.PasswordDoesNotMeetRequirementsException();
                    password = Utility.Passwords.RenderPassword(value, Salt);
                    var troubleshoot = true;
                }
                else {
                    password = "null";
                }
        } }
        private string salt;
        public string Salt {
            get {
            if (string.IsNullOrEmpty(salt)) {
                GenerateSalt();
            }
            return salt;
        }
            set {
                salt = value;    
            }
            
        }
        public PhotoBase PrimaryPhoto { get; set; }
        public IEnumerable<CommentBase> Comments { get; set; }
        [JsonIgnore]
        public IEnumerable<PhotoBase> Photos { get; set; }
        [JsonIgnore]
        public IEnumerable<PhotoBase> PhotoLog { get; set; }
        [JsonIgnore]
        public IEnumerable<SailBase> Sails { get; set; }
        public IEnumerable<Tags> Tags { get; set; }
        [NonSerialized]
        private string firstPass;
        private void GenerateSalt()
        {
            salt=Utility.Passwords.GenerateSalt();
        }

        public Crew()
        {
            this.SailStats = new Stats();
            this.Tags = new List<Tags>();
            CommentsMade = new List<CommentBase>();
        }

        public Crew(string firstName, string lastName) :this()
        {
            if (firstName.Length < 3 || lastName.Length < 3) throw new Exceptions.NameTooShortException();
            FirstName = firstName;
            LastName = lastName;
            if (string.IsNullOrEmpty(UserName))
            {
                UserName = GenerateUserName(firstName,lastName);
            }

            if (string.IsNullOrEmpty(Password))
            {
                Password = Utility.Passwords.GeneratePassword();
                firstPass = Password;
            }

        }

        
        public SIDEnum SID { get; set; }

        public Crew (string firstName, string lastName, string userName) : this(firstName,lastName)
        {
            if (Old.Crew.UserNameExists(userName)) throw new Exceptions.UserNameInUseException(GenerateUserName(firstName, lastName));
            UserName = userName;
        }

        public Crew(string firstName, string lastName, string userName, string password)
            : this(firstName, lastName, userName)
        {
            if (!VerifyPasswordRequirements(password)) throw new Exceptions.PasswordDoesNotMeetRequirementsException();
            Password = password;
        }

        public Crew(Old.Crew oldCrew) :this()
        {
            var name = oldCrew.Name.Split(' ');
            // Sets the first name
            if (name.Count() > 0)
            {
                FirstName = name[0];
            }
            // Sets the last name.
            if (name.Count() > 1)
            {
                for (int i = 1; i < name.Count(); i++)
                {
                    if (string.IsNullOrEmpty(LastName))
                        LastName = name[i];
                    else
                        LastName = LastName + " " + name[i];
                }
            }
            // Set the user name
            UserName = GenerateUserName(FirstName, LastName);

            // Set a random password
            firstPass = "default";
            Password = "default";
        }


        public object[] PrimaryKey
        {
            get {
                return new object[] { 
                    this.ID
                };
            }
        }

        public void ChangePassword(string password)
        {
            if (!VerifyPasswordRequirements(password)) throw new Exceptions.PasswordDoesNotMeetRequirementsException();
            Password = password;
        }

        public void ChangeLastName(string lastName)
        {
            if (lastName.Length < 3) throw new Exceptions.NameTooShortException();
            LastName = lastName;
        }

        public void ChangeFirstName(string firstName)
        {
            if (firstName.Length < 3) throw new Exceptions.NameTooShortException();
            FirstName = firstName;
        }

        private static string GenerateUserName(string firstName, string lastName)
        {
            var retVal=(firstName.Substring(0, 1).ToLower() + lastName.Replace(" ", "").Replace("-", "").Replace("'", "").Replace(",", "").ToLower());
            if (retVal.Length > 40)
            {
                retVal = retVal.Substring(0, 39);
            }
            return GetUniqueUserName(retVal);
        }

        private static string GetUniqueUserName(string userName)
        {
            var tempUsername = userName;
            int count = 1;
            while (Old.Crew.UserNameExists(userName))
            {

                if (count > 10) userName = tempUsername + Utility.Passwords.GetUniqueCharacter().ToString() + Utility.Passwords.Rand.Next(1900, 2016);
                userName = tempUsername + count++;
            }
            return userName;
        }

        public static bool VerifyPasswordRequirements(string password)
        {
            if (password.Length < 7||password.Length>28)
            {
                return false;
            }
            bool hasUpper=false, hasLower=false, hasNumber=false;

            foreach (char c in password)
            { 
                if (Char.IsDigit(c)) hasNumber=true;
                if (Char.IsUpper(c)) hasUpper=true;
                if (Char.IsLower(c)) hasLower=true;

                if (hasNumber&&hasUpper&&hasLower) return true;
            }
            return false;
        }

        public string GetFirstPass()
        {
            return firstPass;
        }

        public void SetID(int id)
        {
            ID = id;
        }

        public CrewFoundation ToCrewFoundation()
        {
            CrewFoundation result = new CrewFoundation()
                                        {
                                            FirstName = this.FirstName,
                                            LastName = this.LastName,
                                            UserName = this.UserName,
                                            ID = this.ID
                                        };
            return result;

        }

        public class Stats { 
        public SailBase LongestSail { get; set; }
        public SailBase WildestRide { get; set; }
        }
    }
}
