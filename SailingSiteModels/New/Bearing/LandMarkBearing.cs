﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New.Bearing
{
    using System.Security.Principal;

    using SailingSiteModels.Common;

    /// <summary>
    /// The land mark bearing.
    /// </summary>
    public class LandMarkBearing : IBearable, IEntityModel
    {
        /// <summary>
        /// Gets or sets the bearing.
        /// </summary>
        public Bearing Bearing { get; set; }

        /// <summary>
        /// Gets or sets the land mark.
        /// </summary>
        public LandMarkBase LandMark { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return "Location";
            }
        }

        public string DefaultDelete
        {
            get
            {
                return "DeleteLandMark";
            }
        }

        public string DefaultSet
        {
            get
            {
                return "NewLandMark";
            }
        }
    }
}
