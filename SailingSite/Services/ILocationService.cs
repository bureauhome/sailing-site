﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSite.Services
{
    using SailingSite.Models;

    using SailingSiteModels.New;

    /// <summary>
    /// The LocationService interface.
    /// </summary>
    interface ILocationService : ICommonService<SailingSiteModels.New.Location,SailingSiteModels.New.LocationBase>, ITaggableService, ICommentableService
    {
        int Create(SailingSiteModels.New.Location entity);

        bool isUnique(SailingSiteModels.New.Location entity);

        LandMarkBase GetLandMarkBase(int id);

        MergedLocationIndex GetLocationsAndLandmarks();

        IEnumerable<SailingSiteModels.New.LandMarkBase> GetLandMarks();

        void DeleteLandMark(int id, int crewId);

        void Delete(int id, int crewId);

        SearchReturnModel<IEnumerable<LandMarkBase>> SearchLandmarks(GenericQuery request);

        List<string> GetLandMarkFirstInitials();

        SearchReturnModel<IEnumerable<LocationBase>> SearchLocations(GenericQuery request);

        List<string> GetLocationFirstInitials();

        int CreateLandMark(LandMarkBase entity);

    }
}
