﻿CREATE TABLE [dbo].[Static.CompassBearings]
(
	[Id] INT IDENTITY (1, 1), 
    [Alias] NVARCHAR(20) NOT NULL, 
    [Bearing] FLOAT NOT NULL
)
