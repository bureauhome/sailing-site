﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSite.Services
{
    public interface ICommentableService
    {
        void AddComment(int entityID, SailingSiteModels.New.Comment comment);

        IEnumerable<SailingSiteModels.New.CommentBase> GetComments(int[] id);
    }
}
