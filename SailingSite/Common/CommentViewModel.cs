﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    public class CommentViewModel
    {
        public IEnumerable<SailingSiteModels.New.CommentBase> Comments { get; set; }
        public int CharacterCount { get; set; }
        public string Title { get; set; }
    }
}