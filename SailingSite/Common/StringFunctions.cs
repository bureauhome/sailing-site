﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;

namespace SailingSite.Common
{
    public class ObscenityFilterException : Exception
    {
        public int ObscenityLevel { get; set; }

        public ObscenityFilterException()
        {
        }

        public ObscenityFilterException(string message) : base(message)
        {

        }

        public ObscenityFilterException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }

    public class StringFunctions
    {
        private static string[] DirtyWords_100 = new string[] { "bitch", "cunt",  "whore", "buttcrack", "shit", "sh1t", "weiner", "wiener", "weener", "cocaine", "shat", "shyt", "sh_t", "sh-t", "gonad", "sexually", "pedophile", "paederast", "paedophile", "paedo", "fuck", "fondle", "semen", "spoog", "uterus", "masturbat", "gangbang", "gang bang", "astroglide", "cocaine", "marijuana", "jack off", "wank", "rape", "bukake", "shlong", "schlong", "coitus", "bukkake", "teat", "gtfo", "boner", "titties", "t tties", "cl toris", "clitoris", "dildo", "penis", "dmn", "fck", "fk", "shit", "f you", "damn", "sphincter", "bust a nut", "rimjob", "sixty nine", "sixtynine", "69", "420", "dirty sanchez", "blowjob", "cleavage", "vagina", "boob", "asshole", "ass hole", "grope", "breasticle", "nipple", "stripper", "hooters", "asswhole", "ass whole", "anal", "bugger", "shat", "rape", "dig up dirt", "xxx", "pre approved", "visit our website", "direct email", "email harvest", "horny", "xanax", "prostitute", "bangtail", "dollymop", "call girl", "callgirl", "sensual", "suicide", "massag", "orgasm", "semen", "analingus", "nigger", "baby batter", "cannibal", "funbags", "fun bags", "intercourse" };
        private static string[] DirtyWords_100_Lonely = new string[] { "ass", "disrobe", "tits", "tit", "lsd", "meth", "vikes", "ky jelly", "breast", "vag", "breasts", "clit", "sht", "cck", "erotic", "sex", "cum", "spic", "spics", "herp" };
        private static string[] DirtyWords_50 = new string[] { "gazungah", "cream", "prostate", "diddle", "heroine", "assault", "juice", "gorilla", "fetish", "vagitarian", "appendage", "stab", "penetrat", "ejaculat", "fanny", "mutilat", "twerk", "lubricat", "defecat", "wench", "needle", "hypodermic", "snuggle", "between the sheets", "in bed", "drug", "inject", "shoot up", "lube", "funeral", "superbowl", "underage", "pregna", "preggo", "without any clothes", "without clothes", "pregger", "suckle", "shunt", "meat curtains", "labia", "piss", "spurt", "kill", "disapoint", "impale", "flatus", "coward", "cheap", "dissapoint", "disown", "feces", "fecal", "urine", "urinat", "urinal", "not welcome", "cock", "rectal", "molest", "murder", "butt", "zerban", "artery", "rolex", "jugular", "naked", "ginger", "torment", "fat", "bastard", "boob", "lick", "ball", "up the", "in her", "football", "baseball", "basketball", "big game", "p nis", "f  k", "cock", "up her", "erect", "suck", "aerola", "ugly", "bust a", "clearance", "nut", "ravage", "babe", "violate", "double your", "serious cash", "hidden charges", "they keep your money", "subject to credit", "credit score", "consolidate", "refinance", "subscribe", "direct email", "baldness", "snoring", "direct to you", "weight loss", "billion", "million", "hooker", "single women", "giving it away", "meat curtains", "stimulate", "abduct", "oral", "herpes", "genital", "ta ta" };
        private static string[] DirtyWords_50_Lonely = new string[] { "pee", "ram", "cram", "boyfriend", "nude", "poop", "splash", "dong", "bestiality", "lick", "nigerian", "gag", "terminate" };
        private static string[] DirtyWords_25 = new string[] { "peach", "stick", "baby", "dongle", "dangle", "crush", "cherry", "vodka", "bun", "jack daniel", "ginger", "crack", "nut", "dribbl", "thigh", "addict", "lather", "shake", "meat", "meep", "bunny", "popsicle", "rat", "beep", "dance", "gore", "smash", "wild", "tush", "blam", "bleed", "pain", "blood", "smoke", "smoking", "vikings", "biblical", "t wolves", "timberwolves", "badgers", "gophers", "dick", "tiny", "die", "death", "ooze", "oozing", "dead", "sad", "kiss", "upset", "laser printer", "excret", "behavior", "pork", "not invit", "uncle rick", "knife", "glass pipette", "insert", "vein", "member", "cage", "unit", "johnson", "daft", "hole", "pickle", "raunchy", "dirty", "rigid", "slide", "fat", "obese", "butter", "housewife", "order status", "singles", "university diploma", "income", "while you sleep", "check", "stock", "debt", "cure", "thousands", "off shore", "gimmick", "financial", "weekend getaway", "ham sandwich", "sword", "blade", "gun", "magnum", "shoot", "glock", "pistol", "revolver", "nail", "weapon", "razor" };
        private static string[] DirtyWords_10 = new string[] { "attract","snob", "hole", "lip", "plug", "sorry", "berry", "wet", "sauc", "bratwurst", "bratworst", "sausage", "male", "topping", "sandwich", "face", "dollar", "fireman", "remove", "wilt", "bum", "bump", "tight", "model", "_", "pickle", "hot dog", "baloney", "pony", "banana", "leg", "foot", "cage", "motor", "sandal", "polish", "bottom", "backside", "derrier", "tear", "freeze", "froze", "bash", "pulverize", "crumple", "giant", "enormous", "beat", "shave", "nuzzle", "clown", "box", "rip", "shred", "flesh", "bean", "hell", "coon", "grandm", "grandf", "flash", "cloud", "shiv", "macintosh", "face", "apple", "employ", "greg", "blend", "acid", "rat", "melt", "tube", "anatomy", "cut", "fart", "sale", "long", "game", "beer", "drunk", "small", "vacation", "base", "pour", "dunk", "immerse", "big", "blast", "bed", "gang" };
        private static string[] Matches50 = new string[] { "bi.ch", "you.{0,3}suck", "s[a-h]{1,2}long", "beau.{0,2}s|={0,4}pedo", "suck.{0,6}ball", "toss.{0,6}her.{0,4}salad", "suck.{0,3}you.{0,6}do|will|have", @"amanda.s.*beep", "camel.*toe", "cameltoe", @"amandas.*beep", "p(e|3).{2,4}r.t..{1,6}(y.{0,3}r|thy)", "murd.r", @"(v).(k).(ngs)", "bu((.{3,4})|(cock))(e|y)", "chocolate.*salty.*balls", "f.{0,3}u.{0,3}c.{0,3}k", "ball.{0,3}gag", "s.ck.{0,5}my", "s.ck.*d.ck", @"(v).(kes)", @"(f)..(k)", @"(s).{1,3}(h).{1,3}(i).{1,3}(t)", @"(f).{1,3}(u).{1,3}(c).{1,3}(k)", @"(p).(nis)", @"game.of.thrones", @"t.tties", @"p..ter", @"not.{0,4}part.{0,8}family", @"(v)..(gr).", @"(c)..(l).(s)", @"(v)..(ium)", @"(repair).+(credit)", @"(have).+(nice).+(life).+(we).+(wont).+(it)", @"(amanda).+(father)", "(father).+(amanda)", "(p).(rn)", "(pr).(n)", "(golden).+(gophers)", "(go).+(twins)", "(go).+(vik)", "(go).+(gopher)", "(greg).+(father)", "(mitz).+(father)", "(greg).+(dad|father)", "(mitz).{0,20}(dad|father|greg)", "splatad", "not.{0,2}see.{0,2}fit", @"\ss.ank", "eat.{0,2}her|amanda.{0,4}out", "stick|ram|insert|shove|put.{0,4}your|my|his", "cup.{0,4}feel", "fill.*her", "d.rt.", "[a-z][0-9][a-z]", @"\ssl.t", @"wink[a-z]\s", @"\sp.ick\s", "p.{1,2}n.{1,2}s", "w.nker", "ni..le", "boin.{0,2}k", "tumbly.{0,4}what.{0,4}it" };

        public static int ProcessSwears(string message)
        {
            if (String.IsNullOrEmpty(message))
            {
                return 0;
            }
            int i = 0;


            foreach (string s in DirtyWords_100)
            {
                if (message.Contains(s))
                    return 100;
            }
            foreach (string s in DirtyWords_100_Lonely)
            {
                if (message.Contains(" " + s + " "))
                {
                    return 100;
                }
            }

            foreach (string s in Matches50)
            {
                try
                {
                    if (Regex.Match(message, s).Length != 0)
                        i += 10;
                }
                catch { }

            }
            foreach (string s in DirtyWords_50)
            {
                if (message.Contains(s))
                    i += 10;


            }
            foreach (string s in DirtyWords_50_Lonely)
            {
                if (message.Contains(" " + s + " "))
                    i += 10;
            }
            foreach (string s in DirtyWords_25)
            {
                if (message.Contains(s))
                    i += 5;
            }
            foreach (string s in DirtyWords_10)
            {
                if (message.Contains(s))
                    i += 1;
            }

            return i;
        }

    }
}