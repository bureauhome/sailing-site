﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Common
{
    using SailingSiteModels.New;

    public interface ISingleVessel : IEntityModel
    {
        VesselBase Vessel { get; set; }
    }
}
