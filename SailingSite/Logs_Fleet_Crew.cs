//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SailingSite
{
    using System;
    using System.Collections.Generic;
    
    public partial class Logs_Fleet_Crew
    {
        public int FleetID { get; set; }
        public int CrewID { get; set; }
        public string Description { get; set; }
        public Nullable<int> Ordinal { get; set; }
    
        public virtual Entities_Crew Entities_Crew { get; set; }
        public virtual Entities_Fleet Entities_Fleet { get; set; }
    }
}
