﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common
{
    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    public class AuditContext
    {
        /// <summary>
        /// The correlation id.
        /// </summary>
        private string correlationId;

        /// <summary>
        /// The active crew.
        /// </summary>
        public CrewFoundation ActiveCrew { get; set; }

        /// <summary>
        /// The correlation id.
        /// </summary>
        public string CorrelationId
        {
            get
            {
                if (this.correlationId == null)
                {
                    this.correlationId = Guid.NewGuid().ToString();
                }
                return this.correlationId;
            }
            set
            {
                correlationId = value;
            }
        }

        public string ControllerName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditContext"/> class.
        /// </summary>
        public AuditContext()
        {
            CorrelationId = Guid.NewGuid().ToString();
        }

        public string EntryPoint { get; set; }

        public string Verb { get; set; }

        public Log ToLog()
        {
            return new Log()
                       {
                           ActiveUser = this.ActiveCrew == null ? null : this.ActiveCrew.UserName,
                           CorrelationID = this.CorrelationId,
                           Verb = this.Verb,
                           EntryPoint = this.EntryPoint
                       };
        }
    }
}