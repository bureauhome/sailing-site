﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Repositories
{
    public interface IPhotographableRepository
    {
        void AddPhoto(int photoID, int entityId);
        void DeletePhoto(int photoID, int entityId);
    }
}