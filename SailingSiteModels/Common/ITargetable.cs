﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Common
{
    using System.Security.Cryptography.X509Certificates;

    public interface ITargetable
    {
        string TargetController { get; set; }
    }
}
