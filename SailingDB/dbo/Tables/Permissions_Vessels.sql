﻿CREATE TABLE [dbo].[Permissions_Vessels] (
    [CrewID]   INT NOT NULL,
    [VesselID] INT NOT NULL,
    CONSTRAINT [FK_Permissions_Vessels_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Permissions_Vessels_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]) ON DELETE CASCADE ON UPDATE CASCADE, 
    CONSTRAINT [PK_Permissions_Vessels] PRIMARY KEY ([CrewID], [VesselID])
);

