﻿CREATE TABLE [dbo].[Permission_Vessel_Crew_Loggers] (
    [CrewID]   INT NOT NULL,
    [VesselID] INT NOT NULL,
    CONSTRAINT [PK_Permission_Vessel_Loggers_Crew] PRIMARY KEY CLUSTERED ([CrewID] ASC, [VesselID] ASC),
    CONSTRAINT [FK_Permission_Vessel_Crew_Loggers_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]),
    CONSTRAINT [FK_Permission_Vessel_Crew_Loggers_Permission_Vessel_Crew_Loggers] FOREIGN KEY ([CrewID], [VesselID]) REFERENCES [dbo].[Permission_Vessel_Crew_Loggers] ([CrewID], [VesselID])
);

