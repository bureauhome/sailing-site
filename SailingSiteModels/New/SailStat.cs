﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    class SailStat
    {
        public SailBase WildestSail { get; set; }
        public SailBase LongestSail { get; set; }
        public SailBase FirstSail { get; set; }

    }
}
