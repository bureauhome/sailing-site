﻿CREATE TABLE [dbo].[Entities.Config] (
    [ConfigID]    INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (255) NULL
);

