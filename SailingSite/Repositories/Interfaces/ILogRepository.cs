﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILogRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The LogRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Repositories.Interfaces
{
    using System.Threading.Tasks;

    using SailingSiteModels.New.Debug;

    /// <summary>
    /// The LogRepository interface.
    /// </summary>
    public interface ILogRepository
    {
        int CreateLog(Log log);

        Debug_Logs GetLog(int id);

        void PurgeTestData();
    }
}
