﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdminService.cs" company="">
//   
// </copyright>
// <summary>
//   The admin service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data.Linq.Mapping;
    using System.Linq;

    using SailingSite.Models;

    /// <summary>
    /// The admin service.
    /// </summary>
    public class AdminService : ServiceBase, IAdminService
    {
        /// <summary>
        /// The get admin model.
        /// </summary>
        /// <returns>
        /// The <see cref="AdminModel"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public AdminModel GetAdminModel()
        {
             AdminModel result=new AdminModel();
            result.CrewList = this.GetSearchReturnObject(this.CrewRepository.CrewSearch, this.CrewRepository.SelectStartsWithIndex);
            result.VesselList = this.GetSearchReturnObject(this.VesselRepository.Search, this.VesselRepository.GetFirstInitialIndex);
            result.LocationList = this.GetSearchReturnObject(
                this.LocationRepository.SearchLocations,
                this.LocationRepository.GetLocationFirstInitials);
            result.LandMarkList = this.GetSearchReturnObject(this.LocationRepository.SearchLandmarks, this.LocationRepository.GetLandMarkFirstInitials);
            result.FleetList = this.GetSearchReturnObject(this.FleetRepository.Search, this.FleetRepository.GetFirstInitials);
            result.AnimalList = this.GetSearchReturnObject(
                this.AnimalRespository.Search,
                this.AnimalRespository.GetFirstInitials);

            return result;
        }

        /// <summary>
        /// The get returned object.
        /// </summary>
        /// <param name="funcMain">
        /// The func Main.
        /// </param>
        /// <param name="funcIndex">
        /// The func Index.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="SearchReturnModel"/>.
        /// </returns>
        public SearchReturnModel<T> GetSearchReturnObject<T>(Func<GeneralSearchRequest, T> funcMain,Func<List<String>> funcIndex)
        {
            var result = new SearchReturnModel<T>();
            result.StartsWithIndex = funcIndex();
            if (result.StartsWithIndex != null && result.StartsWithIndex.Any())
            {
                string letter = result.StartsWithIndex.FirstOrDefault();
                var request = new GeneralSearchRequest() { FirstInitial = letter };
                result.Result = funcMain(request);
            }
    
            return result;

        }
    }
}