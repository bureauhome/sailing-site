﻿CREATE TABLE [dbo].[Entities.Locations] (
    [LocationID]    INT               IDENTITY (1, 1) NOT NULL,
    [Name]          NCHAR (255)        NOT NULL,
    [MinorLocation] NVARCHAR (128)    NULL,
    [MajorLocation] NVARCHAR (128)    NULL,
    [LocationType]  INT               NULL,
    [PictureID]     INT               NULL,
    [DateDeleted]   DATETIME          NULL,
    [TestData] BIT NULL DEFAULT 0, 
    [DeletedBy] NVARCHAR(50) NULL, 
    [Longitude] FLOAT NULL, 
    [Latitude] FLOAT NULL, 
    CONSTRAINT [pk_LocationID] PRIMARY KEY CLUSTERED ([LocationID] ASC),
    CONSTRAINT [FK_PrimaryPhoto] FOREIGN KEY ([PictureID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID])
);

