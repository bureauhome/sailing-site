﻿CREATE TABLE [dbo].[Logs.Tags.Comment] (
    [CommentID] INT NOT NULL,
    [TagID]     INT NOT NULL,
    CONSTRAINT [PK_Logs.Tags.Comment] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Tags.Comment] PRIMARY KEY CLUSTERED ([CommentID] ASC, [TagID] ASC),
    CONSTRAINT [FK_Logs.Tags.Comment_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Tags.Comment_Entities.Tags] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Entities.Tags] ([TagID])
);

