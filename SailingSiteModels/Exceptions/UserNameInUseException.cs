﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Exceptions
{
    class UserNameInUseException : Exception
    {
        public UserNameInUseException(string uniqueID) :
            base(string.Format("Username is not unique.  Try {0}",uniqueID))
        { }
    }
}
