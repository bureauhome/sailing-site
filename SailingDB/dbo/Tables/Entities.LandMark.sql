﻿CREATE TABLE [dbo].[Entities.LandMark](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Longitude] [float] NOT NULL,
	[Latitude] [float] NOT NULL,
	[Description] [nvarchar](255) NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Altitude] [float] NULL
	 CONSTRAINT [PK_Entities.LandMark] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
), 
    [Test] BIT NULL, 
    [DateDeleted] DATETIME2 NULL, 
    [DeletedBy] NVARCHAR(50) NULL
) ON [PRIMARY]