﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.ViewModels
{
    public class RedirectModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether user blocked.
        /// </summary>
        public bool UserBlocked { get; set; }

        /// <summary>
        /// Gets or sets the controller name.
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// Gets or sets the action name.
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }
    }
}