﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Repositories
{
    using System.Data.Entity;
    using System.Data.Entity.Validation;
    using System.Threading.Tasks;

    using SailingSite.Common;
    using SailingSite.Repositories.Interfaces;

    using SailingSiteModels.New.Debug;

    public class LoggingRepository : ILogRepository
    {
        /// <summary>
        /// The create log.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public int CreateLog(Log log)
        {
            using (var context = new SailingEntities())
            {
                if (log != null)
                {
                    var entity = Factories.CommonFactory.CreateLog(log);
                    if (entity.URL.Length > 254)
                    {
                        entity.URL = entity.URL.Substring(0, 254);
                    }
                    context.Debug_Logs.Add(entity);
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        foreach (var exception in ex.EntityValidationErrors)
                        {
                            var i = 0;
                        }
                        var thing = true;
                    }
                    return entity.ID;
                }
            }
            return 0;
        }


        public Debug_Logs GetLog(int id)
        {
            using (var context = new SailingEntities())
            {
                return context.Debug_Logs.FirstOrDefault(i => i.ID == id);
            }
        }


        public void PurgeTestData()
        {
            using (var context = new SailingEntities())
            {
                var entities =
                    context.Debug_Logs.Where(
                        i => i.ActiveUser == StaticTokens.TestValues.TestContext.ActiveCrew.UserName).ToArray();
                if (entities.Any())
                {
                    context.Debug_Logs.RemoveRange(entities);
                    context.SaveChangesAsync();
                }
            }
        }
    }
}