﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite
{
    internal class ImproperFormatException :Exception
    {
        public ImproperFormatException()
        { }
        public ImproperFormatException(string message)
            : base("I'm terrible sorry, but it appears the entity your provided was not formatted properly.  " +message)
        { }
    }
}