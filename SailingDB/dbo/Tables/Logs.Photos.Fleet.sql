﻿CREATE TABLE [dbo].[Logs.Photos.Fleet] (
    [FleetID] INT NOT NULL,
    [PhotoID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Photos.Fleet] PRIMARY KEY CLUSTERED ([FleetID] ASC, [PhotoID] ASC),
    CONSTRAINT [FK_Logs.Photos.Fleet_Entities.Fleet] FOREIGN KEY ([FleetID]) REFERENCES [dbo].[Entities.Fleet] ([FleetID]),
    CONSTRAINT [FK_Logs.Photos.Fleet_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID]) ON DELETE CASCADE
);

