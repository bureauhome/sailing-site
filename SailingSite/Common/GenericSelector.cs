﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite
{
    public class GenericSelector
    {
        public bool? SelectDeleted { get; set; }
        public int? ID { get; set; }
        public string Name { get; set; }
        public List<String> Tags { get; set; }

        public GenericSelector()
        {
        
        }


    }
}