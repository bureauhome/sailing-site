﻿CREATE TABLE [dbo].[Entities.Photos] (
    [PhotoID]     INT            IDENTITY (1, 1) NOT NULL,
    [PhotoPath]   NVARCHAR (MAX) NOT NULL,
    [ThumbPath]   NVARCHAR (MAX) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (255) NULL,
    [DeleteDate]  DATETIME       NULL,
    [DeletedbyID] INT            NULL,
    [Width] INT NOT NULL, 
    [PhotoCarouselPath] NVARCHAR(MAX) NOT NULL, 
    [Height] INT NOT NULL, 
    CONSTRAINT [pk_PhotoID] PRIMARY KEY CLUSTERED ([PhotoID] ASC),
    CONSTRAINT [FK_Entities.DeletedCrew] FOREIGN KEY ([DeletedbyID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Entities.Photos_Entities.Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID])
);

