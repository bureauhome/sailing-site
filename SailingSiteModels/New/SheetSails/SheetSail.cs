﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SheetSail.cs" company="">
//   
// </copyright>
// <summary>
//   The sheet sail.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// An instance of a vessel's sail.  This is the SHEET type sail as opposed to the VOYAGE type sail.
    /// </summary>
    public class SheetSail
    {
        /// <summary>
        /// Gets or sets the sail type.
        /// </summary>
        public Configuration SailType { get; set; }

        public bool IsAssociated { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the vessel.
        /// </summary>
        public VesselFoundation Vessel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether rolling furling.
        /// </summary>
        public bool? RollingFurling { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// The colors.
        /// </summary>
        /// gets or sets the color
        public string Colors { get; set; }

        /// <summary>
        /// The sail area.
        /// </summary>
        public decimal? SailArea { get; set; }

        /// <summary>
        /// Gets or sets the sail lettering.
        /// </summary>
        public string SailLettering { get; set; }

    }
}
