﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSiteModels.Common
{
    using System.Net.Http.Headers;

    using SailingSiteModels.New;

    public static class Helpers
    {

        public static int ToCompassBearing(this object input)
        {
            return ToCompassBearing(input.ToString());
        }

        public static int ToCompassBearing(this string input)
        {
            CardinalDirections output;
            var success = Enum.TryParse(input, out output);
            if (!success)
            {
                return -1;
            }

            return ToCompassBearing(output);
        }

        public static int ToCompassBearing(this CardinalDirections input)
        {
            int bearing = -1;
            switch (input)
            {
                case CardinalDirections.E:
                    bearing = 90;
                    break;
                case CardinalDirections.NNE:
                    bearing = 22;
                    break;
                case CardinalDirections.ESE:
                    bearing = 112;
                    break;
                case CardinalDirections.SE:
                    bearing = 135;
                    break;
                case CardinalDirections.SSE:
                    bearing = 157;
                    break;
                case CardinalDirections.SSW:
                    bearing = 202;
                    break;
                case CardinalDirections.SW:
                    bearing = 225;
                    break;
                case CardinalDirections.W:
                    bearing = 270;
                    break;
                case CardinalDirections.WNW:
                    bearing = 292;
                    break;
                case CardinalDirections.WSW:
                    bearing = 247;
                    break;
                case CardinalDirections.N:
                    bearing = 0;
                    break;
                case CardinalDirections.S:
                    bearing = 180;
                    break;
            }
            return bearing;
        }

        public static CardinalDirections? ToCompassBearing(this decimal input)
        {
            return (ToCompassBearing((int)input));
        }

        public static CardinalDirections? ToCompassBearing(this int input)
        {
            CardinalDirections? cardinalDirection = null;

            if (input >= 348 || input < 11) cardinalDirection = CardinalDirections.N;
            if (input >= 11 && input < 33) cardinalDirection = CardinalDirections.NNE;
            if (input >= 33 && input < 56) cardinalDirection = CardinalDirections.NE;
            if (input >= 56 && input < 78) cardinalDirection = CardinalDirections.ENE;
            if (input >= 78 && input < 101) cardinalDirection = CardinalDirections.E;
            if (input >= 101 && input < 123) cardinalDirection = CardinalDirections.ESE;
            if (input >= 123 && input < 146) cardinalDirection = CardinalDirections.SE;
            if (input >= 146 && input < 168) cardinalDirection = CardinalDirections.SSE;
            if (input >= 168 && input < 191) cardinalDirection = CardinalDirections.S;
            if (input >= 191 && input < 213) cardinalDirection = CardinalDirections.SSW;
            if (input >= 213 && input < 236) cardinalDirection = CardinalDirections.SW;
            if (input >= 236 && input < 258) cardinalDirection = CardinalDirections.WSW;
            if (input >= 258 && input < 281) cardinalDirection = CardinalDirections.W;
            if (input >= 281 && input < 303) cardinalDirection = CardinalDirections.WNW;
            if (input >= 303 && input < 326) cardinalDirection = CardinalDirections.NW;
            if (input >= 326 && input < 348) cardinalDirection = CardinalDirections.NNW;

            return cardinalDirection;

        }




        public static Random Rand = new Random();

        public static string Chop(this String str, int limit)
        {
            if (str!=null)
            {
                if (str.Length > limit)
                {
                    str = str.Substring(0, limit - 1) + "...";
                }
            }
            return str;
        }

        public static DecimalRange ToSeaStateWaveHeightMeters(this int seastate)
        {
            switch (seastate)
            {
                case 0:
                    return new DecimalRange(0,0);
                    break;
                case 1:
                    return new DecimalRange(0,0.1m);
                    break;
                case 2:
                    return new DecimalRange(0.1m,0.5m);
                    break;
                case 3:
                    return new DecimalRange(0.5m,1.25m);
                    break;
                case 4:
                    return new DecimalRange(1.25m,2.5m);
                    break;
                case 5:
                    return new DecimalRange(2.5m,4m);
                    break;
                case 6:
                    return new DecimalRange(4m,6m);
                    break;
                case 7:
                    return new DecimalRange(6m,9m);
                    break;
                case 8:
                    return new DecimalRange(9m,14m);
                    break;
                case 9:
                    return new DecimalRange(14m,100m);
                    break;
                default:
                    return new DecimalRange(null,null);
            }
        }

        public static string ToSeaStateDescriptor(this int seaState)
        {
            switch (seaState)
            {
                case 0:
                    return "Calm (glassy)";
                    break;
                case 1:
                    return "Calm (ripples)";
                    break;
                case 2:
                    return "Smooth (wavelets)";
                    break;
                case 3:
                    return "Slight";
                    break;
                case 4:
                    return "Moderate";
                    break;
                case 5:
                    return "Rough";
                    break;
                case 6:
                    return "Very rough";
                    break;
                case 7:
                    return "High";
                    break;
                case 8:
                    return "Very High";
                    break;
                case 9:
                    return "Phenomenal";
                    break;
                default:
                    return "Unknown.  Value must be between 0-9.";
            }
        }
        //TODO: Landmarks!

        public static int ToBeaufortScale(this decimal windSpeed)
        {
            if (windSpeed < 1)
            {
                return 0;
            }
            if (windSpeed < 3)
            {
                return 1;
            }
            if (windSpeed < 6)
            {
                return 2;
            }
            if (windSpeed < 10)
            {
                return 3;
            }
            if (windSpeed < 16)
            {
                return 4;
            }
            if (windSpeed < 21)
            {
                return 5;
            }
            if (windSpeed < 27)
            {
                return 6;
            }
            if (windSpeed < 33)
            {
                return 7;
            }
            if (windSpeed < 40)
            {
                return 8;
            }
            if (windSpeed < 47)
            {
                return 9;
            }
            if (windSpeed < 55)
            {
                return 10;
            }
            if (windSpeed < 63)
            {
                return 11;
            }
            return 12;
        }

        public static string ToWindColorClass(this decimal wind)
        {
                if (wind < 5)
                {
                    return "factor1";
                }
                if (wind < 10)
                {
                    return "factor2";
                }
                if (wind < 15)
                {
                    return "factor3";
                }
                if (wind < 20)
                {
                    return "factor4";
                }
                if (wind < 30)
                {
                    return "factor5";
                }
                if (wind < 40)
                {
                    return "factor6";
                }
                return "factor7";

            }
        }

    public static class MetricConversions
    {
        public static int FarenheitToCelcius(this int f)
        {
            return ((f - 32) * 5 / 9);
        }

        public static double MiletoKilometer(this double mile)
        {
            return (mile / 0.6214);
        }

        public static double MiletoNauticalMile(this double mile)
        {
            return (mile * 1.15078);
        }

    }

    public class DecimalRange
    {
        public decimal? Floor { get; set; }
        public decimal? Ceiling { get; set; }

        public DecimalRange(decimal? low, decimal? high)
        {
            Floor = low;
            Ceiling = high;
        }
    }


}