﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    using System.Device.Location;

    using SailingSiteModels.Common;

    /// <summary>
    /// The land mark.
    /// </summary>
    public class LandMarkBase : IEntityModel, ILocation
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return "Location";
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is test.
        /// </summary>
        public bool IsTest { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }


        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public double? Altitude { get; set; }

        public string FullDescription()
        {
                return string.Format("{0} - {1},{2}", this.Description, this.Longitude,this.Latitude);
        }

        public string DefaultDelete
        {
            get { return "DeleteLandMark"; }
        }

        public string DefaultSet
        {
            get
            {
                return "NewLandMark";
            }
        }

        double? IGpsContainer.Longitude
        {
            get
            {
                return this.Longitude;
            }
        }

        double? IGpsContainer.Latitude
        {
            get
            {
                return this.Latitude;
            }
        }
    }
}
