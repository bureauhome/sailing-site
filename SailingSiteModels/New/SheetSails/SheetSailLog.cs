﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SheetSailLog.cs" company="">
//   
// </copyright>
// <summary>
//   The sheet sail log.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    /// <summary>
    /// A log entry for a sheet.  Can be logged broadly to a sail or specifically to a sail log.
    /// </summary>
    public class SheetSailLog
    {
        /// <summary>
        /// Gets or sets the sheet sail.
        /// </summary>
        public SheetSail SheetSail { get; set; }

        /// <summary>
        /// Gets or sets the percentage.
        /// </summary>
        public double Percentage { get; set; }

        public bool IsAssociated { get; set; }
    }
}
