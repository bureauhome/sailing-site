﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.ViewModels
{
    public class PhotographableEntityViewModel
    {
        public HttpPostedFileBase File {get;set;}
        public int EntityID {get; set;}
        public string EntityName {get; set;}
        public string PhotoName { get; set; }
    }
}