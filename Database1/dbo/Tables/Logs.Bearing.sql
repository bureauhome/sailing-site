﻿CREATE TABLE [dbo].[Logs.Bearing] (
    [CompassBearing] DECIMAL (4, 2)    NOT NULL,
    [GPS]            [sys].[geography] NULL,
    [SailID]         INT               NOT NULL,
    [DateTime]       DATETIME          NOT NULL,
    [RowID]          INT               IDENTITY (1, 1) NOT NULL
);

