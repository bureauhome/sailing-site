﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPXParser
{
    using System.Device.Location;
    using System.Xml.Linq;

    using SailingSiteModels.GPX;

    public static class GPXParser
    {
        /// <summary>
        /// The parse track.
        /// </summary>
        /// <param name="gpxDoc">
        /// The gpx doc.
        /// </param>
        /// <param name="tolerance">
        /// The tolerance (suggested: 0.0002 for 48 ft, 0.001 for 200 ft.)
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<SailingSiteModels.GPX.GpxTrack> ParseTrack(XDocument gpxDoc, double? tolerance=null, double? variance=15, double mileVariance=.1)
        {
            var result = new List<GpxTrack>();
            XNamespace gpx = GetGpxNameSpace();
            var tracks = from track in gpxDoc.Descendants(gpx + "trk")
                         select
                             new
                                 {
                                     Name = track.Element(gpx + "name") != null ? track.Element(gpx + "name").Value : null,
                                     Segs = from trackpoint in track.Descendants(gpx + "trkpt")
                                             select
                                                 new
                                                     {
                                                         Latitude = trackpoint.Attribute("lat").Value,
                                                         Longitude = trackpoint.Attribute("lon").Value,
                                                         Elevation =
                                                 trackpoint.Element(gpx + "ele") != null
                                                     ? trackpoint.Element(gpx + "ele").Value
                                                     : null,
                                                         Time =
                                                 trackpoint.Element(gpx + "time") != null
                                                     ? trackpoint.Element(gpx + "time").Value
                                                     : null
                                                     }
                                 };

            if (tracks != null)
            {

                foreach (var trk in tracks)
                {
                    
                    var track = new GpxTrack();
                    track.Name = track.Name;
                    track.Track = new List<GpxTrackPoint>();
                    // Populate track data objects. 
                    GpxTrackPoint currentLegPrevCoord = null;
                    List<GpxTrackPoint> currentLeg = new List<GpxTrackPoint>();
                    double totalMiles=0;
                    double legMiles=0;
                    foreach (var trkSeg in trk.Segs)
                    {
                        GpxTrackPoint tPoint = new GpxTrackPoint();
                        double longitude;
                        double latitude;
                        DateTime time;
                        double miles = 0;
                        double elevation;

                        if (!double.TryParse(trkSeg.Longitude, out longitude)
                            || !double.TryParse(trkSeg.Latitude, out latitude)
                            || !DateTime.TryParse(trkSeg.Time, out time)
                            || !double.TryParse(trkSeg.Elevation, out elevation))
                        {
                            continue;
                        }

                        GeoCoordinate coordinate = new GeoCoordinate(latitude, longitude, elevation);
                        GpxTrackPoint prevCoord = null;
                        if (track.Track.Any())
                        {
                            prevCoord = track.Track.Last();
                            if (prevCoord != null && prevCoord.Coordinate != null)
                            {
                            miles = prevCoord.Coordinate.GetDistanceTo(coordinate) / 1609.344;
                            totalMiles += miles;
                            tPoint.TotalMiles = totalMiles;
                                var mph = CalcSpeed(miles, time, prevCoord.Date);
                                var bearing = Math.Abs(Bearing(prevCoord.Coordinate, coordinate));
                                tPoint.SpeedMPH = mph;
                                prevCoord.Coordinate.Course = bearing;
                            }
                        }

                        // only add another entry if it's a specified distance away from the previous one

                        if (tolerance != null)
                        {
                            if (track.Track.Any())
                            {
                                if (prevCoord != null && prevCoord.Coordinate != null)
                                {
                                    if ((Math.Abs(prevCoord.Coordinate.Latitude - latitude)
                                         + Math.Abs(prevCoord.Coordinate.Longitude - longitude)) <= tolerance)
                                    {
                                        continue;
                                    }
                                }
                            }
                        }

                        tPoint.Coordinate = coordinate;
                        tPoint.Date = time;
                        if (variance != null&&mileVariance!=null)
                        {
                            if (currentLegPrevCoord != null)
                            {
                                // calculate our present bearing.
                                var bearing = Bearing(
                                    currentLegPrevCoord.Coordinate,
                                    tPoint.Coordinate);
                                currentLegPrevCoord.Coordinate.Course = Math.Abs(bearing);
                                legMiles += miles;
                                currentLegPrevCoord.LegMiles = legMiles;
                                // Add the previous coord to the current leg
                                currentLeg.Add(currentLegPrevCoord);
                                // average the bearings
                                var avgBearing = currentLeg.Average(i => i.Coordinate.Course);
                                
                                // If we are higher than the variance
                                if ((Math.Abs(currentLegPrevCoord.Coordinate.Course - avgBearing) > variance)||legMiles>mileVariance)
                                {
                                    // Add the previous coordinate to our track.
                                    track.Track.Add(currentLegPrevCoord);
                                    legMiles = 0;
                                    currentLeg.Clear();
                                }
                            }
                            currentLegPrevCoord = tPoint;
                        }
                        else
                        {
                            track.Track.Add(tPoint);
                        }
                    }
                    if (track != null && track.Track != null && track.Track.Any())
                    {
                        result.Add(track);
                    }
                }
            }

            return result;
        }


        /// <summary> 
        /// Load the namespace for a standard GPX document 
        /// </summary> 
        /// <returns></returns> 
        private static XNamespace GetGpxNameSpace()
        {
            XNamespace gpx = XNamespace.Get("http://www.topografix.com/GPX/1/1");
            return gpx;
        }

        public const double EarthRadiusInMiles = 3956.0;
        public const double EarthRadiusInKilometers = 6367.0;
        public const double EarthRadiusInMeters = EarthRadiusInKilometers * 1000;

        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double ToDegree(double val) { return val * 180 / Math.PI; }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }



        public static double CalcSpeed(double distance, DateTime t1, DateTime t2)
        {
            
            var timeSpan = t1.Subtract(t2);
            var minutes = timeSpan.TotalMinutes;
            if (distance == 0 || minutes == 0)
            {
                return 0;
            }

            var mphResult = Math.Abs((distance / minutes) * 60);
            return mphResult;
        }

        public static double CalcDistance(GeoCoordinate p1, GeoCoordinate p2)
        {
            return CalcDistance(p1.Latitude, p1.Longitude, p2.Latitude, p2.Longitude, EarthRadiusInKilometers);
        }

        public static Double Bearing(GeoCoordinate p1, GeoCoordinate p2)
        {
            return Bearing(p1.Latitude, p1.Longitude, p2.Latitude, p2.Longitude);
        }

        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2, double radius)
        {

            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0)
                + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }

        public static Double Bearing(double lat1, double lng1, double lat2, double lng2)
        {

            {
                var dLat = lat2 - lat2;
                var dLon = lng2 - lng1;
                var dPhi = Math.Log(Math.Tan(lat2 / 2 + Math.PI / 4) / Math.Tan(lat1 / 2 + Math.PI / 4));
                var q = (Math.Abs(dLat) > 0) ? dLat / dPhi : Math.Cos(lat1);

                if (Math.Abs(dLon) > Math.PI)
                {
                    dLon = dLon > 0 ? -(2 * Math.PI - dLon) : (2 * Math.PI + dLon);
                }
                //var d = Math.Sqrt(dLat * dLat + q * q * dLon * dLon) * R; 
                var brng = ToDegree(Math.Atan2(dLon, dPhi));
                return brng;
            }
        }

    } 

}
