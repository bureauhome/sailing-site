﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite
{
    public class UserLockedException : Exception
    {
        public UserLockedException()
        {
        }

        public UserLockedException(string message)
            : base(message)
        {
        }

        public UserLockedException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }

    public class EntityAlreadyInDatabaseException : Exception
    {
        public EntityAlreadyInDatabaseException(string message)
            : base(message)
        {
               
        }

    }
    public class EntityNotFoundException :Exception
    {
        string Entity { get; set; }
        public EntityNotFoundException()
        { }
        public EntityNotFoundException(string message):base(message)
        {
            
        }

        public EntityNotFoundException(string message, string entity)
            : base(message)
        {
            Entity = entity;
        }
    }
}