﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SailingUnitTests
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;

    using Moq;

    using SailingSite;
    using SailingSite.Common;
    using SailingSite.Controllers;
    using SailingSite.Controllers.Base;
    using SailingSite.Repositories;
    using SailingSite.Services;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    [TestClass]
    public class CrewTests : TestBase
    {
         [TestMethod]
        public void LoginAttempt_Success()
         {
             var crewRepository=new Mock<ICrewRepository>();
             var crewService=new CrewService();
             crewRepository.Setup(i => i.UserExists(It.IsAny<string>())).Returns(true);
             crewRepository.Setup(i => i.IsUserLocked(It.IsAny<string>())).Returns(false);
             crewRepository.Setup(i => i.NullUserBadLoginCount(It.IsAny<string>()));
             crewRepository.Setup(i => i.GetSecurity(It.IsAny<string>(), It.IsAny<string>())).Returns(new CrewSecurity() {ID=2, Name = "fred",LastName = "fuchs"});
             crewService.CrewRepository = crewRepository.Object;
             Exception exception = null;
             CrewFoundation result = null;
             try
             {
                 result = crewService.Login("test", "test",this.Context);
             }
             catch (Exception ex)
             {
                 exception = ex;
             }
             Assert.IsNotNull(result);
             Assert.IsNull(exception);
             crewRepository.Verify((i => i.UserExists(It.IsAny<string>())), Times.Once);
             crewRepository.Verify((i => i.IsUserLocked(It.IsAny<string>())), Times.Once);
             crewRepository.Verify((i => i.NullUserBadLoginCount(It.IsAny<string>())), Times.Once);
             crewRepository.Verify((i => i.GetSecurity(It.IsAny<string>(),It.IsAny<string>())), Times.Once);
         }

         [TestMethod]
         public void LoginAttempt_Lockout()
         {
             var crewRepository = new Mock<ICrewRepository>();
             var crewService = new CrewService();
             crewRepository.Setup(i => i.UserExists(It.IsAny<string>())).Returns(true);
             crewRepository.Setup(i => i.IsUserLocked(It.IsAny<string>())).Returns(true);
             crewRepository.Setup(i => i.NullUserBadLoginCount(It.IsAny<string>()));
             crewRepository.Setup(i => i.GetSecurity(It.IsAny<string>(), It.IsAny<string>())).Returns(new CrewSecurity() { ID = 2, Name = "fred", LastName = "fuchs" });
             crewService.CrewRepository = crewRepository.Object;
             Exception exception = null;
             CrewFoundation result = null;
             try
             {
                 result = crewService.Login("test", "test", this.Context);
             }
             catch (UserLockedException ex)
             {
                 exception = ex;
             }

             Assert.IsNotNull(exception);
             crewRepository.Verify((i => i.NullUserBadLoginCount(It.IsAny<string>())), Times.Never);
         }

         [TestMethod]
         public void LoginAttempt_BadPassword()
         {
             var crewRepository = new Mock<ICrewRepository>();
             var crewService = new CrewService();
             crewRepository.Setup(i => i.UserExists(It.IsAny<string>())).Returns(true);
             crewRepository.Setup(i => i.IsUserLocked(It.IsAny<string>())).Returns(false);
             crewRepository.Setup(i => i.NullUserBadLoginCount(It.IsAny<string>()));
             crewRepository.Setup(i => i.IncrementUserBadLoginCount(It.IsAny<string>()));
             crewRepository.Setup(i => i.GetSecurity(It.IsAny<string>(), It.IsAny<string>()));
             crewService.CrewRepository = crewRepository.Object;
             Exception exception = null;
             CrewFoundation result = null;
             try
             {
                 result = crewService.Login("test", "test", this.Context);
             }
             catch (EntityNotFoundException ex)
             {
                 exception = ex;
             }

             crewRepository.Verify((i => i.NullUserBadLoginCount(It.IsAny<string>())), Times.Never);
             crewRepository.Verify((i => i.IncrementUserBadLoginCount(It.IsAny<string>())), Times.Once);
             Assert.IsNotNull(exception);
         }

        [TestMethod]
        public void LoginTest()
        {
            SailRepository.Sterilize();
            var crew = UnitTestHelpers.GenerateCrew();
            var password = UnitTestHelpers.GetRandomString(7)+"eF9"+random.Next(1,99).ToString();
            crew.Password = password;
            crew.ID = CrewService.Create(crew, this.Context);
            // tests login
            var sid=CrewRepository.GetSecurity(crew.UserName, password);
            Assert.IsNotNull(sid);
            Assert.IsNotNull(sid.Token);
            // tests token authentication
            var loginToken = CrewRepository.VerifyToken(sid.Token);
            Assert.IsNotNull(loginToken);
            Assert.AreEqual(loginToken.ID , sid.ID);
            Assert.AreEqual(loginToken.Token, sid.Token);

            // update, login
            password = UnitTestHelpers.GetRandomString(7) + "eF9" + random.Next(1, 99).ToString();
            crew.Password = password;
            CrewService.Update(crew, this.Context);
            sid = CrewRepository.GetSecurity(crew.UserName, password);
            Assert.IsNotNull(sid);
            Assert.IsNotNull(sid.Token);

            SailRepository.Sterilize();
        }

         [TestMethod]
        public void LoginTest_LogsCreated()
        {
            var logincontroller = new LoginController();
            var context = new Mock<ControllerContext>();
             var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
             var authenticationFacade = new Mock<IAuthenticationFacade>();
             response.SetupGet(m => m.Cookies).Returns(new HttpCookieCollection());
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
             context.Setup(m => m.HttpContext.Response).Returns(response.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
             context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());
             authenticationFacade.Setup(m => m.SetAuthCookie(It.IsAny<string>()));
            logincontroller.ControllerContext = context.Object;
            logincontroller.AuthenticationFacade = authenticationFacade.Object;

             var crew = new CrewSecurity() { FirstName = "fred", LastName = "fuchs", SID = Crew.SIDEnum.None, ID=2, Token = "testToken"};
             var crewService = new Mock<ICrewService>();
             crewService.Setup(i => i.Login(It.IsAny<string>(), It.IsAny<string>(), this.Context)).Returns(crew);
             var loggingService = new Mock<ILoggingService>();
             loggingService.Setup(
                 i =>
                 i.GenerateLog(
                     It.IsAny<string>(),
                     Log.LogTypes.AuditLogin, 
                     It.IsAny<AuditContext>(),
                     It.IsAny<object>()));
             loggingService.Setup(
    i =>
    i.GenerateLog(
        It.IsAny<string>(),
        It.IsAny<Log.LogTypes>(),
        It.IsAny<AuditContext>(),
        It.IsAny<object>()));
             logincontroller.CrewService = crewService.Object;
             logincontroller.LoggingService = loggingService.Object;
             logincontroller.Login("test", "test");
             loggingService.Verify(i =>
                 i.GenerateLog(
                     It.IsAny<string>(),
                     Log.LogTypes.AuditLogin,
                     It.IsAny<AuditContext>(),
                     It.IsAny<object>()), Times.Once);
        }

        [TestMethod]
        public void TestCrewCreationLog()
        {
            SailRepository.Sterilize();
            var crewRepository = new Mock<ICrewRepository>();
            var loggingService = new Mock<ILoggingService>();
            AuditContext context = StaticTokens.TestValues.TestContext;
            crewRepository.Setup(i => i.Create(It.IsAny<Crew>())).Returns(1);
;           loggingService.Setup(i => i.GenerateLog(It.IsAny<string>(), Log.LogTypes.AuditCreate, context, It.IsAny<object>()));
            this.CrewService.LoggingService = loggingService.Object;
            this.CrewService.CrewRepository = crewRepository.Object;
            var crew = UnitTestHelpers.GenerateCrew();
            crew.ID = this.CrewService.Create(crew, context);
            loggingService.Verify((i => i.GenerateLog(It.IsAny<string>(), Log.LogTypes.AuditCreate, context, It.IsAny<object>())),Times.Once);

        }

        [TestMethod]
        public void CRUDTest()
        {
            SailRepository.Sterilize();
            var crew = UnitTestHelpers.GenerateCrew();
            crew.ID = CrewService.Create(crew, this.Context);
            var dbCrew = CrewService.GetSingle(crew.ID);
            Assert.IsNotNull(dbCrew);
            Assert.IsTrue(MatchCrew(crew, dbCrew));

            ModCrew(crew);
            CrewService.Update(crew, this.Context);
            dbCrew = CrewService.GetSingle(crew.ID);
            Assert.IsNotNull(dbCrew);
            Assert.IsTrue(MatchCrew(crew, dbCrew));

            var sail=this.generateSail(null, crew);
            sail.ID=SailService.CreateSail(sail);
            crew.Sails = new List<Sail>() { sail };

            dbCrew = CrewService.GetSingle(crew.ID);

            var dbSail = SailService.GetSail(sail.ID);
            Assert.IsNotNull(dbSail);
            Assert.IsNotNull(dbSail.Crew);
            Assert.IsTrue(dbSail.Crew.Any(i=>i.ID==crew.ID));

            var comment = this.GenerateSailLog(sail, null,crew);

            SailService.AddSailComment(comment, sail.ID,this.Context);
            dbSail = SailService.GetSail(sail.ID);
            var commentID = dbSail.Logbook.FirstOrDefault().ID;
            var dbComment = CommentService.Get(commentID);
            Assert.IsNotNull(dbComment);
            crew.Comments = new List<CommentBase>() { dbComment };
            dbCrew = CrewService.GetSingle(crew.ID);
            Assert.IsTrue(MatchCrew(crew, dbCrew));
            var dbComments = CommentService.Get(commentID);
            Assert.IsNotNull(dbComments);
            Assert.IsTrue(dbComments.Crew != null || dbComments.Crew.Any(i => i.ID == crew.ID));

            CrewService.Delete(crew.ID,7,this.Context);
            dbSail = SailService.GetSail(sail.ID);
             dbComments = CommentService.Get(commentID);
            Assert.IsNotNull(dbSail);
            Assert.IsTrue(dbSail.Crew==null||!dbSail.Crew.Any(i=>i.ID==crew.ID));
            Assert.IsNotNull(dbComments);
            Assert.IsTrue(dbComments.Crew==null||!dbComments.Crew.Any(i=>i.ID==crew.ID));
            SailRepository.Sterilize();
        }

        // TODO: refactor comments to return int id's
    }
}
