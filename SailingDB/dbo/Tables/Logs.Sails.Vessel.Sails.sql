﻿CREATE TABLE [dbo].[Logs.Sails.Vessels.Sails](
	[SailID] [int] NOT NULL,
	[SheetID] [int] NOT NULL,
	[Percentage] [float] NOT NULL,
 CONSTRAINT [PK_Logs.Sails.Vessels.Sails_1] PRIMARY KEY CLUSTERED 
(
	[SailID] ASC,
	[SheetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Logs.Sails.Vessels.Sails]  WITH CHECK ADD  CONSTRAINT [FK_Logs.Sails.Vessels.Sails_Entities.Vessels.Sails] FOREIGN KEY([SheetID])
REFERENCES [dbo].[Entities.Vessels.Sails] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Logs.Sails.Vessels.Sails] CHECK CONSTRAINT [FK_Logs.Sails.Vessels.Sails_Entities.Vessels.Sails]
GO

ALTER TABLE [dbo].[Logs.Sails.Vessels.Sails]  WITH CHECK ADD  CONSTRAINT [FK_Logs.Sails.Vessels.Sails_Logs.Sails] FOREIGN KEY([SailID])
REFERENCES [dbo].[Logs.Sails] ([SailID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Logs.Sails.Vessels.Sails] CHECK CONSTRAINT [FK_Logs.Sails.Vessels.Sails_Logs.Sails]
GO


