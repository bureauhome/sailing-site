﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBearable.cs" company="">
//   
// </copyright>
// <summary>
//   The Bearable interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using SailingSiteModels.New.Bearing;

    /// <summary>
    /// The Bearable interface.
    /// </summary>
    public interface IBearable
    {
        /// <summary>
        /// Gets or sets the bearing.
        /// </summary>
        Bearing Bearing { get; set; }
    }
}
