﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using SailingSite.Common;
    using SailingSite.Models.FeedModels;

    using SailingSiteModels.Common;
    using SailingSiteModels.New.Debug;

    public class HomeController : BaseController
    {
        public ActionResult Index()
        {         
            
            this.LoggingService.GenerateLog(
                    "Index requested.  Getting session variables.",
                    Log.LogTypes.Audit,
                    this.Context);
                this.GetSessionVariables();
            if (this.CurrentUser != null)
            {

                this.LoggingService.GenerateLog("Index requested.  Getting feed v2.", Log.LogTypes.Audit, this.Context);
                this.LoadFeed();
            }
            else
            {
                this.LoggingService.GenerateLog(
                    "Index requested, but no user logged in.",
                    Log.LogTypes.Audit,
                    this.Context);
            }
            return this.View();

        }

        public ActionResult ExpandFeed(int count)
        {
            IEnumerable<INewsFeedable> result = null;
            var currentFeed = Session[StaticTokens.SessionTokens.NewsFeed] as NewsFeed;
            if (currentFeed != null&&currentFeed.Feed!=null&&currentFeed.Feed.Any())
            {
                if (count < currentFeed.Feed.Count())
                {
                    result = currentFeed.Feed.OrderBy(i=>i.Order).Skip(count).Take(10);
                }
            }
            return this.PartialView("NewsFeedListing",result);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}