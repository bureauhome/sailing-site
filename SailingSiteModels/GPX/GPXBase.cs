﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.GPX
{
    using System.Device.Location;
    using System.Security.Cryptography.X509Certificates;

    public enum GPSType {Track,Waypoint,Route}
    public interface IGps
    {
        GeoCoordinate Coordinate { get; set; }
    }

    public interface IGpsTime : IGps
    {
        DateTime Date { get; set; }
    }

    public abstract class GPXBase : IGps
    {

        public GeoCoordinate Coordinate { get; set; }
    }

    public class GpxTrackPoint : GPXBase, IGpsTime
    {
        public DateTime Date { get; set; }
        public Double TotalMiles { get; set; }
        public Double LegMiles { get; set; }
        public Double SpeedMPH { get; set; }
    }

    public class ErroredGpxTrackPoint : GpxTrackPoint
    {
        public string Error;

        public ErroredGpxTrackPoint(GpxTrackPoint entity, string error)
        {
            Date = entity.Date;
            TotalMiles = entity.TotalMiles;
            LegMiles = entity.LegMiles;
            Error = error;
        }

    }

    public class GpxTrack
    {
        public string Name { get; set; }

        public List<GpxTrackPoint> Track { get; set; }
    }

    public class GpxWayPoint : GPXBase, IGpsTime
    {
        public string Name;
        public DateTime Date { get; set; }
    }

    public class GpxRoute
    {
        public string Name;

        public DateTime DateCreated;
        public Dictionary<int, GpxWayPoint> Route { get; set; }

        public GpxRoute()
        {
            this.Route=new Dictionary<int, GpxWayPoint>();
        }

    }


}
