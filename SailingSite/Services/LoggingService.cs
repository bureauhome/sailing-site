﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoggingService.cs" company="">
//   
// </copyright>
// <summary>
//   The logging service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Services
{
    using System;

    using SailingSite.Common;

    using SailingSiteModels.New.Debug;

    /// <summary>
    /// The logging service.
    /// </summary>
    public class LoggingService : ServiceBase, ILoggingService
    {
        /// <summary>
        /// The generate log.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public void GenerateLog(Exception exception, AuditContext context)
        {
            var log = context.ToLog();
            log.Type = Log.LogTypes.SystemError;
            if (exception != null)
            {
                log.ExceptionID = exception.GetType().Name;
                log.ExceptionMessage = exception.Message;
            }
            else
            {
                log.ExceptionID = "Null Exception";
                log.ExceptionMessage = "Exception was null";
            }

             this.GenerateLog(log);
        }

        /// <summary>
        /// The generate log.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public void GenerateLog(string message, Log.LogTypes type, AuditContext context)
        {
            this.GenerateLog(message, type, context, null);

        }

        /// <summary>
        /// The generate log.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void GenerateLog(string message, Log.LogTypes type, AuditContext context, object entity)
        {
            var log = context.ToLog();
            log.Type = type;
            log.Message = message;
            if (entity != null)
            {
                log.AttachEntity(entity);
            }

            this.GenerateLog(log);

        }

        /// <summary>
        /// The generate log.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GenerateLog(Log log)
        {
            return this.LoggingRepository.CreateLog(log);
        }

    }
}