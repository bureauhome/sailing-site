﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using System.Threading.Tasks;

    public interface ICrewRepository : ITaggableRepository, IPhotographableRepository, ICommentableRepository
    {
        bool CheckUserExists(string username, string email);
        IEnumerable<New.CrewFoundation> CrewSearch(GeneralSearchRequest query);
        IEnumerable<Entities_Crew> GetAllCrew();
        IEnumerable<New.CrewFoundation> GetCrewFoundationForVessel(IEnumerable<int> vesselIds);
        New.CrewSecurity GetSecurity(string username, string password);
        IEnumerable<SailingSiteModels.New.Crew> Get(GeneralSearchRequest query);
        decimal GetCrewHourCount(int id);
        int GetCrewSailCount(int id);
        decimal GetCrewMileCount(int id);

        Task<bool> MatchAnswer(int id, string answer);

        Task UpdatePassword(int id, string salt, string password);

        Task<bool> VerifyToken(int userId, string token);

        bool IsUserLocked(int id);
        void IncrementBadLoginCount(int userId);
        Task<string> CreateToken(int userId);
        Task DeleteQuestion(int id);
        Task<IEnumerable<New.SecurityQuestion>> GetQuestions(int crewId); 
        void SetUserSession(int userId, string guid);
        bool MatchPW(string username, string password);
        IEnumerable<int> GetFleetIDsForCrew(int crewId);
        IEnumerable<int> GetSailIDSForCrew(IEnumerable<int> crewIDs);
        New.Crew GetDetailedCrew(int id);
        int Create(SailingSiteModels.New.Crew entity);
        bool Update(SailingSiteModels.New.Crew entity);
        IEnumerable<New.PhotoBase> GetCrewPhotoLog(int id, int limit);
        bool Delete(int id, int crewId);

        Task<int?> GetUserId(string username);
        New.Crew.SIDEnum GetUserSID(int id);
        int? GetSystemUserID();
        IEnumerable<int> GetCommentsMade(int[] entityID);

        bool IsUserLocked(string username);

        void IncrementUserBadLoginCount(string username);

        void NullUserBadLoginCount(string username);

        bool UserExists(string username);

        New.CrewSecurity VerifyToken(string token);

        List<string> SelectStartsWithIndex();

        Task<int> InsertQuestion(New.SecurityQuestion entity);

        Task UpdateQuestion(New.SecurityQuestion entity);

        Task<bool> IsQuestionDuplicate(int userId, string question);
    }
}