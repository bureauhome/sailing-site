﻿CREATE TABLE [dbo].[Logs.Comments.Alerts] (
    [AlertID]   INT      IDENTITY (1, 1) NOT NULL,
    [CommentID] INT      NOT NULL,
    [RelaxDate] DATETIME NOT NULL,
    [ReRun]     BIT      NULL,
    [StartDate] DATETIME NULL,
    [ReRunType] TINYINT  NULL
);

