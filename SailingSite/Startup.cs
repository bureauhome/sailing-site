﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SailingSite.Startup))]
namespace SailingSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
