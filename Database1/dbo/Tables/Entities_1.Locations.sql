﻿CREATE TABLE [dbo].[Entities.Locations] (
    [LocationID] INT               IDENTITY (1, 1) NOT NULL,
    [Name]       NCHAR (10)        NOT NULL,
    [GPSCoords]  [sys].[geography] NULL,
    CONSTRAINT [pk_LocationID] PRIMARY KEY CLUSTERED,
    CONSTRAINT [pk_LocationID] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);

