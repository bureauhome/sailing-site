﻿CREATE TABLE [dbo].[Photos.Maneuvers](
	[PhotoID] [int] NOT NULL,
	[ManeuverID] [int] NOT NULL,
 CONSTRAINT [PK_Photos.Maneuvers] PRIMARY KEY CLUSTERED 
(
	[PhotoID] ASC,
	[ManeuverID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Photos.Maneuvers]  WITH CHECK ADD  CONSTRAINT [FK_Photos.Maneuvers_Entities.Maneuvers] FOREIGN KEY([ManeuverID])
REFERENCES [dbo].[Entities.Maneuvers] ([ManeuverID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Photos.Maneuvers] CHECK CONSTRAINT [FK_Photos.Maneuvers_Entities.Maneuvers]
GO

ALTER TABLE [dbo].[Photos.Maneuvers]  WITH CHECK ADD  CONSTRAINT [FK_Photos.Maneuvers_Entities.Photos] FOREIGN KEY([PhotoID])
REFERENCES [dbo].[Entities.Photos] ([PhotoID])
ON DELETE CASCADE
GO