﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICrewable.cs" company="">
//   
// </copyright>
// <summary>
//   The Crewable interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The Crewable interface.
    /// </summary>
    public interface ICrewable : IEntityModel
    {
        /// <summary>
        /// Gets or sets the crew.
        /// </summary>
        IEnumerable<CrewFoundation> Crew { get; set; }
    }
}
