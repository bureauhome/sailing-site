﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite.Repositories 
{
    using System.Threading.Tasks;

    using SailingSite.Repositories.Interfaces;

    public interface IVesselRepository : ITaggableRepository, IPhotographableRepository, ICommonRepository<Entities_Vessel,SailingSiteModels.New.Vessel,SailingSiteModels.New.VesselBase>
    {
        int? GetVesselIDForSail(int sailId);
        bool isUserVesselCrewOrSecurity(int userId, int vesselId);

        New.Vessel JiffySaveVessel();
        void RemoveVesselFromFleet(int vesselID, int fleetId);

        IEnumerable<int> GetFleetIDSForVessel(int vesselId);
        IEnumerable<int> GetSailIDSforVessel(IEnumerable<int> ids);
        IEnumerable<New.VesselBase> GetVesselBases(int? fleetId = null);

        Task<IEnumerable<New.VesselEntry>> GetVesselEntriesForFleet(int fleetId);
        IEnumerable<int> GetVesselIDsForFleet(IEnumerable<int> fleetId);
        IEnumerable<New.VesselBase> Search(GenericQuery query); 
        int Create(New.Vessel entity);
        IEnumerable<New.Animal> GetAnimalsForVessel(int id);
        New.Vessel Get(int id);
        int VesselSailCount(int id);
        decimal VesselMileCount(int id);
        decimal VesselHourCount(int id);
        IEnumerable<New.SailBase> GetTopTenSailForVessel(int id);

        bool IsVesselInFleet(int vesselId, int fleetId);
        List<string> GetFirstInitialIndex(); 
        IEnumerable<New.CrewBase> GetCrewForVessel(int id);
        IEnumerable<New.Tags> GetTagsForVessel(int id);
        decimal GetMileTotalForFleet(int id);
        int GetCrewCount(int id);
        int SailTotalForFleet(int id);
        int GetCrewTotalForFleet(int id);
        int GetVesselTotalForFleet(int id);
        New.VesselFoundation GetVesselFoundation(int id);
        int SailTotal(int id);
        IEnumerable<New.CrewBase> GetCrewForSail(int id);
        New.SheetSail GetSheetSail(int id);
        int AddSheetSailToExistingVessel(New.SheetSail entity, int parentID);

        void UpdateSheetSail(New.SheetSail entity);

        void DeleteSheetSail(int id);

    }
}