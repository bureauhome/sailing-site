﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MetadataTests.cs" company="">
//   
// </copyright>
// <summary>
//   The metadata tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingUnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using SailingSite.Models;
    using SailingSite.Services;

    /// <summary>
    /// The metadata tests.
    /// </summary>
    [TestClass]
    public class MetadataTests : TestBase
    {
        /// <summary>
        /// The key.
        /// </summary>
        private const string key = "test";

        /// <summary>
        /// The metadata.
        /// </summary>
        MetaData metadata = new MetaData() { Key = key, Value = "Test1234" };

        /// <summary>
        /// The changed meta data.
        /// </summary>
        MetaData changedMetaData = new MetaData() { Key = key, Value = "Test12345" };

        [TestCleanup]
        [TestInitialize]
        public void DeleteKey()
        {
            var service = new MetaDataService();
            service.Delete(key);
        }

        /// <summary>
        /// The add new metadata.
        /// </summary>
        [TestMethod]
        [TestCategory("IntegrationTest")]
        public void AddNewMetadata()
        {
            var service=new MetaDataService();
            service.Process(metadata);
            var dbMetadata=service.Get(key);
            Assert.IsNotNull(dbMetadata);
            Assert.AreEqual(dbMetadata, metadata.Value);
        }

        /// <summary>
        /// The update meta data.
        /// </summary>
        [TestMethod]
        [TestCategory("IntegrationTest")]
        public void UpdateMetaData()
        {
            var service = new MetaDataService(); 
            service.Process(metadata);
            service.Process(changedMetaData);
            var dbMetadata = service.Get(metadata.Key);
            Assert.IsNotNull(dbMetadata);
            Assert.AreEqual(dbMetadata, changedMetaData.Value);
        }

        [TestMethod]
        [TestCategory("IntegrationTest")]
        public void DeleteMetaData()
        {
            var service = new MetaDataService();
            service.Process(this.metadata);
            var dbMetadata = service.Get(key);
            Assert.IsNotNull(dbMetadata);
            service.Delete(this.metadata.Key);
            dbMetadata = service.Get(this.metadata.Key);
            Assert.IsNull(dbMetadata);
        }
    }
}
