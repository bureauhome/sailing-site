﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.Collections;
    using System.Net;

    using SailingSite.Common;
    using SailingSite.Controllers.Base;
    using SailingSite.Services;
    using SailingSite.ViewModels;

    using SailingSiteModels.Common;
    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    public abstract class BaseController : Controller
    {

        private MemoryCacher memoryCache;
        private IEnumerable<Tags> availableTags;

       
        public BaseController() : base()
        {
        }

        /// <summary>
        /// The logging service.
        /// </summary>
        private ILoggingService loggingService;

        private IAdminService adminService;

        private IAuthenticationFacade authenticationFacade;

        private IMetadataService metadataService;

        private INewsFeedService newsFeedService;

        public INewsFeedService NewsFeedService
        {
            get
            {
                if (this.newsFeedService == null)
                {
                    this.newsFeedService=new NewsFeedService();
                }
                return this.newsFeedService;
            }
            set
            {
                this.newsFeedService = value;
            }
        }

        public IMetadataService MetadataService
        {
            get
            {
                if (this.metadataService == null)
                {
                    this.metadataService=new MetaDataService();
                }
                return this.metadataService;
            }
            set
            {
                this.metadataService = value;
            }
        }

        public IAdminService AdminService
        {
            get
            {
                if (this.adminService == null)
                {
                    this.adminService=new AdminService();
                }
                return this.adminService;
            }
            set
            {
                this.adminService = value;
            }
        }

        public IAuthenticationFacade AuthenticationFacade
        {
            get
            {
                if (this.authenticationFacade == null)
                {
                   this.authenticationFacade=new AuthenticationFacade();
                }
                return this.authenticationFacade;
            }

            set
            {
                this.authenticationFacade = value;
            }
        }

        public ActionResult RedirectNullUser()
        {

                var redirectModel = new RedirectModel() { ActionName = "New", ControllerName = "Sail" };
                return this.View("_AdminRedirect", redirectModel);
        }

        /// <summary>
        /// Handles any un-bubbled exceptions.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        protected override void OnException(ExceptionContext context)
        {
            this.LoggingService.GenerateLog("An exception has occured", Log.LogTypes.SystemError, this.Context, context.Exception);
            context.ExceptionHandled = true;
            context.Result = new ViewResult() { ViewName = "Error" };
        }

        public ILoggingService LoggingService
        {
            get
            {
                if (this.loggingService == null)
                {
                    this.loggingService=new LoggingService();
                }
                return this.loggingService;
            }

            set
            {
                this.loggingService = value;
            }
        }


        private AuditContext context;
        /// <summary>
        /// Gets or sets our audit context, providing the services with everything they need to log and audit.
        /// </summary>
        public AuditContext Context {
            get
            {
                if (this.context == null)
                {
                    this.context = new AuditContext();
                    this.context.ControllerName = this.GetType().Name;
                    if (this.Request != null && this.Request.Url!=null&&this.Request.Url.PathAndQuery!=null&&this.Request.HttpMethod!=null)
                    {
                        this.context.EntryPoint = this.Request.Url.PathAndQuery;
                        this.context.Verb = this.Request.HttpMethod;
                    }

                    if (this.CurrentUser != null)
                    {
                        this.context.ActiveCrew = this.CurrentUser;
                    }
                }
                return this.context;
            }
            set
            {
                this.context = value;
            }
        }

        //// Todo: This is not used anywhere.  REMOVE.
        //public IEnumerable<Tags> AvailableTags
        //{
        //    get
        //    {
        //        if (this.availableTags == null || Session["TagUpdate"] == null || (DateTime)Session["TagUpdate"] < DateTime.Now.AddMinutes(-1))
        //        {
        //            if (Session["Tags"] == null || Session["TagUpdate"] == null || (DateTime)Session["TagUpdate"] < DateTime.Now.AddMinutes(-1))
        //            {
        //                var strings = TagService.GetAllTagsByString();
        //                Session["Tags"] = strings;
        //                Session["TagUpdate"] = DateTime.Now;
        //            }
        //        }
        //        return this.availableTags;
        //    }

        //    set
        //    {
        //        this.availableTags = value;
        //    }
        //}

        /// <summary>
        /// Gets or sets the memory cache.
        /// </summary>
        public MemoryCacher Cache
        {
            get
            {
                if (this.memoryCache == null)
                {
                    this.memoryCache = new MemoryCacher();
                }

                return this.memoryCache;
            }
            set
            {
                this.memoryCache = value;
            }
        }

        /// <summary>
        /// Gets an object from cache or database as required.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="databaseFunction">
        /// The call to get the items from the database.
        /// </param>
        /// <param name="offsetMinutes">
        /// The offset Minutes.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public T RetrieveDatabaseOrCachedObject<T>(string key, Func<T> databaseFunction, int offsetMinutes=15) where T: IEnumerable
        {
            // this is our caching model.  It will set and retrieve all the enumerables for the user views.  We are sending it our cache key, the database function to populate it from, and an offset in minutes.
            // First we attempt to get the value from the cache
            var result = this.Cache.GetValue(key);
            // We also get the token (last update time) and cache needed 
            var tokenResult = this.Cache.GetValue(StaticTokens.CachingTokens.GetTokenKey(key)) as DateTime?;
            var updateNeeded = this.Cache.GetValue(StaticTokens.CachingTokens.GetUpdateNeededKey(key)) as bool?;
            // If we have no results in cache, or the token has expired, or an update is required for the key, proceed to get the values from the database.  Otherwise return the results from cache.
            if ((result == null || tokenResult == null
                 || (DateTime)tokenResult < DateTime.Now.AddMinutes(Math.Abs(offsetMinutes) * -1)) || updateNeeded == true)
            {
                result = databaseFunction();
                if (result != null)
                {
                    this.Cache.Update(key, result, StaticTokens.CachingTokens.StandardCacheExpiration());
                    this.Cache.Update(StaticTokens.CachingTokens.GetTokenKey(key), DateTime.Now, StaticTokens.CachingTokens.StandardCacheExpiration());
                    this.Cache.Update(StaticTokens.CachingTokens.GetUpdateNeededKey(key), false, StaticTokens.CachingTokens.StandardCacheExpiration());
                }
            }
            return (T)result;

        }

        private static Type GetElementTypeOfEnumerable(object o)
        {
            var enumerable = o as IEnumerable;
            // if it's not an enumerable why do you call this method all ?
            if (enumerable == null)
                return null;

            Type[] interfaces = enumerable.GetType().GetInterfaces();

            return (from i in interfaces
                    where i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>)
                    select i.GetGenericArguments()[0]).FirstOrDefault();
        }

        /// <summary>
        /// The set update.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        public void FlagCacheUpdate(string key)
        {
            this.Cache.Update(StaticTokens.CachingTokens.GetUpdateNeededKey(key), true, StaticTokens.CachingTokens.StandardCacheExpiration());
        }

        private Services.IPhotoService photoService = null;

        internal Services.IPhotoService PhotoService
        {
            get
            {
                if (photoService == null)
                {
                    photoService=new PhotoService();
                }
                return photoService;
            }
            set
            {
                photoService = value;
            }

        }

        private Services.ICommentService commentService;

        internal Services.ICommentService CommentService
        {
            get
            {
                if (commentService == null)
                {
                    commentService = new Services.CommentService();
                }
                return commentService;
            }
            set
            {
                commentService = value;
            }
        }

        public SailingSiteModels.New.CrewSecurity CurrentUser
        {
            get
            {
                SailingSiteModels.New.CrewSecurity user = null;

                if (this.Session[StaticTokens.Security.UserToken] == null
                    || (this.Session[StaticTokens.Security.UserToken] != null && (DateTime)this.Session[StaticTokens.Security.UserToken] < DateTime.Today.AddHours(-2)))
                {
                    return null;
                }

                if (this.Session!=null&&Session[StaticTokens.CachingTokens.UserSessionResources] != null)
                {
                    user = this.Session[StaticTokens.CachingTokens.UserSessionResources] as SailingSiteModels.New.CrewSecurity;
                    this.Session[StaticTokens.Security.UserToken] = DateTime.Now;
                }
                return user;
            }

            set
            {
                this.Session[StaticTokens.CachingTokens.UserSessionResources] = value;
            }

        }

        public ActionResult GetSheetSailLocker(int id)
        {
           var data= this.Sailservice.GetSheetSailLocker(id);
            if (data != null)
            {
                ViewData[StaticTokens.CachingTokens.SheetSailLocker] = data;
            }

            return this.PartialView("_SheetSaillCheckList", new List<SheetSailLog>());
        }

        private ISailService sailservice;

        public ISailService Sailservice
        {
            get
            {
                if (sailservice == null)
                {
                    sailservice = new Services.SailingService();
                }
                return sailservice;
            }
            set
            {
                sailservice = value;
            }
        }

        private LoginController loginCotroller = null;

        internal LoginController LoginController
        {
            get
            {
                if (this.loginCotroller == null)
                {
                    this.loginCotroller=new LoginController();
                }
                return this.loginCotroller;
            }
            set
            {
                this.loginCotroller = value;
            }

        }

        private IAnimalService animalService = null;

        internal IAnimalService AnimalService
        {
            get
            {
                if (animalService == null)
                {
                    animalService=new Services.AnimalService();
                }
                return animalService;
            }
            set
            {
                animalService = value;
            }

        }


        Services.ITagService tagService = null;
        internal Services.ITagService TagService
        {
            get
            {
                if (tagService == null)
                {
                    tagService = new Services.TagService();
                }
                return tagService;
            }
            set
            {
                tagService = value;
            }
        }


        private Services.ICrewService crewService;
        public Services.ICrewService CrewService
        {
            get
            {
                if (this.crewService == null)
                {
                    this.crewService = new Services.CrewService();
                }
                return this.crewService;
            }
            set
            {
                this.crewService = value;
            }
        }

        private Services.ILocationService locationService = null;

        internal Services.ILocationService LocationService
        {
            get
            {
                if (locationService == null)
                {
                    locationService=new LocationService();
                
                }
                return locationService;
            }
            set
            {
                locationService = value;
            }

        }

        public ActionResult DeriveCompassBearing(string input)
        {
            decimal? result = null;
            result = this.Sailservice.DeriveCompassRose(input);
            if (result == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.UnsupportedMediaType,"Please input a direction (North,n,ne etc) or compass bearing (0-360.)");
            }
            return this.Content(result.ToString());

        }

        public ActionResult CalculateFarenheit(decimal input)
        {
            string result = string.Empty;
            if (input > 94)
            {
                result = "factor7";
            }
            if (input < 95)
            {
                result = "factor6";
            }
            if (input < 88)
            {
                result = "factor2";
            }

            if (input < 68)
            {
                result = "coolFactor";
            }
            if (input < 50)
            {
                result = "coldFactor";
            }



            return this.Content(result);
        }

        public ActionResult GetTenRankClassName(int input)
        {
            if (input > 10 || input < 0)
            {
                return this.Content("Please enter a number from 1 to 10.");

            }
            var result = string.Empty;
            if (input > 7)
            {
                result = "factor7";
            } 
            if (input < 7)
            {
                result = "factor6";
            }
            if (input < 5)
            {
                result = "factor4";
            }
            if (input < 3)
            {
                result = "factor1";
            }
            return this.Content(result);
        }


        public ActionResult GetBearingName(int input)
        {
            var result= this.Sailservice.DeriveCompassRose(input);
            return this.Content(result);
        }

        public void LoadStandardVesselFromCookie()
        {
            if (Session[StaticTokens.CachingTokens.StandardVessel] == null
    || (Session[StaticTokens.CachingTokens.StandardVessel] == null
        || ((DateTime)Session[StaticTokens.CachingTokens.StandardVessel])
        < DateTime.Now.AddMinutes(-120)))
            {
                var svCookie = this.GetIDFromCookie(StaticTokens.CachingTokens.StandardVessel);
                if (svCookie != null)
                {
                    var sv = this.Sailservice.GetVesselFoundation((int)svCookie);
                    if (sv != null)
                    {
                        Session[StaticTokens.CachingTokens.StandardVessel] = sv;
                        Session[StaticTokens.CachingTokens.StandardVessel] = DateTime.Now;
                    }
                }
            }

        }

        public void LoadUserSpecificSessionVariables()
        {
            this.Session["Vessel"] = this.LoadSessionVessels();
        }

        protected IEnumerable<VesselFoundation> LoadSessionVessels()
        {
            // This is a little out of pattern, as we need to first determine if the system is a sysadmin/power user (who can access ALL vessels.)  If they are not we only pass the vessels that the user is allowed to make entries for.
            if (this.CurrentUser != null)
            {
                if (this.CurrentUser.SID == Crew.SIDEnum.Admin || this.CurrentUser.SID == Crew.SIDEnum.PowerUser)
                {
                    return this.RetrieveDatabaseOrCachedObject(
                 StaticTokens.CachingTokens.VesselEnum,
                 this.Sailservice.GetVesselNames);
                }

                return this.CurrentUser.VesselsCrewed;
            }
            // A user who is not logged in need not see session variables!

            return null;
        }

        public void LoadFeed()
        {
            if (this.CurrentUser != null)
            {
                this.LoggingService.GenerateLog(string.Format("Getting feed for {0}.",this.CurrentUser.Name), Log.LogTypes.Audit, this.Context);
                this.Session[StaticTokens.SessionTokens.NewsFeed] = this.NewsFeedService.GetNewsFeed(this.CurrentUser, this.Context);
            }
        }

        public void GetSessionVariables()
        {
            // Here we are retrieving all our session variables, either from cache or from the database.
            this.LoggingService.GenerateLog("Getting tags.", Log.LogTypes.Audit, this.Context);
            this.Session["Tags"] = this.RetrieveDatabaseOrCachedObject(
                StaticTokens.CachingTokens.TagEnum,
                this.TagService.GetAllTagsByString);
            if (this.Session["Tags"] != null && this.Session["Tags"] is IEnumerable<string>)
            {
                this.LoggingService.GenerateLog(
                    "Getting tags.",
                    Log.LogTypes.Audit,
                    this.Context,
                    string.Join(",", this.Session["Tags"] as IEnumerable<string>));
            }
            else
            {
                this.LoggingService.GenerateLog("No tags.", Log.LogTypes.Audit, this.Context);
            }
            this.LoggingService.GenerateLog("Getting fleets.", Log.LogTypes.Audit, this.Context);
            this.Session["Fleets"]=this.RetrieveDatabaseOrCachedObject(
                StaticTokens.CachingTokens.FleetEnum,
                this.Sailservice.GetAllFleets);
            this.LoggingService.GenerateLog("Getting crew.", Log.LogTypes.Audit, this.Context);
            this.Session["Crew"] = this.RetrieveDatabaseOrCachedObject(
                StaticTokens.CachingTokens.CrewEnum,
                this.Sailservice.GetCrewNames);
            this.LoggingService.GenerateLog("Getting weather.", Log.LogTypes.Audit, this.Context);
            this.Session["Weather"] = this.RetrieveDatabaseOrCachedObject(
                StaticTokens.CachingTokens.WeatherEnum,
                this.Sailservice.GetWeatherBase, 600);

            this.LoggingService.GenerateLog("Getting maneuvers.", Log.LogTypes.Audit, this.Context);
            this.Session["Maneuver"] = this.RetrieveDatabaseOrCachedObject(
                StaticTokens.CachingTokens.ManeuverEnum,
                this.Sailservice.GetManeuvers, 600);

            this.LoggingService.GenerateLog("Getting locations.", Log.LogTypes.Audit, this.Context);
            this.Session["Location"] = this.RetrieveDatabaseOrCachedObject(
                StaticTokens.CachingTokens.LocationEnum,
                this.Sailservice.GetLocations, 30);

            this.LoggingService.GenerateLog("Getting animals.", Log.LogTypes.Audit, this.Context);
            this.Session["Animal"] = this.RetrieveDatabaseOrCachedObject(
                 StaticTokens.CachingTokens.AnimalEnum,
                 this.AnimalService.GetAllAnimals);

            this.LoggingService.GenerateLog("Getting animal categories.", Log.LogTypes.Audit, this.Context);
            this.Session["AnimalCategories"] =
                this.RetrieveDatabaseOrCachedObject(
                    StaticTokens.CachingTokens.AnimalCategoryEnum,
                    this.AnimalService.GetAllCategories);

            this.LoggingService.GenerateLog("Getting sail configurations.", Log.LogTypes.Audit, this.Context);
            this.Session[StaticTokens.CachingTokens.SailConfigurationEnum] = this.RetrieveDatabaseOrCachedObject(
                 StaticTokens.CachingTokens.SailConfigurationEnum,
                 this.Sailservice.GetConfigurations);

            this.LoggingService.GenerateLog("Getting landmarks.", Log.LogTypes.Audit, this.Context);
            this.Session[StaticTokens.CachingTokens.LandMarkEnum] = this.RetrieveDatabaseOrCachedObject(
                   StaticTokens.CachingTokens.LandMarkEnum,
                   this.LocationService.GetLandMarks);

            this.LoggingService.GenerateLog("Getting user specific sessions.", Log.LogTypes.Audit, this.Context);
            this.LoadUserSpecificSessionVariables();

            this.LoggingService.GenerateLog("Loading standard cookies.", Log.LogTypes.Audit, this.Context);
            this.LoadStandardVesselFromCookie();
        }

        internal void IsCreator(IUserCreatable entity)
        {
            bool result = false;
            if (this.CurrentUser != null && entity != null)
            {
                result = this.CurrentUser.ID == entity.CreatedbyID;
            }
            this.ViewData["editor"] = result;
        }

        internal void CreateCookie(string name, string value, DateTime? expiryDate=null, int expiryHours=16, int expiryDays=0, int expiryYears=0)
        {
            DateTime expires=DateTime.Now;
            if (expiryDate != null)
            {
                expires = (DateTime)expiryDate;
            }

            HttpCookie cookie=new HttpCookie(name);
            cookie.Value = value;
            cookie.Expires = expires.AddHours(expiryHours).AddDays(expiryDays).AddYears(expiryYears);
            CreateCookie(cookie);
        }

        internal void CreateCookie(HttpCookie cookie)
        {
            this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
        }

        private HttpCookie getCookie(string name)
        {
            return this.ControllerContext.HttpContext.Request.Cookies[name];
        }

        internal string GetCookieValue(string name)
        {
            var cookie=getCookie(name);
            string result = null;
            if (cookie != null)
            {
                result = cookie.Value;
            }
            return result;
        }

        internal int? GetIDFromCookie(string name)
        {
            int? result = null;
            var asString=GetCookieValue(name);
            if (asString != null)
            {
                int temp = 0;
                int.TryParse(asString, out temp);
                if (temp > 0)
                {
                    result = temp;
                }
            }
            return result;
        }

        internal bool RemoveCookie(string name)
        {
            var cookie=getCookie(name);
            cookie.Expires = DateTime.Now.AddDays(-1);
            CreateCookie(cookie);
            return true;
        }

    }
}