﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommentable.cs" company="">
//   
// </copyright>
// <summary>
//   The CommentableModel interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The CommentableModel interface.
    /// </summary>
    public interface ICommentableModel: IEntityModel
    {
        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        IEnumerable<CommentBase> Comments { get; set; }
    }
}
