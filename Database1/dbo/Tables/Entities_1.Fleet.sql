﻿CREATE TABLE [dbo].[Entities.Fleet] (
    [FleetID]     INT            IDENTITY (1, 1) NOT NULL,
    [FleetName]   NVARCHAR (50)  NOT NULL,
    [FoundedDate] NCHAR (10)     NULL,
    [Description] NVARCHAR (255) NULL,
    [DateFounded] DATETIME       NULL,
    CONSTRAINT [PK_Entities.Fleet] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Entities.Fleet] PRIMARY KEY CLUSTERED ([FleetID] ASC)
);

