﻿SET IDENTITY_INSERT [dbo].[Entities.LandMark] ON;
GO
DELETE FROM  [dbo].[Entities.LandMark] WHERE Test=1
GO
IF (SELECT COUNT(*) FROM [dbo].[Entities.LandMark])<1
INSERT INTO [dbo].[Entities.LandMark]
           ([ID],
		  [Latitude]
           , [Longitude],[Name]
           ,[Description]
           )
     VALUES
	 ( 1,
	 45.115997, 
	 -92.999917,
	 'Bald Eagle Water Tower',
	 'Water tower off Hwy 61 near White Bear Lake just E of Bald Eagle Lake boat launch.'
	 ),
           (2,
		   45.092672
           ,-92.999161
           ,'The White Bear Lake Yacht Club'
           ,'White Bear Yacht Club'),
		   (3,
		   45.047222
           ,-92.986389
           ,'The White Bear Water Tower'
           ,'White Bear Lake Water Tower, County Rd E near Century College.')
GO
SET IDENTITY_INSERT [dbo].[Entities.LandMark] OFF;
GO

