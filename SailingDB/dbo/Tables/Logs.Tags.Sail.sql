﻿CREATE TABLE [dbo].[Logs.Tags.Sail] (
    [SailID] INT NOT NULL,
    [TagID]  INT NOT NULL,
    CONSTRAINT [PK_Logs.Tags.Sail] PRIMARY KEY CLUSTERED ([SailID] ASC, [TagID] ASC),
    CONSTRAINT [FK_Logs.Tags.Sail_Entities.Tags] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Entities.Tags] ([TagID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Tags.Sail_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID]) ON DELETE CASCADE
);

