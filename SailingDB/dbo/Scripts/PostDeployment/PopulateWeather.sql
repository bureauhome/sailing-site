﻿USE [Sailing]
GO
SET IDENTITY_INSERT [dbo].[Entities.Weather] ON
DELETE FROM [dbo].[Entities.Weather]
INSERT INTO [dbo].[Entities.Weather]
           ([WeatherID],
		   [Description]
           ,[IconPath]
           ,[Value])
     VALUES
           (0,'Clear'
           ,'../icons/clear.png'
           ,0),
		   (1,'Partly Cloudy'
           ,'../icons/pcloudy.png'
           ,1),
		   	(2,'Overcast'
           ,'../icons/cloudy.png'
           ,2),
		   (3,'Rain'
           ,'../icons/rain.png'
           ,0),
		   (4,'Sleet'
           ,'../icons/sleet.png'
           ,3),
		   (5,'Hail'
           ,'../icons/hail.png'
           ,4),
		   (6,'Snow'
           ,'../icons/snow.png'
           ,5),
		   (7,'Fog'
           ,'../icons/fog.png'
           ,6)
GO
