﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite
{
    /// <summary>
    /// The generic query.
    /// </summary>
    public class GenericQuery
    {
        public string FirstInitial { get; set; }
        public int? PhotoID { get; set; }
        public int? CommentID {get; set;}
        public int? SailID {get; set;}
        public int? AnimalID { get; set; }
        public string AnimalCategoryID { get; set; }
        public int? WeatherID { get; set; }
        public List<int> TagIDS { get; set; }
        public List<string> Keywods { get; set; }
        public int? CrewID { get; set; }
        public bool SelectDeleted { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
    }
}