﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite
{
    using System.EnterpriseServices.Internal;

    using SailingSite.Repositories;

    public interface IAnimalRepository : ITaggableRepository
    {
        int[] GetAnimalComments(int id);
        void AddAnimalComment(int commentId, int animalId);
        New.AnimalDetails GetDetailedAnimal(int id);
        IEnumerable<New.AnimalCategory> GetAllCategories();
        IEnumerable<New.AnimalCategory> GetSiblingCategories(string name);
        New.AnimalCategory GetTopLevelCategories();
        IEnumerable<SailingSiteModels.New.AnimalCategory> GetSubCategories(string name);
        IEnumerable<SailingSiteModels.New.AnimalCategory> GetParentCategories();
        List<SailingSiteModels.New.PhotoBase> GetAnimalPhotosForCategory(string categoryId);
        int CreateAnimal(SailingSiteModels.New.Animal entity, Entities_Animals_Categories category);

        void AddAnimalPhoto(int animalId, int photoId);
        bool CheckDuplicate(New.Animal entity);
        New.Animal GetAnimal(int id);
         IEnumerable<New.Animal> GetAnimals(GenericQuery query);
         New.AnimalCategory GetCategory(string name);
         void UpdateAnimal(New.Animal entity);
         string UpdateCategory(New.AnimalCategory entity);
         string DeleteAnimal(int id);
         string DeleteCategory(string name);
         bool CreateAnimalCategory(SailingSiteModels.New.AnimalCategory entity);

        IEnumerable<New.Animal> Search(GenericQuery request);

        List<string> GetFirstInitials();

    }
}