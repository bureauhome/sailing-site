﻿CREATE TABLE [dbo].[Logs.Comments.Vessel] (
    [VesselID]  INT NOT NULL,
    [CommentID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Vessel] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Comments.Vessel] PRIMARY KEY CLUSTERED ([VesselID] ASC, [CommentID] ASC),
    CONSTRAINT [FK_Logs.Comments.Vessel_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]),
    CONSTRAINT [FK_Logs.Comments.Vessel_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]) ON DELETE CASCADE
);

