﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Common.Extension
{
    using SailingSite.Models;

    using SailingSiteModels.New;

    public static class SailExtension
    {
        public static string GetDecimalClass(this int? input)
        {
            var result=String.Empty;
            
            if (input > 7)
            {
                result = "factor7";
            }
            if (input <= 7)
            {
                result = "factor6";
            }
            if (input < 5)
            {
                result = "factor4";
            }
            if (input < 3)
            {
                result = "factor1";
            }
            return result;
        }

        public static string ToFarenheitClass(this decimal input)
        {
            string result = string.Empty;
            if (input > 94)
            {
                result = "factor7";
            }
            if (input < 95)
            {
                result = "factor6";
            }
            if (input < 88)
            {
                result = "factor2";
            }

            if (input < 68)
            {
                result = "coolFactor";
            }
            if (input < 50)
            {
                result = "coldFactor";
            }

            return result;
        }

        public static SailFeedViewModel ToFeed(this SailBase entity)
        {
            if (entity != null)
            {
                return new SailFeedViewModel()
                           {
                               Crew = entity.Crew,
                               Date = entity.Date,
                               ID = entity.ID,
                               HoursSailed = entity.HoursSailed,
                               MilesSailed = entity.MilesSailed,
                               Barometer = entity.Barometer,
                               GeneralObservations = entity.GeneralObservations,
                               TempF = entity.TempF,
                               MaxWind = entity.MaxWind,
                               SeaState = entity.SeaState,
                               MinWind = entity.MinWind,
                               Created = entity.Created,
                               CreatedBy = entity.CreatedBy,
                               WindDirection = entity.WindDirection,
                               Centerboard = entity.Centerboard,
                               Tilt = entity.Tilt,
                               Location = entity.Location,
                               Vessel = entity.Vessel
                           };
            }
            return null;
        }
    }
}