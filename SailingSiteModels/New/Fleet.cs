﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SailingSiteModels.Common;
using System.ComponentModel.DataAnnotations;

namespace SailingSiteModels.New
{
    public class VesselEntry : ISingleVessel
    {
        public VesselBase Vessel { get; set; }
        public int Ordinal { get; set; }
        public DateTime DateAdded { get; set; }
        public string NickName { get; set; }
        public decimal MilesSailed { get; set; }
        public int SailTotal { get; set; }
        public int FleetID { get; set; }
        public int ID { get; set; }


        public string ControllerName
        {
            get
            {
                return "Fleet";
            }
        }

        public string DefaultDelete
        {
            get
            {
                return "DeleteVesselEntry";
            }
        }

        public string DefaultSet
        {
            get
            {
                return "GetVesselEntry";
            }
        }
    }

    [Serializable]
    public class Fleet : FleetBase, IDatabaseModel, ITaggableModel, ICommentableEntity, IPhotographedEntity, IEntityModel
    {
        public IEnumerable<PhotoBase> Photos { get; set; }
        public IEnumerable<Tags> Tags { get; set; }
        public IEnumerable<CommentBase> Comments { get; set; }
        public List<VesselEntry> VesselEntries { get; set; }
        public List<CrewBase> Crew { get; set; }
        public bool IsTest { get; set; }
        public List<CrewFoundation> AssignedCrew { get; set; }
        public CrewFoundation Captain { get; set; }
        [DataType(DataType.Date,ErrorMessage = "This field requires a date.")]
        public DateTime? FoundedDate { get; set; }
        [StringLength(200, ErrorMessage = "Too many characters in description.")]
        public string Description { get; set; }

        public decimal MileTotal { get; set; }

        public int SailTotal { get; set; }

        public int CrewTotal { get; set; }

        public Fleet() {
            Tags = new List<Tags>();
        }
        public Fleet(FleetBase entity)
        {
            this.ID = entity.ID;
            this.Name = entity.Name;
            this.MileCount = entity.MileCount;
            this.CrewCount = entity.CrewCount;
            this.VesselCount = entity.VesselCount;
        }
        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
    }
}
