﻿CREATE TABLE [dbo].[Entities.Crew] (
    [CrewID]       INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]    NVARCHAR (128) NOT NULL,
    [LastName]     NVARCHAR (128) NOT NULL,
    [UserName]     NVARCHAR (50)  NOT NULL,
    [PassWord]     TEXT           NULL,
    [EmailAddress] NVARCHAR (128) NULL,
    [Salt]         NVARCHAR (128) NULL,
    [SID]          INT            NULL
);

