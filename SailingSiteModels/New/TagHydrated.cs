﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    using SailingSiteModels.Common;

    public class TagHydrated : Tags, IEntityModel
    {
        public List<Animal> Animals { get; set; }
        public List<CrewBase> Crews { get; set; }
        public List<VesselBase> Vessels { get; set; }
        public List<SailBase> Sails { get; set; }
        public List<CommentBase> Comments { get; set; }
        public List<FleetBase> Fleets { get; set; }
        public List<LocationBase> Locations { get; set; }
        public List<PhotoBase> Photos { get; set; }
        public int Count()
        {
            int count = 0;
            if (Locations != null)
            {
                count += Locations.Count();
            }
            if (Photos != null)
            {
                count += Photos.Count();
            }
            if (Sails != null)
            {
                count += Sails.Count();
            }
            if (Comments != null)
            {
                count += Comments.Count();
            }
            if (Fleets != null)
            {
                count += Fleets.Count();
            }
            if (Animals != null)
            {
                count += Animals.Count();
            }
            if (Crews != null)
            {
                count += Crews.Count();
            }
            if (Vessels != null)
            {
                count += Vessels.Count();
            }
            return count;
        }
    }
}
