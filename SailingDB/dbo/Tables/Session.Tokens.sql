﻿CREATE TABLE [dbo].[Session.Tokens] (
    [GUID]      NVARCHAR (55) NOT NULL,
    [TimeStamp] ROWVERSION    NOT NULL,
    [CrewID]    INT           NOT NULL,
    [Time]      DATETIME      NULL,
    CONSTRAINT [PK_Session.Tokens] PRIMARY KEY CLUSTERED ([GUID] ASC, [CrewID] ASC),
    CONSTRAINT [FK_Session.Tokens_Entities.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]) ON DELETE CASCADE ON UPDATE CASCADE
);

