﻿CREATE TABLE [dbo].[Logs.Comments.Weather] (
    [CommentID] INT NOT NULL,
    [WeatherID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Comments.Weather] PRIMARY KEY CLUSTERED ([CommentID] ASC, [WeatherID] ASC),
    CONSTRAINT [FK_Logs.Comments.Weather_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID])
	ON UPDATE CASCADE
ON DELETE CASCADE,
    CONSTRAINT [FK_Logs.Comments.Weather_Entities.Weather] FOREIGN KEY ([WeatherID]) REFERENCES [dbo].[Entities.Weather] ([WeatherID])
);

