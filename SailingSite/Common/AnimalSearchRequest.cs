﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite
{
    public class AnimalSearchRequest
    {
        public int? ID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
    }
}