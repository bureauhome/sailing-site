﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SailingUnitTests
{
    using System.Configuration;
    using System.Linq;
    using System.Xml.Linq;

    using SailingSite.Services;

    [TestClass]
    public class GPSTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            CommentService service=new CommentService();
            var file =
                @"C:\Users\bbureau\Documents\sailing-site\SailingUnitTests\TestfFiles\Track_A016-05-22 174721.gpx";
            var doc=XDocument.Load(file);
            var result=GPXParser.GPXParser.ParseTrack(doc);
            Assert.IsNotNull(result);
            var track = result.FirstOrDefault().Track;
            Assert.IsNotNull(track);
        }
    }
}
