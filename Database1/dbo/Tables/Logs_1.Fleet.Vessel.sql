﻿CREATE TABLE [dbo].[Logs.Fleet.Vessel] (
    [FleetID]  INT           NOT NULL,
    [VesselID] INT           NOT NULL,
    [Nickname] NVARCHAR (50) NULL,
    [Ordinal]  INT           NULL,
    CONSTRAINT [PK_Logs.Fleet.Vessel] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Fleet.Vessel] PRIMARY KEY CLUSTERED ([FleetID] ASC, [VesselID] ASC),
    CONSTRAINT [FK_Logs.Fleet.Vessel_Entities.Fleet] FOREIGN KEY ([FleetID]) REFERENCES [dbo].[Entities.Fleet] ([FleetID]),
    CONSTRAINT [FK_Logs.Fleet.Vessel_Entities.Vessel] FOREIGN KEY ([VesselID]) REFERENCES [dbo].[Entities.Vessel] ([VesselID]) ON DELETE CASCADE
);

