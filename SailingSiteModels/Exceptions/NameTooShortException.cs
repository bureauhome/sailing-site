﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Exceptions
{
    public class NameTooShortException : Exception
    {
        public NameTooShortException() :
            base("Name is too short.  Both first and last names must have at least three characters.")
        { }
    }
}
