﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New = SailingSiteModels.New;

namespace SailingSite.Factories
{
    public static class CrewFactory
    {

        public static New.Crew CreateHydratedCrew(Entities_Crew entity)
        {

            var result = CreateCrew(entity);
            if (result != null)
            {
                result.Sails = entity.Logs_Sails == null ? null : entity.Logs_Sails.Select(SailFactory.CreateSailbase);
                result.Photos = entity.Entities_Photos == null ? null : entity.Entities_Photos2.Select(PhotoFactory.CreatePhotoBase);
                result.Comments = entity.Entities_Comment == null ? null : entity.Entities_Comment.Select(CommonFactory.CreateCommentBase);
            }
            return result;
        
        }


        public static New.CrewSecurity CreateCrewSecurity(Entities_Crew entity)
        {
            New.VesselFoundation[] captainVessels = null;
            New.VesselFoundation[] crewVessels = null;
            New.FleetBase[] commFleet = null;
            if (entity.Entities_Vessel != null && entity.Entities_Vessel.Any())
            {
                captainVessels = entity.Entities_Vessel.Select(Factories.FleetFactory.CreateVesselFoundation).ToArray();
            }

            if (entity.Entities_Vessel1 != null && entity.Entities_Vessel1.Any())
            {
                crewVessels = entity.Entities_Vessel1.Select(Factories.FleetFactory.CreateVesselFoundation).ToArray();
                if (captainVessels != null && captainVessels.Any())
                {
                    // A vessel captain is always a crewmember as well.
                    crewVessels = crewVessels.Concat(captainVessels).ToArray();
                }
            }
            if (entity.Entities_Fleet != null && entity.Entities_Fleet.Any())
            {
                commFleet = entity.Entities_Fleet.Select(FleetFactory.CreateFleetBase).ToArray();
            }
            var crew = CreateCrewFoundation(entity).ToCrewSecurity();
            crew.VesselsCaptained = captainVessels;
            crew.VesselsCrewed = crewVessels;
            crew.FleetsAdmined = commFleet;
            crew.SID = entity.SID == null ? (New.Crew.SIDEnum)0 : (New.Crew.SIDEnum)entity.SID;
            crew.Name = entity.FirstName;             
            return crew;
        }

        public static Entities_Crew CreateCrew(New.CrewFoundation entity)
        {
            Entities_Crew result = null;
            if (entity != null)
            {
                result= createCrew(entity.ID);
            }
            return result;
        }

        private static Entities_Crew createCrew(int id)
        {
            Entities_Crew result = null;
            using (var context = new SailingEntities())
            {
                result = context.Entities_Crew.FirstOrDefault(i => i.CrewID == id);
            }
            return result;
        }


        public static Entities_Crew CreateCrew(New.CrewBase entity)
        {
            Entities_Crew result = null;
            if (entity != null)
            {
                result = createCrew(entity.ID);
            }
            return result;
        }

        public static Entities_Crew CreateCrew(New.Crew entity)
        {
            var photo = entity.ProfilePhoto == null ? null : Factories.PhotoFactory.CreatePhoto(entity.ProfilePhoto.ToPhoto());
            int? SID = entity.SID != null ? (int)entity.SID : 0;
            var result=new Entities_Crew()
            {
                Salt=entity.Salt,
                SID=SID,
                TestData=entity.IsTestData,
                FirstName = entity.FirstName == null ? null : entity.FirstName.Trim(),
                LastName = entity.LastName == null ? null : entity.LastName.Trim(),
                UserName = entity.UserName == null ? null : entity.UserName,
                Entities_Photos = photo,
                EmailAddress=entity.EmailAddress==null?null:entity.EmailAddress
            };
            if (entity.Password != "Reset Password")
            {
                result.PassWord = entity.Password;
            }
            return result;
        }

        public static New.CrewBase CreateCrewBase (Entities_Crew entity)
            
        {
            if (entity != null)
            {
                var result = CreateCrewFoundation(entity).ToCrewBase();
                if (result != null)
                {
                    result.EmailAddress = entity.EmailAddress;
                    return result;
                }

            }
            return null;
        }

        public static New.CrewFoundation CreateCrewFoundation(Entities_Crew entity)
    {
        if (entity != null)
        {
            return new New.CrewFoundation()
                       {
                           FirstName = entity.FirstName,
                           LastName = entity.LastName,
                           UserName = entity.UserName,
                           ID = entity.CrewID
                       };
        }
        return null;
    }

        public static New.Crew CreateCrew(Entities_Crew entity)
    {
        var result = CreateCrewBase(entity).ToCrew();
        var newsid = New.Crew.SIDEnum.None;
        New.CommentBase[] comments = entity.Entities_Comment5 == null || !entity.Entities_Comment5.Any() ? null : entity.Entities_Comment5.Select(Factories.CommonFactory.CreateCommentBase).ToArray();
        New.Tags[] tags = entity.Entities_Tags == null || !entity.Entities_Tags.Any() ? null : entity.Entities_Tags.Select(Factories.CommonFactory.CreateTag).ToArray();
        New.SailBase[] sails = entity.Logs_Sails3 == null || !entity.Logs_Sails3.Any() ? null : entity.Logs_Sails3.Select(Factories.SailFactory.CreateSailbase).ToArray();
        IEnumerable<New.PhotoBase> photos = entity.Entities_Photos3 == null || !entity.Entities_Photos3.Any() ? null : entity.Entities_Photos3.Select(Factories.PhotoFactory.CreatePhotoBase).ToArray();
            var photo = entity.Entities_Photos == null ? null: Factories.PhotoFactory.CreatePhotoBase(entity.Entities_Photos);
            if (result!=null)
        {
            result.Tags = tags;
            result.Sails = sails;
            result.PhotoLog = photos;
            result.Comments = comments;
            result.SID = entity.SID == null ? (New.Crew.SIDEnum)0 : (New.Crew.SIDEnum)entity.SID;
            result.UserName=entity.UserName==null?null:entity.UserName;
            result.Salt = entity.Salt;
            result.ProfilePhoto = photo;
            result.EmailAddress = entity.EmailAddress == null ? null : entity.EmailAddress;
        };
        return result;
    }
     }
}