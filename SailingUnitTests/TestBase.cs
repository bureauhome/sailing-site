﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingUnitTests
{
    using System.Data.Entity.ModelConfiguration.Configuration;
    using System.Web.Mvc;
    using System.Web;
    using System.Web.Helpers;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.IO;
    using System.Web.UI.WebControls;

    using Moq;
    
    using SailingSite;
    using SailingSite.Common;
    using SailingSite.Repositories;
    using SailingSite.Repositories.Interfaces;
    using SailingSite.Services;
    using SailingSiteModels.Common;
    using SailingSiteModels.New;

    internal class TestImage : System.Web.HttpPostedFileBase
    {
       
    }

    public abstract class TestBase
    {
        private string[] vesselNames =
            {
                "Africa", "Victory", "Temeraire", "Neptune", "Leviathan", "Conqueror",
                "Britannia", "Agamemnon", "Ajax", "Orion", "Minotaur", "Spartiate",
                "Stella's Pink Lady", "Pickle" ,"Bellerophon"
            };

        private string[] firstNames =
            {
                "Anna", "Cora", "Marigold", "Cybil", "Robert", "Alan", "Matthew", "William",
                "Daisy", "Mary", "Edith", "George","Richard","Thomas","Edward","Horatio","Jack","Barrett","Rose","Isis","Richard"
            };

        private string[] lastNames =
            {
                "Bates", "Crawley", "Hughes", "Branson", "Mason", "Bonden", "Aubrey", "Pellew",
                "Collinwood", "Nelson", "Hardy", "St Vincent","Maturin y Villenova","Pattemore","Laponetiere"
            };

        /// <summary>
        /// The animal controller.
        /// </summary>
        private AnimalService animalService = null;

        /// <summary>
        /// The tags service.
        /// </summary>
        private TagService tagsService = null;

        private CommentService commentService=null;

        private CrewBase testUser = null;

        private CrewRepository crewRepository = null;

        private CrewService crewService = null;

        private SailingService sailService = null;

        private SailRepository sailRepository = null;

        private VesselRepository vesselRepository = null;

        private PhotoService photoService = null;

        private ILoggingService loggingService;

        private ILogRepository loggingRepository;

        private AuditContext context;

        public AuditContext AdminContext
        {
            get
            {
                this.context = StaticTokens.TestValues.AdminTestContext;
                return this.context;
            }
            set
            {
                this.context = value;
            }
        }


        public AuditContext Context
        {
            get
            {
                this.context = StaticTokens.TestValues.TestContext;
                return this.context;
            }
            set
            {
                this.context = value;
            }
        }


        public ILogRepository LoggingRepository
        {
            get
            {
                if (this.loggingRepository == null)
                {
                    this.loggingRepository=new LoggingRepository();
                }
                return this.loggingRepository;
            }
            set
            {
                this.loggingRepository = value;
            }
        }

        public ILoggingService LoggingService
        {
            get
            {
                if (this.loggingService == null)
                {
                    this.loggingService=new LoggingService();
                }
                return this.loggingService;
            }

            set
            {
                this.loggingService = value;
            }

        }

        /// <summary>
        /// The generate sheet sail.
        /// </summary>
        /// <returns>
        /// The <see cref="SheetSail"/>.
        /// </returns>
        protected SheetSail GenerateSheetSail()
        {
            var rmodel = new SheetSail
                             {
                                 SailType = new Configuration() { ID = random.Next(1,5) },
                                 Location = UnitTestHelpers.GetRandomString(12),
                                 RollingFurling = false,
                                 Colors= UnitTestHelpers.GetRandomString(10),
                                 SailArea=(decimal)14.4,
                                 SailLettering = UnitTestHelpers.GetRandomString(12)

                             };
            return rmodel;
        }

        protected void CompareSheetSail(SheetSail original, SheetSail compare)
        {
            if (original != null)
            {
                Assert.IsNotNull(compare);
                Assert.AreEqual(original.SailType.ID,compare.SailType.ID);
                Assert.AreEqual(original.RollingFurling,compare.RollingFurling);
                Assert.AreEqual(original.Colors, compare.Colors);
                Assert.AreEqual(original.SailArea, compare.SailArea);
                Assert.AreEqual(original.SailLettering, compare.SailLettering);
            }
        }

        public PhotoService PhotoService
        {
            get
            {
                if (this.photoService == null)
                {
                    this.photoService=new PhotoService();

                }
                return photoService;
            }
            set
            {
                this.photoService = value;
            }
        }

        public CommentService CommentService
        {
            get
            {
                if (this.commentService == null)
                {
                    this.commentService=new CommentService();
                }
                return this.commentService;
            }
        }

        public VesselRepository VesselRepository
        {
            get
            {
                if (this.vesselRepository == null)
                {
                    this.vesselRepository=new VesselRepository();
                }
                return this.vesselRepository;
            }
        }

        public SailingService SailService
        {
            get
            {
                if (this.sailService == null)
                {
                    this.sailService=new SailingService();
                }
                return this.sailService;
            }
        }

        public SailRepository SailRepository
        {
            get
            {
                if (this.sailRepository == null)
                {
                    this.sailRepository=new SailRepository();
                }
                return this.sailRepository;
            }
        }

        public AnimalService AnimalService
        {
            get
            {
                if (this.animalService == null)
                {
                     this.animalService=new AnimalService();
                }
                return this.animalService;
            }
        }

        public CrewRepository CrewRepository
        {
            get
            {
                if (this.crewRepository == null)
                {
                    this.crewRepository = new CrewRepository();
                }
                return this.crewRepository;
            }
        }

        public CrewService CrewService
        {
            get
            {
                if (this.crewService == null)
                {
                    this.crewService=new CrewService();
                }
                return this.crewService;
            }
        }

        /// <summary>
        /// Gets the tag service.
        /// </summary>
        public TagService TagService
        {
            get
            {
                if (this.tagsService == null)
                {
                    this.tagsService=new TagService();
                }

                return this.tagsService;
            }
        }

        internal Fleet GenerateFleet()
        {
             var editors = new List<CrewFoundation>()
                              {
                                  new CrewFoundation() { ID = 11 },
                                  new CrewFoundation() { ID = 12 }
                              };
            var result = new Fleet()
                             {
                                 AssignedCrew = editors,
                                 Captain = new CrewFoundation() { ID = 7 },
                                 Name = "The E Street Band make"+DateTime.Now.Ticks,
                                 Description = "A fleet",
                                 FoundedDate = DateTime.Now.AddYears(random.Next(-100,-1)),
                                 IsTest = true
                             };
            return result;
        }

        internal void ModFleet(Fleet fleet)
        {
            var editors = new List<CrewFoundation>()
                              {
                                  new CrewFoundation() { ID = 10 },
                                  new CrewFoundation() { ID = 12 }
                              };
            fleet.AssignedCrew = editors;
            fleet.Name = "The Range";
            fleet.Captain = new CrewFoundation() { ID = 12 };
            fleet.FoundedDate = DateTime.Now.AddYears(random.Next(-100,-1));
            fleet.Description = "Still a fleet";
        }

        internal bool CompareFleets(Fleet original, Fleet compare)
        {
            Assert.AreEqual(original.Name,compare.Name);
            Assert.AreEqual(original.Description, compare.Description);
            Assert.AreEqual(original.Captain.ID,compare.Captain.ID);
            if (original.FoundedDate != null)
            {
                Assert.IsNotNull(compare.FoundedDate);
                var date1 = ((DateTime)original.FoundedDate).ToLongDateString();
                var date2 = ((DateTime)compare.FoundedDate).ToLongDateString();
                Assert.AreEqual(date2, date1);
            }

            if (original.AssignedCrew != null && original.AssignedCrew.Any())
            {
                Assert.IsTrue(this.compareLists(original.AssignedCrew, compare.AssignedCrew));
            }

            if (original.VesselEntries != null && original.VesselEntries.Any())
            {
                foreach (var entry in original.VesselEntries)
                {
                    var compareEntry = compare.VesselEntries.FirstOrDefault(i => i.Vessel.ID == entry.Vessel.ID);
                    Assert.IsNotNull(compareEntry);
                    this.matchVesselEntries(entry, compareEntry);
                }
            }
            return true;
        }

        protected void CompareSheetSailLog(SheetSailLog original, SheetSailLog compare)
        {
            if (original != null)
            {
                Assert.IsNotNull(compare);
                Assert.AreEqual(original.Percentage,compare.Percentage);
                this.CompareSheetSail(original.SheetSail, compare.SheetSail);
            }
        }

        private bool matchVesselEntries(VesselEntry original, VesselEntry compare)
        {
            Assert.AreEqual(original.Vessel.ID,compare.Vessel.ID);
            Assert.AreEqual(original.DateAdded.ToLongDateString(),compare.DateAdded.ToLongDateString());
            Assert.AreEqual(original.NickName,compare.NickName);
            Assert.AreEqual(original.Ordinal,compare.Ordinal);
            return true;
        }

        public bool MatchCrew(Crew original, Crew compare)
        {
            Assert.AreEqual(original.ID, compare.ID);
            Assert.AreEqual(original.UserName,compare.UserName);
            Assert.AreEqual(original.FirstName,compare.FirstName);
            Assert.AreEqual(original.LastName, compare.LastName);
            Assert.AreEqual(original.Salt, compare.Salt);
            if (original.Sails != null && original.Sails.Any())
            {
                Assert.IsNotNull(compare.Sails);
                Assert.IsTrue(this.compareLists(original.Sails,compare.Sails));
                Assert.AreEqual(compare.SailCount,original.Sails.Count());
                Assert.AreEqual(compare.MileCount,original.Sails.Sum(i=>i.MilesSailed));
                Assert.AreEqual(compare.HourCount,original.Sails.Sum(i=>i.HoursSailed));
            }
            Assert.IsTrue(compareLists(original.Comments,compare.Comments));
      
            return true;
        }

        protected Vessel GenerateVessel()
        {
            var editors = new List<CrewFoundation>()
                              {
                                  new CrewFoundation() { ID = 11 },
                                  new CrewFoundation() { ID = 12 }
                              };
            var result = new Vessel()
                             {
                                 Name = this.vesselNames[random.Next(0, (this.vesselNames.Count() - 1))],
                                 Length = random.Next(9,60),
                                 Beam = random.Next(5,8),
                                 Captain = new CrewFoundation() { ID= 7},
                                 Editors = editors,
                                 Model="ZippityFloop"+random.Next(15,70).ToString(),
                                 Manufacturer = "Acme",
                                 Rig=Vessel.RigTypes.Ship,
                                 HullNumbers = "MN-Z"+random.Next(1024,9999),
                                 HullColors = "Red",
                                 Hull=Vessel.HullTypes.Single,
                                 NumberOfMasts = random.Next(1,12),
                                 LengthOverAll = random.Next(10,20),
                                 LengthWaterLine = random.Next(10,20),
                                 SailColors = "Green, Orange",
                                 LetteringColors = "Brown, Red",
                                 IsTest = true,
                                 SailNumbers = random.Next(20,100).ToString(),
                                 DistinguishingFeatures = "bowsprit"
                             };
            return result;
        }

        internal Crew GenerateRandomCrew()
        {
            var result=new Crew()
            {
                IsTestData = true,
                UserName = CrewRepository.TestUsername,
                Password = DateTime.Now.Ticks.ToString(),
                EmailAddress = CrewRepository.TestEmailAddress,
                FirstName = "Bruce",
                LastName = "TheTester"
            };
            return result;
        }

        internal void ModCrew(Crew crew)
        {
            crew.FirstName = firstNames[random.Next(0, firstNames.Count() - 1)];
            crew.LastName = lastNames[random.Next(0, lastNames.Count() - 1)];
            crew.UserName = UnitTestHelpers.GetRandomString(random.Next(5, 8));
            crew.EmailAddress = UnitTestHelpers.GetRandomString(random.Next(8, 20)) + "@hotmail.com";
        }

        internal Vessel PreGenerateRandomVessel(bool includeSheetLocker)
        {
            var result = this.GenerateVessel();
            if (includeSheetLocker != null)
            {
                List<SheetSail> locker=new List<SheetSail>();
                for (int i = 0; i < random.Next(1, 8); i++)
                {
                    locker.Add(new SheetSail()
                                   {
                                       Location = UnitTestHelpers.GetRandomString(14),
                                       RollingFurling = random.Next(100) < 50 ? null as bool? : true,
                                       SailType = new Configuration() { ID = random.Next(1, 12) }
                                   });
                }

                result.SailLocker = locker;
            }

            result.ID = this.VesselRepository.Create(result);
            result = this.VesselRepository.Get(result.ID);
            return result;
        }

        internal void ModVessel(Vessel vessel)
        {
            var editors = new List<CrewFoundation>()
                              {
                                  new CrewFoundation() { ID = 11 },
                                  new CrewFoundation() { ID = 12 }
                              };
            vessel.Name = this.vesselNames[random.Next(0, (this.vesselNames.Count() - 1))];
            vessel.LengthOverAll = random.Next(9, 60);
            vessel.LengthWaterLine = random.Next(2, 15);
            vessel.Beam = random.Next(5, 8);
            vessel.Captain = new CrewFoundation() { ID = 10 };
            vessel.Editors = editors;
            vessel.Rig = Vessel.RigTypes.Schooner;
            vessel.HullNumbers = "MI-W" + random.Next(1024, 9999);
            vessel.HullColors = "White";
            vessel.Hull = Vessel.HullTypes.Double;
            vessel.NumberOfMasts = random.Next(1, 12);
            vessel.SailColors = "White";
            vessel.LetteringColors = "Yellow, Blue";
            vessel.SailNumbers = random.Next(20, 100).ToString();
            vessel.DistinguishingFeatures = "figurehead";
        }

        internal VesselEntry GenerateVesselEntry(Vessel vessel)
        {
            return new VesselEntry()
                       {
                           DateAdded = DateTime.Now.AddYears(random.Next(-50, -3)),
                           NickName = "Fred",
                           Ordinal = random.Next(1, 10),
                           Vessel = vessel
                       };
        }

        internal void ModVesselEntry(VesselEntry entry)
        {
            entry.DateAdded = DateTime.Now.AddYears(random.Next(-2, 0));
            entry.NickName = "Steve";
            entry.Ordinal = random.Next(11, 20);
        }

        internal bool compareVessel(Vessel original, Vessel compare)
        {
            Assert.AreEqual(original.Name,compare.Name);
            Assert.AreEqual(original.LengthOverAll,compare.LengthOverAll);
            Assert.AreEqual(original.LengthWaterLine,compare.LengthWaterLine);
            Assert.AreEqual(original.Beam, compare.Beam);
            Assert.AreEqual(original.Manufacturer,compare.Manufacturer);
            Assert.AreEqual(original.Model,compare.Model);
            Assert.IsNotNull(compare.Captain);
            Assert.AreEqual(original.Captain.ID, compare.Captain.ID);
            Assert.AreEqual(original.Rig, compare.Rig);
            Assert.AreEqual(original.HullNumbers,compare.HullNumbers);
            Assert.AreEqual(original.HullColors,compare.HullColors);
            Assert.AreEqual(original.Hull,compare.Hull);
            Assert.AreEqual(original.NumberOfMasts,compare.NumberOfMasts);
            Assert.AreEqual(original.SailColors,compare.SailColors);
            Assert.AreEqual(original.SailNumbers,compare.SailNumbers);
            Assert.AreEqual(original.LengthOverAll,compare.LengthOverAll);
            Assert.AreEqual(original.LengthWaterLine, compare.LengthWaterLine);
            Assert.AreEqual(original.DistinguishingFeatures,compare.DistinguishingFeatures);
            Assert.IsTrue(this.compareLists(original.Fleets,compare.Fleets));
            var editors = original.Editors.ToList();
            editors.Add(original.Captain);
            Assert.IsTrue(this.compareLists(editors, compare.Editors));
            Assert.IsTrue(this.compareLists(original.AnimalsSpotted,compare.AnimalsSpotted));
            Assert.IsTrue(this.compareLists(original.Crew,compare.Crew));
            Assert.IsTrue(this.compareLists(original.Sails,compare.Sails));
            return true;
        }

        internal Comment GenerateSailLog(Sail sail = null, Vessel vessel=null, Crew crewmember=null)
        {
            var comment = new Comment();
            comment.Created = DateTime.Now;
            comment.CloudCover = random.Next(0, 100);
            comment.RainSeverity = random.Next(0, 100);
            comment.TempF = random.Next(50, 90);
            comment.Title = "Fred.";
            comment.Subject = "This is a test.";
            comment.IsTest = true;
            comment.IsLog = true;
            comment.CompassBearing = random.Next(0, 360);
            comment.Leeway = random.Next(0, 360);
            comment.SpeedMph = random.Next(0, 120);
            comment.Longitude = random.Next(-180, 180);
            comment.Latitude = random.Next(-90,90);
            comment.Sounding = random.Next(4, 3000);
            comment.SeaState = random.Next(1, 10);
            comment.Barometer = random.Next(29, 32);
            comment.CreatedBy = new CrewFoundation() { ID = 6 };
            comment.HelmsPerson = new CrewBase() { ID = 6 };
            comment.Message = DateTime.Now.ToLongTimeString();
            comment.WindSpeed = sail == null ? 5 : sail.MaxWind + 2;
            comment.WindDirection = random.Next(0, 360);
            comment.LogId = sail.ID;
            comment.CenterboardPct = random.Next(15, 100);
            comment.HeelPct = sail == null ? 5 : sail.Tilt + 2;
            comment.MilesLogged = random.Next(1, 5);
            if (vessel != null)
            {
                comment.Vessels = new List<VesselFoundation>() { vessel };
            }
            List<Animal> animals = new List<Animal>();
            List<WeatherBase> weather=new List<WeatherBase>();
            weather.Add(new WeatherBase() { ID = 0 });
            weather.Add(new WeatherBase() { ID = 1 });
            comment.Weather = weather;
            animals.Add(new Animal { ID = 1 });
            comment.Animals = animals;
            List<CrewFoundation> crew = new List<CrewFoundation>();
            if (crewmember != null)
            {
                crew.Add(crewmember);

            }
            else
            {
                crew.Add(new CrewFoundation { ID = 10 });
                crew.Add(new CrewFoundation { ID = 14 });
            }
            comment.Crew = crew;
            List<Vessel> vessels = new List<Vessel>();
            vessels.Add(new Vessel() { ID = 1 });
            comment.Vessels = vessels;
            return comment;
        }

        internal bool MatchComment(Comment original, Comment compare)
        {
            Assert.AreEqual(original.Created.ToLongDateString(), compare.Created.ToLongDateString());
            Assert.AreEqual(compare.CloudCover, original.CloudCover);
            Assert.AreEqual(compare.SeaState,original.SeaState);
            Assert.AreEqual(compare.RainSeverity, original.RainSeverity);
            Assert.AreEqual(compare.TempF, original.TempF);
            Assert.AreEqual(compare.Subject, original.Subject);
            Assert.AreEqual(compare.IsTest, original.IsTest);
            Assert.AreEqual(compare.CreatedBy.ID, original.CreatedBy.ID);
            Assert.AreEqual(compare.WindDirection, original.WindDirection);
            Assert.AreEqual(compare.WindSpeed, original.WindSpeed);
            Assert.AreEqual(original.HeelPct, original.HeelPct);
            Assert.AreEqual(original.Leeway, original.Leeway);
            Assert.AreEqual(original.IsLog, compare.IsLog);
            Assert.AreEqual(original.Sounding, compare.Sounding);
            Assert.AreEqual(original.SpeedMph,compare.SpeedMph);
            Assert.AreEqual(original.Longitude,compare.Longitude);
            Assert.AreEqual(original.CompassBearing, compare.CompassBearing);
            Assert.AreEqual(original.Latitude,compare.Latitude);
            Assert.AreEqual(original.MilesLogged, compare.MilesLogged);
            if (original.Longitude != null)
            {
                Assert.AreEqual(original.Longitude,compare.Longitude);
            }
            if (original.Latitude != null)
            {
                Assert.AreEqual(original.Latitude,compare.Latitude);
            }
        Assert.AreEqual(original.CenterboardPct, compare.CenterboardPct);
            Assert.IsTrue(this.compareLists(original.Animals, compare.Animals));
            Assert.IsTrue(this.compareLists(original.Crew, compare.Crew));
            Assert.IsTrue(this.compareLists(original.Vessels, compare.Vessels));
            Assert.IsTrue(this.compareLists(original.Weather, compare.Weather));
            return true;
        }

        internal void ModComment(Comment comment)
        {
            comment.CloudCover = random.Next(0, 100);
            comment.RainSeverity = random.Next(0, 100);
            comment.TempF = random.Next(60, 90);
            comment.Title = "James.";
            comment.Subject = "This is also a test.";
            comment.IsTest = true;
            comment.Sounding = random.Next(3000, 6000);
            comment.IsLog = true;
            comment.Barometer = random.Next(29, 32);
            comment.ModifiedBy = new CrewFoundation() { ID = 6 };
            comment.Message = DateTime.Now.ToLongTimeString();
            comment.WindSpeed += random.Next(1, 3);
            comment.WindDirection = random.Next(0, 360);
            comment.CenterboardPct = random.Next(15, 100);
            comment.HeelPct += random.Next(1, 5);
            comment.MilesLogged = random.Next(5, 10);
            List<WeatherBase> weather = new List<WeatherBase>();
            weather.Add(new WeatherBase() { ID = 0 });
            weather.Add(new WeatherBase() { ID = 2 });
            comment.Weather = weather;
            List<Animal> animals = new List<Animal>();
            animals.Add(new Animal { ID = 1 });
            comment.Animals = animals;
            List<CrewFoundation> crew = new List<CrewFoundation>();
            crew.Add(new CrewFoundation { ID = 8 });
            crew.Add(new CrewFoundation { ID = 6 });
            comment.Crew = crew;
            List<Vessel> vessels = new List<Vessel>();
            vessels.Add(new Vessel() { ID = 1 });
            comment.Vessels = vessels;
        }

        internal bool compareLists(IEnumerable<IEntityModel> original, IEnumerable<IEntityModel> compare)
        {
            if (original != null && original.Any())
            {
                Assert.IsNotNull(compare);
                Assert.AreEqual(compare.Count(), original.Count());
                foreach (var entity in original)
                {
                    Assert.IsTrue(compare.Any(i => i.ID == entity.ID));
                }
            }
            return true;
        }

        internal Photo generatePhoto(Comment log)
        {
                var httpContextMock = new Mock<HttpContextBase>();
    var serverMock = new Mock<HttpServerUtilityBase>();
    serverMock.Setup(x => x.MapPath("~/app_data")).Returns(@"c:\work\app_data");
    httpContextMock.Setup(x => x.Server).Returns(serverMock.Object);

    var file1Mock = new Mock<HttpPostedFileBase>();
    file1Mock.Setup(x => x.FileName).Returns("file1.pdf");
    var file2Mock = new Mock<HttpPostedFileBase>();
    file2Mock.Setup(x => x.FileName).Returns("file2.doc");
    var files = new[] { file1Mock.Object, file2Mock.Object };

            var animals = new List<Animal>() { new Animal() { ID = 1 } };
            var vessels = new List<Vessel>() { new Vessel() { ID = 1 } };
            var crew = new List<Crew>() { new Crew() { ID = 7 }, new Crew() { ID = 8 } };
            var weather = new List<Weather>() { new Weather() { ID = 3 } };
            var maneuvers = new List<Maneuver>() { new Maneuver() { ID = 2 } };
            Photo result = new Photo()
                               {
                                   Path = file1Mock.Object.FileName,
                                   Name = UnitTestHelpers.GetRandomString(12),
                                   EntityName = UnitTestHelpers.GetRandomString(8),
                                   IsTest = true,
                                   Animals = animals,
                                   Vessels = vessels,
                                   Crew = crew,
                                   Comments = new List<Comment>() { log },
                                   Weather = weather,
                                   Maneuvers = maneuvers,
                               };
            return result;
        }

        internal Sail generateSail(Vessel vessel=null, Crew crew=null)
        {
            var location = new Location();
            location.ID = 2;
            vessel = vessel ?? new Vessel() { ID = 1 };
            List<CrewFoundation> list = new List<CrewFoundation>();
            List<Maneuver> maneuvers = new List<Maneuver>() { new Maneuver() { ID = 1 }, new Maneuver() { ID = 2 } };
            List<Weather> weather = new List<Weather>() { new Weather() { ID = 1 }, new Weather() { ID = 2 } };
            List<Configuration> configs = new List<Configuration>() { new Configuration() {ID=0}, new Configuration() {ID=8} };
            if (crew == null)
            {
                list.Add(new CrewFoundation { ID = 7 });
                list.Add(new CrewFoundation { ID = 8 });
            }
            else
            {
                list.Add(crew);
            }

            Sail result = new Sail
            {
                Barometer = random.Next(27,31),
                Date = DateTime.Now,
                Location = location,
                Vessel = vessel,
                TempF = random.Next(45, 97),
                Weather = weather,
                Crew = list,
                GeneralObservations = UnitTestHelpers.GetRandomString(random.Next(45,255)),
                MinWind = 9,
                Maneuvers = maneuvers,
                Tilt = 5,
                Created = DateTime.Now,
                CreatedBy = new CrewFoundation() {ID=7},
                MaxWind = 32,
                SeaState = random.Next(1,10),
                MilesSailed = random.Next(0,10),
                WindDirection = 0,
                HoursSailed = 2,
                IsTest = true
            };

            return result;
        }

        internal static Random random = new Random();
    }
}
