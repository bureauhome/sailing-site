﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IManeuverableModel.cs" company="">
//   
// </copyright>
// <summary>
//   The ManeuverableModel interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The ManeuverableModel interface.
    /// </summary>
    public interface IManeuverableModel : IEntityModel
    {
        /// <summary>
        /// Gets or sets the maneuvers.
        /// </summary>
        IEnumerable<Maneuver> Maneuvers { get; set; }
    }
}
