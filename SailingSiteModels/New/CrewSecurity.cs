﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    public class CrewSecurity :CrewFoundation
    {
        public CrewSecurity()
        {
        }

        public IEnumerable<VesselFoundation> VesselsCaptained { get; set; }

        public IEnumerable<VesselFoundation> VesselsCrewed { get; set; }

        public IEnumerable<VesselFoundation> VesselTotal { get; set; }

        public IEnumerable<FleetBase> Fleets { get; set; } 

        public IEnumerable<FleetBase> FleetsAdmined { get; set; }

        public Crew.SIDEnum SID { get; set; }

        public string Token { get; set; }

        public string Name;

    }
}
