﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDatabaseModel.cs" company="">
//   
// </copyright>
// <summary>
//   The DatabaseModel interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    /// <summary>
    /// The DatabaseModel interface.
    /// </summary>
    public interface IDatabaseModel
    {
        /// <summary>
        /// Gets the primary key.
        /// </summary>
        object[] PrimaryKey { get; }
    }
}
