﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VesselFoundation.cs" company="">
//   
// </copyright>
// <summary>
//   The vessel foundation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.New
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Configuration;

    using SailingSiteModels.Common;

    /// <summary>
    /// The vessel foundation.
    /// </summary>
    public class VesselFoundation :IEntityModel
    {

        /// <summary>
        /// Gets the controller name.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return "Vessel";
            }
        }

        public bool IsAssociated { get; set; }

        /// <summary>
        /// The name of the vessel.
        /// </summary>
        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        public int NewsFeedRank { get; set; }

        /// <summary>
        /// Gets or sets the rig.
        /// </summary>
        public Vessel.RigTypes? Rig { get; set; }

        /// <summary>
        /// The length of the vessel.
        /// </summary>
        public double Length { get; set; }

        /// <summary>
        /// Gets or sets the hull.
        /// </summary>
        public Vessel.HullTypes? Hull { get; set; }

        /// <summary>
        /// Gets or sets the captain.
        /// </summary>
        public CrewFoundation Captain { get; set; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        public string Description
        {
            get
            {
                var description = Name;

                if (this.Captain != null)
                {
                    description += string.Format(", Skipper: {0}", this.Captain.FullName());
                }

                return description;
            }
            set
            {
                this.Description = value;
            }

        }

        public virtual Vessel ToVessel()
        {
            return new Vessel()
            {
                ID = this.ID,
                Rig = this.Rig,
                Name = this.Name,
                Length=this.Length,
                Description = this.Description,
                Captain = this.Captain,
                Hull = this.Hull
            };
        }

        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is VesselFoundation))
            {
                return false;
            }
            return this.ID == ((VesselFoundation)obj).ID;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode();
        }
    }
}
