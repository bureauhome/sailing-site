﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMetadataRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The MetadataRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Repositories.Interfaces
{
    using SailingSite.Models;

    /// <summary>
    /// The MetadataRepository interface.
    /// </summary>
    public interface IMetadataRepository
    {
        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string Get(string key);

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Insert(MetaData entity);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Update(MetaData entity);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        void Delete(string key);

    }
}
