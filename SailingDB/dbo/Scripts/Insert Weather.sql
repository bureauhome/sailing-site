﻿USE [Sailing]
GO

SET IDENTITY_INSERT [dbo].[Entities.Weather] ON

INSERT INTO [dbo].[Entities.Weather]
           ([WeatherID]
		   ,[Description]
           ,[IconPath]
           ,[Value])
     VALUES
           (0,'Clear'
           ,'../icons/clear.png'
           ,0),
		    (1,'Partly Cloudy'
           ,'../icons/pcloudy.png'
           ,1),
		    (2,'Overcast'
           ,'../icons/cloudy.png'
           ,2),
		   (3,'Rain'
           ,'../icons/rain.png'
           ,3),
		   (4,'Sleet'
           ,'../icons/sleet.png'
           ,4),
		    (5,'Hail'
           ,'../icons/hail.png'
           ,5),
		   (6,'Snow'
           ,'../icons/snow.png'
           ,6),
		      (7,'Snow'
           ,'../icons/snow.png'
           ,7)
SET IDENTITY_INSERT [dbo].[Entities.Weather] OFF;