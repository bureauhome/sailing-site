﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Factories
{
    using System.Data.Linq.Mapping;

    using SailingSiteModels.New;

    public class FleetFactory
    {
        public static SailingSiteModels.New.VesselFoundation CreateVesselFoundation(Entities_Vessel entity)
        {
            if (entity != null)
            {
                SailingSiteModels.New.Vessel.RigTypes r;
                Enum.TryParse(entity.Rig, out r);
                Vessel.HullTypes h;
                Enum.TryParse(entity.HullType, out h);
                return new SailingSiteModels.New.VesselFoundation()
                {
                    Name = entity.Name,
                    ID = entity.VesselID,
                    Hull = h,
                    Captain = entity.Entities_Crew == null ? null : CrewFactory.CreateCrewBase(entity.Entities_Crew),
                    Length = (double)entity.Length,
                    Rig = r
                };
            }
            return null;
        }

       public static Entities_Vessel CreateVessel(SailingSiteModels.New.Vessel entity)
        {

             if (entity != null)
            {
        
                var result = new Entities_Vessel
                {
                    Name = entity.Name,
                    Beam = entity.Beam,
                    VesselID = entity.ID,
                    Length = (decimal)entity.Length,
                    HullColor = entity.HullColors,
                    HullNumbers = entity.HullNumbers,
                    LetterColor = entity.LetteringColors,
                    Rig = entity.Rig != null ? entity.Rig.ToString() : null,
                    Keel=entity.Keel !=null? entity.Rig.ToString():null,
                    LengthWaterline = entity.LengthWaterLine,
                    LengthOverall = entity.LengthOverAll,
                    HullType = entity.Hull.ToString(),
                    Model = entity.Model,
                    SailColors = entity.SailColors,
                    SailNumbers = entity.SailNumbers,
                    NumberofMasts = entity.Masts,
                    DistinguishingFeatures=entity.DistinguishingFeatures,
                    Manufacturer = entity.Manufacturer,
                    TestData = entity.IsTest,
                    CreatedDate = entity.ManufacturedDate   
                };
                return result;
                 //TODO: Add hull type
            }
            return null;
        }

        // Used to ID only
        public static Entities_Vessel CreateVessel(SailingSiteModels.New.VesselFoundation entity)
        {
            if (entity != null)
            {
                var result = createVessel(entity.ID);
                return result;
            }
            return null;
        }

        private static Entities_Vessel createVessel(int id)
        {
            using (var context = new SailingEntities())
            {
                return context.Entities_Vessel.FirstOrDefault(i => i.VesselID == id);
            }
        }

        public static SailingSiteModels.New.VesselBase CreateVesselBase(Entities_Vessel entity)
        {
            if (entity != null)
            {
                var captain = entity.Entities_Crew == null
                                  ? null
                                  : Factories.CrewFactory.CreateCrewFoundation(entity.Entities_Crew);
                var locker = entity.Entities_Vessels_Sails == null || !entity.Entities_Vessels_Sails.Any()
                    ? null: entity.Entities_Vessels_Sails.Select(Factories.CommonFactory.CreateSheetSail);
                return new SailingSiteModels.New.VesselBase()
                {
                    ManufacturedDate= entity.CreatedDate,
                    Model = entity.Model,
                    Beam=entity.Beam,
                    Captain = captain,
                    SailLocker =
                        locker,
                    Name = entity.Name,
                    LengthOverAll = entity.LengthOverall,
                    LengthWaterLine = entity.LengthWaterline,
                    ID = entity.VesselID
                };
            }
            return null;
        }

        public static SailingSiteModels.New.Vessel CreateVessel(Entities_Vessel entity)
        {
            // TODO: Condense from CreateVesselBase and DRY
            if (entity != null)
            {
                var captain=entity.Entities_Crew==null?null:Factories.CrewFactory.CreateCrewFoundation(entity.Entities_Crew);
                var editors = entity.Entities_Crew2 == null ? new List<SailingSiteModels.New.CrewFoundation>() : entity.Entities_Crew2.Select(Factories.CrewFactory.CreateCrewFoundation).ToList();
                editors.Add(captain);
                SailingSiteModels.New.Vessel.KeelTypes k;
                Enum.TryParse(entity.Keel, out k);
                SailingSiteModels.New.Vessel.RigTypes r;
                Enum.TryParse(entity.Rig, out r);
                List<SailingSiteModels.New.SailBase> sailLog = null;
                Vessel.HullTypes h;
                Enum.TryParse(entity.HullType, out h);
                var comments = entity.Entities_Comment == null || !entity.Entities_Comment.Any()
                                   ? null
                                   : entity.Entities_Comment.Select(CommonFactory.CreateCommentBase).ToArray();
                if (entity.Logs_Sails!=null&&entity.Logs_Sails.Any())
                {
                    sailLog=entity.Logs_Sails.Select(Factories.SailFactory.CreateSailbase).ToList();
                }
                var result = new SailingSiteModels.New.Vessel
                {
                    Captain=captain,
                    Name = entity.Name,
                    Editors=editors,
                    SailLocker =
                       entity.Entities_Vessels_Sails == null || !entity.Entities_Vessels_Sails.Any()
                           ? null
                           : entity.Entities_Vessels_Sails.Select(Factories.CommonFactory.CreateSheetSail),
                    Beam = entity.Beam,
                    ID = entity.VesselID,
                    Comments = comments,
                    Length = (double)entity.Length,
                    LengthOverAll = entity.LengthOverall,
                    LengthWaterLine = entity.LengthWaterline,
                    HullColors = entity.HullColor,
                    HullNumbers = entity.HullNumbers,
                    LetteringColors = entity.LetterColor,
                    Manufacturer=entity.Manufacturer,
                    Model = entity.Model,
                    Hull=h,
                    SailColors = entity.SailColors,
                    SailNumbers = entity.SailNumbers,
                   IsTest = entity.TestData==true,
                   NumberOfMasts = entity.NumberofMasts==null?0:(int)entity.NumberofMasts,
                    Keel=k,
                    Rig=r,
                    Masts = entity.NumberofMasts==null?0:(int)entity.NumberofMasts,
                    Photos = entity.Entities_Photos1 == null ? null : entity.Entities_Photos1.Select(Factories.PhotoFactory.CreatePhotoBase),
                    ManufacturedDate = entity.CreatedDate,
                    Sails=sailLog,
                    DistinguishingFeatures=entity.DistinguishingFeatures,
                    Photo =
                        entity.Entities_Photos == null
                            ? null
                            : Factories.PhotoFactory.CreatePhotoBase(entity.Entities_Photos)
                };

                return result;
            }
            return null;
        }
        
    

        public static Entities_Fleet CreateFleet(SailingSiteModels.New.Fleet entity)
        { 
            if (entity!=null)
            {
                Entities_Crew captain = null;
                if (entity.Captain != null && entity.Captain.ID > 0)
                {
                    captain = new Entities_Crew
                    {
                        CrewID = entity.Captain.ID
                    };
                }
            var result= new Entities_Fleet()
            {
               Entities_Crew=captain,
               FleetName=entity.Name,
               FleetID=entity.ID,
               DateFounded=entity.FoundedDate,
               Description=entity.Description,
               TestData=entity.IsTest

            };
            if (captain != null)
            {
                result.CaptainID = captain.CrewID;
            }
                return result;
            }
            return null;
        }

            public static SailingSiteModels.New.FleetBase CreateFleetBase(Entities_Fleet entity)
    {
        if (entity != null)
        {
            return new SailingSiteModels.New.FleetBase()
            {
                ID = entity.FleetID,
                Name = entity.FleetName,
                
            };
        }
        return null;
    
    }

       public static SailingSiteModels.New.Fleet CreateFleet(Entities_Fleet entity)
        { 
            if (entity!=null)
            {
                IEnumerable<SailingSiteModels.New.CrewFoundation> crew = null;
            IEnumerable<SailingSiteModels.New.VesselEntry> vessels = null;
            if (entity.Logs_Fleet_Crew!=null&&entity.Logs_Fleet_Crew.Any())
            {
                var dbCrew=(from c in entity.Logs_Fleet_Crew select c.Entities_Crew);
                if (dbCrew != null)
                {
                    crew = dbCrew.Select(Factories.CrewFactory.CreateCrewFoundation).ToList();
                }
            }

            if (entity.Logs_Fleet_Vessel!=null&&entity.Logs_Fleet_Vessel.Any())
            {
                var dbvesselentries=(from v in entity.Logs_Fleet_Vessel select v).ToArray();
                if (dbvesselentries != null)
                {
                    vessels = dbvesselentries.Select(Factories.FleetFactory.CreateVesselEntry);
                }
            }
            List<SailingSiteModels.New.Tags> tags = new List<SailingSiteModels.New.Tags>();

            if (entity.Entities_Tags != null & entity.Entities_Tags.Any())
            {
                tags = entity.Entities_Tags.Select(Factories.CommonFactory.CreateTag).ToList();
            }

            var result = new SailingSiteModels.New.Fleet(CreateFleetBase(entity));

            if (result!=null)
            {
                if (entity.Entities_Crew != null)
                {
                    result.Captain = Factories.CrewFactory.CreateCrewFoundation(entity.Entities_Crew);
                }
                result.AssignedCrew = (crew!=null&&crew.Any()) ? crew.ToList() : null;
                result.VesselEntries = vessels != null && vessels.Any() ? vessels.ToList() : null;
                result.Tags = tags;
                result.IsTest = entity.TestData == true;
                result.Description = entity.Description;
                if (entity.DateFounded != null)
                {
                    result.FoundedDate = entity.DateFounded;
                }

            };
                return result;
            }
            return null;
        }

        public static VesselEntry CreateVesselEntry(Logs_Fleet_Vessel entity)
        {
            var result=new VesselEntry();
            result.Vessel = CreateVesselBase(entity.Entities_Vessel);
            result.NickName = entity.Nickname;
            result.Ordinal = entity.Ordinal > 0 ? (int)entity.Ordinal : 0;
            result.DateAdded = entity.DateAdded != null ? (DateTime)entity.DateAdded : DateTime.Today;
            return result;
        }

        public static Logs_Fleet_Vessel CreateVesselEntry(VesselEntry entity)
        {
            var result=new Logs_Fleet_Vessel();
            result.DateAdded = entity.DateAdded != null ? (DateTime)entity.DateAdded : DateTime.Today;
            result.Nickname = entity.NickName;
            result.Ordinal = entity.Ordinal > 0 ? entity.Ordinal : 0;
            return result;
        }
    }
}