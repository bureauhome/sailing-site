﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models.FeedModels
{
    using SailingSiteModels.Common;

    public class NewsFeed
    {
        public NewsFeed()
        {
            this.Feed = new List<INewsFeedable>();
        }

        public IEnumerable<INewsFeedable> Feed { get; set; }
    }
}