﻿CREATE TABLE [dbo].[Entities.Weather] (
    [WeatherID]   INT            IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (32)  NOT NULL,
    [IconPath]    NVARCHAR (256) NOT NULL,
    [Value]       INT            NULL,
    CONSTRAINT [PK_Entities.Weather] PRIMARY KEY CLUSTERED ([WeatherID] ASC)
);

