﻿CREATE TABLE [dbo].[Logs_Comments_Comments] (
    [CommentID]       INT NOT NULL,
    [ParentCommentID] INT NOT NULL,
    CONSTRAINT [PK_Logs_Comments_Comments] PRIMARY KEY CLUSTERED ([CommentID] ASC, [ParentCommentID] ASC),
    CONSTRAINT [FK_Logs_Comments_Comments_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID]),
    CONSTRAINT [FK_Logs_Comments_Comments_Entities.Comment1] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID])
);

