﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models
{
    using SailingSiteModels.Common;
    using SailingSiteModels.New;

    public class CommentNewsFeedViewModel : CommentBase, INewsFeedable
    {
        public int Order { get; set; }

        public PhotoBase RandomlyChosenPhoto { get; set; }

        public string BlurbPartial
        {
            get
            {
                return "_Comment_Feed";
            }
        }


        public string Blurb
        {
            get
            {
                if (this.Message!=null&&this.Message.Length > 256)
                {
                    return string.Format("{0}...", this.Message.Substring(0, 255));
                }
                return this.Message;
            }
        }
    }
}