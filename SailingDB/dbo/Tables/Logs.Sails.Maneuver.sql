﻿CREATE TABLE [dbo].[Logs.Sails.Maneuver] (
    [ManeuverID] INT NOT NULL,
    [SailID]     INT NOT NULL,
    CONSTRAINT [PK_Logs.Sails.Maneuver] PRIMARY KEY CLUSTERED ([ManeuverID] ASC, [SailID] ASC),
    CONSTRAINT [FK_Logs.Sails.Maneuver_Entities.Maneuvers] FOREIGN KEY ([ManeuverID]) REFERENCES [dbo].[Entities.Maneuvers] ([ManeuverID]),
    CONSTRAINT [FK_Logs.Sails.Maneuver_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID]) ON DELETE CASCADE
);

