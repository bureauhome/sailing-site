﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEntityModel.cs" company="">
//   
// </copyright>
// <summary>
//   The EntityModel interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    /// <summary>
    /// The EntityModel interface.
    /// </summary>
    public interface IEntityModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Gets the controller name.
        /// </summary>
        string ControllerName { get; }

        string DefaultDelete { get; }

        string DefaultSet { get; }
    }
}
