﻿CREATE TABLE [dbo].[Entities.Chart.Type] (
    [ChartType] NVARCHAR (50) NOT NULL,
    [MinHeight] INT           NULL,
    CONSTRAINT [PK_Entities.Chart.Type] PRIMARY KEY CLUSTERED ([ChartType] ASC)
);

