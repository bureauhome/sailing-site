﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using System.Collections;
    using System.Data.Entity;
    using System.Data.Entity.Validation;

    using New;

    using SailingSite.Factories;
    using SailingSite.Models;
    using SailingSite.Repositories.Interfaces;

    using SailingSiteModels.Common;

    public class LocationRepository : ILocationRepository
    {
        public IEnumerable<New.LocationBase> GetAll(object search=null)
        {
            LocationSearch query = null;
            if (search is LocationSearch)
            {
                query = search as LocationSearch;
            }
            IEnumerable <New.LocationBase> result= null;
            using (var context = new SailingEntities())
            {
                var dbquery = context.Entities_Locations.Where(i => i.DateDeleted==null);
                if (query != null)
                {
                    if (query.City != null)
                    {
                        dbquery = dbquery.Where(i => i.MinorLocation == query.City);
                    }
                    if (query.Region != null)
                    {
                        dbquery = dbquery.Where(i => i.MajorLocation == query.Region);
                    }
                    if (query.LocationType != null)
                    {
                        dbquery = dbquery.Where(i => i.LocationType == query.LocationType.ID);
                    }
                }
               var entities=dbquery.Distinct().ToArray();
                if (entities != null)
                {
                    result = entities.Select(Factories.SailFactory.CreateLocationBase);
                }
                return result;
            }
       
        }

        public New.Location Get(int id)
        {
            New.Location result = null;
            using (var context = new SailingEntities())
            {
                var entity = context.Entities_Locations.Where(i => i.LocationID == id).Include(i=>i.Entities_Photos).Include(i=>i.Entities_Photos1).Include(i=>i.Entities_Tags).Include(i=>i.Entities_Comment).FirstOrDefault();
                if (entity != null)
                {
                    result= Factories.SailFactory.CreateLocation(entity);
                }
                return result;
            }
        }

        public int Create(New.Location entity)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                var dbEntity = Factories.CommonFactory.CreateLocation(entity);
                context.Entities_Locations.Add(dbEntity);
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException exception)
                {
                    var ex = exception.EntityValidationErrors;
                }
                result = dbEntity.LocationID;
            }
            return result;
        }


        public IEnumerable<New.Tags> GetTags(int id)
        {
            using (var context = new SailingEntities())
            {
                IEnumerable<New.Tags> result = null;
                var entities =
                    context.Entities_Locations.Where(i => i.LocationID == id).Select(i => i.Entities_Tags).FirstOrDefault();
                if (entities != null&&entities.Any())
                {
                    result = entities.Select(Factories.CommonFactory.CreateTag);
                }

                return result;
            }
           
        }

        public void AddTags(List<int> tagIds, int id)
        {
            using (var context = new SailingEntities())
            {
                List<Entities_Tags> tags = new List<Entities_Tags>();
                foreach (var tagId in tagIds)
                {
                    var tag = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                    if (tag != null && tag.Name != null)
                    {
                        tags.Add(tag);
                    }
                }

                var location = (from i in context.Entities_Locations where i.LocationID == id select i).FirstOrDefault();
                if (location != null && location.LocationID>0)
                {
                    foreach (var tag in tags)
                    {
                        var match = location.Entities_Tags.Where(i => i.TagID == tag.TagID).FirstOrDefault();
                        if (match == null || match.TagID <= 0)
                        {
                            location.Entities_Tags.Add(tag);
                        }
                    }
                }
                context.SaveChanges();

            }
        }

        public void DeleteTag(int tagId, int id)
        {
            using (var context = new SailingEntities())
            {
                var tag = context.Entities_Tags.FirstOrDefault(i => i.TagID == tagId);
                var location = context.Entities_Locations.FirstOrDefault(i => i.LocationID == id);
                if (tag != null && location != null)
                {
                    location.Entities_Tags.Remove(tag);
                    context.SaveChanges();
                }
            }
        }

        public void AddComment(int commentID, int entityID)
        {
            using (var context = new SailingEntities())
            {
                var comment = context.Entities_Comment.FirstOrDefault(i => i.CommentID == commentID);
                var location = context.Entities_Locations.FirstOrDefault(i => i.LocationID == entityID);
                if (comment != null & location != null)
                {
                    location.Entities_Comment.Add(comment); 
                    context.SaveChanges();
                }
            }
        }


        public bool IsUnique(New.Location entity, out List<string> errorMessage)
        {
            bool result = true;
            errorMessage = new List<string>();
            using (var context = new SailingEntities())
            {
                if (entity.Name != null)
                {
                    var parsedName = entity.Name.ToLower().Trim().Replace(" ", "");
                    var dbentity =
                        context.Entities_Locations.FirstOrDefault(
                            i =>
                            i.Name.ToLower().Trim().Replace(" ", "") == parsedName
                            && i.MinorLocation == entity.MinorLocation && i.MajorLocation == entity.MajorLocation);
                    if (dbentity != null)
                    {
                        throw new Exception(string.Format("{0} already exists in {1},{2}.",dbentity.Name,dbentity.MinorLocation,dbentity.MajorLocation));
                    }
                }
 
            }
            return result;
        }

        public void Delete(int id, int crewid)
        {
            using (var context = new SailingEntities())

            {
                var dbentity = context.Entities_Locations.FirstOrDefault(i => i.LocationID == id);
                if (dbentity != null)
                {
                    var crew = context.Entities_Crew.FirstOrDefault(i => i.CrewID == id);
                    dbentity.DateDeleted = DateTime.Now;
                    if (crew != null)
                    {
                        dbentity.DeletedBy = crew.UserName;
                    }
                    context.SaveChanges();
                }
            }
        }

        public void DeleteLandmark(int id, int crewid)
        {
            using (var context = new SailingEntities())
            {
                var dbentity = context.Entities_LandMark.FirstOrDefault(i => i.Id == id);
                if (dbentity != null)
                {
                    var crew = context.Entities_Crew.FirstOrDefault(i => i.CrewID == id);
                    dbentity.DateDeleted = DateTime.Now;
                    if (crew != null)
                    {
                        dbentity.DeletedBy = crew.UserName;
                    }
                    context.SaveChanges();
                }
            }
        }
    

        public void Update(New.Location entity)
        {
            using (var context = new SailingEntities())
            {
                var dbentity = context.Entities_Locations.FirstOrDefault(i => i.LocationID == entity.ID);
                if (dbentity != null)
                {
                    dbentity.Name = entity.Name;
                    dbentity.Latitude = entity.Latitude;
                    dbentity.Longitude = entity.Longitude;

                    dbentity.MajorLocation = entity.MajorLocation;
                    dbentity.MinorLocation = entity.MinorLocation;
                    if (entity.Location != null)
                    {
                        dbentity.LocationType = entity.Location.ID;
                    }
                    context.SaveChanges();
                }
            }
        }



//public IEnumerable<New.CommentBase> GetComments(IEnumerable<int> entityID)
//{
//    IEnumerable<New.CommentBase> result = null;
//    using (var context = new SailingEntities())
//    {
//        var entities =
//            context.Entities_Locations.Where(i => entityID.Contains(i.LocationID))
//                .Select(i => i.Entities_Comment)
//                .FirstOrDefault();
//        if (entities != null)
//        {
//            result = entities.Select(CommonFactory.CreateCommentBase);
//        }
//        return result;
//    }
//}

public void AddPhoto(int photoID, int entityId)
{
    using (var context = new SailingEntities())
    {
        var photo = context.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoID);
        var entity = context.Entities_Locations.FirstOrDefault(i => i.LocationID == entityId);
        if (entity != null && photo != null)
        {
             entity.Entities_Photos1.Add(photo);
            context.SaveChanges();
        }
    }
}

public void DeletePhoto(int photoID, int entityId)
{
    using (var context = new SailingEntities())
    {
        var photo = context.Entities_Photos.FirstOrDefault(i => i.PhotoID == photoID);
        var entity = context.Entities_Locations.FirstOrDefault(i => i.LocationID == entityId);
        if (entity != null && photo != null)
        {
            entity.Entities_Photos1.Remove(photo);
            context.SaveChanges();
        }
    };
}


public IEnumerable<int> GetCommentIDsForSail(IEnumerable<int> entityID)
{
    IEnumerable<int> result = null;
    using (var context = new SailingEntities())
    {
        result =
            context.Entities_Locations.Where(i => entityID.Contains(i.LocationID))
                .Select(i => i.Entities_Comment.Select(j => j.CommentID)).FirstOrDefault().ToArray();
    }
    return result;
}

public IEnumerable<New.LandMarkBase> GetLandMarks()
{
    IEnumerable<New.LandMarkBase> results = null;
    using (var context = new SailingEntities())
    {
        var query = context.Entities_LandMark.Where(i => i.Test != true&&i.DateDeleted == null).ToArray();
        if (query != null)
        {
            results = query.Select(Factories.CommonFactory.CreateLandMarkBase);
        }
        return results;
    }
}

        /// <summary>
        /// The get land mark base.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="LandMarkBase"/>.
        /// </returns>
        public New.LandMarkBase GetLandMarkBase(int id)
        {
            New.LandMarkBase result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_LandMark.FirstOrDefault(i => i.Id==id);
                if (query != null)
                {
                    result = Factories.CommonFactory.CreateLandMarkBase(query);
                }
            }
            return result;
        }


        public IEnumerable<New.LandMarkBase> SearchLandmarks(GenericQuery request)
        {
            IEnumerable<LandMarkBase> result = null;
            using (var context = new SailingEntities())
            {
                var query =
                    context.Entities_LandMark.Where(i => i.Name != null);
                if (request.SelectDeleted != null)
                {
                    query = query.Where(i => i.DateDeleted == null);
                }
                if (!string.IsNullOrEmpty(request.FirstInitial))
                {
                    query = query.Where(i => i.Name.StartsWith(request.FirstInitial));
                }

                if (!string.IsNullOrEmpty(request.Name))
                {
                    query = query.Where(i => i.Name == request.Name);
                }

                if (query != null)
                {
                    result = query.Select(CommonFactory.CreateLandMarkBase).ToArray();
                }
            }

            return result;
        }

        public List<string> GetLandMarkFirstInitials()
        {
            List<string> result = new List<string>();
            using (var context = new SailingEntities())
            {
                result = context.Entities_LandMark.Where(i => !string.IsNullOrEmpty(i.Name)).Select(i => i.Name.Substring(0, 1).ToUpper()).Distinct().ToList();
            }
            return result;
        }

        public IEnumerable<New.LocationBase> SearchLocations(GenericQuery request)
        {
            IEnumerable<LocationBase> result = null;
            using (var context = new SailingEntities())
            {
                var query = context.Entities_Locations.Where(i => i.Name != null);
                if (request.SelectDeleted != true)
                {
                    query = query.Where(i => i.DateDeleted == null);
                }

                if (!string.IsNullOrEmpty(request.FirstInitial))
                {
                    query = query.Where(i => i.Name.StartsWith(request.FirstInitial));
                }

                if (!string.IsNullOrEmpty(request.Name))
                {
                    query = query.Where(i => i.Name == request.Name);
                }

                if (query != null)
                {
                    result = query.Select(CommonFactory.CreateLocationBase).ToArray();
                }
            }

            return result;
        }

        public List<string> GetLocationFirstInitials()
        {
            List<string> result = new List<string>();
            using (var context = new SailingEntities())
            {
                result = context.Entities_Locations.Where(i => !string.IsNullOrEmpty(i.Name)).Select(i => i.Name.Substring(0, 1).ToUpper()).ToList();
            }
            return result;
        }


        public int CreateLandmark(LandMarkBase entity)
        {
            int result = 0;
            using (var context = new SailingEntities())
            {
                var dbEntity = Factories.CommonFactory.CreateLandMark(entity);
                context.Entities_LandMark.Add(dbEntity);
                context.SaveChanges();
                result = dbEntity.Id;
            }
            return result;
        }


        public void UpdateLandmark(LandMarkBase entity)
        {
            if (entity != null && entity != null)
            {
                using (var context = new SailingEntities())
                {
                    var dbEntity = context.Entities_LandMark.FirstOrDefault(i => i.Id == entity.ID);
                    dbEntity.Name = entity.Name;
                    dbEntity.Description = entity.Description;
                    dbEntity.Test = entity.IsTest;
                    dbEntity.Longitude = entity.Longitude;
                    dbEntity.Latitude = entity.Latitude;
                    dbEntity.Altitude = entity.Altitude;
                    context.SaveChanges();
                }
            }
        }
    }
}