﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Services
{
    using System.Security.Cryptography;

    using SailingSite.Common;
    using SailingSite.Repositories;
    using SailingSite.Repositories.Interfaces;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    public abstract class ServiceBase : BaseStructure
    {

        private Repositories.Interfaces.ILocationRepository locationRepository = null;
        private Repositories.iFleetRepository fleetRepository=null;  
        private Repositories.ISailRepository sailRepository=null;
        private Repositories.IVesselRepository vesselRepository=null;

        private IAnimalRepository animalRepository = null;
        private Repositories.ICrewRepository crewRepository = null;

        private ILoggingService loggingService;

        private ILogRepository loggingRepository;
        private Repositories.ICommentRepository commentRepository = null;

        private IMetadataRepository metadataRepository;

        private IAdminService adminService;

        public IAdminService AdminService
        {
            get
            {
                if (this.adminService == null)
                {
                    this.adminService = new AdminService();
                }
                return this.adminService;
             
            }
            set
            {
                this.adminService = value;
            }
        }

        public IMetadataRepository MetadataRepository 
        {
            get
            {
                if (metadataRepository == null)
                {
                    metadataRepository=new MetadataRepository();
                }
                return this.metadataRepository;
            }
            set
            {
                this.metadataRepository = value;
            }
        }

        public IAnimalRepository AnimalRespository
        {
            get
            {
                if (this.animalRepository == null)
                {
                    animalRepository=new AnimalRepository();
                }
                return this.animalRepository;
            }
            set
            {
                this.animalRepository = value;
            }
        }

        public ILoggingService LoggingService
        {
            get
            {
                if (loggingService == null)
                {
                    loggingService=new LoggingService();
                }
                return this.loggingService;
            }
            set
            {
                this.loggingService = value;
            }
        }

        public ILogRepository LoggingRepository
        {
            get
            {
                if (this.loggingRepository == null)
                {
                    this.loggingRepository = new LoggingRepository();
                }
                return this.loggingRepository;
            }

            set
            {
                this.loggingRepository = value;
            }
        }

        public Repositories.ICrewRepository CrewRepository
        {
            get
            {
                if (crewRepository == null)
                {
                    crewRepository=new CrewRepository();
                }
                return crewRepository;
            }

            set
            {
                crewRepository = value;
            }

        }


        public Repositories.ICommentRepository CommentRepository
        {
            get
            {
                if (commentRepository == null)
                {
                    commentRepository=new CommentRepository();
                }
                return commentRepository;
            }
            set
            {
                commentRepository = value;
            }

        }

        public Repositories.Interfaces.ILocationRepository LocationRepository
        {
            get
            {
                if (locationRepository == null)
                {
                    locationRepository=new LocationRepository();
                }
                return locationRepository;
            }
            set
            {
                locationRepository = value;
            }

        }

        public Repositories.iFleetRepository FleetRepository {
            get {
            if(fleetRepository==null)
            {
                fleetRepository=new Repositories.FleetRepository();
            }
                return fleetRepository;

            }
        }

        public Repositories.ISailRepository SailRepository
        {
            get
            {
                if (sailRepository == null)
                {
                    sailRepository=new Repositories.SailRepository();
                }
                return sailRepository;
            }
            set
            {
                sailRepository = value;
            }
        }

        public Repositories.IVesselRepository VesselRepository
        {
            get
            {
                if (vesselRepository == null)
                {
                    vesselRepository=new VesselRepository();
                }
                return vesselRepository;
            }

            set
            {
                vesselRepository = value;
            }
        }

        protected void LogUserEvent(string description, AuditContext context)
        {
            this.LoggingService.GenerateLog(description, Log.LogTypes.Audit, context);
        }

        protected void LogUserEvent(string description, AuditContext context, object obj)
        {
            this.LoggingService.GenerateLog(description, Log.LogTypes.Audit, context, obj);
        }

        protected void LogMaintenanceEvent(string description)
        {
            AuditContext context = new AuditContext { ActiveCrew = new CrewFoundation { UserName = "System"} };
            this.LoggingService.GenerateLog(description, Log.LogTypes.Maintenance, context);
        }

    }
}