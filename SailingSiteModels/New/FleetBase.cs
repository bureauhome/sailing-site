﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SailingSiteModels.New
{
    using SailingSiteModels.Common;

    /// <summary>
    /// The base model of the fleet.
    /// </summary>
    public class FleetBase : IEntityModel
    {
        public int ID { get; set; }
        [StringLength(50,ErrorMessage = "The name field must be 50 or fewer characters."), Required(ErrorMessage = "The name field is required.")]
        public string Name { get; set; }
        public int? VesselCount { get; set; }
        public int? CrewCount { get; set; }
        public decimal? MileCount { get; set; }

        public string ControllerName
        {
            get
            {
                return "Fleet";
            }
        }

        public object[] PrimaryKey
        {
            get
            {
                return new object[] { 
                    this.ID
                };
            }
        }
        public string DefaultDelete
        {
            get { return "Delete"; }
        }

        public string DefaultSet
        {
            get
            {
                return "New";
            }
        }
    }
}
