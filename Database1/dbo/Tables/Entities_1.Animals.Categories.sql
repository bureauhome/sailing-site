﻿CREATE TABLE [dbo].[Entities.Animals.Categories] (
    [AnimalCategory] NVARCHAR (50) NOT NULL,
    [RowID]          INT           IDENTITY (1, 1) NOT NULL,
    [ParentCategory] NVARCHAR (50) NULL,
    [Description]    NCHAR (10)    NULL,
    [ImageID]        INT           NULL,
    [DateToDelete]   DATETIME      NULL,
    CONSTRAINT [PK_Entities.Animals.Categories] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Entities.Animals.Categories] PRIMARY KEY CLUSTERED ([AnimalCategory] ASC),
    CONSTRAINT [FK_Entities.ParentKey] FOREIGN KEY ([ParentCategory]) REFERENCES [dbo].[Entities.Animals.Categories] ([AnimalCategory]),
    CONSTRAINT [FK_PhotoID] FOREIGN KEY ([ImageID]) REFERENCES [dbo].[Entities.Photos] ([PhotoID])
);

