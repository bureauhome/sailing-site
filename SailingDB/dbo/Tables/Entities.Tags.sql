﻿CREATE TABLE [dbo].[Entities.Tags] (
    [TagID]       INT           IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50) NOT NULL,
    [Description] NCHAR (10)    NULL,
    [CrewID]      INT           NOT NULL,
    [Created]     DATETIME      NOT NULL,
    [DateDeleted] DATETIME      NULL,
    CONSTRAINT [pk_TagID] PRIMARY KEY CLUSTERED ([TagID] ASC),
    CONSTRAINT [FK_CrewID] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID])
);

