﻿namespace SailingSiteModels.Common
{
    using System;

    using SailingSiteModels.New;

    public interface IMetadata
    {
        DateTime Created { get; set; }
        CrewFoundation CreatedBy { get; set; }
    }
}