﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace SailingSiteModels.New
{
    using System.ComponentModel.DataAnnotations;

    using SailingSiteModels.Common;

    public class LocationType
    {
        public string Name { get; set; }
        public string RegionUnit { get; set; }
        public string SecondaryRegionUnit { get; set; }
        public int ID { get; set; }
       
    }

    /// <summary>
    /// The base model of the location
    /// </summary>
    public class LocationBase : ILocation
    {
        public LocationBase()
        {
        }

        public string ControllerName
        {
            get
            {
                return "Location";
            }
        }

        /// <summary>
        /// Gets or sets the minor location.
        /// </summary>
        [Required(ErrorMessage = "Please choose a minor location.")]
        public string MinorLocation { get; set; }

        /// <summary>
        /// Gets or sets the major location.
        /// </summary>
        [Required(ErrorMessage = "Please choose a major location.")]
        public string MajorLocation { get; set; }


        private static List<LocationType> locationTypes = null;

        public static List<LocationType> LocationTypes
        {
            get
            {
                if (locationTypes == null)
                {
                    LocationType Lake = new LocationType()
                                            {
                                                ID = 1,
                                                Name = "Lake",
                                                RegionUnit = "City",
                                                SecondaryRegionUnit = "State"
                                            };
                    LocationType River = new LocationType()
                                             {
                                                 ID = 2,
                                                 Name = "River or Stream",
                                                 RegionUnit = "City",
                                                 SecondaryRegionUnit = "State"
                                             };
                    LocationType GreatLake = new LocationType()
                                                 {
                                                     ID = 3,
                                                     Name = "Great Lake",
                                                     RegionUnit = "Continent"
                                                 };
                    LocationType Ocean = new LocationType() { ID = 4, Name = "Ocean", RegionUnit = "Hemisphere" };


                    locationTypes=new List<LocationType>();
                    locationTypes.Add(Lake);
                    locationTypes.Add(River);
                    locationTypes.Add(GreatLake);
                    locationTypes.Add(Ocean);
                }
                return locationTypes;
                
            }
        }

        public LocationType Location { get; set; }
        public int ID { get; set; }
        [Required(ErrorMessage = "Location name is required"), StringLength(50, ErrorMessage = "Location name must be 50 or fewer characters.")]
        public string Name { get; set; }
        [Required (ErrorMessage = "Please choose a location from the map.")]
        public double? Longitude { get; set; }
        [Required]
        public double? Latitude { get; set; }

        public Location ToLocation()
        {
            return new Location() { ID = this.ID, Name = this.Name, Longitude = this.Longitude, Latitude = this.Latitude};
        }


    }


}
