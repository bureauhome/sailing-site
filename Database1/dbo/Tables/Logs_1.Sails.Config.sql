﻿CREATE TABLE [dbo].[Logs.Sails.Config] (
    [ConfigID] INT NOT NULL,
    [SailID]   INT NOT NULL,
    CONSTRAINT [PK_Logs.Sails.Config] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Sails.Config] PRIMARY KEY CLUSTERED ([ConfigID] ASC, [SailID] ASC),
    CONSTRAINT [FK_Logs.Sails.Config_Entities.Config] FOREIGN KEY ([ConfigID]) REFERENCES [dbo].[Entities.Config] ([ConfigID]),
    CONSTRAINT [FK_Logs.Sails.Config_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID]) ON DELETE CASCADE
);

