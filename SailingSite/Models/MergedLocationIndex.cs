﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MergedLocationIndex.cs" company="">
//   
// </copyright>
// <summary>
//   The merged location index.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Models
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The merged location index.
    /// </summary>
    public class MergedLocationIndex
    {
        /// <summary>
        /// Gets or sets the locations.
        /// </summary>
        public SearchReturnModel<IEnumerable<LocationBase>> Locations { get; set; }

        /// <summary>
        /// Gets or sets the land marks.
        /// </summary>
        public SearchReturnModel<IEnumerable<LandMarkBase>> LandMarks { get; set; } 
    }
}