﻿CREATE TABLE [dbo].[Bearings.LandMark](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LandMarkID] [int] NOT NULL,
	[BearingID] [int] NOT NULL,
 CONSTRAINT [PK_BearingsLandMark] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Bearings.LandMark]  WITH CHECK ADD  CONSTRAINT [FK_BearingsLandMark_Entities.Bearing] FOREIGN KEY([BearingID])
REFERENCES [dbo].[Entities.Bearing] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Bearings.LandMark] CHECK CONSTRAINT [FK_BearingsLandMark_Entities.Bearing]
GO

ALTER TABLE [dbo].[Bearings.LandMark]  WITH CHECK ADD  CONSTRAINT [FK_BearingsLandMark_Entities.LandMark] FOREIGN KEY([LandMarkID])
REFERENCES [dbo].[Entities.LandMark] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Bearings.LandMark] CHECK CONSTRAINT [FK_BearingsLandMark_Entities.LandMark]
GO