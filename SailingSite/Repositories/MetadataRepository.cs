﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MetadataRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The metadata repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Repositories
{
    using System.Linq;

    using SailingSite.Factories;
    using SailingSite.Models;
    using SailingSite.Repositories.Interfaces;

    /// <summary>
    /// The metadata repository.
    /// </summary>
    public class MetadataRepository : IMetadataRepository
    {
        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Get(string key)
        {
            string result = null;
            using (var context = new SailingEntities())
            {
                var entity=context.Entities_MetaData.FirstOrDefault(i => i.MetadataKey == key);
                if (entity != null)
                {
                    result = entity.Value;
                }
            }

            return result;
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Insert(MetaData entity)
        {
            if (entity != null)
            {
                using (var context = new SailingEntities())
                {
                    var insert = CommonFactory.CreateMetaData(entity);
                    context.Entities_MetaData.Add(insert);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Update(MetaData entity)
        {
            if (entity == null || entity.Key==null) 
            {
                return;
            }

            string result = null;
            using (var context = new SailingEntities())
            {
                var dbentity = context.Entities_MetaData.FirstOrDefault(i => i.MetadataKey == entity.Key);
                if (dbentity != null)
                {
                    dbentity.Value = entity.Value;
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        public void Delete(string key)
        {
            if (key == null)
            {
                return;
            }

            string result = null;
            using (var context = new SailingEntities())
            {
                var dbentity = context.Entities_MetaData.FirstOrDefault(i => i.MetadataKey == key);
                if (dbentity != null)
                {
                    context.Entities_MetaData.Remove(dbentity);
                    context.SaveChanges();
                }
            }
        }
    }
}