﻿using System;
using Resources;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SailingSite.Controllers
{
    using System.Data;
    using System.Data.Linq;
    using System.Net;

    using SailingSite.App_GlobalResources;
    using SailingSite.Models;
    using SailingSite.Repositories;
    using System.Xml.Linq;

    using SailingSite.Common;
    using SailingSite.ViewModels;

    using SailingSiteModels.Common;
    using SailingSiteModels.GPX;
    using SailingSiteModels.New;

    using SessionVariables = Resources.SessionVariables;

    /// <summary>
    /// The sail controller.
    /// </summary>
    public class SailController : BaseController
    {
        //
        // GET: /Sail/
        public ActionResult Index(int vessel)
        {
            return View();
        }


        public ActionResult Edit(int id)
        {
            var data = Sailservice.GetSail(id);
            return this.View(ActionVerbs.New, new { data });
        }

        public ActionResult Get(int id)
        {
            
            base.GetSessionVariables();
            var data = Sailservice.GetSail(id);
            if (data != null)
            {
                SetAdminVariable(data.Vessel.ID);
                var editor = false;
                if (CurrentUser != null)
                {
                    editor = Sailservice.isUserVesselCrewOrSecurity(data.Vessel.ID, this.CurrentUser.ID);
                }

                ViewData["editable"] = editor;
                return View(data);
            }
            else return this.View("NullObject");
            
        }

        private void SetAdminVariable(int vesselId)
        {
            Session[StaticTokens.Security.IsAdminToken] = this.CurrentUser != null
                                                          && this.Sailservice.isUserVesselCrewOrSecurity(
                                                              vesselId,
                                                              this.CurrentUser.ID);
        }

        public ActionResult ValidateSail(SailingSiteModels.New.Sail sail)
        {
            if (sail.Crew.Count() < 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Surely there was at least one person on the voyage.");
            }
            if (CurrentUser == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, SharedStrings.UsernotLoggedinError);
            }
            if (sail.Vessel != null || !Sailservice.isUserVesselCrewOrSecurity(sail.Vessel.ID, CurrentUser.ID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, sail.Vessel != null ? SharedStrings.UsernotinCrewError : "Please select a vesel.");
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult DeleteTrack(string trackKey)
        {
            var result = CommentService.DeleteTrack(trackKey);
            return new HttpStatusCodeResult(HttpStatusCode.OK, string.Format("{0} results purged from database.",result));
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpDelete]
        public ActionResult Delete(int id)
        {

            if (CurrentUser == null)
            {
                var redirectModel = new RedirectModel() { ActionName = "New", ControllerName = "Sail" };
                return this.View("_AdminRedirect", redirectModel);
            }

            var data = this.Sailservice.GetSail(id);
            if (data != null && data.Vessel != null)
            {
                if (this.Sailservice.isUserVesselCrewOrSecurity(data.Vessel.ID, CurrentUser.ID, true))
                {
                    this.Sailservice.DeleteSail(id, this.Context);
                    var result = this.Sailservice.GetSailsForVessel(new List<int>() {data.Vessel.ID});
                    return this.View("SailBaseTable", result);
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "You are not allowed to delete this entry.  Please contact your vessel captain.");
                }
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "The sail was not found.");
            }
        }


        public ActionResult Create(SailingSiteModels.New.Sail data, string tagList, HttpPostedFileBase file, HttpPostedFileBase gpsLog, int? slop, bool doLogAnother=false)
        {
            this.Session[SessionVariables.TemporarySailVariable] = data;
            if (CurrentUser.SID == Crew.SIDEnum.Admin)
            {
                var trouble = 1;
            }
            if (CurrentUser == null)
            {
                var redirectModel = new RedirectModel() { ActionName = "New", ControllerName = "Sail" };
                return this.View("_AdminRedirect", redirectModel);
            }

            if (CurrentUser != null||data==null||data.Vessel==null)
            {

                if (Sailservice.isUserVesselCrewOrSecurity(data.Vessel.ID, CurrentUser.ID))
                {
                    int id = 0;
                    data.CreatedBy = this.CurrentUser;
                    data.IsTest = false;
                    if (data.Vessel != null)
                    {
                        CreateCookie(SessionVariables.StandardVesselVariable, data.Vessel.ID.ToString());
                    }

                    if (data != null)
                    {
                        if (data.ID == 0)
                        {
                            id = this.Sailservice.CreateSail(data);
                        }
                        else
                        {
                            id = data.ID;
                            this.Sailservice.UpdateSail(data, this.Context);
                        }
                    }

                    if (tagList != null)
                    {
                        Sailservice.AddSailTags(id, tagList);
                    }

                    if (gpsLog != null)
                    {
                        var logFile = this.PhotoService.SaveLog(gpsLog, this.CurrentUser.ID);
                        var doc = XDocument.Load(logFile);

                        var tracks = GPXParser.GPXParser.ParseTrack(doc, slop);
                        var tlr = new List<ParsedLogResult>();
                        foreach (var track in tracks)
                        {
                            tlr.Add(this.CommentService.ParseLogs(track, id, this.CurrentUser.ID));
                        }
                        if (tlr.Any(i => i.BaseError != null))
                        {
                            Session["Errors"] = new string[] { string.Join(",", tlr.Select(i => i.BaseError)) };
                        }

                    }

                    if (file != null)
                    {
                        var photo = new SailingSiteModels.New.Photo() { Name = string.Format("S{0}V{1}", data.ID, data.Vessel.ID), FileName = file.FileName, EntityName = "Sail", Subdirectory = Resources.SessionVariables.ImageFolderSail };
                        this.AddPhoto(file, photo, null, data.ID);
                    }

                    if (doLogAnother==true)
                    {
                        Session[StaticTokens.SessionTokens.DoLogAnotherSail] = true;
                        return this.RedirectToAction(ActionVerbs.New);
                    }
                    else
                    {
                        Session.Remove(StaticTokens.SessionTokens.DoLogAnotherSail);
                        SailingSiteModels.New.Sail result = Sailservice.GetSail(id);
                        return RedirectToAction(ActionVerbs.Get, result);
                    }
                }
                else
                {

                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden, SharedStrings.UsernotinCrewError);
                }
            }

            if (CurrentUser == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden,SharedStrings.UsernotLoggedinError);
            }

            if (data == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "We appear to have received a request, but it was empty");
            }

            if (data.Vessel == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest,"Come now.  You must have sailed on SOMEthing.");
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Unknown error.");
        }

        public ActionResult New(int? id)
        {
            

            if (CurrentUser == null)
            {
                var redirectModel = new RedirectModel() { ActionName = "New", ControllerName = "Sail" };
                return this.View("_AdminRedirect", redirectModel);
            }
            Sail data = null;
            if (id == null || id == 0)
            {
                data = Sailservice.JiffySail(this.Context, this.CurrentUser.ID, true);
            }
            else
            {
                data = Sailservice.GetSail((int)id);
            }

            this.GetSessionVariables();
            return View(data);
        }

        [HttpPost]
        public ActionResult PostPhoto(int entityId, HttpPostedFileBase file, string photoName, string photoDescription)
        {
            if (CurrentUser == null)
            {
                return new HttpStatusCodeResult(
                    HttpStatusCode.Forbidden,
                    "You must be logged in to add a photo to a sail");
            }

            if (file != null)
            {
                var photo = new SailingSiteModels.New.Photo() { Name = photoName, Description=photoDescription, FileName = file.FileName, EntityName = "Sail", Subdirectory = Resources.SessionVariables.ImageFolderSail };
                this.AddPhoto(file, photo, null, entityId);
            }
            // TODO: Tie to View.
            var data = this.Sailservice.GetSail(entityId);
            return this.PartialView("PhotoControls/AddPhoto", data);
        }


        private  ActionResult AddPhoto(
            HttpPostedFileBase file,
            SailingSiteModels.New.Photo photo,
            string tagList,
            int entityId)
        {
            var vesselId=Sailservice.GetVesselIDForSail(entityId);
            if (vesselId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Unable to load crew for sail");
            }
                if (file != null)
                {
                    var sail = Sailservice.GetSail(entityId);
                    photo.Details=new PhotoDetails();
                        photo.Details.Creator = this.CurrentUser;
                        photo.Details.CreatedDate = DateTime.Now;

                        var photoid = PhotoService.SavePhoto(file, photo, this.Context, Server.MapPath("~"));
                        Sailservice.AddSailPhoto(photoid, entityId);
                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

        public ActionResult GetSeaStateDescriptor(int seastate)
        {
            string data = null;
            try
            {
                data = seastate.ToSeaStateDescriptor();
            }
            catch (Exception ex)
            {
                return this.Content("Please insert a valid number from 0-10");
            }

            return this.Content(data);
        }

        public ActionResult GetBeaufortScaleIntResult(decimal wind)
        {

            try
            {
                var data = wind.ToBeaufortScale();
                return this.Content(data.ToString());
            }
            catch (Exception ex)
            {
                return this.Content("Please input a valid wind reading.");
            }
        }

        public ActionResult GetWindColorClass(decimal wind)
        {
            var data = wind.ToWindColorClass();
            return this.Content(data);
        }

        /// <summary>
        /// The add comment.
        /// </summary>
        /// <param name="entityId">
        /// The entityid.
        /// </param>
        /// <param name="comment">
        /// The comment.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult AddComment(int entityId, SailingSiteModels.New.Comment comment)
        {
            if (CurrentUser != null)
            {
                if (comment.ID < 1)
                {
                    comment.Created = comment.Created == null ? DateTime.Now : comment.Created;
                    comment.CreatedBy =  new Crew() {ID=CurrentUser.ID};
                }
                else
                {
                    comment.Updated = DateTime.Now;
                    comment.UpdatebyID = CurrentUser.ID;
                }

                Sailservice.AddSailComment(comment, entityId, this.Context);
                ViewData["entityID"] = entityId;
                Comment data = new Comment()
                {
                    Created = comment.Created,
                    LogId = entityId,
                    IsLog = true,
                    Subject = comment.Subject,
                    TargetController = new Sail().ControllerName,
                    LogFlag = true
                };
                return this.PartialView("_AddComment", data);
            }

            return new HttpStatusCodeResult(HttpStatusCode.Conflict, "You must be logged in to post a comment");
        }

        /// <summary>
        /// The validate comment date.
        /// </summary>
        /// <param name="sailDate">
        /// The sail date.
        /// </param>
        /// <param name="commentDate">
        /// The comment date.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ValidateCommentDate(DateTime sailDate, DateTime commentDate)
        {
            if (sailDate.AddHours(-12) > commentDate)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult NewSailLog(int entityID, DateTime date)
        {
            Comment data = new Comment()
                                  {
                                      Created = date,
                                      Subject = SharedStrings.DefaultLogSubject,
                                      LogId = entityID,
                                      IsLog=true,
                                      TargetController =new Sail().ControllerName,
                                      LogFlag = true
                                  };
            return this.PartialView("_AddComment", data);
        }

    }
}