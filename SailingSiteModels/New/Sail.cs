﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.New
{
    using SailingSiteModels.Common;

    public enum CardinalDirections {NONE,N,NNW, NW, WNW, W, WSW, SW, SSW, S, SSE, SE, ESE, E, NE, ENE, NNE};

    public class CompassRose
    {
        /// <summary>
        /// The north.
        /// </summary>
        private static string[] North = { "n", "north" };

        /// <summary>
        /// The nne.
        /// </summary>
        private static string[] NNE = { "nne", "northnortheast" };

        /// <summary>
        /// The ene.
        /// </summary>
        private static string[] ENE = { "ene", "eastnortheast" };

        /// <summary>
        /// The north east.
        /// </summary>
        private static string[] NorthEast = { "ne", "northeast" };

        /// <summary>
        /// The east.
        /// </summary>
        private static string[] East = { "e", "east" };

        /// <summary>
        /// The east south east.
        /// </summary>
        private static string[] EastSouthEast = { "ese", "eastsoutheast" };

        private static string[] SE = { "se", "southeast" };

        private static string[] SSE = { "sse", "southsoutheast" };

        private static string[] South = { "s", "south" };

        private static string[] SSW = { "ssw", "southsouthwest" };

        private static string[] SW = { "sw", "southwest" };

        private static string[] WSW = { "wsw", "westsouthwest" };

        private static string[] W = { "w", "west" };

        private static string[] WNW = { "wnw", "westnorthwest" };

        private static string[] NW = { "nw", "nw" };

        private static string[] NNW = { "nnw", "northnorthwest" };

        private static Dictionary<string[], decimal> list = new Dictionary<string[], decimal>();

        public string ParseDirections(string direction)
        {
            decimal? result = list.Where(i => i.Key.Contains(direction)).Select(i => i.Value).FirstOrDefault();
            if (result != null)
            {
                return result.ToString();
            }
            else
            {
                return null;
            }
        }
    }


    /// <summary>
    /// The concrete model of a sailing log.
    /// </summary>
    [Serializable]
    public class Sail : SailBase,  Common.IDatabaseModel, ISingleVessel, ICrewable, Common.IConfigurableModel, Common.ICommentableEntity, Common.IPhotographedEntity, Common.ITaggableModel, Common.IEntityModel,Common.IMeteorologicalModel, Common.IVesselable, Common.IManeuverableModel,Common.IZooModel
    {
        #region Constructors and Destructors

        /// <summary>
        /// The Default constructor.
        /// </summary>
        public Sail()
        {

        }

        /// <summary>
        /// The advanced constructor that covers all default values
        /// </summary>
        /// <param name="barometer">The barometer reading (in inchesh)</param>
        /// <param name="crew">A list of crew members present</param>
        /// <param name="date">The date of the sail</param>
        /// <param name="direction">The averaged bearing of the boat</param>
        /// <param name="hours">Hours sailed</param>
        ///<param name="location">The location of the sail</param>
        ///<param name="miles">Miles logged while under sail</param>
        ///<param name="vessel">The vessel</param>
        ///<param name="windDirection">The averaged direction of the wind</param>
        ///<param name="windHigh">The highest wind reading.</param>
        ///<param name="windLow">The lowest wind reading.</param>
        public Sail(DateTime date, Vessel vessel, Location location, decimal barometer, decimal windHigh, decimal windLow,
            decimal miles, decimal hours, IEnumerable<CrewFoundation> crew, CardinalDirections direction, CardinalDirections windDirection)
        {
            WindDirection = WindDirection;
            Date = date;
            this.Vessel = vessel;
            Location = location;
            Barometer = barometer;
            MaxWind = windHigh;
            MinWind = windLow;
            MilesSailed = miles;
            HoursSailed = hours;
            this.Crew = crew.ToList();

        }

        /// <summary>
        /// The advanced constructor that covers all default values
        /// </summary>
        /// <param name="barometer">The barometer reading (in inchesh)</param>
        /// <param name="crew">A list of crew members present</param>
        /// <param name="date">The date of the sail</param>
        /// <param name="direction">The averaged bearing of the boat</param>
        /// <param name="hours">Hours sailed</param>
        ///<param name="location">The location of the sail</param>
        ///<param name="miles">Miles logged while under sail</param>
        ///<param name="vessel">The vessel</param>
        ///<param name="windDirection">The averaged direction of the wind</param>
        ///<param name="windHigh">The highest wind reading.</param>
        ///<param name="windLow">The lowest wind reading.</param>
        public Sail(DateTime date, Vessel vessel, Location location, decimal barometer, decimal windHigh, decimal windLow,
            decimal miles, decimal hours, IEnumerable<CrewFoundation> crew, CardinalDirections direction, string windDirection)
        {
            Date = date;
            this.Vessel = vessel;
            Location = location;
            Barometer = barometer;
            MaxWind = windHigh;
            MinWind = windLow;
            MilesSailed = miles;
            HoursSailed = hours;
            this.Crew = crew.ToList();
            WindDirection = direction.ToCompassBearing();

        }
        


        #endregion 
        #region Public Properties



        #region Wind

        /// <summary>
        /// The average wind.
        /// </summary>
        public decimal? AvgWind { get; set; }




        #endregion 

        #region Hours and Mileage

        /// <summary>
        /// The total number of miles motored.
        /// </summary>
        public decimal MilesMotored { get; set; }

        /// <summary>
        /// The total number of hours motored.
        /// </summary>
        public decimal HoursMotored { get; set; }

        
        #endregion 

        #region IEnumerables

        /// <summary>
        /// Gets or sets the bool indicating wether this is test data.
        /// </summary>
        public bool IsTest { get; set; }

        /// <summary>
        /// A list of user-defined tags.
        /// </summary>
        public IEnumerable<Tags> Tags { get; set; }
        /// <summary>
        /// A list of Maneuvers.
        /// </summary>
        public IEnumerable<Maneuver> Maneuvers { get; set; }

        public IEnumerable<SheetSailLog> SheetSailLogs { get; set; }
        
        // <summary>
        /// A list of photos.
        /// </summary>
        public IEnumerable<PhotoBase> Photos { get; set; }
        /// <summary>
        /// A list of weather conditions present during the sail.
        /// </summary>
        public IEnumerable<WeatherBase> Weather { get; set; }
        /// <summary>
        /// A list of non-log related comments left on the sail.
        /// </summary>
        public IEnumerable<CommentBase> Comments { get; set; }

        /// <summary>
        /// The official logbook.
        /// </summary>
        public IEnumerable<CommentBase> Logbook { get; set; }

        /// <summary>
        /// The GPS Track.  These entries are separate from the logbook.  There will be many of them.
        /// </summary>
        public IEnumerable<CommentBase> Track { get; set; } 

        /// <summary>
        /// A list of animals spotted during the voyage.
        /// </summary>
        public IEnumerable<Animal> Animals { get; set; }

        public IEnumerable<VesselFoundation> Vessels { get; set; }


        public string RenderGoogleCoordinates()
        {
            var mappedComments = this.MappedComments();
            if (mappedComments == null || !mappedComments.Any())
            {
                return null;
            }

            var listing=mappedComments.Select(i => string.Format("{{lat: {0}, lng: {1}}}", i.Latitude, i.Longitude));
            return string.Join(",", listing);

        }

        public IEnumerable<CommentBase> MappedComments()
        {
            if (this.Track == null)
            {
                return null;
            }
            return this.Track.Where(i => i.IsLog && i.Latitude != null && i.Longitude != null).ToArray();
        }

        public void PreProcess()
        {
            if (this.SheetSailLogs!=null&&this.SheetSailLogs.Any() && this.SheetSailLogs.Any(i => i.Percentage < 1))
            {
                this.SheetSailLogs = this.SheetSailLogs.Where(i => i.Percentage > 0);
            }
        }

        #endregion 

        #endregion

        #region Public Methods
        /// <summary>
        /// An average of the bearings.
        /// </summary>
        public void SetWindDirection(CardinalDirections direction)
        {
            WindDirection = direction.ToCompassBearing();
        }

        public void SetWindirection(int bearing)
        {
            WindDirection = bearing;
        }
 


        public string HeelColor
        {
            get
            {
                if (this.Tilt < 5)
                {
                    return "factor1";
                }

                if (this.Tilt < 7)
                {
                    return "factor2";
                }

                if (this.Tilt < 12)
                {
                    return "factor3";
                }

                if (this.Tilt < 20)
                {
                    return "factor4";
                }

                if (this.Tilt < 30)
                {
                    return "factor5";
                }

                if (this.Tilt < 60)
                {
                    return "factor6";
                }

                    return "factor7";
                
            }

        }

        public string WindColor
        {
            get
            {
                return MaxWind.ToWindColorClass();
            }
        }

        public void SetID(int id)
        {
            ID = id;
        }

        public object[] PrimaryKey
        {
            get {
                return new object[] { 
                    this.ID
                };
            }
        }
        #endregion 
    }
}
