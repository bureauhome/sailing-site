﻿CREATE TABLE [dbo].[Logs.Sails.Favorites.Monthly] (
    [SailID] INT      NOT NULL,
    [CrewID] INT      NOT NULL,
    [Month]  TINYINT  NOT NULL,
    [YearID] SMALLINT NOT NULL,
    CONSTRAINT [PK_Logs.Sails.Favorites.Monthly] PRIMARY KEY CLUSTERED ([CrewID] ASC, [Month] ASC, [YearID] ASC),
    CONSTRAINT [FK_Table_1_Entities.Favorites.Crew] FOREIGN KEY ([CrewID]) REFERENCES [dbo].[Entities.Crew] ([CrewID]),
    CONSTRAINT [FK_Table_1_Logs.Favorites.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID])
);

