﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Repositories.Interfaces
{
    using System.Dynamic;

    public interface ICommonRepository<DBENTITY, MODEL, MODELBASE>
    {
        MODEL Get(int id);

        IEnumerable<MODELBASE> GetAll(object search=null);

        int Create(MODEL entity);

        bool IsUnique(MODEL entity, out List<string> errorMessage);

        void Delete(int id, int userId);

        void Update(MODEL entity);

    }
}