﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AnimalTestMethods.cs" company="">
//   
// </copyright>
// <summary>
//   The animal controller tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingUnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using SailingSite;
    using SailingSite.Common;
    using SailingSite.Repositories;
    using SailingSite.Services;

    using SailingSiteModels.New;
    using SailingSiteModels.New.Debug;

    /// <summary>
    /// The animal controller tests.
    /// </summary>
    [TestClass]
    public class AnimalControllerTests : TestBase
    {
        private AnimalRepository animalRepository = null;

        internal AnimalRepository AnimalRepository
        {
            get
            {
                if (animalRepository == null)
                {
                    animalRepository=new AnimalRepository();
                }
                return animalRepository;
            }
        }

        [TestMethod]
        public void AnimalLog_Verify()
        {
            this.SailRepository.Sterilize();
            var loggingService = new Mock<ILoggingService>();
            var animalRepository = new Mock<IAnimalRepository>();
            var sailingService = new SailingService();
            AuditContext context = StaticTokens.TestValues.TestContext;
            animalRepository.Setup(i => i.CreateAnimal(It.IsAny<Animal>(), It.IsAny<Entities_Animals_Categories>())).Returns(1);
            animalRepository.Setup(i => i.GetCategory(It.IsAny<string>())).Returns(new AnimalCategory());
            loggingService.Setup(i => i.GenerateLog(It.IsAny<string>(), Log.LogTypes.AuditCreate, context, It.IsAny<object>()));
            // Create the entity
            AnimalService.LoggingService = loggingService.Object;
            AnimalService.AnimalRepository = animalRepository.Object;
            var animal = this.GenerateAnimal();
            AnimalService.CreateAnimal(animal, this.Context);
            loggingService.Verify(i => i.GenerateLog(It.IsAny<string>(), Log.LogTypes.AuditCreate, context, It.IsAny<object>()), Times.Once);
            this.SailRepository.Sterilize();
        }

        [TestMethod]
        public void AnimalCommentTest()
        {
            // Pre-nuke the DB
            this.SailRepository.Sterilize();
            // Create the entity
            var animal = this.GenerateAnimal();
            animal.ID = AnimalService.CreateAnimal(animal, this.Context);
            var sail = this.generateSail();
            sail.ID = SailService.CreateSail(sail);
            var comment = this.GenerateSailLog(sail);
            var animeaux = new List<Animal>() { new Animal() { ID = animal.ID } };
            comment.Animals = animeaux;
            SailService.AddSailComment(comment,sail.ID, this.Context);
            var dbAnimal=AnimalService.GetAnimal(animal.ID);
            Assert.IsNotNull(dbAnimal);
            Assert.IsNotNull(dbAnimal.Comments);
            Assert.IsTrue(dbAnimal.Comments.Count()==1);
            Assert.AreEqual(dbAnimal.Comments.FirstOrDefault().Message,comment.Message);

            CommentService.Delete(dbAnimal.Comments.FirstOrDefault().ID, dbAnimal.Comments.FirstOrDefault().CreatedBy.ID);
            dbAnimal = AnimalService.GetAnimal(animal.ID);
            Assert.IsNotNull(dbAnimal);
            Assert.IsTrue(dbAnimal.Comments==null||dbAnimal.Comments.Count()==0);

            this.SailRepository.Sterilize();
        }

        /// <summary>
        /// The test method 1.
        /// </summary>
        [TestMethod]
        public void AnimalCRUD()
        {
            // Pre-nuke the DB
            this.AnimalRepository.NukeTempData();
            // Create the entity
            var animal = this.GenerateAnimal();
            int id = AnimalService.CreateAnimal(animal, this.Context);
            var tags = TagService.GetAllTagsByString().FirstOrDefault();
            animal.Tags = new List<Tags> { new Tags() { Name = tags.FirstOrDefault().ToString() } };

            // Read the entity
            var dbAnimal =
                this.AnimalService.GetAnimal(id);
            Assert.IsNotNull(dbAnimal);
            Assert.IsTrue(dbAnimal.ID > 0);
            AnimalService.AddTags(dbAnimal.ID, tags);

            // Verify the base entities are the same
            Assert.AreEqual(animal.Name, dbAnimal.Name);
            Assert.AreEqual(animal.Category, dbAnimal.Category);

            // Verify the aliases.
            foreach (var alias in animal.Aliases)
            {
                var match = dbAnimal.Aliases.FirstOrDefault(i => i == alias);
                Assert.IsNotNull(match);
            }

            // verify the tags
            var dbTags = AnimalService.GetAnimalTags(id);

            Assert.IsNotNull(dbTags);
            Assert.AreEqual(dbTags.Count(),animal.Tags.Count());

            foreach (var tag in animal.Tags)
            {
                var match = dbTags.Where(i => i.Name == tag.Name);
            }

            // Perform an update
            string newName = "Hooch";
            animal.Name = newName;
            animal.ID = id;
            var newCat=AnimalService.GetAllCategories().FirstOrDefault(i=>i.Name!=animal.Category);
            animal.Category = newCat.Name;
            AnimalService.UpdateAnimal(animal, this.Context);
            dbAnimal=AnimalService.GetAnimal(id);
            Assert.AreEqual(dbAnimal.Name,newName);
            Assert.AreEqual(dbAnimal.Category,animal.Category);

            // Delete the entity
            AnimalService.DeleteAnimal(dbAnimal.ID);
            dbAnimal = AnimalService.GetAnimal(id);
            Assert.IsNotNull(dbAnimal);
            // Verify it was deleted
            Assert.IsNotNull(dbAnimal.DateDeleted);
            // Make sure it's not being picked up in searches
            var dbAnimals = AnimalService.GetAnimals(new GenericQuery() { AnimalID = id });
            Assert.IsTrue(dbAnimals==null||!dbAnimals.Any());
            // The bottom of the nuke sandwich
            this.AnimalRepository.NukeTempData();

        }

        [TestMethod]
        [ExpectedException(typeof(EntityAlreadyInDatabaseException))]
        public void DualAnimalTest()
        {
            // Pre-nuke the test data
            this.AnimalRepository.NukeTempData();
            var animal = this.GenerateAnimal();
            Exception expectedException=null;
            // Add the animal
            AnimalService.CreateAnimal(animal, this.Context);
            // do it again.  Test.
            try
            {
                AnimalService.CreateAnimal(animal, this.Context);
            }
            catch (Exception e)
            {
                expectedException = e;
            }
            // verify the exception has been caught and exists
            Assert.IsNotNull(expectedException);
            Assert.IsTrue(expectedException is EntityAlreadyInDatabaseException);

            // Verify it only exists once
            GenericQuery query = new GenericQuery() { Name= animal.Name };
            var result=AnimalService.GetAnimals(query);
            Assert.IsTrue(result.Count()==1);

            // The bottom of the nuke sandwich
            AnimalRepository.NukeTempData();
        }

        [TestMethod]
        public void VerifyDualTagInsertionFail()
        {
            // Pre-nuke the DB
            AnimalRepository.NukeTempData();
            // Create the entity
            var animal = this.GenerateAnimal();
            int id = AnimalService.CreateAnimal(animal, this.Context);
            var origTag = TagService.GetAllTagsByString().FirstOrDefault();
            var tags = origTag + "," + origTag;
            AnimalService.AddTags(id , tags);

            // Get the tags
            var dbTags=AnimalService.GetAnimalTags(id);
            // Verify there is only one
            Assert.IsNotNull(dbTags);
            Assert.IsTrue(dbTags.Count()==1);
            // The crust of the nuke sandwich
            AnimalRepository.NukeTempData();
        }

        /// <summary>
        /// The generate animal.
        /// </summary>
        /// <returns>
        /// The <see cref="Animal"/>.
        /// </returns>
        public Animal GenerateAnimal()
        {
           Animal result = null;
            var categories = new AnimalCategory() { Name = "Waterfowl" };
            if (categories != null)
            {
                result = new Animal() { Name = "Gullywump", CreatedBy = CrewRepository.GetTestUser().ToCrew(), CreatedDate= DateTime.Now, Category = categories.Name };
                result.Aliases.Add("Gully");
                result.Aliases.Add("Wump");
            }
            return result;
        }
    }
}
