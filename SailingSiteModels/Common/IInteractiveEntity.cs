﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Common
{
    interface IInteractiveEntity
    {
        DateTime Updated { get; set; }
        SailingSiteModels.New.Crew CreatedBy { get; set; }
        SailingSiteModels.New.Crew ModifiedBy { get; set; }
        void Update(object[] input, SailingSiteModels.New.Crew crew);
       
    }
}
