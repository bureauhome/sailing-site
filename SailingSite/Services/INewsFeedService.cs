﻿namespace SailingSite.Services
{
    using System.Collections.Generic;
    using System.Security.Cryptography.X509Certificates;

    using SailingSite.Common;
    using SailingSite.Models.FeedModels;

    using SailingSiteModels.New;

    public interface INewsFeedService
    {
        NewsFeed GetNewsFeed(CrewSecurity crew, AuditContext context);
    }
}