﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StaticKeys.cs" company="">
//   
// </copyright>
// <summary>
//   The static tokens.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Common
{
    using System;
    using System.Configuration;

    using SailingSiteModels.New;

    /// <summary>
    /// The static tokens.
    /// </summary>
    public static class StaticTokens
    {
        /// <summary>
        /// The test values.
        /// </summary>
        public static class TestValues
        {
            /// <summary>
            /// The test context.
            /// </summary>
            public static readonly AuditContext TestContext = new AuditContext
                                                                  {
                                                                      ActiveCrew =
                                                                          new CrewFoundation
                                                                              {
                                                                                  UserName =
                                                                                      "TESTUSER139"
                                                                              }
                                                                  };

            /// <summary>
            /// The test context.
            /// </summary>
            public static readonly AuditContext AdminTestContext = new AuditContext
            {
                ActiveCrew =
                    new CrewFoundation
                    {
                        ID = 6
                    }
            };
        }

        /// <summary>
        /// The security.
        /// </summary>
        public static class Security
        {
            /// <summary>
            /// The user token.
            /// </summary>
            public const string UserToken = "UserToken";

            /// <summary>
            /// The is admin token.
            /// </summary>
            public const string IsAdminToken = "UserIsAdmin";
        }

        /// <summary>
        /// The admin.
        /// </summary>
        public static class Admin
        {
            /// <summary>
            /// The admin banner.
            /// </summary>
            public const string AdminBanner = "AdminBanner";
        }

        /// <summary>
        /// The defaults.
        /// </summary>
        public static class Defaults
        {
            /// <summary>
            /// The default fleet id.
            /// </summary>
            public const int DefaultFleetId = 2;

            /// <summary>
            /// The delete verb.
            /// </summary>
            public const string DeleteVerb = "Delete";

            /// <summary>
            /// The image path.
            /// </summary>
            /// <returns>
            /// The <see cref="string"/>.
            /// </returns>
            public static string ImagePath(string localpath)
            {
                var result =localpath;
                result = System.IO.Path.Combine(result, ImageFolder());
                return result;
            }

            public static string ImageFolder()
            {
                return "imagedir";
            }

            /// <summary>
            /// The thumbnail subdirectory.
            /// </summary>
            public const string ThumbnailSubdirectory = "_petit";

            /// <summary>
            /// The edit verb.
            /// </summary>
            public const string EditVerb = "New";

            /// <summary>
            /// The default captain.
            /// </summary>
            public const string DefaultCaptain = "Davey Jones (Default)";

            /// <summary>
            /// The default captain id.
            /// </summary>
            public const int DefaultCaptainId = -1;

            /// <summary>
            /// The thumbnail suffix.
            /// </summary>
            public const string ThumbnailSuffix = "_mini";
        }

        public static class SessionTokens
        {
            public const string DoLogAnotherSail = "DoLogAnotherSail";
            public const string NewsFeed = "NewsFeed";
        }

        /// <summary>
        /// The caching tokens.
        /// </summary>
        public static class CachingTokens
        {

            public const string SheetSailLocker = "SheetSailLockerKey";
            /// <summary>
            /// The user session resources.
            /// </summary>
            public const string UserSessionResources = "UserResources";

            /// <summary>
            /// The tag enum.
            /// </summary>
            public const string TagEnum = "TagKey";

            /// <summary>
            /// The tag update token.
            /// </summary>
            public const string TagUpdateToken = "TagToken";

            /// <summary>
            /// The vessel enum.
            /// </summary>
            public const string VesselEnum = "VesselKey";

            /// <summary>
            /// The vessel update token.
            /// </summary>
            public const string VesselUpdateToken = "VesselToken";

            /// <summary>
            /// The fleet enum.
            /// </summary>
            public const string FleetEnum = "FleetKey";

            /// <summary>
            /// The fleet update token.
            /// </summary>
            public const string FleetUpdateToken = "FleetToken";

            /// <summary>
            /// The crew enum.
            /// </summary>
            public const string CrewEnum = "CrewKey";

            /// <summary>
            /// The standard vessel.
            /// </summary>
            public const string StandardVessel = "StandardVesselKey";

            /// <summary>
            /// The crew update token.
            /// </summary>
            public const string CrewUpdateToken = "CrewToken";

            /// <summary>
            /// The weather enum.
            /// </summary>
            public const string WeatherEnum = "WeatherKey";

            /// <summary>
            /// The weather update token.
            /// </summary>
            public const string WeatherUpdateToken = "WeatherToken";

            /// <summary>
            /// The animal category enum.
            /// </summary>
            public const string AnimalCategoryEnum = "AnimalCategoryKey";

            /// <summary>
            /// The maneuver enum.
            /// </summary>
            public const string ManeuverEnum = "ManeuverKey";

            /// <summary>
            /// The maneuver update token.
            /// </summary>
            public const string ManeuverUpdateToken = "ManeuverToken";

            /// <summary>
            /// The location enum.
            /// </summary>
            public const string LocationEnum = "LocationKey";

            /// <summary>
            /// The location update token.
            /// </summary>
            public const string LocationUpdateToken = "LocationToken";

            /// <summary>
            /// The animal enum.
            /// </summary>
            public const string AnimalEnum = "AnimalKey";

            /// <summary>
            /// The animal update token.
            /// </summary>
            public const string AnimalUpdateToken = "AnimalToken";

            /// <summary>
            /// The land mark enum.
            /// </summary>
            public const string LandMarkEnum = "LandmarkKey";

            /// <summary>
            /// The land mark token.
            /// </summary>
            public const string LandMarkToken = "LandMarkToken";

            /// <summary>
            /// The sail configuration enum.
            /// </summary>
            public const string SailConfigurationEnum = "SailConfigurationsKey";

            /// <summary>
            /// The sail configuration token.
            /// </summary>
            public const string SailConfigurationToken = "SailConfigurationToken";

            /// <summary>
            /// The token suffix.
            /// </summary>
            private const string tokenSuffix = "_Token";

            /// <summary>
            /// The update needed suffix.
            /// </summary>
            private const string UpdateNeededSuffix = "_Update";

            /// <summary>
            /// The get token key.
            /// </summary>
            /// <param name="key">
            /// The key.
            /// </param>
            /// <returns>
            /// The <see cref="string"/>.
            /// </returns>
            public static string GetTokenKey(string key)
            {
                return key + tokenSuffix;
            }

            /// <summary>
            /// The get update needed key.
            /// </summary>
            /// <param name="key">
            /// The key.
            /// </param>
            /// <returns>
            /// The <see cref="string"/>.
            /// </returns>
            public static string GetUpdateNeededKey(string key)
            {
                return key + UpdateNeededSuffix;
            }

            /// <summary>
            /// The standard cache expiration.
            /// </summary>
            /// <returns>
            /// The <see cref="DateTimeOffset"/>.
            /// </returns>
            public static DateTimeOffset StandardCacheExpiration()
            {
                DateTime localTime1 = DateTime.Now.AddHours(3);
                localTime1 = DateTime.SpecifyKind(localTime1, DateTimeKind.Local);
                DateTimeOffset localTime2 = localTime1;
                return localTime2;
             

            }
        }
    }
}