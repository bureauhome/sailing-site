﻿CREATE TABLE [dbo].[Logs.Sails.Animal] (
    [SailID]   INT NOT NULL,
    [AnimalID] INT NOT NULL,
    CONSTRAINT [PK_Logs.Sails.Animal] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Logs.Sails.Animal] PRIMARY KEY CLUSTERED ([SailID] ASC, [AnimalID] ASC),
    CONSTRAINT [FK_Logs.Sails.Animal_Entities.Animals.Names] FOREIGN KEY ([AnimalID]) REFERENCES [dbo].[Entities.Animals.Names] ([AnimalID]),
    CONSTRAINT [FK_Logs.Sails.Animal_Logs.Sails] FOREIGN KEY ([SailID]) REFERENCES [dbo].[Logs.Sails] ([SailID]) ON DELETE CASCADE
);

