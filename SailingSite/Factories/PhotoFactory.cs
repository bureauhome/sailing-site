﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;

namespace SailingSite.Factories
{
    using System.Collections;

    using New;

    using SailingSiteModels.Old;

    public static class PhotoFactory
    {
        public static Entities_Photos CreatePhoto(New.Photo entity)
        {

            return new Entities_Photos()
            {
                PhotoID=entity.ID,
                PhotoPath = entity.Path==null?null:entity.Path.Trim(),
                Name=entity.Name.Trim(),
                Width=entity.Width,
                Height=entity.Height,
                PhotoCarouselPath = entity.CarouselPath,
                Description=entity.Description==null?null:entity.Description.Trim(),
                ThumbPath=entity.ThumbPath==null?null:entity.ThumbPath.Trim(),

            };
        }

        public static New.PhotoBase CreatePhotoBase(Entities_Photos entity)
        {
            return new New.Photo()
            {
                ID = entity.PhotoID,
                Width = entity.Width,
                Height=entity.Height,
                CarouselPath = entity.PhotoCarouselPath,
                Description = entity.Description == null ? string.Empty : entity.Description.Trim(),
                Path = entity.PhotoPath == null ? string.Empty : entity.PhotoPath.Trim(),
                ThumbPath = entity.ThumbPath == null ? string.Empty : entity.ThumbPath.Trim()
            };
        }

        /// <summary>
        /// The create photo.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Photo"/>.
        /// </returns>
        public static New.Photo CreatePhoto(Entities_Photos entity)
        {
            IEnumerable<SailingSiteModels.New.Maneuver> maneuvers = entity.Entities_Maneuvers == null || !entity.Entities_Maneuvers.Any()
                                                   ? null
                                                   : entity.Entities_Maneuvers.Select(SailFactory.CreateManeuver);
            IEnumerable<SailingSiteModels.New.Animal> animals = entity.Entities_Animals_Names == null
                                                                || !entity.Entities_Animals_Names.Any()
                                                                    ? null
                                                                    : entity.Entities_Animals_Names.Select(
                                                                        AnimalFactory.CreateAnimal);
            IEnumerable<SailingSiteModels.New.VesselFoundation> vessels = entity.Entities_Vessel == null
                                                                   || !entity.Entities_Vessel.Any()
                                                                       ? null
                                                                       : entity.Entities_Vessel.Select(
                                                                           FleetFactory.CreateVesselFoundation);
            IEnumerable<SailingSiteModels.New.CrewFoundation> crew = entity.Entities_Crew == null
                                                                     || !entity.Entities_Crew.Any()
                                                                         ? null
                                                                         : entity.Entities_Crew.Select(
                                                                             CrewFactory.CreateCrewFoundation);
            IEnumerable<SailingSiteModels.New.Tags> tags = entity.Entities_Tags == null || !entity.Entities_Tags.Any()
                                                  ? null
                                                  : entity.Entities_Tags.Select(CommonFactory.CreateTag);

            IEnumerable<SailingSiteModels.New.LocationBase> locations = entity.Entities_Locations == null
                                                                       || !entity.Entities_Locations.Any()
                                                                           ? null
                                                                           : entity.Entities_Locations.Select(
                                                                               CommonFactory.CreateLocationBase);
            IEnumerable<New.Configuration> configuration = entity.Entities_Config == null
                                                           || !entity.Entities_Config.Any()
                                                               ? null
                                                               : entity.Entities_Config.Select(
                                                                   SailFactory.CreateConfiguration);
            IEnumerable<New.WeatherBase> weather = entity.Entities_Weather == null || !entity.Entities_Weather.Any()
                                                       ? null
                                                       : entity.Entities_Weather.Select(CommonFactory.CreateWeatherBase);

            var result = CreatePhotoBase(entity).ToPhoto();
            result.Weather = weather;
            result.Locations = locations;
            result.Tags = tags;
            result.Maneuvers = maneuvers;
            result.Crew = crew;
            result.Vessels = vessels;
            result.Animals = animals;
            result.Configurations = configuration;
            if (entity.Entities_Photos_Details != null)
            {
                result.Details.CreatedDate = entity.Entities_Photos_Details.CreatedDate;
                result.Details.Creator = CrewFactory.CreateCrewFoundation(entity.Entities_Photos_Details.Entities_Crew);
                if (entity.Entities_Photos_Details.Entities_Crew1 != null)
                {
                    result.Details.ModifiedBy =
                        CrewFactory.CreateCrewFoundation(entity.Entities_Photos_Details.Entities_Crew1);
                    result.Details.ModifiedDate = entity.Entities_Photos_Details.ModifiedDate;
                }
            }

            return result;
        }
    }
}