﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SailingUnitTests
{
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Device.Location;
    using System.Linq;
    using System.Security.Policy;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    using Moq;

    using SailingSite;
    using SailingSite.Common;
    using SailingSite.Controllers;
    using SailingSite.Repositories;
    using SailingSite.Services;

    using SailingSiteModels.Common;
    using SailingSiteModels.New;
    using SailingSiteModels.New.Bearing;
    using SailingSiteModels.Old;

    using Crew = SailingSiteModels.New.Crew;
    using Sail = SailingSiteModels.New.Sail;

    [TestClass]
    public class SailTests : TestBase
    {
        /// <summary>
        /// The sterilize test.
        /// </summary>
        [TestInitialize]
        [TestCleanup]
        public void SterilizeTest()
        {
            SailRepository.Sterilize();
            LoggingRepository.PurgeTestData();
        }

        [TestMethod]
        [TestCategory("IntegrationTest")]
        public void TestLocationCreate()
        {
            var service=new LocationService();
            var loc = new SailingSiteModels.New.Location()
                          {
                              Name = "Test",
                              IsTest = true,
                              Longitude = 131,
                              Latitude = -25.363,
                              Location = new LocationType() {ID=1},
                              MajorLocation = "City",
                              MinorLocation = "State"
                          };
            var id=service.Create(loc);
            var result=service.Get(id);
            Assert.AreEqual(loc.Longitude,result.Longitude);
            Assert.AreEqual(loc.Latitude,result.Latitude);
            Assert.AreEqual(loc.Name, result.Name.Trim());
            Assert.AreEqual(loc.MajorLocation, result.MajorLocation);
            Assert.AreEqual(loc.MinorLocation, result.MinorLocation);
        }

        /// <summary>
        /// The test jiffy sail.
        /// </summary>
        [TestMethod]
        public void TestJiffySail()
        {
           var sail=this.SailService.JiffySail(this.Context);
            Assert.IsNotNull(sail);
        }

        /// <summary>
        /// The test entry.
        /// </summary>
        [TestMethod]
        public void TestEntry()
        {
            
            var id=this.SailService.CreateSail(this.generateSail());
            Assert.IsTrue(id!=0);
        }

        [TestMethod]
        public void DeriveProperCompassBearing()
        {
            var service = new SailingSite.Services.SailingService();
            string[] expectedVals = {
                    "north", "n", "nne", "north north east", "northnortheast", "ne", "north east",
                    "northeast", "ene", "east north east", "eastnortheast", "east by north east", "by west"
                };
            foreach (var val in expectedVals)
            {
                var result = service.DeriveCompassRose(val);
                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void DeriveImproperCompassBearing()
        {
            var service = new SailingSite.Services.SailingService();
            string[] expectedVals = {
                    "fred", "pizza", "george", "by the lee", "fjeoifjerio", "foofer", "by", "by oliver north"
                };
            foreach (var val in expectedVals)
            {
                var result = service.DeriveCompassRose(val);
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public void DeleteSail_NotInCrewOrAdmin_BlockedSuccess()
        {
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
            var sailController = new SailController();
            var sailingService = new Mock<ISailService>();
                        var crew = new CrewSecurity()
            {
                ID = 2,
                SID = Crew.SIDEnum.None,
                VesselsCaptained = null,
                VesselsCrewed = new List<VesselBase> { new VesselBase() { ID = 1 } }
            };
            object result = null;
            session.Setup(m => m[StaticTokens.CachingTokens.UserSessionResources]).Returns(crew);
            sailingService.Setup(i => i.GetSail(It.IsAny<int>()))
                .Returns(new Sail() { ID = 2, Vessel = new VesselBase() { ID = 2 } });
            sailingService.Setup(i => i.isUserVesselCrewOrSecurity(It.IsAny<int>(), It.IsAny<int>(), true)).Returns(false);
            sailingService.Setup(i => i.DeleteSail(It.IsAny<int>(), It.IsAny<AuditContext>()));
            sailingService.Setup(i => i.GetSailsForVessel(It.IsAny<IEnumerable<int>>())).Returns(new List<SailBase>() {new SailBase(){ID=1}});
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
            context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());

            sailController.ControllerContext = context.Object;
            sailController.Sailservice = sailingService.Object;

            result = sailController.Delete(42);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var status = result as HttpStatusCodeResult;
            Assert.IsTrue(status.StatusCode==403);
            sailingService.Verify((i => i.isUserVesselCrewOrSecurity(It.IsAny<int>(), It.IsAny<int>(), true)), Times.Once);
            sailingService.Verify(i => i.DeleteSail(It.IsAny<int>(), It.IsAny<AuditContext>()), Times.Never);
        }


        [TestMethod]
        public void DeleteSail_InCrewOrAdmin_Success()
        {
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();
            var request = new Mock<HttpRequestBase>();
            var sailController = new SailController();
            var sailingService = new Mock<ISailService>();
            var crew = new CrewSecurity()
            {
                ID = 2,
                SID = Crew.SIDEnum.Admin,
                VesselsCaptained = null,
                VesselsCrewed = new List<VesselBase> { new VesselBase() { ID = 1 } }
            };
            object result = null;
            session.Setup(m => m[StaticTokens.CachingTokens.UserSessionResources]).Returns(crew);
            sailingService.Setup(i => i.GetSail(It.IsAny<int>()))
                .Returns(new Sail() { ID = 2, Vessel = new VesselBase() { ID = 2 } });
            sailingService.Setup(i => i.isUserVesselCrewOrSecurity(It.IsAny<int>(), It.IsAny<int>(), true)).Returns(true);
            sailingService.Setup(i => i.DeleteSail(It.IsAny<int>(), It.IsAny<AuditContext>()));
            sailingService.Setup(i => i.GetSailsForVessel(It.IsAny<IEnumerable<int>>())).Returns(new List<SailBase>() { new SailBase() { ID = 1 } });
            context.Setup(m => m.HttpContext.Request).Returns(request.Object);
            context.Setup(m => m.HttpContext.Session).Returns(session.Object);
            context.Setup(m => m.HttpContext.Request.Cookies).Returns(new HttpCookieCollection());

            sailController.ControllerContext = context.Object;
            sailController.Sailservice = sailingService.Object;

            result = sailController.Delete(42);
            Assert.IsNotNull(result);
            sailingService.Verify((i => i.isUserVesselCrewOrSecurity(It.IsAny<int>(), It.IsAny<int>(), true)), Times.Once);
            sailingService.Verify(i => i.DeleteSail(It.IsAny<int>(), It.IsAny<AuditContext>()), Times.Once);
        }

        [TestMethod]
        public void AddNullSail_AndDontExplode()
        {
            Sail sail = null;
            int id = this.SailService.CreateSail(sail);
            Assert.AreEqual(id,0);
            var result = this.SailService.GetSail(id);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestLogsAndTracks()
        {
            var sail = this.generateSail();
            int id=SailRepository.CreateSail(sail);
            var file =
                     @"C:\Users\bbureau\Documents\sailing-site\SailingUnitTests\TestfFiles\Track_A016-05-22 174721.gpx";
            var doc = XDocument.Load(file);
            var track = GPXParser.GPXParser.ParseTrack(doc);
            CommentService.ParseLogs(track.FirstOrDefault(),id,7);
            var dbSail = SailService.GetSail(id);
            Assert.IsNotNull(dbSail);
            Assert.IsNull(dbSail.Logbook);
            Assert.IsNotNull(dbSail.Track);
            Assert.IsTrue(dbSail.Track.Count()==track.FirstOrDefault().Track.Count());
            var newcomm = (this.GenerateSailLog(sail));
            newcomm.LogId = id;
            newcomm.Longitude = null;
            newcomm.Latitude = null;
            SailService.AddSailComment(newcomm, id, this.Context);
            dbSail = SailService.GetSail(id);
            
            Assert.IsNotNull(dbSail);
            Assert.IsNotNull(dbSail.Logbook);
            Assert.IsNotNull(dbSail.Track);
            var count1 = track.FirstOrDefault().Track.Count();
            var count2 = dbSail.Track.Count();
            Assert.AreEqual(count1,count2);
            Assert.IsTrue(dbSail.Logbook.Count()==1);
        }

         [TestMethod]
        public void TestSheetSailEntry()
        {
            this.SailRepository.Sterilize();
            var vessel = this.PreGenerateRandomVessel(true);
            // This should already be working!
            Assert.IsNotNull(vessel);
            Assert.IsNotNull(vessel.SailLocker);
            Assert.IsTrue(vessel.SailLocker.Any());

            var sail=this.generateSail(vessel);
            var log = new SheetSailLog();
            log.Percentage = 100;
            log.SheetSail = vessel.SailLocker.First();
            sail.SheetSailLogs=new List<SheetSailLog>() {log};
            sail.ID = SailRepository.CreateSail(sail);
            var dbSail = SailService.GetSail(sail.ID);
            Assert.IsNotNull(dbSail.SheetSailLogs);
            Assert.IsTrue(dbSail.SheetSailLogs.Count()==1);
            this.CompareSheetSailLog(log, dbSail.SheetSailLogs.First());
            this.SailRepository.Sterilize();
        }

         [TestMethod]
         public void TestSheetSailLogEntry()
         {
             this.SailRepository.Sterilize();
             var vessel = this.PreGenerateRandomVessel(true);
             // This should already be working!
             Assert.IsNotNull(vessel);
             Assert.IsNotNull(vessel.SailLocker);
             Assert.IsTrue(vessel.SailLocker.Any());

             var sail = this.generateSail(vessel);
             var sheetEntry = new SheetSailLog();
             sheetEntry.Percentage = 100;
             sheetEntry.SheetSail = vessel.SailLocker.First();
             sail.ID = SailRepository.CreateSail(sail);
             var comment = this.GenerateSailLog(sail);
             comment.SheetSailLog = new List<SheetSailLog>() { sheetEntry };
             SailService.AddSailComment(comment,sail.ID, this.Context);
             var dbSail = SailService.GetSail(sail.ID);
             Assert.IsNotNull(dbSail);
             // Remember, there are THREE total types of comments:
             // 1. Logs: log entries made by the crew.  May or may not contain coordinates
             // 2. Track: Just GPS coordinates.  Probably fed in by the GPS.  Only used in mapping.
             // 3. Comment: Non-log comments that have nothing to do with logging or tracking.
             Assert.IsNotNull(dbSail.Logbook);
             Assert.AreEqual(dbSail.Logbook.Count(), 1);
             var commentID = dbSail.Logbook.First().ID;
             var dbComment = CommentService.Get(commentID);
             Assert.IsNotNull(dbComment);
             Assert.IsNotNull(dbComment.SheetSailLog);
             Assert.AreEqual(dbComment.SheetSailLog.Count(), 1);
             this.CompareSheetSailLog(sheetEntry, dbComment.SheetSailLog.First());
             this.SailRepository.Sterilize();
         }


        [TestMethod]
        public void TestLandMark()
        {
            this.SailRepository.Sterilize();
            var lmId = this.CreateTestLandMark();
            var sail = this.generateSail();
            sail.ID = SailRepository.CreateSail(sail);
            var lmBearing = new LandMarkBearing()
                                {
                                    Bearing =
                                        new Bearing()
                                            {
                                                CompassReading =
                                                    UnitTestHelpers.rdm.Next(2, 40)
                                            },
                                    LandMark = new LandMarkBase() { ID = lmId }
                                };

            var newcomm = (this.GenerateSailLog(sail));
            newcomm.LandMarkBearings = new List<LandMarkBearing>() { lmBearing };
            this.SailService.AddSailComment(newcomm, sail.ID, this.Context);

            var testResult = this.SailService.GetSail(sail.ID);

            Assert.IsNotNull(testResult);
            Assert.IsNotNull(testResult.Logbook);
            Assert.AreEqual(testResult.Logbook.Count(),1);
            var log = testResult.Logbook.FirstOrDefault();
            Assert.IsNotNull(log);
            var commentDb = this.CommentService.Get(log.ID);
            Assert.IsNotNull(commentDb.LandMarkBearings);
            Assert.AreEqual(commentDb.LandMarkBearings.Count(),1);
            var testBearing = commentDb.LandMarkBearings.FirstOrDefault();
            Assert.AreEqual(testBearing.Bearing.CompassReading,lmBearing.Bearing.CompassReading);
            Assert.AreEqual(testBearing.LandMark.ID, lmId);
     
            this.SailRepository.Sterilize();
        }

        private int CreateTestLandMark()
        {
            LandMarkBase entity = new LandMarkBase()
                                      {
                                          
                                                      Longitude =
                                                          16.024,
                                                      Latitude =
                                                          32.021,
                                          Name = UnitTestHelpers.GetRandomString(4)
                                      };
            return this.SailRepository.CreateLandMark(entity);
        }


        // Verifies that the system merges a track entry with a log entry occuring at the same time
        [TestMethod]
        public void MergeStandardCommentAndLog()
        {
            var sail = this.generateSail();
            int id = SailRepository.CreateSail(sail);
            var file =
                     @"C:\Users\bbureau\Documents\sailing-site\SailingUnitTests\TestfFiles\Track_A016-05-22 174721.gpx";
            var doc = XDocument.Load(file);
            var track = GPXParser.GPXParser.ParseTrack(doc);
            Assert.IsNotNull(track);
            var entry=track.FirstOrDefault().Track.FirstOrDefault();
            var newcomm = (this.GenerateSailLog(sail));
            Assert.IsNotNull(entry);
            newcomm.LogId = id;
            newcomm.Created = entry.Date;
            newcomm.Latitude = null;
            newcomm.Longitude = null;
            SailService.AddSailComment(newcomm, id, this.Context);
            CommentService.ParseLogs(track.FirstOrDefault(), id, 7);
            var dbSail = SailService.GetSail(id);
            Assert.IsNotNull(dbSail);
            Assert.IsNotNull(dbSail.Logbook);
            Assert.IsTrue(dbSail.Logbook.Count()==1);
            var dbComment = dbSail.Logbook.FirstOrDefault();
            Assert.IsNotNull(dbComment.Longitude);
            Assert.IsNotNull(dbComment.Latitude);
            Assert.IsTrue(entry.Coordinate.Longitude==(double)dbComment.Longitude);
            Assert.IsTrue(entry.Coordinate.Latitude == (double)dbComment.Latitude);

        }


        [TestMethod]
        public void CRUDTest()
        {
            var context = StaticTokens.TestValues.TestContext;
            SailRepository.Sterilize();
            var sail = this.generateSail();
            int id=SailRepository.CreateSail(sail);
            var dbSail = SailService.GetSail(id);
           Assert.IsTrue(matchSails(dbSail,sail));
            // let's make sure they pull for their respective members
            var sailBases = CrewService.GetSailsForCrew(new List<int> { 7,8 });
            Assert.IsNotNull(sailBases);
            Assert.IsTrue(sailBases.Count()==1);
            Assert.IsTrue(sailBases.Any(i=>i.ID==id));

            sailBases = SailService.GetSailsForVessel(new List<int> { 1 });
            Assert.IsNotNull(sailBases);
            Assert.IsTrue(sailBases.Count()==1);
            Assert.IsTrue(sailBases.Any(i => i.ID == id));
            //TODO: Add photos!
            // Let's modify each of these values.
            sail.ID = id;
            sail.Date = sail.Date.AddDays(1);
            sail.Barometer += 1;
            sail.MinWind -= 1;
            sail.TempF += 1;
            sail.MilesSailed += 4;
            sail.WindDirection=180;
            sail.HoursSailed += 2;
            var newWeather = new List<Weather>() { new Weather() { ID = 3 } };
            var newManeuvers = new List<Maneuver>() { new Maneuver() { ID = 3 } };
            sail.Weather = newWeather;
            sail.Maneuvers = newManeuvers;
            changeCrew(sail);
            addAnimalsToSail(sail);
            SailService.UpdateSail(sail, context);
            dbSail = SailService.GetSail(id);
            Assert.IsTrue(matchSails(dbSail, sail));

            // We're going to add some logs
            var newcomm = (this.GenerateSailLog(sail));
            newcomm.LogId = id;
            SailService.AddSailComment(newcomm,id, this.Context);
            dbSail = SailService.GetSail(id);
            Assert.IsTrue(dbSail.Logbook.Count()==1);
            Assert.IsTrue(dbSail.MaxWind>sail.MaxWind);
            Assert.IsTrue(dbSail.Tilt>sail.Tilt);
                
           var originalComment = newcomm;
           var compareComment = CommentService.Get(dbSail.Logbook.FirstOrDefault().ID);

           Assert.IsTrue(this.MatchComment(originalComment,compareComment));

            this.SailService.DeleteSail(id, this.Context);
           sailBases = SailService.GetSailsForVessel(new List<int> { 1 });
           Assert.IsNull(sailBases);
           sailBases = CrewService.GetSailsForCrew(new List<int> { 7, 8 });
           Assert.IsNull(sailBases);
            

            // TODO: Test deletion, updating of comments

            SailRepository.Sterilize();

        }

        [TestMethod]
        public void AddHelmsPerson_Success()
        {
            SailRepository.Sterilize();
            var sail = this.generateSail();
            int id = SailRepository.CreateSail(sail);

            var newcomm = (this.GenerateSailLog(sail));
            newcomm.LogId = id;
            SailService.AddSailComment(newcomm, id, this.Context);
            var result = SailService.GetSail(id);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Logbook);
            Assert.IsTrue(result.Logbook.Any());

            var log = result.Logbook.FirstOrDefault();
            Assert.IsNotNull(log);
            Assert.AreEqual(log.HelmsPerson.ID,newcomm.HelmsPerson.ID);
            SailRepository.Sterilize();
        }

        [TestMethod]
        public void AddHelmsPersonID0_Success()
        {
            SailRepository.Sterilize();
            var sail = this.generateSail();
            int id = SailRepository.CreateSail(sail);

            var newcomm = (this.GenerateSailLog(sail));
            newcomm.HelmsPerson = new CrewFoundation() { ID = 0 };
            newcomm.LogId = id;
            SailService.AddSailComment(newcomm, id, this.Context);
            var result = SailService.GetSail(id);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Logbook);
            Assert.IsTrue(result.Logbook.Any());

            var log = result.Logbook.FirstOrDefault();
            Assert.IsNotNull(log);
            Assert.IsNull(log.HelmsPerson);
            SailRepository.Sterilize();
        }

        [TestMethod]
        public void TestJiffyLogDate()
        {
            SailingService service=new SailingService();
            var data = service.JiffySail(this.Context, 7, true);
            Assert.IsNotNull(data.Created);
        }

        [TestMethod]
        public void VerifyLogs()
        {
            SailRepository.Sterilize();
            var sail = this.generateSail();
            int id = SailRepository.CreateSail(sail);
            var log = (this.GenerateSailLog(sail));
            log.LogId = id;
            var log2 = (this.GenerateSailLog(sail));
            log2.LogId = id;
            SailService.AddSailComment(log,id, this.Context);
            SailService.AddSailComment(log2,id, this.Context);
            var dbSail = SailService.GetSail(id);
            Assert.IsNotNull(dbSail.Logbook);
            Assert.IsTrue(dbSail.Logbook.Count()==2);
            SailRepository.Sterilize();
        }

        [TestMethod]
        public void VerifyLogsUpdateSails()
        {
            var sail = this.generateSail();
            sail.Animals = new List<Animal>();
            sail.ID=SailService.CreateSail(sail);
            var comment = this.GenerateSailLog(sail);
            comment.LogId = sail.ID;
            comment.WindSpeed = sail.MaxWind + 1;
            comment.HeelPct = sail.Tilt + 1;
            comment.Animals = new List<Animal>() { new Animal() { ID = 14 } };
            
             SailService.AddSailComment(comment,sail.ID, this.Context);
             var dbSail = SailService.GetSail(sail.ID);
             var dbComment=dbSail.Logbook.FirstOrDefault(i=>i.WindSpeed==dbSail.MaxWind);
             comment.ID = dbComment.ID;
            Assert.IsNotNull(dbSail);
            Assert.IsNotNull(dbSail.Logbook);
            Assert.AreEqual(comment.WindSpeed,dbSail.MaxWind);
            Assert.AreEqual(comment.HeelPct,dbSail.Tilt);
            Assert.IsNotNull(dbSail.Animals);
            Assert.IsNotNull(comment.Animals.FirstOrDefault());
            Assert.IsTrue(dbSail.Animals.Any(i=>i.ID==comment.Animals.FirstOrDefault().ID));
            var animal = new Animal() { ID = 16 };
            comment.WindSpeed++;
            comment.HeelPct++;
            comment.Animals = new List<Animal>() { new Animal() { ID = 16 }, animal };
            CommentService.Update(comment);
            dbSail = SailService.GetSail(sail.ID);
            Assert.IsNotNull(dbSail);
            Assert.IsNotNull(dbSail.Logbook);
            Assert.AreEqual(comment.WindSpeed, dbSail.MaxWind);
            Assert.AreEqual(comment.HeelPct, dbSail.Tilt);
            Assert.IsNotNull(dbSail.Animals);
            Assert.IsTrue(dbSail.Animals.Any(i => i.ID == animal.ID));
        }


        private bool matchSails(Sail dbSail, Sail sail)
        {
            Assert.IsNotNull(dbSail);
            Assert.AreEqual(sail.Date.ToLongDateString(), dbSail.Date.ToLongDateString());
            Assert.IsTrue(dbSail.Barometer == sail.Barometer);
            Assert.IsTrue(dbSail.Location.ID == sail.Location.ID);
            Assert.IsNotNull(dbSail.Crew);
            Assert.IsTrue(dbSail.Crew.Count() == sail.Crew.Count());
            foreach (var member in sail.Crew)
            {
                Assert.IsTrue(dbSail.Crew.Any(i=>i.ID==member.ID));
            }
            Assert.IsTrue(sail.TempF == dbSail.TempF);
            Assert.IsTrue(sail.SeaState==dbSail.SeaState);
            Assert.IsTrue(dbSail.Tilt == sail.Tilt);
            Assert.AreEqual(dbSail.MilesSailed, sail.MilesSailed);
            Assert.IsTrue(dbSail.MinWind == sail.MinWind);
            Assert.AreEqual(dbSail.GeneralObservations, sail.GeneralObservations);
            Assert.IsTrue(dbSail.MaxWind == sail.MaxWind);
            Assert.IsTrue(dbSail.WindDirection == sail.WindDirection);
            Assert.IsTrue(dbSail.IsTest==sail.IsTest);
            if (sail.Comments != null && sail.Comments.Any())
            {
                Assert.IsNotNull(dbSail.Comments);
                Assert.IsTrue(dbSail.Comments.Count()==sail.Comments.Count());
                foreach (var comment in sail.Comments)
                {
                    Assert.IsTrue(dbSail.Comments.Any(i=>i.Message==comment.Message));
                }
            }

            if (sail.Weather != null && sail.Weather.Any())
            {
                Assert.IsTrue(compareLists(sail.Weather, dbSail.Weather));
            }

            if (sail.Maneuvers != null && sail.Maneuvers.Any())
            {
                Assert.IsNotNull(dbSail.Maneuvers);
                Assert.IsTrue(compareLists(sail.Maneuvers, dbSail.Maneuvers));
            }

            if (sail.Animals != null && sail.Animals.Any())
            {
                Assert.IsNotNull(dbSail.Animals);
                Assert.IsTrue(dbSail.Animals.Count()==sail.Animals.Count());
                foreach (var animal in sail.Animals)
                {
                    Assert.IsTrue(dbSail.Animals.Any(i=>i.ID==animal.ID));
                }

            }

            Assert.IsTrue(dbSail.HoursSailed == sail.HoursSailed);
            return true;
        }

        private void addAnimalsToSail(Sail sail)
        {
            List<Animal> animals=new List<Animal>();
            animals.Add(new Animal {ID=1});
            sail.Animals = animals;
        }

        private void changeCrew(Sail sail)
        {
            List<CrewFoundation> list = new List<CrewFoundation>();
            list.Add(new CrewFoundation { ID = 10 });
            list.Add(new CrewFoundation { ID = 11 });
            sail.Crew = null;
            sail.Crew = list;
        }

     
    }
}
