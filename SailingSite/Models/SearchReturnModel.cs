﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SearchReturnModel.cs" company="">
//   
// </copyright>
// <summary>
//   The search return model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSite.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The search return model.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public class SearchReturnModel<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchReturnModel{T}"/> class.
        /// </summary>
        public SearchReturnModel()
        {
            this.StartsWithIndex = new List<string>();
        }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        public T Result { get; set; }

        /// <summary>
        /// Gets or sets the starts with index.
        /// </summary>
        public List<string> StartsWithIndex { get; set; } 
    }
}