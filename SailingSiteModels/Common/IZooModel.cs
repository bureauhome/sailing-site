﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingSiteModels.Common
{
    using SailingSiteModels.New;

    public interface IZooModel : IEntityModel
    {
        IEnumerable<Animal> Animals { get; set; }
    }
}
