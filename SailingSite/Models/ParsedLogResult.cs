﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SailingSite.Models
{
    using SailingSiteModels.GPX;

    public class ParsedLogResult
    {
        public List<ErroredGpxTrackPoint> TrackPointErrors = new List<ErroredGpxTrackPoint>();

        public string BaseError = null;

        public int ResultsParsed = 0;

        public string GetError()
        {
            if (BaseError != null)
            {
                return BaseError;
            }
            if (TrackPointErrors.Any())
            {
                var result = string.Join(",", TrackPointErrors.Select(i => string.Format("{0}: {1}", i.Date, i.Error)));
                return result;
            }
            return null;
        }
    }
}