﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using New=SailingSiteModels.New;

namespace SailingSite.Repositories
{
    using System.Threading.Tasks;

    using SailingSite.Factories;
    using SailingSite.Services;

    public class PhotoRepository : IPhotoRepository
    {
        public async Task<New.PhotoBase> GetPhotoBase(int id)
        {
            New.PhotoBase result = null;
            using (SailingEntities context = new SailingEntities())
            {
                var dbPhoto = await context.Entities_Photos.FirstOrDefaultAsync(i => i.PhotoID == id);
                if (dbPhoto != null)
                {
                    result = PhotoFactory.CreatePhotoBase(dbPhoto);
                }
            }
            return result;
        }

        public New.Photo Get(int id)
        {
            using (SailingEntities context = new SailingEntities())
            {
                var result = new New.Photo();
                var dbEntity =
                    (from i in context.Entities_Photos where i.PhotoID == id select i)
                        .Include(i => i.Entities_Photos_Details.Entities_Crew)
                        .Include(i => i.Entities_Photos_Details.Entities_Crew1)
                        .Include(i => i.Entities_Locations)
                        .Include(i => i.Entities_Comment)
                        .Include(i => i.Entities_Crew)
                        .Include(i => i.Entities_Crew1)
                        .Include(i => i.Entities_Crew1)
                        .Include(i => i.Entities_Vessel)
                        .Include(i => i.Entities_Tags)
                        .Include(i => i.Entities_Weather)
                        .FirstOrDefault();
                if (dbEntity != null)
                {
                    result = Factories.PhotoFactory.CreatePhoto(dbEntity);
                }
                return result;
            }
        }

        public void DeleteTag(int id, int eid)
        {
            using (var context = new SailingEntities())
            {
                var trial = (from i in context.Entities_Tags where i.TagID == id select i).FirstOrDefault();
                if (trial != null)
                {
                    var photo = (from i in trial.Entities_Photos where i.PhotoID == eid select i).FirstOrDefault();
                    if (photo != null)
                    {
                        trial.Entities_Photos.Remove(photo);
                        context.SaveChanges();
                    }


                }
            }
        }

        public IEnumerable<New.Tags> GetTags(int id)
        {
            IEnumerable<New.Tags> result = null;
            using (var context = new SailingEntities())
            {
                var query = (from i in context.Entities_Photos.Include(j=>j.Entities_Tags) where i.PhotoID == id select i.Entities_Tags.Where(k => k.DateDeleted == null)).FirstOrDefault();
                result = query.ToArray().Select(Factories.CommonFactory.CreateTag);
            }
            return result;
        }

        public void AddTags(List<int> tagIds, int id)
        {
            using (var context = new SailingEntities())
            {
                List<Entities_Tags> tags = new List<Entities_Tags>();
                foreach (var tagId in tagIds)
                {
                    var tag = (from i in context.Entities_Tags where i.TagID == tagId select i).FirstOrDefault();
                    if (tag != null && tag.Name != null)
                    {
                        tags.Add(tag);
                    }
                }

                var photo = (from i in context.Entities_Photos where i.PhotoID == id select i).FirstOrDefault();
                if (photo != null && photo.PhotoID>0)
                {
                    foreach (var tag in tags)
                    {
                        var match = photo.Entities_Tags.Where(i => i.TagID == tag.TagID).FirstOrDefault();
                        if (match == null || match.TagID <= 0)
                        {
                            photo.Entities_Tags.Add(tag);
                        }
                    }
                }
                context.SaveChanges();

            }

        }

        // TODO: ADD PHOTO TAGGER/DISPLAYER

        public IEnumerable<Entities_Photos> GetPhotos(GenericQuery query)
        { 
            List<Entities_Photos> retVal=new List<Entities_Photos>();
            using (SailingEntities context = new SailingEntities())
            {
                var dbCollection = from i in context.Entities_Photos select i;
                if (query.SelectDeleted!=true)
                {
                    dbCollection = dbCollection.Where(i => i.DeleteDate == null);
                }
                if (query.PhotoID != null)
                {
                    dbCollection = dbCollection.Where(i => i.PhotoID == (int)query.PhotoID);
                    
                    retVal.Add(dbCollection.FirstOrDefault());
                }
                if (query.AnimalID != null)
                {
                    var list = (from i in context.Entities_Animals_Names where i.AnimalID == (int)query.AnimalID select i.Entities_Photos1).FirstOrDefault();
                    retVal.AddRange(list.ToList());
                }
                if (query.CommentID != null)
                {
                    var list = (from i in context.Entities_Comment where i.CommentID == (int)query.CommentID select i.Entities_Photos).FirstOrDefault();
                    retVal.AddRange(list.ToList());
                }
                if (query.Keywods != null && query.Keywods.Any())
                { 
                    foreach (var key in query.Keywods)
                    {
                        var list = (from i in context.Entities_Photos where i.Description.Contains(key) || i.Name.Contains(key) select i);
                        retVal.AddRange(list.ToList());
                              }
                }
                if (query.SailID != null)
                {
                    var list = (from i in context.Logs_Sails where i.SailID == query.SailID select i.Entities_Photos).FirstOrDefault();
                    retVal.AddRange(list.ToList());
                }
                if (query.TagIDS != null && query.TagIDS.Any())
                { 
                    foreach (var tag in query.TagIDS)
                    {
                        var list = (from i in context.Entities_Tags where i.TagID == tag select i.Entities_Photos).First();
                        retVal.AddRange(list.ToList());
                    }
                }
                if (query.WeatherID !=null)
                {
                    var list=(from i in context.Entities_Weather where i.WeatherID==query.WeatherID select i.Entities_Photos).First();
                    retVal.AddRange(list.ToList());
                }
                if (query.CrewID != null)
                {
                    var list = (from i in context.Entities_Crew where i.CrewID == query.CrewID select i.Entities_Photos1).FirstOrDefault();
                    retVal.AddRange(list.ToList());
                }
               
                if (query.SelectDeleted!=true)
                {
                    retVal.RemoveAll(i=>i.DeleteDate!=null);
                }
                return retVal;

            }
        }

        public int Create(SailingSiteModels.New.Photo photo) {
            using (SailingEntities context = new SailingEntities())
            {
                Entities_Crew crew=null;
                if (HttpContext.Current.Session["Crew"] != null)
                {
                    crew=HttpContext.Current.Session["Crew"] as Entities_Crew;
                }
                var dbEntity = Factories.PhotoFactory.CreatePhoto(photo);

                if (photo.Weather != null && photo.Weather.Any())
                {
                    var weatherInts = photo.Weather.Select(i => i.ID);
                    dbEntity.Entities_Weather = context.Entities_Weather.Where(i => weatherInts.Contains(i.WeatherID)).ToArray();

                }

                if (photo.Animals != null && photo.Animals.Any())
                {
                    var animalIdList = photo.Animals.Select(i => i.ID);
                    dbEntity.Entities_Animals_Names = context.Entities_Animals_Names.Where(i => animalIdList.Contains(i.AnimalID)).ToArray();
                }
                if (photo.Crew != null && photo.Crew.Any())
                {
                    var crewIdList = photo.Crew.Select(i => i.ID);
                    dbEntity.Entities_Crew = context.Entities_Crew.Where(i => crewIdList.Contains(i.CrewID)).ToArray();
                }
                if (photo.Maneuvers != null && photo.Maneuvers.Any())
                {
                    var maneuvers = photo.Maneuvers.Select(i => i.ID);
                    dbEntity.Entities_Maneuvers = context.Entities_Maneuvers.Where(i => maneuvers.Contains(i.ManeuverID)).ToArray();
                }
                if (photo.Locations != null && photo.Locations.Any())
                {
                    var maneuvers = photo.Locations.Select(i => i.ID);
                    dbEntity.Entities_Locations = context.Entities_Locations.Where(i => maneuvers.Contains(i.LocationID)).ToArray();
                }
                if (photo.Configurations != null && photo.Configurations.Any())
                {
                    var ids = photo.Configurations.Select(i => i.ID);
                    dbEntity.Entities_Config = context.Entities_Config.Where(i => ids.Contains(i.ConfigID)).ToArray();
                }
                context.Entities_Photos.Add(dbEntity);
                var details = new Entities_Photos_Details();
                details.PhotoID = dbEntity.PhotoID;
                details.ModifiedDate = DateTime.Now;

                if (crew != null)
                {
                    details.CrewID = crew.CrewID;
                    details.Entities_Crew = crew;
                }
                context.SaveChanges();
                return dbEntity.PhotoID;

                
            }
        }

        public async Task Purge(int id)
        {
            using (var context = new SailingEntities())
            {
                var photo = await context.Entities_Photos.FirstOrDefaultAsync(i => i.PhotoID == id);
                if (photo != null)
                {
                    photo.Entities_Animals_Categories.Clear();
                    photo.Logs_Sails.Clear();
                    photo.Entities_Crew3.Clear();
                    photo.Entities_Comment.Clear();
                    photo.Entities_Vessel.Clear();
                    photo.Entities_Config.Clear();
                    photo.Entities_Locations.Clear();
                    photo.Entities_Locations1.Clear();
                    photo.Logs_Photos_Likes.Clear();
                    photo.Entities_Comment1.Clear();
                    photo.Entities_Vessel1.Clear();
                    await context.SaveChangesAsync();
                    context.Entities_Photos.Remove(photo);
                    await context.SaveChangesAsync();
                }
            }
        }

        public void Delete(int id, int crewid)
        {
            using (SailingEntities context = new SailingEntities())
            {
                Entities_Crew crew = null;
                if (HttpContext.Current.Session["Crew"] != null)
                {
                    crew = HttpContext.Current.Session["Crew"] as Entities_Crew;
                }
                var dbEntity = (from i in context.Entities_Photos where i.PhotoID==id select i).FirstOrDefault();
                if (dbEntity != null)
                {
                    dbEntity.DeleteDate = DateTime.Now;
                    // TODO: Add deleted by
                }
      
                if (crew != null)
                {
                    var details = new Entities_Photos_Details();
                    details.CrewID = crew.CrewID;
                    details.Entities_Crew = crew;
                    details.PhotoID = dbEntity.PhotoID;
                    details.ModifiedDate = DateTime.Now;
                }

                context.SaveChanges();

            }
        }


        public IEnumerable<New.Photo> GetAll(object search = null)
        {
            throw new NotImplementedException();
        }

        public bool IsUnique(New.Photo entity, out List<string> errorMessage)
        {
            throw new NotImplementedException();
        }

        public void Update(New.Photo entity)
        {
            throw new NotImplementedException();
        }
    }
}