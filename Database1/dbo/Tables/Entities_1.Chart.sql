﻿CREATE TABLE [dbo].[Entities.Chart] (
    [ChartName]        NVARCHAR (50)  NOT NULL,
    [ChartDescription] NVARCHAR (128) NULL,
    [SQLCall]          NVARCHAR (MAX) NOT NULL,
    [YAxis]            NVARCHAR (32)  NOT NULL,
    [XAxis]            NVARCHAR (32)  NOT NULL,
    CONSTRAINT [PK_Entities.Chart] PRIMARY KEY CLUSTERED,
    CONSTRAINT [PK_Entities.Chart] PRIMARY KEY CLUSTERED ([ChartName] ASC)
);

