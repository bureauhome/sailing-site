﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMeteorological.cs" company="">
//   
// </copyright>
// <summary>
//   The MeteorologicalModel interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SailingSiteModels.Common
{
    using System.Collections.Generic;

    using SailingSiteModels.New;

    /// <summary>
    /// The MeteorologicalModel interface.
    /// </summary>
    public interface IMeteorologicalModel : IEntityModel
    {
        /// <summary>
        /// Gets or sets the weather.
        /// </summary>
        IEnumerable<WeatherBase> Weather { get; set; }
    }
}
