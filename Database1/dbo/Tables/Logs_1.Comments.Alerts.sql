﻿CREATE TABLE [dbo].[Logs.Comments.Alerts] (
    [AlertID]   INT      IDENTITY (1, 1) NOT NULL,
    [CommentID] INT      NOT NULL,
    [RelaxDate] DATETIME NOT NULL,
    [ReRun]     BIT      NULL,
    [StartDate] DATETIME NULL,
    [ReRunType] TINYINT  NULL,
    CONSTRAINT [PK_Logs.Comments.Alerts] PRIMARY KEY CLUSTERED ([AlertID] ASC, [CommentID] ASC),
    CONSTRAINT [PK_Logs.Comments.Alerts] PRIMARY KEY CLUSTERED ([AlertID] ASC, [CommentID] ASC),
    CONSTRAINT [FK_Logs.Comments.Alerts_Entities.Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Entities.Comment] ([CommentID])
);

