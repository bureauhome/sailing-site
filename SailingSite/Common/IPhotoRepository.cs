﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using New=SailingSiteModels.New;
namespace SailingSite
{
    using System.Threading.Tasks;

    using SailingSite.Repositories;
    using SailingSite.Repositories.Interfaces;

    public interface IPhotoRepository : ITaggableRepository, ICommonRepository<Entities_Photos,New.Photo,New.Photo>
    {
        Task Purge(int id);

        Task<New.PhotoBase> GetPhotoBase(int id); 
        int Create(SailingSiteModels.New.Photo photo);
    }
}